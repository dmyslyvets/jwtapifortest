﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Regency.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/Test")]
    [Authorize]
    public class TestController : Controller
    {
        [HttpGet("getSome")]
        public string Get()
        {
            return "Test result";
        }
    }
}