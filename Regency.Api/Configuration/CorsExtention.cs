﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Regency.Api.Configuration
{
    public static class CorsExtention
    {
        public static void ConfigureCors(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    b => b.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin().AllowCredentials());
                //TODO:retrieve origin
                //options.AddPolicy("CorsPolicy",
                //  b => b.AllowAnyHeader().AllowAnyMethod().WithOrigins("http://localhost:4200").AllowCredentials());
            });
        }
    }
}
