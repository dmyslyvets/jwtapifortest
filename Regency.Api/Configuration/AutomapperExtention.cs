﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using Regency.BusinessLogic.Mapping;

namespace Regency.Api.Configuration
{
    public static class AutomapperExtention
    {
        public static void ConfigureAutomapper(this IServiceCollection services)
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingsProfile());
            });
            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
        }
    }
}
