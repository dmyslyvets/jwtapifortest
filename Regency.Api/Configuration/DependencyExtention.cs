﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Regency.BusinessLogic.Auth;

namespace Regency.Api.Configuration
{
    public static class DependencyExtention
    {
        public static void ConfigureDependency(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton<IJwtFactory, JwtFactory>();
            services.AddServices();
            services.AddRepositories(configuration);
        }

        private static void AddServices(this IServiceCollection services)
        {

        }

        private static void AddRepositories(this IServiceCollection services, IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString("DefaultConnection");
            //services.AddScoped(r => new JobPaymentRepository(connectionString));
        }
    }    
}
