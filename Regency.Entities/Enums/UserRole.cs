﻿using System.ComponentModel;

namespace Regency.Entities.Enums
{
    public enum UserRole
    {
        [Description("Undefined")]
        None = 0,
        [Description("Admin")]
        Admin = 1,
        [Description("Manager")]
        Manager = 2,
        [Description("Member")]
        Member = 3,
    }
}
