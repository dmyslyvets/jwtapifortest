﻿using System;
using Microsoft.AspNetCore.Identity;
using Regency.Entities.Enums;

namespace Regency.Entities.Entities
{
    public class ApplicationUser : IdentityUser
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime DateOfBirth { get; set; }
 
        public UserRole UserRole { get; set; }
    }
}
