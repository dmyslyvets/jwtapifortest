﻿using System.Security.Claims;
using System.Security.Principal;

namespace Regency.BusinessLogic.Auth
{
    public static class IdentityExtensions
    {
        public static string GetUserId(this IIdentity identity)
        {
            ClaimsIdentity claimsIdentity = identity as ClaimsIdentity;
            Claim claim = claimsIdentity?.FindFirst(Constants.JwtClaimIdentifiers.Id);
            if (claim == null)
            {
                return string.Empty;
            }               
            return claim.Value;
        }
    }
}
