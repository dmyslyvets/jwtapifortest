﻿namespace Regency.BusinessLogic.Auth
{
    public static class Constants
    {
        public static class JwtClaimIdentifiers
        {
            public const string Role = "role", Id = "id";
        }
    }
}
