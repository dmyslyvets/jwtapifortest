﻿
using System.Security.Claims;
using System.Threading.Tasks;

namespace Regency.BusinessLogic.Auth
{
    public interface IJwtFactory
    {
        Task<string> GenerateEncodedToken(string userName, ClaimsIdentity identity);
        ClaimsIdentity GenerateClaimsIdentity(string userName, string id, string userRole);
    }
}
