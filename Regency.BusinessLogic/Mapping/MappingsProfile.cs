﻿using AutoMapper;
using Regency.Entities.Entities;
using Regency.ViewModels.Account;

namespace Regency.BusinessLogic.Mapping
{
    public class MappingsProfile : Profile
    {
        public MappingsProfile()
        {
            #region "Account"
            CreateMap<RegistrationViewModel, ApplicationUser>()
                .ForMember(au => au.UserName, map => map.MapFrom(vm => vm.Email))
                .ForMember(au => au.DateOfBirth, map => map.MapFrom(vm => vm.DateOfBirth.Value))
                .ForMember(au => au.UserRole, map => map.MapFrom(vm => vm.Role));
            #endregion
        }
    }
}
