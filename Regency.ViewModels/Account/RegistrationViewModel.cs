﻿using Regency.ViewModels.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Regency.ViewModels.Account
{
    public class RegistrationViewModel
    {
        [Required]
        [MinLength(6)]
        public string Password { get; set; }

         
        public UserRole Role { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public DateTime? DateOfBirth { get; set; }

      
    }
}
