﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Regency.ViewModels.Enums
{
    public enum UserRole
    {
        [Description("Undefined")]
        None = 0,
        [Description("Admin")]
        Admin = 1,
        [Description("Manager")]
        Manager = 2,
        [Description("Member")]
        Member = 3,
    }
}
