(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["project-and-orders-projects-projects-module-module"],{

/***/ "./src/app/projects-and-orders/project-and-orders/projects/create-project/create-project.component.html":
/*!**************************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/projects/create-project/create-project.component.html ***!
  \**************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<br>\r\n<img src=\"../../../../../assets/img/left-arrow.svg\" alt=\"\" class=\"project-list-button\" (click)=\"showingProjectsList()\">\r\n<div class=\"row card-row\">\r\n  <div class=\"col-12 \">\r\n    <br>\r\n    <h6>Project Details</h6>\r\n    <mat-card class=\"details-card\">\r\n      <br>\r\n      <mat-form-field class=\"exasmple-full-width col-md-4\">\r\n        <input matInput\r\n               [(ngModel)]=\"createProject.name\"\r\n               placeholder=\"Project Name\"\r\n               value=\"\">\r\n      </mat-form-field>\r\n      <mat-form-field class=\"example-full-width col-md-5\">\r\n        <input matInput\r\n               placeholder=\"Description\"\r\n               [(ngModel)]=\"createProject.description\">\r\n      </mat-form-field>\r\n\r\n\r\n      <mat-form-field class=\"example-full-width col-md-3\">\r\n        <mat-select placeholder=\"Project Manager\"\r\n                    [(ngModel)]=\"createProject.userId\">\r\n          <mat-option *ngFor=\"let item of projectManagers.users\" [value]=\"item.id\">\r\n            {{item.firstName}} {{item.lastName}}\r\n          </mat-option>\r\n        </mat-select>\r\n      </mat-form-field>\r\n\r\n      <mat-form-field class=\"example-full-width col-md-4\">\r\n        <mat-select placeholder=\"Customer\"\r\n                    [(ngModel)]=\"createProject.customerId\">\r\n          <mat-option *ngFor=\"let cust of customers.customers\" [value]=\"cust.id\">\r\n            {{cust.name}}\r\n          </mat-option>\r\n        </mat-select>\r\n      </mat-form-field>\r\n\r\n      <mat-form-field class=\"example-full-width col-md-2\">\r\n        <input matInput\r\n               [matDatepicker]=\"picker\"\r\n               [(ngModel)]=\"createProject.date\"\r\n               placeholder=\"Choose a date\">\r\n\r\n        <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\r\n        <mat-datepicker #picker></mat-datepicker>\r\n      </mat-form-field>\r\n\r\n      <mat-form-field>\r\n        <input matInput\r\n               [ngModel]=\"createProject.projectType\"\r\n               (keyup)=\"typedProjectType($event)\"\r\n               [matAutocomplete]=\"auto\"\r\n               placeholder=\"Project Type\">\r\n      </mat-form-field>\r\n\r\n      <mat-autocomplete #auto=\"matAutocomplete\"\r\n                        (optionSelected)=\"selectedProjectType($event)\"\r\n                        [displayWith]=\"displayFn\">\r\n        <mat-option *ngFor=\"let item of projectTypesView.projectTypes\"\r\n                    [value]=\"item\">\r\n          {{item.name}}\r\n        </mat-option>\r\n      </mat-autocomplete>\r\n\r\n      <mat-form-field class=\"example-full-width  col-md-12\">\r\n        <textarea matInput [(ngModel)]=\"createProject.memo\" matTextareaAutosize placeholder=\"Memmo\"></textarea>\r\n      </mat-form-field>\r\n      <div class=\"edit-button-wrapper\">\r\n        <button class=\"cancel-btn btn btn-block btn-outline-primary\"\r\n                (click)=\"showingProjectsList()\">\r\n          Cancel\r\n        </button>\r\n        <button class=\"save-btn btn btn-block btn-outline-primary\"\r\n                (click)=\"saveProjectChange()\">\r\n          Save\r\n        </button>\r\n      </div>\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n\r\n\r\n<br>\r\n<div class=\"row \">\r\n  <div class=\"col-6 attach-card-wrapper\">\r\n    <h6>Attachments</h6>\r\n    <span *ngIf=\"showSpinner\">\r\n      <mat-progress-bar mode=\"indeterminate\"></mat-progress-bar>\r\n    </span>\r\n\r\n    <mat-card class=\"attach-card\">\r\n\r\n        <table class=\"table  datatable  attachment-table\" [mfData]=\"createProjectFileList\" #mf=\"mfDataTable\"\r\n        [mfRowsOnPage]=\"10\">\r\n          <tbody>\r\n              <tr *ngFor=\"let item of mf.data; let i = index\">\r\n                  <td>{{item.name}}</td>\r\n                  <td>{{item.type}}</td>\r\n                  <td>\r\n                    <div class=\"attachment-button text-right\" (click)=\"openDeleteAttachment(i)\"><i class=\"fas fa-times\"></i></div>\r\n                  </td>\r\n                </tr>\r\n          </tbody>\r\n        </table>\r\n      \r\n        <div ng2FileDrop [ngClass]=\"{'nv-file-over': hasBaseDropZoneOver}\" (fileOver)=\"fileOverBase($event)\" (onFileDrop)=\"dropped($event)\"\r\n          [uploader]=\"uploader\" class=\"table-responsive col-md-12 p-0\">\r\n          <span *ngIf=\"showSpinner\">\r\n              <mat-progress-bar mode=\"indeterminate\"></mat-progress-bar>\r\n            </span>\r\n          <div class=\"drag\">           \r\n            <span *ngIf=\"!showSpinner\">Drag and drop files here</span>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-md-12 d-flex justify-content-end p-0 align-items-center\">       \r\n          <label class=\"custom-file-upload\">\r\n            <input (change)=\"inputAdd($event)\" type=\"file\" multiple />\r\n            Add New Files\r\n          </label>\r\n        </div>\r\n      </mat-card>\r\n\r\n    <!-- <mat-card>\r\n      <div class=\"general-info-input-wrapper user row\">\r\n        <div ng2FileDrop\r\n             [ngClass]=\"{'nv-file-over': hasBaseDropZoneOver}\"\r\n             (fileOver)=\"fileOverBase($event)\"\r\n             (onFileDrop)=\"dropped($event)\"\r\n             [uploader]=\"uploader\"\r\n             class=\"table-responsive col-md-12\">\r\n          <table class=\"table  datatable  attachment-table\" [mfData]=\"createProjectFileList | projectDataFilter : projectsFilter\" #mf=\"mfDataTable\"\r\n                 [mfRowsOnPage]=\"10\">\r\n            <thead>\r\n              <tr role=\"row\" style=\"cursor:pointer\">\r\n                <th style=\"width: 40%\" class=\"sorting\">\r\n                </th>\r\n                <th style=\"width: 20%\" class=\"sorting\">\r\n                </th>\r\n              </tr>\r\n            </thead>\r\n            <tbody>\r\n              <tr *ngFor=\"let item of mf.data; let i = index\">\r\n                <td>{{item.name}}</td>\r\n                <td>{{item.type}}</td>\r\n                <td>\r\n                  <div class=\"attachment-button\" (click)=\"openDeleteAttachment(i)\"><i class=\"fas fa-times\"></i></div>\r\n                </td>\r\n              </tr>\r\n            </tbody>\r\n            <tfoot>\r\n              <tr>\r\n                <td class=\"p-0\" colspan=\"6\">\r\n                  <mfBootstrapPaginator [rowsOnPageSet]=\"[5,10,15]\"></mfBootstrapPaginator>\r\n                </td>\r\n              </tr>\r\n            </tfoot>\r\n          </table>\r\n        </div>\r\n        <div class=\"col-md-12 d-flex justify-content-end\">\r\n          <label class=\"custom-file-upload\">\r\n            <input (change)=\"inputAdd($event)\" type=\"file\" multiple />\r\n            Add New Files\r\n          </label>\r\n        </div>\r\n      </div>\r\n    </mat-card> -->\r\n  </div>\r\n</div>\r\n\r\n<!--Delete popup-->\r\n<div class=\"add-project\">\r\n  <div class=\"overlay\" *ngIf='openDeleteAttachmentModal'></div>\r\n  <div class=\"modal-add-project\" [ngClass]=\"{'active':openDeleteAttachmentModal}\">\r\n    <div class=\"card-header\">\r\n      <span>\r\n        <strong>\r\n          <h5>Are you sure you want to delete the project attachment?</h5>\r\n        </strong>\r\n      </span>\r\n      <div class=\"close-project-button\" (click)=\"cancelDeleteAttachment()\">\r\n        <i class=\"fas fa-times\"></i>\r\n      </div>\r\n    </div>\r\n    <div class=\"modal-body\">\r\n      <div class=\"row button-wrapper\">\r\n        <button class=\"btn btn-block btn-outline-primary\" (click)=\"cancelDeleteAttachment()\">Cancel</button>\r\n        <button class=\"btn btn-block btn-outline-primary\" (click)=\"yesDeleteAttachment()\">Yes</button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<!--Show image attachment popup-->\r\n<div class=\"add-project\">\r\n  <div class=\"overlay\" *ngIf='showAttachmentImg'></div>\r\n  <div class=\"modal-add-project\" [ngClass]=\"{'active':showAttachmentImg}\">\r\n    <div class=\"card-header\">\r\n      <span>\r\n        <strong>\r\n\r\n          <!-- <h5>Attachment: {{currentProjectAttachment.fileName}}</h5> -->\r\n        </strong>\r\n      </span>\r\n      <div class=\"close-project-button\" (click)=\"closeShowAttachment()\">\r\n        <i class=\"fas fa-times\"></i>\r\n      </div>\r\n    </div>\r\n    <div class=\"modal-body\">\r\n      <div>\r\n\r\n        <img src=\"{{currentAttachmentBase64}}\" width=\"200px;\" height=\"200px;\" />\r\n      </div>\r\n      <div class=\"row button-wrapper\">\r\n        <button class=\"btn btn-block btn-outline-primary\" (click)=\"closeShowAttachment()\">Close</button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<!--Show txt attachment popup-->\r\n<div class=\"add-project\">\r\n  <div class=\"overlay\" *ngIf='showAttachedTxt'></div>\r\n  <div class=\"modal-show-pdf\" [ngClass]=\"{'active':showAttachedTxt}\">\r\n    <div class=\"card-header\">\r\n      <span>\r\n        <strong>\r\n\r\n          <!-- <h5>Attachment: {{currentProjectAttachment.fileName}}</h5> -->\r\n        </strong>\r\n      </span>\r\n      <div class=\"close-project-button\" (click)=\"closeShowAttachment()\">\r\n        <i class=\"fas fa-times\"></i>\r\n      </div>\r\n    </div>\r\n    <div class=\"modal-body\">\r\n      <div class=\"text-container\">\r\n        <p>{{attachmentTxtText}}</p>\r\n      </div>\r\n      <div class=\"row button-wrapper\">\r\n        <button class=\"btn btn-block btn-outline-primary\" (click)=\"closeShowAttachment()\">Close</button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!--Show pdf file popup-->\r\n<div class=\"add-project\">\r\n  <div class=\"overlay\" *ngIf='showAttachedPdf'></div>\r\n  <div class=\"modal-show-pdf\" [ngClass]=\"{'active':showAttachedPdf}\">\r\n    <div class=\"card-header\">\r\n      <span>\r\n        <strong>\r\n\r\n          <!-- <h5>Attachment: {{currentProjectAttachment.fileName}}</h5> -->\r\n        </strong>\r\n      </span>\r\n      <div class=\"close-project-button\" (click)=\"closeShowAttachment()\">\r\n        <i class=\"fas fa-times\"></i>\r\n      </div>\r\n    </div>\r\n    <div class=\"modal-body\">\r\n      <div class=\"pdf-container\">\r\n        <pdf-viewer [src]=\"currentPdfLink\"\r\n                    [original-size]=\"false\"\r\n                    [show-all]=\"true\"\r\n                    [render-text-mode]=\"true\"\r\n                    [autoresize]=\"true\">\r\n        </pdf-viewer>\r\n      </div>\r\n      <div class=\"row button-wrapper\">\r\n        <button class=\"btn btn-block btn-outline-primary\" (click)=\"closeShowAttachment()\">Close</button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n\r\n\r\n<!-- Orders\r\n<div class=\"card-body generalinfo\">\r\n  <div class=\"card-header\">\r\n    <strong>\r\n      <h5>Orders</h5>\r\n    </strong>\r\n  </div>\r\n  <div class=\"card-body project-details project\">\r\n    <div class=\"general-info-input-wrapper user row\">\r\n      <div class=\"col-md-12\">\r\n        <button class=\"btn btn-primary\">New Order</button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  </div> -->\r\n"

/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/projects/create-project/create-project.component.scss":
/*!**************************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/projects/create-project/create-project.component.scss ***!
  \**************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".custom-file-upload {\n  color: #0077c5;\n  cursor: pointer; }\n\nlabel {\n  margin-bottom: 0px; }\n\ninput[type=\"file\"] {\n  display: none; }\n\np {\n  margin-top: 1rem !important;\n  margin-bottom: 1rem; }\n\n.custom-table {\n  border-spacing: 0px 15px;\n  border-collapse: separate !important; }\n\n.custom-table thead tr th {\n    border-bottom: none !important;\n    border-top: none !important; }\n\n.custom-table tbody tr {\n    border-bottom: 20px solid white !important;\n    background: #F2F4F8 !important; }\n\n.custom-table tbody tr:after {\n      content: '';\n      border-bottom: 1px solid black;\n      position: absolute;\n      bottom: 0; }\n\n.custom-table tbody tr .info-task .success {\n      background: #4CAF50;\n      height: 48px;\n      width: 48px;\n      position: relative;\n      border-radius: 50%; }\n\n.custom-table tbody tr .info-task .success .counter {\n        position: absolute;\n        height: 15px;\n        width: 15px;\n        border: 1px solid #4CAF50;\n        border-radius: 50%;\n        top: 0;\n        right: 0;\n        background: white;\n        display: flex;\n        justify-content: center;\n        align-items: center;\n        font-size: 10px;\n        font-weight: bold; }\n\n.custom-table tbody tr .info-task .success:before {\n        content: \"\";\n        position: absolute;\n        left: 20px;\n        top: 12px;\n        width: 10px;\n        height: 18px;\n        border: solid white;\n        border-width: 0 3px 3px 0;\n        -webkit-transform: rotate(45deg);\n        transform: rotate(45deg); }\n\n.custom-table tbody tr .info-task .error {\n      background: #CD3232;\n      height: 48px;\n      width: 48px;\n      position: relative;\n      border-radius: 50%;\n      text-align: center;\n      vertical-align: middle;\n      display: table-cell;\n      color: white; }\n\n.custom-table tbody tr .info-task .error .counter-error {\n        position: absolute;\n        height: 15px;\n        width: 15px;\n        border: 1px solid #CD3232;\n        border-radius: 50%;\n        top: 0;\n        right: 0;\n        background: white;\n        display: flex;\n        justify-content: center;\n        align-items: center;\n        font-size: 10px;\n        font-weight: bold;\n        color: black; }\n\n.custom-table tbody tr td {\n      border-top: 1px solid #D0D0D0;\n      border-bottom: 1px solid #D0D0D0;\n      vertical-align: middle; }\n\n.custom-table tbody tr td:first-child {\n        border-left: 1px solid #D0D0D0; }\n\n.custom-table tbody tr td:last-child {\n        border-right: 1px solid #D0D0D0; }\n\n.custom-table tbody tr:hover {\n      background: #d5d5d5 !important; }\n\n.custom-table tfoot tr td {\n    border-top: none !important; }\n\n.selected-game {\n  display: inline-block;\n  margin-right: 15px;\n  font-size: 15px;\n  background: #B9CFEA;\n  margin-bottom: 10px;\n  padding: 2px 11px 4px 11px;\n  border-radius: 8px; }\n\n.delete-game {\n  font-size: 10px;\n  padding: 3px 6px;\n  vertical-align: middle;\n  cursor: pointer; }\n\n#newProject {\n  width: 180px;\n  height: 45px;\n  font-size: large; }\n\n.add-project .overlay {\n  position: fixed;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  background: rgba(0, 0, 0, 0.4);\n  z-index: 10000; }\n\n.add-project .modal-add-project {\n  position: fixed;\n  background: #fff;\n  z-index: 10000;\n  top: -100%;\n  left: calc(50% - 200px);\n  width: 400px; }\n\n.add-project .modal-add-project.active {\n    top: 200px;\n    left: calc(50% - 200px);\n    transition: top .5s; }\n\n.add-project .modal-add-project .row {\n    width: 100%; }\n\n.add-project .modal-add-project .row .mt-10 {\n      margin-top: 15px; }\n\n.add-project .modal-add-project .button-wrapper {\n    display: flex;\n    justify-content: flex-end;\n    margin-top: 10px; }\n\n.add-project .modal-add-project .button-wrapper .btn {\n      width: 100px;\n      margin: 0;\n      margin-left: 10px; }\n\n.add-project .modal-show-pdf {\n  position: fixed;\n  background: #fff;\n  z-index: 10000;\n  top: -100%;\n  left: calc(50% - 400px);\n  width: 800px; }\n\n.add-project .modal-show-pdf .modal-body {\n    padding-right: 0; }\n\n.add-project .modal-show-pdf .modal-body .pdf-container {\n      max-height: 400px;\n      overflow-y: auto; }\n\n.add-project .modal-show-pdf .modal-body .text-container {\n      max-height: 400px;\n      overflow-y: auto; }\n\n.add-project .modal-show-pdf.active {\n    top: 200px;\n    left: calc(50% - 400px);\n    transition: top .5s; }\n\n.add-project .modal-show-pdf .row {\n    width: 100%; }\n\n.add-project .modal-show-pdf .row .mt-10 {\n      margin-top: 15px; }\n\n.add-project .modal-show-pdf .button-wrapper {\n    display: flex;\n    justify-content: flex-end;\n    margin-top: 10px; }\n\n.add-project .modal-show-pdf .button-wrapper .btn {\n      width: 100px;\n      margin: 0;\n      margin-left: 10px; }\n\n.save-attachment .overlay {\n  position: fixed;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  background: rgba(0, 0, 0, 0.4);\n  z-index: 10000; }\n\n.save-attachment .modal-save-attachment {\n  position: fixed;\n  background: #fff;\n  z-index: 10000;\n  top: -100%;\n  left: calc(50% - 400px);\n  width: 900px; }\n\n.save-attachment .modal-save-attachment.active {\n    top: 200px;\n    left: calc(50% - 400px);\n    transition: top .5s; }\n\n.save-attachment .modal-save-attachment .row {\n    width: 100%; }\n\n.save-attachment .modal-save-attachment .row .mt-10 {\n      margin-top: 15px; }\n\n.save-attachment .modal-save-attachment .button-wrapper {\n    display: flex;\n    justify-content: flex-end;\n    margin-top: 10px; }\n\n.save-attachment .modal-save-attachment .button-wrapper .btn {\n      width: 100px;\n      margin: 0;\n      margin-left: 10px; }\n\n.project-list-button {\n  width: 11.1px;\n  height: 18.1px;\n  margin-left: 15px;\n  margin-bottom: 28.6px; }\n\n.close-project-button {\n  margin-left: auto; }\n\n.attachment-button {\n  display: inline; }\n\n.button-wrapper {\n  vertical-align: bottom;\n  margin-top: auto; }\n\n.card-body.project-details.project {\n  border: 1px solid #e1e6ef; }\n\n.card-body {\n  padding: 16px !important; }\n\n.card-header {\n  padding: 0.75rem 1.25rem;\n  margin-bottom: 0;\n  background-color: #f9f9fa;\n  display: flex;\n  border-bottom: 0px;\n  border: 1px solid #e1e6ef; }\n\n.project-details-input-wrapper.project {\n  display: flex; }\n\n.row {\n  display: flex;\n  flex-wrap: wrap;\n  margin-left: 0 !important; }\n\n.project-name-description-wrapper {\n  display: flex; }\n\n.card {\n  border: 0px !important; }\n\n.edit-project-button {\n  margin-left: auto; }\n\n.attachment-table {\n  text-align: center; }\n\n.attachment-table thead th {\n    border-top: none !important;\n    border-bottom: none !important; }\n\n.attachment-table tbody td {\n    border-top: none !important;\n    border-bottom: none !important; }\n\n.attachment-table tbody .link {\n    color: deepskyblue !important; }\n\n.edit-button-wrapper {\n  display: flex;\n  justify-content: flex-end;\n  margin-top: 20px; }\n\n.edit-button-wrapper .btn {\n    width: 100px;\n    margin: 0;\n    margin-left: 10px; }\n\n.mat-form-field.mat-focused.mat-form-field-ripple {\n  background-color: #20a8d8 !important; }\n\n.mat-form-field-ripple {\n  background-color: #20a8d8 !important; }\n\ntextarea.mat-autosize {\n  resize: auto !important;\n  overflow: hidden !important; }\n\n.drag {\n  height: 100px;\n  width: 100%;\n  border: 1px dashed #636363;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  margin-bottom: 30px;\n  color: #636363;\n  font-weight: 300; }\n"

/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/projects/create-project/create-project.component.ts":
/*!************************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/projects/create-project/create-project.component.ts ***!
  \************************************************************************************************************/
/*! exports provided: CreateProjectComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateProjectComponent", function() { return CreateProjectComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ng2-file-upload */ "./node_modules/ng2-file-upload/index.js");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(ng2_file_upload__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var app_shared_constans_projects_and_orders_constans__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/shared/constans/projects-and-orders.constans */ "./src/app/shared/constans/projects-and-orders.constans.ts");
/* harmony import */ var app_shared_providers_services_project_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! app/shared/providers/services/project.service */ "./src/app/shared/providers/services/project.service.ts");
/* harmony import */ var app_shared_providers_services_customer_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! app/shared/providers/services/customer.service */ "./src/app/shared/providers/services/customer.service.ts");
/* harmony import */ var app_shared_providers_services_noitification_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! app/shared/providers/services/noitification.service */ "./src/app/shared/providers/services/noitification.service.ts");
/* harmony import */ var app_shared_providers_services_project_attachment_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! app/shared/providers/services/project-attachment.service */ "./src/app/shared/providers/services/project-attachment.service.ts");
/* harmony import */ var app_shared_providers_services_load_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! app/shared/providers/services/load.service */ "./src/app/shared/providers/services/load.service.ts");
/* harmony import */ var app_shared_models_users_get_all_users_view__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! app/shared/models/users/get-all-users.view */ "./src/app/shared/models/users/get-all-users.view.ts");
/* harmony import */ var app_shared_models_customer_get_all_customer_view__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! app/shared/models/customer/get-all-customer.view */ "./src/app/shared/models/customer/get-all-customer.view.ts");
/* harmony import */ var app_shared_models_project_create_project_view__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! app/shared/models/project/create-project.view */ "./src/app/shared/models/project/create-project.view.ts");
/* harmony import */ var app_shared_models_project_get_all_project_types_view__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! app/shared/models/project/get-all-project-types.view */ "./src/app/shared/models/project/get-all-project-types.view.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};














var CreateProjectComponent = /** @class */ (function () {
    function CreateProjectComponent(notificationService, projectService, customerService, projectAttachmentService, activateRoute, route, loadService, constans) {
        this.notificationService = notificationService;
        this.projectService = projectService;
        this.customerService = customerService;
        this.projectAttachmentService = projectAttachmentService;
        this.activateRoute = activateRoute;
        this.route = route;
        this.loadService = loadService;
        this.constans = constans;
        //post request
        this.createProject = new app_shared_models_project_create_project_view__WEBPACK_IMPORTED_MODULE_12__["CreateProjectView"]();
        this.currentPdfLink = " ";
        this.projectManagers = new app_shared_models_users_get_all_users_view__WEBPACK_IMPORTED_MODULE_10__["GetAllUserView"]();
        this.customers = new app_shared_models_customer_get_all_customer_view__WEBPACK_IMPORTED_MODULE_11__["GetAllCustomerView"]();
        this.projectTypesView = new app_shared_models_project_get_all_project_types_view__WEBPACK_IMPORTED_MODULE_13__["GetAllProjectTypesView"]();
        //form 
        this.myControl = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]();
        //spinner
        this.showSpinner = false;
        //popups
        this.activeEditProject = false;
        this.formValidation = false;
        this.openSaveAttachment = false;
        this.openDeleteAttachmentModal = false;
        this.droppedFiles = [];
        this.hasBaseDropZoneOver = false;
        //attachment
        this.uploader = new ng2_file_upload__WEBPACK_IMPORTED_MODULE_2__["FileUploader"]({ url: this.constans.URL });
        this.showAttachmentImg = false;
        this.showAttachedPdf = false;
        this.showAttachedTxt = false;
        this.showAttachment = false;
        this.createProjectFileList = [];
    }
    CreateProjectComponent.prototype.ngOnInit = function () {
        this.loadService.set(true);
        this.getProjectManager();
        this.getCustomers();
        this.getProjectTypes();
    };
    //temp
    CreateProjectComponent.prototype.change = function () {
        console.log(this.createProject.projectType);
    };
    //get
    CreateProjectComponent.prototype.getCustomers = function () {
        var _this = this;
        this.customerService.getCustomers().subscribe(function (response) {
            _this.customers = response;
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    CreateProjectComponent.prototype.getProjectTypes = function () {
        var _this = this;
        this.projectService.getAllProjectTypes().subscribe(function (response) {
            _this.projectTypesView = response;
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    CreateProjectComponent.prototype.getProjectManager = function () {
        var _this = this;
        this.projectService.getProjectManager().subscribe(function (response) {
            _this.projectManagers = response;
            _this.loadService.set(false);
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    //functions for project types
    CreateProjectComponent.prototype.displayFn = function (val) {
        return val ? val.name : val;
    };
    CreateProjectComponent.prototype.selectedProjectType = function (item) {
        this.createProject.projectType.id = item.option.value.id;
        this.createProject.projectType.name = item.option.value.name;
    };
    CreateProjectComponent.prototype.typedProjectType = function (event) {
        this.createProject.projectType.name = '';
        this.createProject.projectType.id = null;
        this.createProject.projectType.name = event.target.value;
    };
    //back on page
    CreateProjectComponent.prototype.showingProjectsList = function () {
        this.route.navigate(['projects-and-orders/projects/projectslist']);
    };
    //update project
    CreateProjectComponent.prototype.activateEditProject = function () {
        this.activeEditProject = true;
    };
    CreateProjectComponent.prototype.saveProjectChange = function () {
        var _this = this;
        if (!this.createProject.userId) {
            this.notificationService.showError(this.constans.cantCreateProjectWithoutPMMessage);
        }
        if (!this.createProject.customerId) {
            this.notificationService.showError(this.constans.cantCreateProjectWithoutCustomerMessage);
        }
        if (!this.createProject.date) {
            this.notificationService.showError(this.constans.cantCreateProjectWitoutDate);
        }
        if (this.createProject.customerId && this.createProject.userId && this.createProject.date) {
            var invalidFiles = [];
            var formData = new FormData();
            if (!this.createProject.name) {
                this.createProject.name = "";
            }
            for (var i = 0; i < this.createProjectFileList.length; i++) {
                var success = false;
                this.constans.allowableExtensions.forEach(function (ex) {
                    var index = _this.createProjectFileList[i].name.lastIndexOf(".");
                    var extensions = _this.createProjectFileList[i].name.substr(index + 1);
                    if (ex == extensions) {
                        success = true;
                        return;
                    }
                });
                if (success) {
                    formData.append("file", this.createProjectFileList[i], this.createProjectFileList[i].name);
                }
            }
            ;
            formData.append("name", this.createProject.name);
            formData.append("description", this.createProject.description);
            formData.append("memo", this.createProject.memo);
            formData.append("projectType[id]", this.createProject.projectType.id);
            formData.append("projectType[name]", this.createProject.projectType.name);
            formData.append("customerId", this.createProject.customerId);
            formData.append("userId", this.createProject.userId);
            formData.append("date", this.createProject.date.toUTCString());
            this.projectService.createProject(formData).subscribe(function (response) {
                _this.notificationService.showSuccess(_this.constans.projectCreateMessage);
                _this.showingProjectsList();
            }),
                function (error) {
                    _this.notificationService.showError(error.error);
                };
        }
    };
    //attacments
    CreateProjectComponent.prototype.openDeleteAttachment = function (index) {
        this.deletedAttachmentId = index;
        this.openDeleteAttachmentModal = true;
    };
    CreateProjectComponent.prototype.cancelDeleteAttachment = function () {
        this.deletedAttachmentId = null;
        this.openDeleteAttachmentModal = false;
    };
    CreateProjectComponent.prototype.yesDeleteAttachment = function () {
        this.deleteAttachment();
        this.openDeleteAttachmentModal = false;
    };
    CreateProjectComponent.prototype.deleteAttachment = function () {
        this.createProjectFileList.splice(this.deletedAttachmentId, 1);
        this.deletedAttachmentId = null;
    };
    CreateProjectComponent.prototype.inputAdd = function (event) {
        var fileList = event.target.files;
        var invalidFiles = this.checkAttachmentExtensions(fileList);
        if (invalidFiles.length > 0) {
            this.notificationService.showWarning(this.constans.fileExtensionNotAllowedMessage);
        }
    };
    CreateProjectComponent.prototype.fileLeave = function (file) {
        var index = this.droppedFiles.indexOf(file);
        if (index > -1) {
            this.droppedFiles.splice(index, 1);
        }
    };
    CreateProjectComponent.prototype.fileOver = function (event) { };
    CreateProjectComponent.prototype.fileOverBase = function (event) {
        this.hasBaseDropZoneOver = event;
    };
    CreateProjectComponent.prototype.dropped = function (fileList) {
        var invalidFiles = this.checkAttachmentExtensions(fileList);
        if (invalidFiles.length > 0) {
            this.notificationService.showWarning(this.constans.fileExtensionNotAllowedMessage);
        }
    };
    CreateProjectComponent.prototype.checkAttachmentExtensions = function (files) {
        var invalidFiles = [];
        for (var i = 0; i < files.length; i++) {
            this.showSpinner = true;
            var success = false;
            this.constans.allowableExtensions.forEach(function (ex) {
                var index = files[i].name.lastIndexOf(".");
                var extensions = files[i].name.substr(index + 1);
                if (ex == extensions) {
                    success = true;
                    return;
                }
            });
            if (!success) {
                invalidFiles.push(files[i].name);
            }
            if (success) {
                this.createProjectFileList.push(files[i]);
            }
        }
        ;
        this.showSpinner = false;
        return invalidFiles;
    };
    CreateProjectComponent.prototype.closeShowAttachment = function () {
        this.showAttachmentImg = false;
    };
    CreateProjectComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-create-project',
            template: __webpack_require__(/*! ./create-project.component.html */ "./src/app/projects-and-orders/project-and-orders/projects/create-project/create-project.component.html"),
            styles: [__webpack_require__(/*! ./create-project.component.scss */ "./src/app/projects-and-orders/project-and-orders/projects/create-project/create-project.component.scss")]
        }),
        __metadata("design:paramtypes", [app_shared_providers_services_noitification_service__WEBPACK_IMPORTED_MODULE_7__["NotificationService"],
            app_shared_providers_services_project_service__WEBPACK_IMPORTED_MODULE_5__["ProjectService"],
            app_shared_providers_services_customer_service__WEBPACK_IMPORTED_MODULE_6__["CustomerService"],
            app_shared_providers_services_project_attachment_service__WEBPACK_IMPORTED_MODULE_8__["ProjectAttachmentservice"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            app_shared_providers_services_load_service__WEBPACK_IMPORTED_MODULE_9__["LoadService"],
            app_shared_constans_projects_and_orders_constans__WEBPACK_IMPORTED_MODULE_4__["ProjectsAndOrdersConstans"]])
    ], CreateProjectComponent);
    return CreateProjectComponent;
}());



/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/projects/project-details/project-details.component.html":
/*!****************************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/projects/project-details/project-details.component.html ***!
  \****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<br>\r\n<img src=\"../../../../../assets/img/left-arrow.svg\" alt=\"\" class=\"project-list-button\" (click)=\"showingProjectsList()\">\r\n<div class=\"row card-row\" >\r\n  <div class=\"col-12\">\r\n    <strong>\r\n      <h4 class=\"name-project\">Project {{currentProject.name}}</h4>\r\n    </strong>\r\n    <h6 style=\"margin-bottom: 17px;\">Project Details</h6>\r\n    <mat-card class=\"details-card\">\r\n      <div class=\"edit-project-button\">\r\n        <img *ngIf=\"!activeEditProject\" src=\"../../../../../assets/img/pencil-edit-button.svg\" (click)=\"activateEditProject()\"\r\n             class=\"edit-button\" alt=\"\">\r\n        <div class=\"edit-button-wrapper\">\r\n          <a class=\"proj-button\" *ngIf=\"activeEditProject\" (click)=\"canceltProjectChange()\">\r\n            Cancel\r\n          </a>\r\n          <a class=\"proj-button\" *ngIf=\"activeEditProject\" (click)=\"saveProjectChange()\">\r\n            Save\r\n          </a>\r\n        </div>\r\n        <br>\r\n        <mat-form-field class=\"example-full-width col-md-4\">\r\n          <input matInput [disabled]=\"!activeEditProject\" [(ngModel)]=\"currentProject.name\" placeholder=\"Project Name\"\r\n                 value=\"\">\r\n        </mat-form-field>\r\n        <mat-form-field class=\"example-full-width col-md-5\">\r\n          <input matInput [disabled]=\"!activeEditProject\" placeholder=\"Description\" [(ngModel)]=\"currentProject.description\">\r\n        </mat-form-field>\r\n\r\n\r\n        <mat-form-field class=\"example-full-width col-md-2\">\r\n          <mat-select placeholder=\"Project Manager\" [disabled]=\"!activeEditProject\" [(ngModel)]=\"currentProject.userId\">\r\n            <mat-option *ngFor=\"let item of projectManagers.users\" [value]=\"item.id\">\r\n              {{item.firstName}} {{item.lastName}}\r\n            </mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n\r\n        <mat-form-field class=\"example-full-width col-md-4\">\r\n          <mat-select placeholder=\"Customer\" [(ngModel)]=\"currentProject.customerId\" [disabled]=\"!activeEditProject\">\r\n            <mat-option *ngFor=\"let cust of customers.customers\" [value]=\"cust.id\">\r\n              {{cust.name}}\r\n            </mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n\r\n        <mat-form-field class=\"example-full-width col-md-5\">\r\n          <input matInput [disabled]=\"!activeEditProject\"\r\n                 [ngModel]=\"currentProject.projectType\"\r\n                 (keyup)=\"typedProjectType($event)\"                 \r\n                 [matAutocomplete]=\"auto\"\r\n                 placeholder=\"Project Type\">\r\n        </mat-form-field>\r\n\r\n        <mat-autocomplete #auto=\"matAutocomplete\"\r\n                          (optionSelected)=\"selectedProjectType($event)\"\r\n                          [displayWith]=\"displayFn\"\r\n                          >\r\n          <mat-option *ngFor=\"let item of projectTypesView.projectTypes\"\r\n                      [value]=\"item\"\r\n                      [disabled]=\"!activeEditProject\">\r\n            {{item.name}}\r\n          </mat-option>\r\n        </mat-autocomplete>\r\n\r\n\r\n        <!--<mat-form-field class=\"example-full-width col-md-5\">\r\n      <mat-select placeholder=\"Project type\" [(ngModel)]=\"currentProject.projectType.id\" [disabled]=\"!activeEditProject\">\r\n        <mat-option *ngFor=\"let item of projectTypesView.projectTypes\" [value]=\"item.id\">\r\n          {{item.name}}\r\n        </mat-option>\r\n      </mat-select>\r\n    </mat-form-field>-->\r\n\r\n        <mat-form-field class=\"example-full-width col-md-2\">\r\n          <input matInput [disabled]=\"!activeEditProject\" [matDatepicker]=\"picker\" [(ngModel)]=\"currentProject.date\"\r\n                 placeholder=\"Choose a date\">\r\n\r\n          <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\r\n          <mat-datepicker #picker></mat-datepicker>\r\n        </mat-form-field>\r\n\r\n\r\n        <mat-form-field class=\"example-full-width col-md-12\">\r\n          <textarea matInput [disabled]=\"!activeEditProject\" matTextareaAutosize [(ngModel)]=\"currentProject.memo\"\r\n                    placeholder=\"Memmo\"></textarea>\r\n        </mat-form-field>\r\n      </div>\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"row activity-wrapper\">\r\n  <div class=\"col-6 attach-card-wrapper\">\r\n    <h6 >Attachments</h6>\r\n    <mat-card class=\"attach-card\">\r\n\r\n      <table class=\"table  datatable  attachment-table\" [mfData]=\"projectAttachments.attachments\"\r\n        #mf=\"mfDataTable\" [mfRowsOnPage]=\"10\">\r\n        <tbody>\r\n          <tr *ngFor=\"let item of mf.data\">\r\n            <td class=\"link text-left pl-0\" (click)=\"openShowAttachment(item.id)\">{{item.fileName}}{{item.fileExtantion}}</td>\r\n            <td class=\"text-right pr-0\">\r\n              <div class=\"attachment-button mr-3\" (click)=\"downloadProjectAttachment(item.id)\"><i class=\"fa fa-arrow-down\"></i></div>\r\n              <div class=\"attachment-button text-right\" (click)=\"openDeleteAttachment(item.id)\"><i class=\"fas fa-times\"></i></div>\r\n            </td>\r\n          </tr>\r\n        </tbody>\r\n      </table>\r\n    \r\n      <div ng2FileDrop [ngClass]=\"{'nv-file-over': hasBaseDropZoneOver}\" (fileOver)=\"fileOverBase($event)\" (onFileDrop)=\"dropped($event)\"\r\n        [uploader]=\"uploader\" class=\"table-responsive col-md-12 p-0\">\r\n        <span *ngIf=\"showSpinner\">\r\n            <mat-progress-bar mode=\"indeterminate\"></mat-progress-bar>\r\n          </span>\r\n        <div class=\"drag\">          \r\n          <span *ngIf=\"!showSpinner\">Drag and drop files here</span>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-md-12 d-flex justify-content-end p-0 align-items-center\">\r\n        <button class=\"btn mr-3 download-button\" (click)=\"downloadAllProjectAttachment(currentProject.id)\">Download All</button>\r\n        <label class=\"custom-file-upload\">\r\n          <input (change)=\"inputAdd($event)\" type=\"file\" multiple />\r\n          Add New Files\r\n        </label>\r\n      </div>\r\n    </mat-card>\r\n  </div>\r\n  <div class=\"col-6 activity-card-wrapper\">\r\n    <h6 >Activity</h6>\r\n    <mat-card class=\"activity-card\">\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"row \">\r\n  <div class=\"col-12 order-wrapper\">\r\n    <br>\r\n    <h6>Orders</h6>\r\n    <mat-card class=\"order-card\">\r\n      <div class=\"kanban\">\r\n        <div class=\"row\">\r\n          <div class=\"col-md-3 drag-wrapper\">\r\n            <div class=\"section-heading\">New</div>\r\n            <div class=\"drag-container\">\r\n              <div cdkDropList #newList=\"cdkDropList\" [cdkDropListData]=\"newOrder\" [cdkDropListConnectedTo]=\"[activeList,processingList,doneList]\"\r\n                class=\"item-list\" (cdkDropListDropped)=\"drop($event)\">\r\n                <div class=\"item-box\" *ngFor=\"let item of newOrder\" cdkDrag>\r\n                  <div class=\"d-flex align-items-center mb-1\">\r\n                    <img class=\"order-blue\" src=\"../../../../../assets/img/order-2.svg\" alt=\"\">\r\n                    <span class=\"order-count\">50</span>\r\n                    <span class=\"order-date\">{{item.date | date:'shortDate' }}</span>\r\n                  </div>\r\n                  <div class=\"clients mb-1\">\r\n                    <div><span>Customer</span>{{item.customer.name}}</div>\r\n                    <div><span>Item</span>{{item.item.name}}</div>\r\n                  </div>\r\n                  <div class=\"d-flex justify-content-between align-items-center mb-1\" style=\"font-size:16px;\">\r\n                    <div class=\"red-circle\">\r\n                      {{item.project.user.firstName[0]}}\r\n                    </div>\r\n                    <span class=\"name-order\">\r\n                      {{item.project.user.firstName}}   {{item.project.user.lastName}}\r\n                    </span>\r\n                    <div class=\"clipboard\">\r\n                      <img src=\"../../../../../assets/img/clipboard-1.svg\" alt=\"\">\r\n                      <span>0/1</span>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n\r\n          <div class=\"col-md-3 drag-wrapper\">\r\n            <div class=\"section-heading\">Active</div>\r\n            <div class=\"drag-container\">\r\n              <div cdkDropList #activeList=\"cdkDropList\" [cdkDropListData]=\"activeOrder\" [cdkDropListConnectedTo]=\"[newList,processingList,doneList]\"\r\n                class=\"item-list\" (cdkDropListDropped)=\"drop($event)\">\r\n                <div class=\"item-box\" *ngFor=\"let item of activeOrder\" cdkDrag>\r\n                  <div class=\"d-flex align-items-center mb-1\">\r\n                    <img class=\"order-blue\" src=\"../../../../../assets/img/order-2.svg\" alt=\"\">\r\n                    <span class=\"order-count\">50</span>\r\n                    <span class=\"order-date\">{{item.date | date:'shortDate' }}</span>\r\n                  </div>\r\n                  <div class=\"clients mb-1\">\r\n                    <div><span>Customer</span>{{item.customer.name}}</div>\r\n                    <div><span>Item</span>{{item.item.name}}</div>\r\n                  </div>\r\n                  <div class=\"d-flex justify-content-between align-items-center mb-1\" style=\"font-size:16px;\">\r\n                    <div class=\"red-circle\">\r\n                      {{item.project.user.firstName[0]}}\r\n                    </div>\r\n                    <span class=\"name-order\">\r\n                      {{item.project.user.firstName}}   {{item.project.user.lastName}}\r\n                    </span>\r\n                    <div class=\"clipboard\">\r\n                      <img src=\"../../../../../assets/img/clipboard-1.svg\" alt=\"\">\r\n                      <span>0/1</span>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n\r\n          <div class=\"col-md-3 drag-wrapper\">\r\n            <div class=\"section-heading\">Processing</div>\r\n            <div class=\"drag-container\">\r\n              <div cdkDropList #processingList=\"cdkDropList\" [cdkDropListData]=\"processingOrder\"\r\n                [cdkDropListConnectedTo]=\"[activeList,newList,doneList]\" class=\"item-list\" (cdkDropListDropped)=\"drop($event)\">\r\n                <div class=\"item-box\" *ngFor=\"let item of processingOrder\" cdkDrag>\r\n                  <div class=\"d-flex align-items-center mb-1\">\r\n                    <img class=\"order-blue\" src=\"../../../../../assets/img/order-2.svg\" alt=\"\">\r\n                    <span class=\"order-count\">50</span>\r\n                    <span class=\"order-date\">{{item.date | date:'shortDate' }}</span>\r\n                  </div>\r\n                  <div class=\"clients mb-1\">\r\n                    <div><span>Customer</span>{{item.customer.name}}</div>\r\n                    <div><span>Item</span>{{item.item.name}}</div>\r\n                  </div>\r\n                  <div class=\"d-flex justify-content-between align-items-center mb-1\" style=\"font-size:16px;\">\r\n                    <div class=\"red-circle\">\r\n                      {{item.project.user.firstName[0]}}\r\n                    </div>\r\n                    <span class=\"name-order\">\r\n                      {{item.project.user.firstName}}   {{item.project.user.lastName}}\r\n                    </span>\r\n                    <div class=\"clipboard\">\r\n                      <img src=\"../../../../../assets/img/clipboard-1.svg\" alt=\"\">\r\n                      <span>0/1</span>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n\r\n          <div class=\"col-md-3 drag-wrapper\">\r\n            <div class=\"section-heading\">Done</div>\r\n            <div class=\"drag-container\">\r\n              <div cdkDropList #doneList=\"cdkDropList\" [cdkDropListData]=\"doneOrder\" [cdkDropListConnectedTo]=\"[processingList,activeList,newList]\"\r\n                class=\"item-list\" (cdkDropListDropped)=\"drop($event)\">\r\n                <div class=\"item-box\" *ngFor=\"let item of doneOrder\" cdkDrag>\r\n                  <div class=\"d-flex align-items-center mb-1\">\r\n                    <img class=\"order-blue\" src=\"../../../../../assets/img/order-2.svg\" alt=\"\">\r\n                    <span class=\"order-count\">50</span>\r\n                    <span class=\"order-date\">{{item.date | date:'shortDate' }}</span>\r\n                  </div>\r\n                  <div class=\"clients mb-1\">\r\n                    <div><span>Customer</span>{{item.customer.name}}</div>\r\n                    <div><span>Item</span>{{item.item.name}}</div>\r\n                  </div>\r\n                  <div class=\"d-flex justify-content-between align-items-center mb-1\" style=\"font-size:16px;\">\r\n                    <div class=\"red-circle\">\r\n                      {{item.project.user.firstName[0]}}\r\n                    </div>\r\n                    <span class=\"name-order\">\r\n                      {{item.project.user.firstName}}   {{item.project.user.lastName}}\r\n                    </span>\r\n                    <div class=\"clipboard\">\r\n                      <img src=\"../../../../../assets/img/clipboard-1.svg\" alt=\"\">\r\n                      <span>0/1</span>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"save-attachment\">\r\n  <div class=\"overlay\" *ngIf='openSaveAttachment'></div>\r\n  <div class=\"modal-save-attachment\" [ngClass]=\"{'active':openSaveAttachment}\">\r\n    <form name=\"form\" #saveAttachmentsForm=\"ngForm\" novalidate>\r\n      <div class=\"card-header\">\r\n        <span>\r\n          <strong>\r\n            <h5>Drag your files</h5>\r\n          </strong>\r\n        </span>\r\n        <div class=\"close-project-button\" (click)=\"closeSaveAttachmentModal()\">\r\n          <i class=\"fas fa-times\"></i>\r\n        </div>\r\n      </div>\r\n      <div class=\"modal-body\">\r\n        <div class=\"center\">\r\n          <div class=\"upload-table\">\r\n            <table class=\"table\">\r\n              <tbody class=\"upload-name-style\">\r\n                <tr *ngFor=\"let item of droppedFiles; let i=index\">\r\n                  <td><strong>{{ item.relativePath }}</strong><button (click)=\"fileLeave(item)\">Delete</button></td>\r\n                  <td></td>\r\n                </tr>\r\n              </tbody>\r\n            </table>\r\n          </div>\r\n        </div>\r\n        <div class=\"button-wrapper\">\r\n          <button class=\"btn btn-block btn-outline-primary\" (mousedown)=\"closeSaveAttachmentModal()\">Cancel</button>\r\n          <button class=\"btn btn-block btn-outline-primary\" (click)=\"formValidation = true\">Save</button>\r\n        </div>\r\n      </div>\r\n    </form>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"add-project\">\r\n  <div class=\"overlay\" *ngIf='openDeleteAttachmentModal'></div>\r\n  <div class=\"modal-add-project\" [ngClass]=\"{'active':openDeleteAttachmentModal}\">\r\n    <div class=\"card-header\">\r\n      <span>\r\n        <strong>\r\n          <h5>Are you sure you want to delete the project attachment?</h5>\r\n        </strong>\r\n      </span>\r\n      <div class=\"close-project-button\" (click)=\"cancelDeleteAttachment()\">\r\n        <i class=\"fas fa-times\"></i>\r\n      </div>\r\n    </div>\r\n    <div class=\"modal-body\">\r\n      <div class=\"row button-wrapper\">\r\n        <button class=\"btn btn-block btn-outline-primary\" (click)=\"cancelDeleteAttachment()\">Cancel</button>\r\n        <button class=\"btn btn-block btn-outline-primary\" (click)=\"yesDeleteAttachment()\">Yes</button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"add-project\">\r\n  <div class=\"overlay\" *ngIf='showAttachmentImg'></div>\r\n  <div class=\"modal-add-project\" [ngClass]=\"{'active':showAttachmentImg}\">\r\n    <div class=\"card-header\">\r\n      <span>\r\n        <strong>\r\n          <h5>Attachment: {{currentProjectAttachment.fileName}}</h5>\r\n        </strong>\r\n      </span>\r\n      <div class=\"close-project-button\" (click)=\"closeShowAttachment()\">\r\n        <i class=\"fas fa-times\"></i>\r\n      </div>\r\n    </div>\r\n    <div class=\"modal-body\">\r\n      <div>\r\n        <img src=\"{{currentProjectAttachment.link}}\" width=\"200px;\" height=\"200px;\" />\r\n      </div>\r\n      <div class=\"row button-wrapper\">\r\n        <button class=\"btn btn-block btn-outline-primary\" (click)=\"closeShowAttachment()\">Close</button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"add-project\">\r\n  <div class=\"overlay\" *ngIf='showAttachedTxt'></div>\r\n  <div class=\"modal-show-pdf\" [ngClass]=\"{'active':showAttachedTxt}\">\r\n    <div class=\"card-header\">\r\n      <span>\r\n        <strong>\r\n          <h5>Attachment: {{currentProjectAttachment.fileName}}</h5>\r\n        </strong>\r\n      </span>\r\n      <div class=\"close-project-button\" (click)=\"closeShowAttachment()\">\r\n        <i class=\"fas fa-times\"></i>\r\n      </div>\r\n    </div>\r\n    <div class=\"modal-body\">\r\n      <div class=\"text-container\">\r\n        <p>{{attachmentTxtText}}</p>\r\n      </div>\r\n      <div class=\"row button-wrapper\">\r\n        <button class=\"btn btn-block btn-outline-primary\" (click)=\"closeShowAttachment()\">Close</button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"add-project\">\r\n  <div class=\"overlay\" *ngIf='showAttachedPdf'></div>\r\n  <div class=\"modal-show-pdf\" [ngClass]=\"{'active':showAttachedPdf}\">\r\n    <div class=\"card-header\">\r\n      <span>\r\n        <strong>\r\n          <h5>Attachment: {{currentProjectAttachment.fileName}}</h5>\r\n        </strong>\r\n      </span>\r\n      <div class=\"close-project-button\" (click)=\"closeShowAttachment()\">\r\n        <i class=\"fas fa-times\"></i>\r\n      </div>\r\n    </div>\r\n    <div class=\"modal-body\">\r\n      <div class=\"pdf-container\">\r\n        <pdf-viewer [(src)]=\"attachmentPdf\" [original-size]=\"false\" [show-all]=\"true\" [render-text-mode]=\"true\"\r\n          [autoresize]=\"true\">\r\n        </pdf-viewer>\r\n      </div>\r\n      <div class=\"row button-wrapper\">\r\n        <button class=\"btn btn-block btn-outline-primary\" (click)=\"closeShowAttachment()\">Close</button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/projects/project-details/project-details.component.scss":
/*!****************************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/projects/project-details/project-details.component.scss ***!
  \****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "h6 {\n  font-size: 18px; }\n\n.download-button {\n  background: none;\n  border: none;\n  color: #0077c5;\n  font-size: 16px; }\n\n.download-button:focus {\n    outline: none !important;\n    box-shadow: none !important; }\n\n.custom-file-upload {\n  color: #0077c5;\n  cursor: pointer; }\n\nlabel {\n  margin-bottom: 0px; }\n\ninput[type=\"file\"] {\n  display: none; }\n\np {\n  margin-top: 1rem !important;\n  margin-bottom: 1rem; }\n\n.custom-table {\n  border-spacing: 0px 15px;\n  border-collapse: separate !important; }\n\n.custom-table thead tr th {\n    border-bottom: none !important;\n    border-top: none !important; }\n\n.custom-table tbody tr {\n    border-bottom: 20px solid white !important;\n    background: #F2F4F8 !important; }\n\n.custom-table tbody tr:after {\n      content: '';\n      border-bottom: 1px solid black;\n      position: absolute;\n      bottom: 0; }\n\n.custom-table tbody tr .info-task .success {\n      background: #4CAF50;\n      height: 48px;\n      width: 48px;\n      position: relative;\n      border-radius: 50%; }\n\n.custom-table tbody tr .info-task .success .counter {\n        position: absolute;\n        height: 17px;\n        width: 17px;\n        border: 1px solid #4CAF50;\n        border-radius: 50%;\n        top: 0;\n        right: -5px;\n        background: white;\n        display: flex;\n        justify-content: center;\n        align-items: center;\n        font-size: 10px;\n        font-weight: bold; }\n\n.custom-table tbody tr .info-task .success:before {\n        content: \"\";\n        position: absolute;\n        left: 20px;\n        top: 12px;\n        width: 10px;\n        height: 18px;\n        border: solid white;\n        border-width: 0 3px 3px 0;\n        -webkit-transform: rotate(45deg);\n        transform: rotate(45deg); }\n\n.custom-table tbody tr .info-task .error {\n      background: #CD3232;\n      height: 48px;\n      width: 48px;\n      position: relative;\n      border-radius: 50%;\n      text-align: center;\n      vertical-align: middle;\n      display: table-cell;\n      color: white; }\n\n.custom-table tbody tr .info-task .error .counter-error {\n        position: absolute;\n        height: 15px;\n        width: 15px;\n        border: 1px solid #CD3232;\n        border-radius: 50%;\n        top: 0;\n        right: 0;\n        background: white;\n        display: flex;\n        justify-content: center;\n        align-items: center;\n        font-size: 10px;\n        font-weight: bold;\n        color: black; }\n\n.custom-table tbody tr td {\n      border-top: 1px solid #D0D0D0;\n      border-bottom: 1px solid #D0D0D0;\n      vertical-align: middle; }\n\n.custom-table tbody tr td:first-child {\n        border-left: 1px solid #D0D0D0; }\n\n.custom-table tbody tr td:last-child {\n        border-right: 1px solid #D0D0D0; }\n\n.custom-table tbody tr:hover {\n      background: #d5d5d5 !important; }\n\n.custom-table tfoot tr td {\n    border-top: none !important; }\n\n.custom-table tfoot tr .footer-border {\n    border-top: none; }\n\n.drag {\n  height: 100px;\n  width: 100%;\n  border: 1px dashed #636363;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  margin-bottom: 30px;\n  color: #636363;\n  font-weight: 300; }\n\n.selected-game {\n  display: inline-block;\n  margin-right: 15px;\n  font-size: 15px;\n  background: #B9CFEA;\n  margin-bottom: 10px;\n  padding: 2px 11px 4px 11px;\n  border-radius: 8px; }\n\n.delete-game {\n  font-size: 10px;\n  padding: 3px 6px;\n  vertical-align: middle;\n  cursor: pointer; }\n\n#newProject {\n  width: 180px;\n  height: 45px;\n  font-size: large; }\n\n.add-project .overlay {\n  position: fixed;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  background: rgba(0, 0, 0, 0.4);\n  z-index: 10000; }\n\n.add-project .modal-add-project {\n  position: fixed;\n  background: #fff;\n  z-index: 10000;\n  top: -100%;\n  left: calc(50% - 200px);\n  width: 400px; }\n\n.add-project .modal-add-project.active {\n    top: 200px;\n    left: calc(50% - 200px);\n    transition: top .5s; }\n\n.add-project .modal-add-project .row {\n    width: 100%; }\n\n.add-project .modal-add-project .row .mt-10 {\n      margin-top: 15px; }\n\n.add-project .modal-add-project .button-wrapper {\n    display: flex;\n    justify-content: flex-end;\n    margin-top: 10px; }\n\n.add-project .modal-add-project .button-wrapper .btn {\n      width: 100px;\n      margin: 0;\n      margin-left: 10px; }\n\n.add-project .modal-show-pdf {\n  position: fixed;\n  background: #fff;\n  z-index: 10000;\n  top: -100%;\n  left: calc(50% - 400px);\n  width: 800px; }\n\n.add-project .modal-show-pdf .modal-body {\n    padding-right: 0; }\n\n.add-project .modal-show-pdf .modal-body .pdf-container {\n      max-height: 400px;\n      overflow-y: auto; }\n\n.add-project .modal-show-pdf .modal-body .text-container {\n      max-height: 400px;\n      overflow-y: auto; }\n\n.add-project .modal-show-pdf.active {\n    top: 200px;\n    left: calc(50% - 400px);\n    transition: top .5s; }\n\n.add-project .modal-show-pdf .row {\n    width: 100%; }\n\n.add-project .modal-show-pdf .row .mt-10 {\n      margin-top: 15px; }\n\n.add-project .modal-show-pdf .button-wrapper {\n    display: flex;\n    justify-content: flex-end;\n    margin-top: 10px; }\n\n.add-project .modal-show-pdf .button-wrapper .btn {\n      width: 100px;\n      margin: 0;\n      margin-left: 10px; }\n\n.save-attachment .overlay {\n  position: fixed;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  background: rgba(0, 0, 0, 0.4);\n  z-index: 10000; }\n\n.save-attachment .modal-save-attachment {\n  position: fixed;\n  background: #fff;\n  z-index: 10000;\n  top: -100%;\n  left: calc(50% - 400px);\n  width: 900px; }\n\n.save-attachment .modal-save-attachment.active {\n    top: 200px;\n    left: calc(50% - 400px);\n    transition: top .5s; }\n\n.save-attachment .modal-save-attachment .row {\n    width: 100%; }\n\n.save-attachment .modal-save-attachment .row .mt-10 {\n      margin-top: 15px; }\n\n.save-attachment .modal-save-attachment .button-wrapper {\n    display: flex;\n    justify-content: flex-end;\n    margin-top: 10px; }\n\n.save-attachment .modal-save-attachment .button-wrapper .btn {\n      width: 100px;\n      margin: 0;\n      margin-left: 10px; }\n\n.project-list-button {\n  width: 11.1px;\n  height: 18.1px;\n  margin-left: 15px;\n  margin-bottom: 28.6px; }\n\n.close-project-button {\n  margin-left: auto; }\n\n.attachment-button {\n  display: inline; }\n\n.button-wrapper {\n  vertical-align: bottom;\n  margin-top: auto; }\n\n.card-body.project-details.project {\n  border: 1px solid #e1e6ef; }\n\n.card-body {\n  padding: 16px !important; }\n\n.card-header {\n  padding: 0.75rem 1.25rem;\n  margin-bottom: 0;\n  background-color: #f9f9fa;\n  display: flex;\n  border-bottom: 0px;\n  border: 1px solid #e1e6ef; }\n\n.project-details-input-wrapper.project {\n  display: flex; }\n\n.row {\n  display: flex;\n  flex-wrap: wrap;\n  margin-left: 0 !important; }\n\n.project-name-description-wrapper {\n  display: flex; }\n\n.card {\n  border: 0px !important; }\n\n.edit-project-button {\n  margin-left: auto; }\n\n.attachment-table {\n  text-align: center; }\n\n.attachment-table thead th {\n    border-top: none !important;\n    border-bottom: none !important; }\n\n.attachment-table tbody td {\n    border-top: none !important;\n    border-bottom: none !important; }\n\n.attachment-table tbody .link {\n    color: #0077c5 !important; }\n\n.edit-button-wrapper {\n  display: flex;\n  justify-content: flex-end;\n  margin-bottom: 13px;\n  position: absolute;\n  right: 37px;\n  top: 23px; }\n\n.edit-button-wrapper .btn {\n    width: 100px;\n    margin: 0;\n    margin-left: 10px; }\n\n.edit-button {\n  height: 24px;\n  width: 24px;\n  cursor: pointer;\n  position: absolute;\n  right: 24.5px;\n  top: 18.6px; }\n\n.proj-button {\n  color: #0077c5 !important;\n  cursor: pointer;\n  margin-right: 18px; }\n\n.proj-button:last-child {\n    margin-right: 0px; }\n\n.section-heading {\n  padding: 5px 10px 11px 0px;\n  font-size: 18px;\n  font-weight: bold; }\n\n.drag-container {\n  width: 100%;\n  height: auto;\n  max-width: 100%;\n  margin: 0 25px 25px 0;\n  display: inline-block;\n  vertical-align: top;\n  background-color: #E9ECEF;\n  padding: 15px; }\n\n.item-list {\n  min-height: 60px;\n  border-radius: 4px;\n  display: block; }\n\n.item-box {\n  border: solid 1px #808080;\n  margin-bottom: 15px;\n  color: rgba(0, 0, 0, 0.87);\n  box-sizing: border-box;\n  cursor: move;\n  background: white;\n  font-size: 13px;\n  padding: 15px; }\n\n.item-box p {\n    margin-bottom: 5px;\n    color: #636363; }\n\n.item-box p span {\n      color: black; }\n\n.item-box p .date-proj {\n      margin-left: 25px; }\n\n.item-box p .red {\n      background: #CD3232;\n      display: block;\n      height: 25px;\n      width: 25px;\n      margin-right: 10px;\n      border-radius: 50%; }\n\n.item-box p .blue {\n      background: #0077c5;\n      display: block;\n      width: 15px;\n      height: 15px; }\n\n.item-box p .yellow {\n      background: #ffba00;\n      display: block;\n      width: 20px;\n      height: 22.1px; }\n\n.cdk-drag-preview {\n  box-sizing: border-box;\n  border-radius: 4px;\n  box-shadow: 0 5px 5px -3px rgba(0, 0, 0, 0.2), 0 8px 10px 1px rgba(0, 0, 0, 0.14), 0 3px 14px 2px rgba(0, 0, 0, 0.12);\n  border-radius: 20px; }\n\n#newOrders {\n  font-size: 14px;\n  font-weight: bold;\n  color: #636363;\n  background: transparent;\n  border: 1px solid #707070; }\n\n.item-box .order-blue {\n  width: 16.5px;\n  height: 20px;\n  margin-right: 10px; }\n\n.item-box .order-count {\n  margin-right: 31px;\n  font-size: 16px;\n  font-weight: bold; }\n\n.item-box .order-date {\n  font-size: 16px; }\n\n.item-box .clipboard {\n  display: flex;\n  align-items: center;\n  margin-left: 10px; }\n\n.item-box .clipboard img {\n    width: 20px;\n    height: 22.1px;\n    margin-right: 10px; }\n\n.item-box .red-circle {\n  min-width: 30px;\n  min-height: 30px;\n  background-color: #cd3232;\n  border-radius: 50%;\n  color: white;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  text-transform: uppercase;\n  margin-right: 10px; }\n\n.item-box .clients {\n  font-size: 16px; }\n\n.item-box .clients span {\n    color: #636363;\n    margin-right: 9px; }\n\n/*#circle {\r\n    margin-right: 20px;\r\n    width: 20px;\r\n    height: 20px;\r\n    background: #cd3232;\r\n    -moz-border-radius: 20px;\r\n    -webkit-border-radius: 20px;\r\n    border-radius: 20px;\r\n  }*/\n\n.name-project {\n  font-size: 24px;\n  font-weight: bold;\n  margin-bottom: 54px; }\n\n.orders-name {\n  font-size: 24px;\n  font-weight: bold;\n  margin-bottom: 36px;\n  margin-top: 25px; }\n\n.kanban {\n  margin-top: 105px; }\n\n.name-order {\n  white-space: nowrap;\n  text-overflow: ellipsis;\n  overflow: hidden; }\n\ntextarea.mat-autosize {\n  resize: auto !important;\n  overflow: hidden !important; }\n"

/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/projects/project-details/project-details.component.ts":
/*!**************************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/projects/project-details/project-details.component.ts ***!
  \**************************************************************************************************************/
/*! exports provided: ProjectDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProjectDetailsComponent", function() { return ProjectDetailsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var file_saver__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! file-saver */ "./node_modules/file-saver/dist/FileSaver.min.js");
/* harmony import */ var file_saver__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(file_saver__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/cdk/drag-drop */ "./node_modules/@angular/cdk/esm5/drag-drop.es5.js");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ng2-file-upload */ "./node_modules/ng2-file-upload/index.js");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(ng2_file_upload__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var app_shared_constans_projects_and_orders_constans__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! app/shared/constans/projects-and-orders.constans */ "./src/app/shared/constans/projects-and-orders.constans.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var app_shared_providers_services_project_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! app/shared/providers/services/project.service */ "./src/app/shared/providers/services/project.service.ts");
/* harmony import */ var app_shared_providers_services_customer_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! app/shared/providers/services/customer.service */ "./src/app/shared/providers/services/customer.service.ts");
/* harmony import */ var app_shared_providers_services_noitification_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! app/shared/providers/services/noitification.service */ "./src/app/shared/providers/services/noitification.service.ts");
/* harmony import */ var app_shared_providers_services_project_attachment_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! app/shared/providers/services/project-attachment.service */ "./src/app/shared/providers/services/project-attachment.service.ts");
/* harmony import */ var app_shared_providers_services_load_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! app/shared/providers/services/load.service */ "./src/app/shared/providers/services/load.service.ts");
/* harmony import */ var app_shared_providers_services_order_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! app/shared/providers/services/order.service */ "./src/app/shared/providers/services/order.service.ts");
/* harmony import */ var app_shared_models_users_get_all_users_view__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! app/shared/models/users/get-all-users.view */ "./src/app/shared/models/users/get-all-users.view.ts");
/* harmony import */ var app_shared_models_project_get_project_view__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! app/shared/models/project/get-project.view */ "./src/app/shared/models/project/get-project.view.ts");
/* harmony import */ var app_shared_models_customer_get_all_customer_view__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! app/shared/models/customer/get-all-customer.view */ "./src/app/shared/models/customer/get-all-customer.view.ts");
/* harmony import */ var app_shared_models_project_update_project_view__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! app/shared/models/project/update-project.view */ "./src/app/shared/models/project/update-project.view.ts");
/* harmony import */ var app_shared_models_project_attachment_get_all_by_project_id_projects_attachments_view__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! app/shared/models/project-attachment/get-all-by-project-id-projects-attachments-view */ "./src/app/shared/models/project-attachment/get-all-by-project-id-projects-attachments-view.ts");
/* harmony import */ var app_shared_models_project_attachment_get_project_attachment_view__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! app/shared/models/project-attachment/get-project-attachment.view */ "./src/app/shared/models/project-attachment/get-project-attachment.view.ts");
/* harmony import */ var app_shared_models_project_attachment_delete_project_attachment_view__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! app/shared/models/project-attachment/delete-project-attachment.view */ "./src/app/shared/models/project-attachment/delete-project-attachment.view.ts");
/* harmony import */ var app_shared_models_project_get_all_project_types_view__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! app/shared/models/project/get-all-project-types.view */ "./src/app/shared/models/project/get-all-project-types.view.ts");
/* harmony import */ var app_shared_models_order_get_all_order_by_projectId_view__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! app/shared/models/order/get-all-order-by-projectId.view */ "./src/app/shared/models/order/get-all-order-by-projectId.view.ts");
/* harmony import */ var app_shared_models_order_update_order_view__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! app/shared/models/order/update-order.view */ "./src/app/shared/models/order/update-order.view.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};























var ProjectDetailsComponent = /** @class */ (function () {
    function ProjectDetailsComponent(notificationService, projectService, customerService, projectAttachmentService, activateRoute, ordersService, route, loadService, constans) {
        this.notificationService = notificationService;
        this.projectService = projectService;
        this.customerService = customerService;
        this.projectAttachmentService = projectAttachmentService;
        this.activateRoute = activateRoute;
        this.ordersService = ordersService;
        this.route = route;
        this.loadService = loadService;
        this.constans = constans;
        //post request
        this.updateProject = new app_shared_models_project_update_project_view__WEBPACK_IMPORTED_MODULE_16__["UpdateProjectView"]();
        //form
        this.myControl = new _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormControl"]();
        this.projectManagers = new app_shared_models_users_get_all_users_view__WEBPACK_IMPORTED_MODULE_13__["GetAllUserView"]();
        this.currentProject = new app_shared_models_project_get_project_view__WEBPACK_IMPORTED_MODULE_14__["GetProjectView"]();
        this.customers = new app_shared_models_customer_get_all_customer_view__WEBPACK_IMPORTED_MODULE_15__["GetAllCustomerView"]();
        this.projectAttachments = new app_shared_models_project_attachment_get_all_by_project_id_projects_attachments_view__WEBPACK_IMPORTED_MODULE_17__["GetAllByProjectIdProjectAttachmentsView"]();
        this.ordersByProjectId = new app_shared_models_order_get_all_order_by_projectId_view__WEBPACK_IMPORTED_MODULE_21__["GetAllOrdersByProjectIdView"]();
        this.newOrder = [];
        this.activeOrder = [];
        this.processingOrder = [];
        this.doneOrder = [];
        this.updateOrder = new app_shared_models_order_update_order_view__WEBPACK_IMPORTED_MODULE_22__["UpdateOrderView"]();
        this.projectTypesView = new app_shared_models_project_get_all_project_types_view__WEBPACK_IMPORTED_MODULE_20__["GetAllProjectTypesView"]();
        //popups
        this.activeEditProject = false;
        this.formValidation = false;
        this.openSaveAttachment = false;
        this.openDeleteAttachmentModal = false;
        this.droppedFiles = [];
        this.deletedAttachment = new app_shared_models_project_attachment_delete_project_attachment_view__WEBPACK_IMPORTED_MODULE_19__["DeleteProjectAttachmentView"]();
        this.hasBaseDropZoneOver = false;
        //spinner
        this.showSpinner = false;
        //view attachment
        this.uploader = new ng2_file_upload__WEBPACK_IMPORTED_MODULE_4__["FileUploader"]({ url: this.constans.URL });
        this.showAttachmentImg = false;
        this.showAttachedPdf = false;
        this.showAttachedTxt = false;
        this.showAttachment = false;
        this.currentProjectAttachment = new app_shared_models_project_attachment_get_project_attachment_view__WEBPACK_IMPORTED_MODULE_18__["GetProjectAttachmentView"]();
        this.projectId = this.activateRoute.snapshot.params['id'];
    }
    ProjectDetailsComponent.prototype.ngOnInit = function () {
        this.loadService.set(true);
        this.getProjectManager();
        this.getCustomers();
        this.getProjectTypes();
        this.showProjectDetailsById(this.projectId);
        this.getAllOrdersByProjectId(this.projectId);
    };
    //gets
    ProjectDetailsComponent.prototype.getAllOrdersByProjectId = function (projectId) {
        var _this = this;
        this.ordersService.getAllOrdersByProjectId(projectId).subscribe(function (response) {
            _this.ordersByProjectId = response;
            _this.sortOrderByStatus(_this.ordersByProjectId.orders);
        }, function (error) {
            _this.errors = error;
        });
    };
    ProjectDetailsComponent.prototype.getProjectAttachments = function (id) {
        var _this = this;
        this.projectAttachmentService.getAllProjectsAttachmentsByProjectId(id).subscribe(function (response) {
            _this.projectAttachments = response;
            _this.showSpinner = false;
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    ProjectDetailsComponent.prototype.getProjectTypes = function () {
        var _this = this;
        this.projectService.getAllProjectTypes().subscribe(function (response) {
            _this.projectTypesView = response;
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    ProjectDetailsComponent.prototype.getCustomers = function () {
        var _this = this;
        this.customerService.getCustomers().subscribe(function (response) {
            _this.customers = response;
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    ProjectDetailsComponent.prototype.getProjectManager = function () {
        var _this = this;
        this.projectService.getProjectManager().subscribe(function (response) {
            _this.projectManagers = response;
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    ProjectDetailsComponent.prototype.displayFn = function (val) {
        return val ? val.name : val;
    };
    ProjectDetailsComponent.prototype.selectedProjectType = function (item) {
        this.currentProject.projectType.id = item.option.value.id;
        this.currentProject.projectType.name = item.option.value.name;
    };
    ProjectDetailsComponent.prototype.typedProjectType = function (event) {
        this.currentProject.projectType.name = '';
        this.currentProject.projectType.id = null;
        this.currentProject.projectType.name = event.target.value;
    };
    //attachments
    ProjectDetailsComponent.prototype.openShowAttachment = function (id) {
        var _this = this;
        this.projectAttachmentService.getProjectAttachment(id).subscribe(function (response) {
            _this.currentProjectAttachment = response;
            if (_this.currentProjectAttachment.fileName.length > 35) {
                _this.currentProjectAttachment.fileName = _this.currentProjectAttachment.fileName.substring(0, 35) + "...";
            }
            _this.constans.allowableImageExtensions.forEach(function (x) {
                if (x === _this.currentProjectAttachment.fileExtantion) {
                    _this.showAttachmentImg = true;
                }
            });
            if (_this.currentProjectAttachment.fileExtantion === _this.constans.attacmentsData.pdf.extension) {
                _this.projectAttachmentService.downloadAttachmentToArray(id).subscribe(function (response) {
                    _this.attachmentPdf = window.URL.createObjectURL(response);
                });
                _this.showAttachedPdf = true;
            }
            if (_this.currentProjectAttachment.fileExtantion === _this.constans.attacmentsData.text.extension) {
                _this.projectAttachmentService.downloadAttachment(id).subscribe(function (data) {
                    _this.attachmentTxtText = data;
                });
                _this.showAttachedTxt = true;
            }
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    ProjectDetailsComponent.prototype.closeShowAttachment = function () {
        this.showAttachedTxt = false;
        this.showAttachment = false;
        this.showAttachmentImg = false;
        this.showAttachedPdf = false;
        this.currentProjectAttachment = new app_shared_models_project_attachment_get_project_attachment_view__WEBPACK_IMPORTED_MODULE_18__["GetProjectAttachmentView"]();
        this.currentPdfLink = "";
        this.attachmentTxtText = "";
    };
    ProjectDetailsComponent.prototype.sortOrderByStatus = function (allOrders) {
        var _this = this;
        this.newOrder = allOrders.filter(function (x) { return x.status == _this.constans.status.new; });
        this.activeOrder = allOrders.filter(function (x) { return x.status == _this.constans.status.active; });
        this.processingOrder = allOrders.filter(function (x) { return x.status == _this.constans.status.processing; });
        this.doneOrder = allOrders.filter(function (x) { return x.status == _this.constans.status.done; });
    };
    ProjectDetailsComponent.prototype.openDeleteAttachment = function (id) {
        this.deletedAttachmentId = id;
        this.openDeleteAttachmentModal = true;
    };
    ProjectDetailsComponent.prototype.cancelDeleteAttachment = function () {
        this.deletedAttachmentId = null;
        this.openDeleteAttachmentModal = false;
    };
    ProjectDetailsComponent.prototype.yesDeleteAttachment = function () {
        this.deleteAttachment();
        this.openDeleteAttachmentModal = false;
    };
    ProjectDetailsComponent.prototype.openSaveAttachmentModal = function () {
        this.openSaveAttachment = true;
        this.formValidation = false;
        this.droppedFiles = [];
    };
    ProjectDetailsComponent.prototype.closeSaveAttachmentModal = function () {
        this.openSaveAttachment = false;
        this.formValidation = false;
    };
    ProjectDetailsComponent.prototype.checkAttachmentExtensions = function (files) {
        var invalidFiles = [];
        for (var i = 0; i < files.length; i++) {
            this.showSpinner = true;
            var success = false;
            this.constans.allowableExtensions.forEach(function (ex) {
                var index = files[i].name.lastIndexOf(".");
                var extensions = files[i].name.substr(index + 1);
                if (ex == extensions) {
                    success = true;
                    return;
                }
            });
            if (!success) {
                invalidFiles.push(files[i].name);
                this.showSpinner = false;
            }
            if (success) {
                this.saveAttachments(files[i]);
            }
        }
        ;
        return invalidFiles;
    };
    ProjectDetailsComponent.prototype.dropped = function (fileList) {
        var invalidFiles = this.checkAttachmentExtensions(fileList);
        if (invalidFiles.length > 0) {
            this.notificationService.showWarning(this.constans.fileExtensionNotAllowedMessage);
        }
    };
    ProjectDetailsComponent.prototype.drop = function (event) {
        var _this = this;
        if (event.previousContainer === event.container) {
            Object(_angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_3__["moveItemInArray"])(event.container.data, event.previousIndex, event.currentIndex);
        }
        else {
            Object(_angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_3__["transferArrayItem"])(event.previousContainer.data, event.container.data, event.previousIndex, event.currentIndex);
            this.curentOrder = event.container.data[event.currentIndex];
            if (event.container.id == "cdk-drop-list-0") {
                this.curentOrder.status = this.constans.status.new;
            }
            if (event.container.id == "cdk-drop-list-1") {
                this.curentOrder.status = this.constans.status.active;
            }
            if (event.container.id == "cdk-drop-list-2") {
                this.curentOrder.status = this.constans.status.processing;
            }
            if (event.container.id == "cdk-drop-list-3") {
                this.curentOrder.status = this.constans.status.done;
            }
            this.updateOrder.id = this.curentOrder.id;
            this.updateOrder.date = this.curentOrder.date;
            this.updateOrder.dueDate = this.curentOrder.dueDate;
            this.updateOrder.memo = this.curentOrder.memo;
            this.updateOrder.status = this.curentOrder.status;
            this.updateOrder.customerId = this.curentOrder.customer.id;
            this.updateOrder.projectId = this.curentOrder.project.id;
            this.updateOrder.itemId = this.curentOrder.item.id;
            this.ordersService.updateOrder(this.updateOrder).subscribe(function (response) {
                _this.notificationService.showSuccess(_this.constans.orderUpdateMessage);
            });
        }
    };
    ProjectDetailsComponent.prototype.inputAdd = function (event) {
        var fileList = event.target.files;
        var invalidFiles = this.checkAttachmentExtensions(fileList);
        if (invalidFiles.length > 0) {
            this.notificationService.showWarning(this.constans.fileExtensionNotAllowedMessage);
        }
    };
    ProjectDetailsComponent.prototype.fileLeave = function (file) {
        var index = this.droppedFiles.indexOf(file);
        if (index > -1) {
            this.droppedFiles.splice(index, 1);
        }
    };
    ProjectDetailsComponent.prototype.fileOver = function (event) { };
    ProjectDetailsComponent.prototype.fileOverBase = function (event) {
        this.hasBaseDropZoneOver = event;
    };
    ProjectDetailsComponent.prototype.saveAttachments = function (file) {
        var _this = this;
        var userId = "9c4989af-de13-4852-9fa8-d5a40d851a75";
        var formData = new FormData();
        formData.append("file", file, file.name);
        formData.append("userId", userId);
        formData.append("projectId", this.currentProject.id);
        this.projectAttachmentService.createProjectAttachment(formData).subscribe(function (response) {
            _this.notificationService.showSuccess(_this.constans.fileUploadeMessage + " " + file.name);
            _this.getProjectAttachments(_this.currentProject.id);
        });
        this.openSaveAttachment = false;
        this.formValidation = true;
    };
    //download files
    ProjectDetailsComponent.prototype.downloadProjectAttachment = function (id) {
        var _this = this;
        this.projectAttachmentService.getProjectAttachment(id).subscribe(function (response) {
            _this.currentProjectAttachment = response;
            _this.projectAttachmentService.downloadAttachmentToBlob(id).subscribe(function (result) {
                var file = result;
                var filename = _this.currentProjectAttachment.fileName + _this.currentProjectAttachment.fileExtantion;
                if (_this.currentProjectAttachment.fileExtantion === _this.constans.attacmentsData.pdf.extension) {
                    var blob = new Blob([file], { type: _this.constans.attacmentsData.pdf.type });
                    file_saver__WEBPACK_IMPORTED_MODULE_1___default()(blob, filename);
                }
                if (_this.currentProjectAttachment.fileExtantion === _this.constans.attacmentsData.imageJpeg.extension) {
                    var blob = new Blob([file], { type: _this.constans.attacmentsData.imageJpeg.type });
                    file_saver__WEBPACK_IMPORTED_MODULE_1___default()(blob, filename);
                }
                if (_this.currentProjectAttachment.fileExtantion === _this.constans.attacmentsData.imagePng.extension) {
                    var blob = new Blob([file], { type: _this.constans.attacmentsData.imagePng.type });
                    file_saver__WEBPACK_IMPORTED_MODULE_1___default()(blob, filename);
                }
                if (_this.currentProjectAttachment.fileExtantion === _this.constans.attacmentsData.text.extension) {
                    var blob = new Blob([file], { type: _this.constans.attacmentsData.text.type });
                    file_saver__WEBPACK_IMPORTED_MODULE_1___default()(blob, filename);
                }
                if (_this.currentProjectAttachment.fileExtantion === _this.constans.attacmentsData.docx.extension) {
                    var blob = new Blob([file], { type: _this.constans.attacmentsData.docx.type });
                    file_saver__WEBPACK_IMPORTED_MODULE_1___default()(blob, filename);
                }
                if (_this.currentProjectAttachment.fileExtantion === _this.constans.attacmentsData.xlsx.extension) {
                    var blob = new Blob([file], { type: _this.constans.attacmentsData.xlsx.type });
                    file_saver__WEBPACK_IMPORTED_MODULE_1___default()(blob, filename);
                }
                if (_this.currentProjectAttachment.fileExtantion === _this.constans.attacmentsData.msg.extension) {
                    var blob = new Blob([file], { type: _this.constans.attacmentsData.msg.type });
                    file_saver__WEBPACK_IMPORTED_MODULE_1___default()(blob, filename);
                }
            });
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    ProjectDetailsComponent.prototype.downloadAllProjectAttachment = function (id) {
        var _this = this;
        this.projectAttachmentService.downloadAllProjectsAttachment(id).subscribe(function (response) {
            var zip = response;
            var filename = _this.constans.attacmentsData.zip.filename;
            var blob = new Blob([zip], { type: _this.constans.attacmentsData.zip.type });
            file_saver__WEBPACK_IMPORTED_MODULE_1___default()(blob, filename);
        });
    };
    ProjectDetailsComponent.prototype.downloadFromBlob = function (link) {
        window.open(link);
    };
    ProjectDetailsComponent.prototype.deleteAttachment = function () {
        var _this = this;
        this.deletedAttachment.id = this.deletedAttachmentId;
        this.projectAttachmentService.deleteAttachment(this.deletedAttachment).subscribe(function (response) {
            _this.notificationService.showSuccess(_this.constans.atachmentDeleteMessage);
            _this.getProjectAttachments(_this.projectAttachments.projectId);
        });
        this.deletedAttachmentId = null;
    };
    //back on page
    ProjectDetailsComponent.prototype.showingProjectsList = function () {
        this.route.navigate(['projects-and-orders/projects/projectslist']);
    };
    ProjectDetailsComponent.prototype.showProjectDetailsById = function (id) {
        var _this = this;
        this.projectService.getProjectById(id).subscribe(function (response) {
            _this.currentProject = response;
            if (_this.currentProject.name.length > 40) {
                _this.currentProject.name = _this.currentProject.name.substring(0, 40) + "...";
            }
            _this.activeEditProject = false;
            _this.loadService.set(false);
        });
        this.getProjectAttachments(id);
    };
    //update project
    ProjectDetailsComponent.prototype.activateEditProject = function () {
        this.activeEditProject = true;
    };
    ProjectDetailsComponent.prototype.canceltProjectChange = function () {
        this.showProjectDetailsById(this.currentProject.id);
    };
    ProjectDetailsComponent.prototype.saveProjectChange = function () {
        var _this = this;
        if (!this.currentProject.name) {
            this.notificationService.showError(this.constans.saveProjectErrorMessage);
        }
        if (this.currentProject.name) {
            this.updateProject = this.currentProject;
            if (this.updateDate != null) {
                this.updateProject.date = this.updateDate;
            }
            if (this.updateDate == null) {
                this.updateProject.date = this.currentProject.date;
            }
            this.projectService.updateProject(this.updateProject).subscribe(function (response) {
                _this.showProjectDetailsById(_this.currentProject.id);
                _this.notificationService.showSuccess(_this.constans.projectUpdateMessage);
            }),
                function (error) {
                    _this.notificationService.showError(error.error);
                };
        }
    };
    ProjectDetailsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-project-details',
            template: __webpack_require__(/*! ./project-details.component.html */ "./src/app/projects-and-orders/project-and-orders/projects/project-details/project-details.component.html"),
            styles: [__webpack_require__(/*! ./project-details.component.scss */ "./src/app/projects-and-orders/project-and-orders/projects/project-details/project-details.component.scss")]
        }),
        __metadata("design:paramtypes", [app_shared_providers_services_noitification_service__WEBPACK_IMPORTED_MODULE_9__["NotificationService"],
            app_shared_providers_services_project_service__WEBPACK_IMPORTED_MODULE_7__["ProjectService"],
            app_shared_providers_services_customer_service__WEBPACK_IMPORTED_MODULE_8__["CustomerService"],
            app_shared_providers_services_project_attachment_service__WEBPACK_IMPORTED_MODULE_10__["ProjectAttachmentservice"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            app_shared_providers_services_order_service__WEBPACK_IMPORTED_MODULE_12__["OrderService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            app_shared_providers_services_load_service__WEBPACK_IMPORTED_MODULE_11__["LoadService"],
            app_shared_constans_projects_and_orders_constans__WEBPACK_IMPORTED_MODULE_5__["ProjectsAndOrdersConstans"]])
    ], ProjectDetailsComponent);
    return ProjectDetailsComponent;
}());



/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/projects/projects-list/projects-list.component.html":
/*!************************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/projects/projects-list/projects-list.component.html ***!
  \************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<br>\r\n<div  class=\"content projects-tab\">\r\n  <div class=\"row col-md-12 d-flex justify-content-between align-items-center project-title\">\r\n    <h3 class=\"project-name\">\r\n        Projects ({{projectsCount}})\r\n      </h3>\r\n    <div  class=\"add-team-button\">\r\n      <button id=\"newProject\"  class=\"btn btn-outline-secondary\" (click)=\"saveNewProject()\">+ New Project</button>\r\n    </div>\r\n  </div>\r\n\r\n  <dx-data-grid [dataSource]=\"dataGrid.dataSource\"\r\n                [showBorders]=\"true\"\r\n                [hoverStateEnabled]=\"true\"\r\n                id=\"gridContainer\"\r\n                (onRowUpdating)=\"saveReasignPM($event)\"\r\n                (onRowClick)=\"showProjectDetailsById($event)\"\r\n                style=\"top: 12px;\">\r\n\r\n    <dxo-search-panel [visible]=\"true\"\r\n                      alignment=\"left\"\r\n                      placeholder=\"\"></dxo-search-panel>\r\n    <dxo-header-filter [allowSearch]=\"true\"\r\n                       [visible]=\"showHeaderFilter\" class=\"search-panel\"></dxo-header-filter>\r\n    <dxo-editing mode=\"cell\"\r\n                 [allowUpdating]=\"true\">\r\n    </dxo-editing>\r\n    <dxi-column [allowEditing]=\"false\" dataField=\"customer.name\"\r\n                caption=\"Customer\">\r\n    </dxi-column>\r\n    <dxo-load-panel [enabled]=\"false\"></dxo-load-panel>\r\n    <dxi-column [allowEditing]=\"false\" dataField=\"name\" caption=\"Project Name\">\r\n    </dxi-column>\r\n\r\n    <dxi-column [allowEditing]=\"false\" dataField=\"projectType.name\" caption=\"Project Type\">\r\n    </dxi-column>\r\n\r\n    <dxi-column [allowEditing]=\"false\" dataField=\"description\">\r\n    </dxi-column>\r\n\r\n    <dxi-column [allowEditing]=\"false\" dataField=\"date\"\r\n                dataType=\"date\"\r\n                format=\"M/d/yyyy\"\r\n                width=\"8%\"></dxi-column>\r\n\r\n    <dxi-column dataField=\"user.id\" width=\"15%\" caption=\"Project Manager\">\r\n      <dxo-header-filter [allowSearch]=\"true\"></dxo-header-filter>\r\n      <dxo-lookup [dataSource]=\"projectManagers.users\"\r\n                  displayExpr=\"fullName\"\r\n                  valueExpr=\"id\">\r\n      </dxo-lookup>\r\n    </dxi-column>\r\n\r\n    <dxi-column [allowEditing]=\"false\" dataField=\"projectStatus\" width=\"8%\" alignment=\"left\" cellTemplate=\"statusTemplate\" caption=\"Status\">\r\n      <div *dxTemplate=\"let cell of 'statusTemplate'\">\r\n        <div class=\"success\">\r\n          <div class=\"counter\">{{ cell.text }}</div>\r\n        </div>\r\n      </div>\r\n    </dxi-column>\r\n    <dxo-filter-panel [visible]=\"true\"></dxo-filter-panel>\r\n    <dxo-filter-builder-popup [position]=\"popupPosition\">\r\n    </dxo-filter-builder-popup>\r\n    <dxo-paging [pageSize]=\"10\"></dxo-paging>\r\n    <dxo-pager [showPageSizeSelector]=\"true\"\r\n               [allowedPageSizes]=\"[5, 10, 20]\"\r\n               [visible]=\"true\"\r\n               [showInfo]=\"true\">\r\n    </dxo-pager>\r\n  </dx-data-grid>\r\n\r\n  <!-- popups-->\r\n  <div class=\"add-project\">\r\n    <div class=\"overlay\" *ngIf='openAddProject'></div>\r\n    <div class=\"modal-add-project\" [ngClass]=\"{'active':openAddProject}\">\r\n      <form name=\"form\" (ngSubmit)=\"addProject.form.valid && saveNewProject()\" #addProject=\"ngForm\" novalidate>\r\n        <div class=\"card-header\">\r\n          <span>\r\n            <strong>\r\n              <h5>Add new project</h5>\r\n            </strong>\r\n          </span>\r\n          <div class=\"close-project-button\" (click)=\"closeAddProjectModal()\">\r\n            <i class=\"fas fa-times\"></i>\r\n          </div>\r\n        </div>\r\n        <div class=\"modal-body\">\r\n\r\n\r\n          <div class=\"mt-10\">\r\n            <label>Project Name</label>\r\n            <input class=\"form-control\"\r\n                   type=\"text\"\r\n                   placeholder=\"Enter project name\"\r\n                   [(ngModel)]=\"newProject.name\"\r\n                   name=\"project\"\r\n                   #projectName=\"ngModel\"\r\n                   [ngClass]=\"{ 'is-invalid': addProject.submitted && projectName.invalid && formValidation}\"\r\n                   required />\r\n            <div *ngIf=\"addProject.submitted && projectName.invalid\" class=\"invalid-feedback\">\r\n              <div *ngIf=\"projectName.errors.required\">Project name is required</div>\r\n            </div>\r\n          </div>\r\n\r\n          <div class=\"mt-10\">\r\n            <label>Customers</label>\r\n            <select class=\"form-control\"\r\n                    [(ngModel)]=\"newProject.customerId\"\r\n                    name=\"cust\"\r\n                    #customer=\"ngModel\"\r\n                    [ngClass]=\"{ 'is-invalid': addProject.submitted && customer.invalid && formValidation}\"\r\n                    required>\r\n              <option *ngFor=\"let item of customers.customers\" [ngValue]=\"item.id\">{{item.name}}</option>\r\n            </select>\r\n            <div *ngIf=\"addProject.submitted && customer.invalid\" class=\"invalid-feedback\">\r\n              <div *ngIf=\"customer.errors.required\">Customer not selected</div>\r\n            </div>\r\n          </div>\r\n\r\n          <div class=\"mt-10\">\r\n            <label>Project Managers</label>\r\n            <select class=\"form-control\"\r\n                    [(ngModel)]=\"newProject.userId\"\r\n                    name=\"manager\"\r\n                    #manager=\"ngModel\"\r\n                    [ngClass]=\"{ 'is-invalid': addProject.submitted && manager.invalid && formValidation}\"\r\n                    required>\r\n              <option *ngFor=\"let item of projectManagers.users\" [ngValue]=\"item.id\">{{item.firstName}}</option>\r\n            </select>\r\n            <div *ngIf=\"addProject.submitted && manager.invalid\" class=\"invalid-feedback\">\r\n              <div *ngIf=\"manager.errors.required\">Project manager not selected</div>\r\n            </div>\r\n          </div>\r\n\r\n          <div class=\"mt-10\">\r\n            <label>Description</label>\r\n            <input class=\"form-control\"\r\n                   type=\"text\"\r\n                   name=\"description\"\r\n                   placeholder=\"Enter description\"\r\n                   [(ngModel)]=\"newProject.description\" />\r\n          </div>\r\n\r\n          <div class=\"mt-10\">\r\n            <label>Memo</label>\r\n            <textarea class=\"form-control\"\r\n                      type=\"text\"\r\n                      placeholder=\"Enter memo\"\r\n                      [(ngModel)]=\"newProject.memo\"\r\n                      name=\"memo\">\r\n                 </textarea>\r\n          </div>\r\n\r\n          <div class=\"button-wrapper\">\r\n            <button class=\"btn btn-block btn-outline-primary\" (click)=\"closeAddProjectModal()\">Cancel</button>\r\n            <button class=\"btn btn-block btn-outline-primary\" (click)=\"formValidation = true\">Save</button>\r\n          </div>\r\n        </div>\r\n      </form>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"add-project\">\r\n  <div class=\"overlay\" *ngIf='openReassignPM'></div>\r\n  <div class=\"modal-add-project\" [ngClass]=\"{'active':openReassignPM}\">\r\n    <form name=\"form\" (ngSubmit)=\"reassignPM.form.valid && saveReasignPM($event)\" #reassignPM=\"ngForm\" novalidate>\r\n      <div class=\"card-header\">\r\n        <span>\r\n          <strong>\r\n            <h5>Reassign project manager</h5>\r\n          </strong>\r\n        </span>\r\n        <div class=\"close-project-button\" (click)=\"closeReassignPMDialog()\">\r\n          <i class=\"fas fa-times\"></i>\r\n        </div>\r\n      </div>\r\n      <div class=\"modal-body\">\r\n        <div class=\"mt-10\">\r\n          <label>Project Managers</label>\r\n          <select class=\"form-control\"\r\n                  [(ngModel)]=\"reassignProjectManager.projectManagerId\"\r\n                  name=\"reasignedManager\"\r\n                  #reasignedManager=\"ngModel\"\r\n                  [ngClass]=\"{ 'is-invalid': reassignPM.submitted && reasignedManager.invalid && formValidation}\"\r\n                  required>\r\n            <option *ngFor=\"let item of projectManagers.users\" [ngValue]=\"item.id\">{{item.firstName}} {{item.lastName}}</option>\r\n          </select>\r\n          <div *ngIf=\"reassignPM.submitted && reasignedManager.invalid\" class=\"invalid-feedback\">\r\n            <div *ngIf=\"reasignedManager.errors.required\">Project manager not selected</div>\r\n          </div>\r\n        </div>\r\n        <div class=\"button-wrapper\">\r\n          <button class=\"btn btn-block btn-outline-primary\" (click)=\"closeReassignPMDialog()\">Cancel</button>\r\n          <button class=\"btn btn-block btn-outline-primary\" (click)=\"formValidation = true\">Save</button>\r\n        </div>\r\n      </div>\r\n    </form>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/projects/projects-list/projects-list.component.scss":
/*!************************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/projects/projects-list/projects-list.component.scss ***!
  \************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".custom-table[_ngcontent-c4] tbody[_ngcontent-c4] tr[_ngcontent-c4] {\n  border-bottom: 20px solid white !important;\n  background: #fff !important; }\n\n.project-title {\n  padding-left: 0px;\n  padding-right: 0px; }\n\n#newProject[_ngcontent-c4] {\n  width: 147px;\n  height: 40px;\n  font-size: 14px;\n  color: black; }\n\n.project-name {\n  font-size: 24px;\n  font-weight: bold;\n  margin-bottom: 16px;\n  margin-top: 25px; }\n\n.custom-file-upload {\n  border: 1px solid #ccc;\n  display: inline-block;\n  color: #fff;\n  background-color: #20a8d8;\n  border-color: #20a8d8;\n  padding: 6px 12px;\n  cursor: pointer; }\n\nlabel {\n  margin-bottom: 0px; }\n\ninput[type=\"file\"] {\n  display: none; }\n\np {\n  margin-top: 1rem !important;\n  margin-bottom: 1rem; }\n\n.custom-table {\n  border-spacing: 0px 15px;\n  border-collapse: separate !important; }\n\n.custom-table thead tr th {\n    border-bottom: none !important;\n    border-top: none !important; }\n\n.custom-table tbody tr {\n    border-bottom: 20px solid white !important;\n    background: #f2f4f8 !important; }\n\n.custom-table tbody tr:after {\n      content: \"\";\n      border-bottom: 1px solid black;\n      position: absolute;\n      bottom: 0; }\n\n.custom-table tbody tr .info-task .success {\n      background: #4caf50;\n      height: 48px;\n      width: 48px;\n      position: relative;\n      border-radius: 50%; }\n\n.custom-table tbody tr .info-task .success .counter {\n        position: absolute;\n        height: 17px;\n        width: 17px;\n        border: 1px solid #4caf50;\n        border-radius: 50%;\n        top: 0;\n        right: -5px;\n        background: white;\n        display: flex;\n        justify-content: center;\n        align-items: center;\n        font-size: 10px;\n        font-weight: bold; }\n\n.custom-table tbody tr .info-task .success:before {\n        content: \"\";\n        position: absolute;\n        left: 20px;\n        top: 12px;\n        width: 10px;\n        height: 18px;\n        border: solid white;\n        border-width: 0 3px 3px 0;\n        -webkit-transform: rotate(45deg);\n        transform: rotate(45deg); }\n\n.custom-table tbody tr .info-task .error {\n      background: #cd3232;\n      height: 48px;\n      width: 48px;\n      position: relative;\n      border-radius: 50%;\n      text-align: center;\n      vertical-align: middle;\n      display: table-cell;\n      color: white; }\n\n.custom-table tbody tr .info-task .error .counter-error {\n        position: absolute;\n        height: 15px;\n        width: 15px;\n        border: 1px solid #cd3232;\n        border-radius: 50%;\n        top: 0;\n        right: 0;\n        background: white;\n        display: flex;\n        justify-content: center;\n        align-items: center;\n        font-size: 10px;\n        font-weight: bold;\n        color: black; }\n\n.custom-table tbody tr td {\n      border-top: 1px solid #d0d0d0;\n      border-bottom: 1px solid #d0d0d0;\n      vertical-align: middle; }\n\n.custom-table tbody tr td:first-child {\n        border-left: 1px solid #d0d0d0; }\n\n.custom-table tbody tr td:last-child {\n        border-right: 1px solid #d0d0d0; }\n\n.custom-table tbody tr:hover {\n      background: #d5d5d5 !important; }\n\n.custom-table tfoot tr td {\n    border-top: none !important; }\n\n.selected-game {\n  display: inline-block;\n  margin-right: 15px;\n  font-size: 15px;\n  background: #b9cfea;\n  margin-bottom: 10px;\n  padding: 2px 11px 4px 11px;\n  border-radius: 8px; }\n\n.delete-game {\n  font-size: 10px;\n  padding: 3px 6px;\n  vertical-align: middle;\n  cursor: pointer; }\n\n#newProject {\n  width: 180px;\n  height: 45px;\n  font-size: large; }\n\n.add-project .overlay {\n  position: fixed;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  background: rgba(0, 0, 0, 0.4);\n  z-index: 10000; }\n\n.add-project .modal-add-project {\n  position: fixed;\n  background: #fff;\n  z-index: 10000;\n  top: -100%;\n  left: calc(50% - 200px);\n  width: 400px; }\n\n.add-project .modal-add-project.active {\n    top: 200px;\n    left: calc(50% - 200px);\n    transition: top 0.5s; }\n\n.add-project .modal-add-project .row {\n    width: 100%; }\n\n.add-project .modal-add-project .row .mt-10 {\n      margin-top: 15px; }\n\n.add-project .modal-add-project .button-wrapper {\n    display: flex;\n    justify-content: flex-end;\n    margin-top: 10px; }\n\n.add-project .modal-add-project .button-wrapper .btn {\n      width: 100px;\n      margin: 0;\n      margin-left: 10px; }\n\n.add-project .modal-show-pdf {\n  position: fixed;\n  background: #fff;\n  z-index: 10000;\n  top: -100%;\n  left: calc(50% - 400px);\n  width: 800px; }\n\n.add-project .modal-show-pdf .modal-body {\n    padding-right: 0; }\n\n.add-project .modal-show-pdf .modal-body .pdf-container {\n      max-height: 400px;\n      overflow-y: auto; }\n\n.add-project .modal-show-pdf .modal-body .text-container {\n      max-height: 400px;\n      overflow-y: auto; }\n\n.add-project .modal-show-pdf.active {\n    top: 200px;\n    left: calc(50% - 400px);\n    transition: top 0.5s; }\n\n.add-project .modal-show-pdf .row {\n    width: 100%; }\n\n.add-project .modal-show-pdf .row .mt-10 {\n      margin-top: 15px; }\n\n.add-project .modal-show-pdf .button-wrapper {\n    display: flex;\n    justify-content: flex-end;\n    margin-top: 10px; }\n\n.add-project .modal-show-pdf .button-wrapper .btn {\n      width: 100px;\n      margin: 0;\n      margin-left: 10px; }\n\n.save-attachment .overlay {\n  position: fixed;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  background: rgba(0, 0, 0, 0.4);\n  z-index: 10000; }\n\n.save-attachment .modal-save-attachment {\n  position: fixed;\n  background: #fff;\n  z-index: 10000;\n  top: -100%;\n  left: calc(50% - 400px);\n  width: 900px; }\n\n.save-attachment .modal-save-attachment.active {\n    top: 200px;\n    left: calc(50% - 400px);\n    transition: top 0.5s; }\n\n.save-attachment .modal-save-attachment .row {\n    width: 100%; }\n\n.save-attachment .modal-save-attachment .row .mt-10 {\n      margin-top: 15px; }\n\n.save-attachment .modal-save-attachment .button-wrapper {\n    display: flex;\n    justify-content: flex-end;\n    margin-top: 10px; }\n\n.save-attachment .modal-save-attachment .button-wrapper .btn {\n      width: 100px;\n      margin: 0;\n      margin-left: 10px; }\n\n.project-list-button {\n  border: none;\n  background: none;\n  color: #20a8d8;\n  padding: 0px;\n  margin-left: 16px;\n  margin-bottom: 20px; }\n\n.close-project-button {\n  margin-left: auto; }\n\n.button-wrapper {\n  vertical-align: bottom;\n  margin-top: auto; }\n\n.card-body.project-details.project {\n  border: 1px solid #e1e6ef; }\n\n.card-body {\n  padding: 16px !important; }\n\n.card-header {\n  padding: 0.75rem 1.25rem;\n  margin-bottom: 0;\n  background-color: #f9f9fa;\n  display: flex;\n  border-bottom: 0px;\n  border: 1px solid #e1e6ef; }\n\n.project-details-input-wrapper.project {\n  display: flex; }\n\n.row {\n  display: flex;\n  flex-wrap: wrap;\n  margin-left: 0 !important; }\n\n.project-name-description-wrapper {\n  display: flex; }\n\n.card {\n  border: 0px !important; }\n\n.edit-project-button {\n  margin-left: auto; }\n\n.attachment-table {\n  text-align: center; }\n\n.attachment-table thead th {\n    border-top: none !important;\n    border-bottom: none !important; }\n\n.edit-button-wrapper {\n  display: flex;\n  justify-content: flex-end;\n  margin-top: 20px; }\n\n.edit-button-wrapper .btn {\n    width: 100px;\n    margin: 0;\n    margin-left: 10px; }\n\n.success {\n  background: #32cd32;\n  height: 37px;\n  width: 37px;\n  position: relative;\n  border-radius: 50%; }\n\n.success .counter {\n    position: absolute;\n    height: 15px;\n    width: 15px;\n    border: 1px solid #32cd32;\n    border-radius: 50%;\n    top: 0;\n    right: 0;\n    background: white;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    font-size: 10px;\n    font-weight: bold; }\n\n.success:before {\n    content: \"\";\n    position: absolute;\n    left: 13px;\n    top: 7px;\n    width: 10px;\n    height: 18px;\n    border: solid white;\n    border-width: 0 3px 3px 0;\n    -webkit-transform: rotate(45deg);\n    transform: rotate(45deg); }\n\nh1 {\n  font-size: 24px; }\n"

/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/projects/projects-list/projects-list.component.ts":
/*!**********************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/projects/projects-list/projects-list.component.ts ***!
  \**********************************************************************************************************/
/*! exports provided: ProjectsListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProjectsListComponent", function() { return ProjectsListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var app_shared_constans_projects_and_orders_constans__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/shared/constans/projects-and-orders.constans */ "./src/app/shared/constans/projects-and-orders.constans.ts");
/* harmony import */ var devextreme_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! devextreme-angular */ "./node_modules/devextreme-angular/index.js");
/* harmony import */ var devextreme_angular__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(devextreme_angular__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var app_shared_providers_services_project_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/shared/providers/services/project.service */ "./src/app/shared/providers/services/project.service.ts");
/* harmony import */ var app_shared_providers_services_customer_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! app/shared/providers/services/customer.service */ "./src/app/shared/providers/services/customer.service.ts");
/* harmony import */ var app_shared_providers_services_noitification_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! app/shared/providers/services/noitification.service */ "./src/app/shared/providers/services/noitification.service.ts");
/* harmony import */ var app_shared_providers_services_load_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! app/shared/providers/services/load.service */ "./src/app/shared/providers/services/load.service.ts");
/* harmony import */ var app_shared_models_users_get_all_users_view__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! app/shared/models/users/get-all-users.view */ "./src/app/shared/models/users/get-all-users.view.ts");
/* harmony import */ var app_shared_models_project_create_project_view__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! app/shared/models/project/create-project.view */ "./src/app/shared/models/project/create-project.view.ts");
/* harmony import */ var app_shared_models_project_reassign_project_manager_view__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! app/shared/models/project/reassign-project-manager.view */ "./src/app/shared/models/project/reassign-project-manager.view.ts");
/* harmony import */ var app_shared_models_project_get_all_active_project_view__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! app/shared/models/project/get-all-active-project.view */ "./src/app/shared/models/project/get-all-active-project.view.ts");
/* harmony import */ var app_shared_models_customer_get_all_customer_view__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! app/shared/models/customer/get-all-customer.view */ "./src/app/shared/models/customer/get-all-customer.view.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













var ProjectsListComponent = /** @class */ (function () {
    function ProjectsListComponent(notificationService, projectService, customerService, route, loadService, constans) {
        this.notificationService = notificationService;
        this.projectService = projectService;
        this.customerService = customerService;
        this.route = route;
        this.loadService = loadService;
        this.constans = constans;
        //post request
        this.newProject = new app_shared_models_project_create_project_view__WEBPACK_IMPORTED_MODULE_9__["CreateProjectView"]();
        this.reassignProjectManager = new app_shared_models_project_reassign_project_manager_view__WEBPACK_IMPORTED_MODULE_10__["ReassignProjectManagerView"]();
        //get request
        this.projectManagers = new app_shared_models_users_get_all_users_view__WEBPACK_IMPORTED_MODULE_8__["GetAllUserView"]();
        this.activeProjects = new app_shared_models_project_get_all_active_project_view__WEBPACK_IMPORTED_MODULE_11__["GetAllActiveProjectView"]();
        this.customers = new app_shared_models_customer_get_all_customer_view__WEBPACK_IMPORTED_MODULE_12__["GetAllCustomerView"]();
        //popups
        this.openAddProject = false;
        this.formValidation = false;
        this.openReassignPM = false;
        //loader
        this.showProgresBar = true;
        //slider 
        this.isChecked = false;
        this.showFilterRow = true;
        this.showHeaderFilter = true;
        this.currentFilter = "auto";
        this.popupPosition = { of: window, at: "top", my: "top", offset: { y: 10 } };
    }
    ProjectsListComponent.prototype.ngOnInit = function () {
        this.loadService.set(true);
        this.getProjectManager();
        this.getCustomers();
        this.getActiveProject();
    };
    //get with Api
    ProjectsListComponent.prototype.getCustomers = function () {
        var _this = this;
        this.customerService.getCustomers().subscribe(function (response) {
            _this.customers = response;
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    ProjectsListComponent.prototype.getProjectManager = function () {
        var _this = this;
        this.projectService.getProjectManager().subscribe(function (response) {
            _this.projectManagers = response;
            _this.projectManagers.users.forEach(function (projectManager) {
                projectManager.fullName = projectManager.firstName + " " + projectManager.lastName;
            });
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    ProjectsListComponent.prototype.getActiveProject = function () {
        var _this = this;
        this.projectService.getAllActivePorjects().subscribe(function (respone) {
            _this.activeProjects = respone;
            _this.updateDate = null;
            _this.projectsCount = _this.activeProjects.projects.length;
            _this.dataGrid.dataSource = _this.activeProjects.projects;
            _this.loadService.set(false);
        }, function (error) {
            _this.errors = error;
            _this.loadService.set(false);
        });
    };
    //popups
    ProjectsListComponent.prototype.openReassignPMDialog = function (projectId) {
        this.formValidation = false;
        this.reassignProjectManager = new app_shared_models_project_reassign_project_manager_view__WEBPACK_IMPORTED_MODULE_10__["ReassignProjectManagerView"]();
        this.reassignProjectManager.projectId = projectId;
        this.openReassignPM = true;
    };
    ProjectsListComponent.prototype.closeReassignPMDialog = function () {
        this.reassignProjectManager = new app_shared_models_project_reassign_project_manager_view__WEBPACK_IMPORTED_MODULE_10__["ReassignProjectManagerView"]();
        this.openReassignPM = false;
    };
    ProjectsListComponent.prototype.saveReasignPM = function (event) {
        var _this = this;
        this.reassignProjectManager = new app_shared_models_project_reassign_project_manager_view__WEBPACK_IMPORTED_MODULE_10__["ReassignProjectManagerView"]();
        this.reassignProjectManager.projectId = event.key.id;
        this.reassignProjectManager.projectManagerId = event.newData.user.id;
        this.openReassignPM = false;
        this.projectService.reassignProjectManeger(this.reassignProjectManager).subscribe(function (response) {
            _this.getActiveProject();
            _this.notificationService.showSuccess(_this.constans.projectManagerUpdateMessage);
        });
    };
    ProjectsListComponent.prototype.openAddProjectModal = function () {
        this.openAddProject = true;
        this.formValidation = false;
        this.newProject = new app_shared_models_project_create_project_view__WEBPACK_IMPORTED_MODULE_9__["CreateProjectView"]();
    };
    ProjectsListComponent.prototype.closeAddProjectModal = function () {
        this.openAddProject = false;
        this.formValidation = false;
        this.newProject = new app_shared_models_project_create_project_view__WEBPACK_IMPORTED_MODULE_9__["CreateProjectView"]();
    };
    ProjectsListComponent.prototype.saveNewProject = function () {
        this.route.navigate(['projects-and-orders/projects/createproject']);
    };
    //back on page
    ProjectsListComponent.prototype.showingProjectsList = function () {
        this.getActiveProject();
    };
    ProjectsListComponent.prototype.showProjectDetailsById = function (event) {
        if (this.openReassignPM != true) {
            this.route.navigate(['projects-and-orders/projects/projectdetails', event.data.id]);
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(devextreme_angular__WEBPACK_IMPORTED_MODULE_3__["DxDataGridComponent"]),
        __metadata("design:type", devextreme_angular__WEBPACK_IMPORTED_MODULE_3__["DxDataGridComponent"])
    ], ProjectsListComponent.prototype, "dataGrid", void 0);
    ProjectsListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-projects-list',
            template: __webpack_require__(/*! ./projects-list.component.html */ "./src/app/projects-and-orders/project-and-orders/projects/projects-list/projects-list.component.html"),
            styles: [__webpack_require__(/*! ./projects-list.component.scss */ "./src/app/projects-and-orders/project-and-orders/projects/projects-list/projects-list.component.scss")]
        }),
        __metadata("design:paramtypes", [app_shared_providers_services_noitification_service__WEBPACK_IMPORTED_MODULE_6__["NotificationService"],
            app_shared_providers_services_project_service__WEBPACK_IMPORTED_MODULE_4__["ProjectService"],
            app_shared_providers_services_customer_service__WEBPACK_IMPORTED_MODULE_5__["CustomerService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            app_shared_providers_services_load_service__WEBPACK_IMPORTED_MODULE_7__["LoadService"],
            app_shared_constans_projects_and_orders_constans__WEBPACK_IMPORTED_MODULE_2__["ProjectsAndOrdersConstans"]])
    ], ProjectsListComponent);
    return ProjectsListComponent;
}());



/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/projects/projects-module-routing.module.ts":
/*!***************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/projects/projects-module-routing.module.ts ***!
  \***************************************************************************************************/
/*! exports provided: ProjectsModuleRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProjectsModuleRoutingModule", function() { return ProjectsModuleRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _projects_list_projects_list_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./projects-list/projects-list.component */ "./src/app/projects-and-orders/project-and-orders/projects/projects-list/projects-list.component.ts");
/* harmony import */ var _project_details_project_details_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./project-details/project-details.component */ "./src/app/projects-and-orders/project-and-orders/projects/project-details/project-details.component.ts");
/* harmony import */ var _create_project_create_project_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./create-project/create-project.component */ "./src/app/projects-and-orders/project-and-orders/projects/create-project/create-project.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var routes = [
    {
        path: '',
        redirectTo: 'projectslist',
    },
    {
        path: 'projectslist',
        component: _projects_list_projects_list_component__WEBPACK_IMPORTED_MODULE_2__["ProjectsListComponent"]
    },
    {
        path: 'projectdetails/:id',
        component: _project_details_project_details_component__WEBPACK_IMPORTED_MODULE_3__["ProjectDetailsComponent"]
    },
    {
        path: 'createproject',
        component: _create_project_create_project_component__WEBPACK_IMPORTED_MODULE_4__["CreateProjectComponent"]
    }
];
var ProjectsModuleRoutingModule = /** @class */ (function () {
    function ProjectsModuleRoutingModule() {
    }
    ProjectsModuleRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], ProjectsModuleRoutingModule);
    return ProjectsModuleRoutingModule;
}());



/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/projects/projects-module.module.ts":
/*!*******************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/projects/projects-module.module.ts ***!
  \*******************************************************************************************/
/*! exports provided: ProjectsModuleModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProjectsModuleModule", function() { return ProjectsModuleModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _projects_module_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./projects-module-routing.module */ "./src/app/projects-and-orders/project-and-orders/projects/projects-module-routing.module.ts");
/* harmony import */ var _projects_list_projects_list_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./projects-list/projects-list.component */ "./src/app/projects-and-orders/project-and-orders/projects/projects-list/projects-list.component.ts");
/* harmony import */ var _project_details_project_details_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./project-details/project-details.component */ "./src/app/projects-and-orders/project-and-orders/projects/project-details/project-details.component.ts");
/* harmony import */ var _create_project_create_project_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./create-project/create-project.component */ "./src/app/projects-and-orders/project-and-orders/projects/create-project/create-project.component.ts");
/* harmony import */ var app_shared_filters_project_data_filter_pipe__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! app/shared/filters/project-data-filter.pipe */ "./src/app/shared/filters/project-data-filter.pipe.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var angular_6_datatable__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! angular-6-datatable */ "./node_modules/angular-6-datatable/index.js");
/* harmony import */ var angular_6_datatable__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(angular_6_datatable__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-bootstrap/datepicker */ "./node_modules/ngx-bootstrap/datepicker/index.js");
/* harmony import */ var ng_busy__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ng-busy */ "./node_modules/ng-busy/fesm5/ng-busy.js");
/* harmony import */ var ngx_uploader__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ngx-uploader */ "./node_modules/ngx-uploader/fesm5/ngx-uploader.js");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ng2-file-upload */ "./node_modules/ng2-file-upload/index.js");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(ng2_file_upload__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var ng_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ng-multiselect-dropdown */ "./node_modules/ng-multiselect-dropdown/fesm5/ng-multiselect-dropdown.js");
/* harmony import */ var _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/slide-toggle */ "./node_modules/@angular/material/esm5/slide-toggle.es5.js");
/* harmony import */ var ng2_pdf_viewer__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ng2-pdf-viewer */ "./node_modules/ng2-pdf-viewer/ng2-pdf-viewer.es5.js");
/* harmony import */ var _progress_kendo_angular_grid__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @progress/kendo-angular-grid */ "./node_modules/@progress/kendo-angular-grid/dist/es/index.js");
/* harmony import */ var app_shared_modules_material_module__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! app/shared/modules/material.module */ "./src/app/shared/modules/material.module.ts");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/esm5/table.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var devextreme_angular__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! devextreme-angular */ "./node_modules/devextreme-angular/index.js");
/* harmony import */ var devextreme_angular__WEBPACK_IMPORTED_MODULE_20___default = /*#__PURE__*/__webpack_require__.n(devextreme_angular__WEBPACK_IMPORTED_MODULE_20__);
/* harmony import */ var _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @angular/cdk/drag-drop */ "./node_modules/@angular/cdk/esm5/drag-drop.es5.js");
/* harmony import */ var _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/material/grid-list */ "./node_modules/@angular/material/esm5/grid-list.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};























var ProjectsModuleModule = /** @class */ (function () {
    function ProjectsModuleModule() {
    }
    ProjectsModuleModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _projects_module_routing_module__WEBPACK_IMPORTED_MODULE_2__["ProjectsModuleRoutingModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                devextreme_angular__WEBPACK_IMPORTED_MODULE_20__["DxDataGridModule"],
                devextreme_angular__WEBPACK_IMPORTED_MODULE_20__["DxSelectBoxModule"],
                devextreme_angular__WEBPACK_IMPORTED_MODULE_20__["DxCheckBoxModule"],
                app_shared_modules_material_module__WEBPACK_IMPORTED_MODULE_17__["MaterialModule"],
                angular_6_datatable__WEBPACK_IMPORTED_MODULE_8__["DataTableModule"],
                ng2_file_upload__WEBPACK_IMPORTED_MODULE_12__["FileUploadModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"],
                _progress_kendo_angular_grid__WEBPACK_IMPORTED_MODULE_16__["GridModule"],
                _angular_material_table__WEBPACK_IMPORTED_MODULE_18__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_19__["MatPaginatorModule"],
                _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_14__["MatSlideToggleModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ReactiveFormsModule"],
                ng_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_13__["NgMultiSelectDropDownModule"].forRoot(),
                ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_9__["BsDatepickerModule"].forRoot(),
                ng_busy__WEBPACK_IMPORTED_MODULE_10__["NgBusyModule"],
                ngx_uploader__WEBPACK_IMPORTED_MODULE_11__["NgxUploaderModule"],
                ng2_pdf_viewer__WEBPACK_IMPORTED_MODULE_15__["PdfViewerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_19__["MatSortModule"],
                _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_21__["DragDropModule"],
                _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_22__["MatGridListModule"]
            ],
            declarations: [
                _projects_list_projects_list_component__WEBPACK_IMPORTED_MODULE_3__["ProjectsListComponent"],
                _project_details_project_details_component__WEBPACK_IMPORTED_MODULE_4__["ProjectDetailsComponent"],
                _create_project_create_project_component__WEBPACK_IMPORTED_MODULE_5__["CreateProjectComponent"],
                app_shared_filters_project_data_filter_pipe__WEBPACK_IMPORTED_MODULE_6__["ProjectDataFilter"]
            ]
        })
    ], ProjectsModuleModule);
    return ProjectsModuleModule;
}());



/***/ }),

/***/ "./src/app/shared/filters/project-data-filter.pipe.ts":
/*!************************************************************!*\
  !*** ./src/app/shared/filters/project-data-filter.pipe.ts ***!
  \************************************************************/
/*! exports provided: ProjectDataFilter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProjectDataFilter", function() { return ProjectDataFilter; });
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var ProjectDataFilter = /** @class */ (function () {
    function ProjectDataFilter() {
    }
    ProjectDataFilter.prototype.transform = function (array, query) {
        if (query) {
            return lodash__WEBPACK_IMPORTED_MODULE_0__["filter"](array, function (row) { return row.name.toLowerCase().indexOf(query.toLowerCase()) > -1; });
        }
        return array;
    };
    ProjectDataFilter = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'projectDataFilter'
        })
    ], ProjectDataFilter);
    return ProjectDataFilter;
}());



/***/ }),

/***/ "./src/app/shared/models/project/get-project.view.ts":
/*!***********************************************************!*\
  !*** ./src/app/shared/models/project/get-project.view.ts ***!
  \***********************************************************/
/*! exports provided: GetProjectView, GetProjectProjectTypeViewItem, GetProjectUserViewItem, GetProjectCustomerViewItem */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetProjectView", function() { return GetProjectView; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetProjectProjectTypeViewItem", function() { return GetProjectProjectTypeViewItem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetProjectUserViewItem", function() { return GetProjectUserViewItem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetProjectCustomerViewItem", function() { return GetProjectCustomerViewItem; });
var GetProjectView = /** @class */ (function () {
    function GetProjectView() {
    }
    return GetProjectView;
}());

var GetProjectProjectTypeViewItem = /** @class */ (function () {
    function GetProjectProjectTypeViewItem() {
    }
    return GetProjectProjectTypeViewItem;
}());

var GetProjectUserViewItem = /** @class */ (function () {
    function GetProjectUserViewItem() {
    }
    return GetProjectUserViewItem;
}());

var GetProjectCustomerViewItem = /** @class */ (function () {
    function GetProjectCustomerViewItem() {
    }
    return GetProjectCustomerViewItem;
}());



/***/ })

}]);
//# sourceMappingURL=project-and-orders-projects-projects-module-module.js.map