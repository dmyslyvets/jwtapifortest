(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["projects-and-orders-projects-and-orders-module"],{

/***/ "./src/app/projects-and-orders/project-and-orders/projects-and-orders.component.html":
/*!*******************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/projects-and-orders.component.html ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<div>\r\n  <mat-tab-group (selectedTabChange)=\"navigate($event)\" [selectedIndex]=\"selectedIndex\">\r\n    <mat-tab label=\"Projects\">\r\n        <ng-template  mat-tab-label>\r\n        <img style=\"margin-right: 10px;\" *ngIf =\"selectedIndex != 0\" width=\"22px\" height=\"22px\" src=\"assets/img/project-active-2.svg \"/>\r\n        <img style=\"margin-right: 10px;\" *ngIf =\"selectedIndex == 0\" width=\"22px\" height=\"22px\" src=\"assets/img/project-active.svg \"/>\r\n        <span>Projects</span>\r\n        </ng-template>\r\n    </mat-tab>\r\n    <mat-tab label=\"Orders\">\r\n        <ng-template  mat-tab-label>\r\n            <img style=\"margin-right: 10px;\" *ngIf =\"selectedIndex != 1\" width=\"16.5px\" height=\"20px\" src=\"assets/img/order.svg \"/>\r\n            <img style = \"margin-right: 10px;\" *ngIf =\"selectedIndex == 1\" width=\"16.5px\" height=\"20px\" src=\"assets/img/order-1.svg \"/>\r\n            <span>Orders</span>\r\n      </ng-template>\r\n    </mat-tab>\r\n    <mat-tab label=\"Tasks\">\r\n        <ng-template  mat-tab-label>\r\n        <img style=\"margin-right: 10px;\" *ngIf =\"selectedIndex != 2\" width=\"16.5px\" height=\"20px\" src=\"assets/img/task-active.svg \"/>\r\n        <img style = \"margin-right: 10px;\" *ngIf =\"selectedIndex == 2\" width=\"16.5px\" height=\"20px\" src=\"assets/img/task-active-2.svg \"/>\r\n        <span>Tasks</span>\r\n        </ng-template>\r\n    </mat-tab>\r\n    <!-- <mat-tab label=\"New Projects\">\r\n        <ng-template  mat-tab-label>\r\n            <img style=\"margin-right: 10px;\" *ngIf =\"selectedIndex != 3\" width=\"22px\" height=\"22px\" src=\"assets/img/project-active-2.svg \"/>\r\n            <img style=\"margin-right: 10px;\" *ngIf =\"selectedIndex == 3\" width=\"22px\" height=\"22px\" src=\"assets/img/project-active.svg \"/>\r\n            <span>New Projects</span>\r\n        </ng-template>\r\n    </mat-tab> -->\r\n  </mat-tab-group>\r\n  <router-outlet></router-outlet>\r\n</div>\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/projects-and-orders.component.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/projects-and-orders.component.ts ***!
  \*****************************************************************************************/
/*! exports provided: ProjectsAndOrdersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProjectsAndOrdersComponent", function() { return ProjectsAndOrdersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var app_shared_constans_projects_and_orders_constans__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/shared/constans/projects-and-orders.constans */ "./src/app/shared/constans/projects-and-orders.constans.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ProjectsAndOrdersComponent = /** @class */ (function () {
    function ProjectsAndOrdersComponent(router, constans) {
        this.router = router;
        this.constans = constans;
    }
    ProjectsAndOrdersComponent.prototype.ngOnInit = function () {
        this.selectedTab();
    };
    ProjectsAndOrdersComponent.prototype.selectedTab = function () {
        var currentRouteLabel = this.router.url.substring(this.router.url.lastIndexOf('/'));
        if (currentRouteLabel == this.constans.tabsData.projects.routeLabel) {
            this.selectedIndex = this.constans.tabsData.projects.index;
        }
        if (currentRouteLabel == this.constans.tabsData.ordersKanban.routeLabel) {
            this.selectedIndex = this.constans.tabsData.ordersKanban.index;
        }
        if (currentRouteLabel == this.constans.tabsData.ordersList.routeLabel) {
            this.selectedIndex = this.constans.tabsData.ordersList.index;
        }
        if (currentRouteLabel == this.constans.tabsData.tasksKanban.routeLabel) {
            this.selectedIndex = this.constans.tabsData.tasksKanban.index;
        }
        if (currentRouteLabel == this.constans.tabsData.tasksList.routeLabel) {
            this.selectedIndex = this.constans.tabsData.tasksList.index;
        }
        // if (currentRouteLabel == this.constans.tabsData.newProjects.routeLabel) {
        //   this.selectedIndex = this.constans.tabsData.newProjects.index;
        // }
    };
    ProjectsAndOrdersComponent.prototype.navigate = function (event) {
        if (event.tab.textLabel == this.constans.tabsData.projects.label) {
            this.selectedIndex = this.constans.tabsData.projects.index;
            this.router.navigate(['projects-and-orders', 'newProjects']);
        }
        if (event.tab.textLabel == this.constans.tabsData.ordersKanban.label) {
            this.selectedIndex = this.constans.tabsData.ordersKanban.index;
            this.router.navigate(['projects-and-orders', 'orders']);
        }
        if (event.tab.textLabel == this.constans.tabsData.ordersList.label) {
            this.selectedIndex = this.constans.tabsData.ordersList.index;
            this.router.navigate(['projects-and-orders', 'orders']);
        }
        if (event.tab.textLabel == this.constans.tabsData.tasksKanban.label) {
            this.selectedIndex = this.constans.tabsData.tasksKanban.index;
            this.router.navigate(['projects-and-orders', 'tasks']);
        }
        if (event.tab.textLabel == this.constans.tabsData.tasksList.label) {
            this.selectedIndex = this.constans.tabsData.tasksList.index;
            this.router.navigate(['projects-and-orders', 'tasks']);
        }
        // if (event.tab.textLabel == this.constans.tabsData.newProjects.label) {
        //   this.selectedIndex = this.constans.tabsData.newProjects.index;
        //   this.router.navigate(['projects-and-orders', 'newProjects']);
        // }
    };
    ProjectsAndOrdersComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'projects-and-orders',
            template: __webpack_require__(/*! ./projects-and-orders.component.html */ "./src/app/projects-and-orders/project-and-orders/projects-and-orders.component.html"),
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], app_shared_constans_projects_and_orders_constans__WEBPACK_IMPORTED_MODULE_2__["ProjectsAndOrdersConstans"]])
    ], ProjectsAndOrdersComponent);
    return ProjectsAndOrdersComponent;
}());



/***/ }),

/***/ "./src/app/projects-and-orders/projects-and-orders-routing.module.ts":
/*!***************************************************************************!*\
  !*** ./src/app/projects-and-orders/projects-and-orders-routing.module.ts ***!
  \***************************************************************************/
/*! exports provided: routes, ProjectsAndOrdersRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routes", function() { return routes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProjectsAndOrdersRoutingModule", function() { return ProjectsAndOrdersRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _project_and_orders_projects_and_orders_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./project-and-orders/projects-and-orders.component */ "./src/app/projects-and-orders/project-and-orders/projects-and-orders.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: _project_and_orders_projects_and_orders_component__WEBPACK_IMPORTED_MODULE_2__["ProjectsAndOrdersComponent"],
        children: [
            {
                path: '',
                redirectTo: 'newProjects'
            },
            {
                path: 'projects',
                data: { title: 'Projects', hidden: true },
                loadChildren: './project-and-orders/projects/projects-module.module#ProjectsModuleModule'
            },
            {
                path: 'orders',
                data: { title: 'Orders', hidden: true },
                loadChildren: './project-and-orders/orders/orders-module.module#OrdersModuleModule'
            },
            {
                path: 'tasks',
                data: { title: 'Tasks', hidden: true },
                loadChildren: './project-and-orders/tasks/tasks-module.module#TasksModuleModule'
            },
            {
                path: 'newProjects',
                data: { title: 'Projects', hidden: true },
                loadChildren: './project-and-orders/new-projects/new-projects.module#NewProjectsModule'
            }
        ]
    }
];
var ProjectsAndOrdersRoutingModule = /** @class */ (function () {
    function ProjectsAndOrdersRoutingModule() {
    }
    ProjectsAndOrdersRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], ProjectsAndOrdersRoutingModule);
    return ProjectsAndOrdersRoutingModule;
}());



/***/ }),

/***/ "./src/app/projects-and-orders/projects-and-orders.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/projects-and-orders/projects-and-orders.module.ts ***!
  \*******************************************************************/
/*! exports provided: ProjectsAndOrdersModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProjectsAndOrdersModule", function() { return ProjectsAndOrdersModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var app_shared_modules_material_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/shared/modules/material.module */ "./src/app/shared/modules/material.module.ts");
/* harmony import */ var _projects_and_orders_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./projects-and-orders-routing.module */ "./src/app/projects-and-orders/projects-and-orders-routing.module.ts");
/* harmony import */ var _project_and_orders_projects_and_orders_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./project-and-orders/projects-and-orders.component */ "./src/app/projects-and-orders/project-and-orders/projects-and-orders.component.ts");
/* harmony import */ var _shared_constans_projects_and_orders_constans__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../shared/constans/projects-and-orders.constans */ "./src/app/shared/constans/projects-and-orders.constans.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var ProjectsAndOrdersModule = /** @class */ (function () {
    function ProjectsAndOrdersModule() {
    }
    ProjectsAndOrdersModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                app_shared_modules_material_module__WEBPACK_IMPORTED_MODULE_2__["MaterialModule"],
                _projects_and_orders_routing_module__WEBPACK_IMPORTED_MODULE_3__["ProjectsAndOrdersRoutingModule"],
            ],
            declarations: [_project_and_orders_projects_and_orders_component__WEBPACK_IMPORTED_MODULE_4__["ProjectsAndOrdersComponent"]],
            providers: [_shared_constans_projects_and_orders_constans__WEBPACK_IMPORTED_MODULE_5__["ProjectsAndOrdersConstans"]]
        })
    ], ProjectsAndOrdersModule);
    return ProjectsAndOrdersModule;
}());



/***/ }),

/***/ "./src/app/shared/constans/projects-and-orders.constans.ts":
/*!*****************************************************************!*\
  !*** ./src/app/shared/constans/projects-and-orders.constans.ts ***!
  \*****************************************************************/
/*! exports provided: ProjectsAndOrdersConstans */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProjectsAndOrdersConstans", function() { return ProjectsAndOrdersConstans; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var ProjectsAndOrdersConstans = /** @class */ (function () {
    function ProjectsAndOrdersConstans() {
        this.cantCreateProjectWitoutDate = "Can't create project without date";
        this.addProjectMessage = "Project is added";
        this.projectUpdateMessage = "Project is update";
        this.projectManagerUpdateMessage = "Project Manager is updated";
        this.projectCreateMessage = "Project create success";
        this.saveProjectErrorMessage = "Can't save project without name";
        this.cantCreateProjectWithoutCustomerMessage = "Can't create project without customer";
        this.cantCreateProjectWithoutNameMessage = "Can't create project without name";
        this.cantCreateProjectWithoutPMMessage = "Can't create project without project manager";
        this.updateConfirmedOrderVenorQuoteMessage = "Confirmed order vendor quote was successfully updated";
        this.orderUploadMessage = "Order update success";
        this.orderUpdateMessage = "Order update success";
        this.fileUploadeMessage = "File is upload";
        this.fileExtensionNotAllowedMessage = "File with this extension not allowed";
        this.atachmentDeleteMessage = "Atachment delete";
        this.URL = " ";
        this.allowableExtensions = ["pdf", "jpg", "png", "docs", "xlsx", "txt", "msg"];
        this.allowableImageExtensions = [".jpg", ".png"];
        this.status = {
            new: "New",
            active: "Active",
            processing: "Processing",
            done: "Done"
        };
        this.kanbanBlockStatus = {
            new: "cdk-drop-list-0",
            active: "cdk-drop-list-1",
            processing: "cdk-drop-list-2",
            done: "cdk-drop-list-3"
        };
        this.tabsData = {
            projects: {
                index: 0,
                label: "Projects",
                routeLabel: "/newProjects"
            },
            ordersKanban: {
                index: 1,
                label: "Orders",
                routeLabel: "/ordersKanban"
            },
            ordersList: {
                index: 1,
                label: "Orders",
                routeLabel: "/ordersList"
            },
            tasksKanban: {
                index: 2,
                label: "Tasks",
                routeLabel: "/tasksKanban"
            },
            tasksList: {
                index: 2,
                label: "Tasks",
                routeLabel: "/tasksList"
            },
            newProjects: {
                index: 3,
                label: "New Projects",
                routeLabel: "/newProjects"
            }
        };
        this.attacmentsData = {
            pdf: {
                type: 'application/pdf',
                extension: ".pdf"
            },
            imageJpeg: {
                type: 'image/jpeg',
                extension: ".jpg"
            },
            imagePng: {
                type: 'image/png',
                extension: ".png"
            },
            text: {
                type: 'text/txt',
                extension: ".txt"
            },
            docx: {
                type: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                extension: ".docx"
            },
            xlsx: {
                type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                extension: ".xlsx"
            },
            msg: {
                type: 'application/octet-stream',
                extension: ".msg"
            },
            zip: {
                type: 'application/zip',
                filename: 'Attachments.zip'
            }
        };
    }
    ProjectsAndOrdersConstans = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])()
    ], ProjectsAndOrdersConstans);
    return ProjectsAndOrdersConstans;
}());



/***/ })

}]);
//# sourceMappingURL=projects-and-orders-projects-and-orders-module.js.map