(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["project-and-orders-tasks-tasks-module-module"],{

/***/ "./src/app/projects-and-orders/project-and-orders/tasks/create-task/create-task.component.html":
/*!*****************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/tasks/create-task/create-task.component.html ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  create-task works!\r\n</p>\r\n<button type=\"button\" class=\"btn btn-default\" (click)=\"bsModalRef.hide()\">Close</button>"

/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/tasks/create-task/create-task.component.scss":
/*!*****************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/tasks/create-task/create-task.component.scss ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/tasks/create-task/create-task.component.ts":
/*!***************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/tasks/create-task/create-task.component.ts ***!
  \***************************************************************************************************/
/*! exports provided: CreateTaskComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateTaskComponent", function() { return CreateTaskComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-bootstrap */ "./node_modules/ngx-bootstrap/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CreateTaskComponent = /** @class */ (function () {
    function CreateTaskComponent(bsModalRef) {
        this.bsModalRef = bsModalRef;
    }
    CreateTaskComponent.prototype.ngOnInit = function () {
    };
    CreateTaskComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-create-task',
            template: __webpack_require__(/*! ./create-task.component.html */ "./src/app/projects-and-orders/project-and-orders/tasks/create-task/create-task.component.html"),
            styles: [__webpack_require__(/*! ./create-task.component.scss */ "./src/app/projects-and-orders/project-and-orders/tasks/create-task/create-task.component.scss")]
        }),
        __metadata("design:paramtypes", [ngx_bootstrap__WEBPACK_IMPORTED_MODULE_1__["BsModalRef"]])
    ], CreateTaskComponent);
    return CreateTaskComponent;
}());



/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/tasks/task-details/task-details.component.html":
/*!*******************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/tasks/task-details/task-details.component.html ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n  <div class=\"col-md-12 mb-3\">\r\n      <mat-form-field class=\"task-details-select select-bordered\">\r\n          <mat-select [(value)]=\"selected\">\r\n            <mat-option value=\"\">\r\n              ghbdtn 12342\r\n            </mat-option>\r\n            <mat-option value=\"option1\">\r\n              ghbdtn\r\n            </mat-option>\r\n            <mat-option value=\"option2\">\r\n              ghbdtn\r\n            </mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n  </div>\r\n  <div class=\"col-md-3 d-flex align-items-center\">\r\n      <mat-form-field class=\"task-details-select select-bordered\">\r\n          <mat-select [(value)]=\"selected\">\r\n            <mat-option value=\"\">\r\n              ghbdtn 12342\r\n            </mat-option>\r\n            <mat-option value=\"option1\">\r\n              ghbdtn\r\n            </mat-option>\r\n            <mat-option value=\"option2\">\r\n              ghbdtn\r\n            </mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n  </div>\r\n  <div class=\"col-md-3 d-flex align-items-center\">\r\n    <mat-form-field class=\"task-details-select select-bordered\">\r\n      <mat-select [(value)]=\"selected\">\r\n        <mat-option value=\"\">\r\n          ghbdtn 12342\r\n        </mat-option>\r\n        <mat-option value=\"option1\">\r\n          ghbdtn\r\n        </mat-option>\r\n        <mat-option value=\"option2\">\r\n          ghbdtn\r\n        </mat-option>\r\n      </mat-select>\r\n    </mat-form-field>\r\n  </div>\r\n  <div class=\"col-md-2 d-flex align-items-center\">\r\n    <mat-form-field class=\"task-details-select\">\r\n      <mat-select [(value)]=\"selected\">\r\n        <mat-option value=\"\">\r\n          ghbdtn 12342\r\n        </mat-option>\r\n        <mat-option value=\"option1\">\r\n          ghbdtn\r\n        </mat-option>\r\n        <mat-option value=\"option2\">\r\n          ghbdtn\r\n        </mat-option>\r\n      </mat-select>\r\n    </mat-form-field>\r\n  </div>\r\n  <div class=\"col-md-2 d-flex align-items-center\">\r\n    <mat-form-field class=\"task-details-select\">\r\n      <mat-select [(value)]=\"selected\">\r\n        <mat-option value=\"\">\r\n          ghbdtn 12342\r\n        </mat-option>\r\n        <mat-option value=\"option1\">\r\n          ghbdtn\r\n        </mat-option>\r\n        <mat-option value=\"option2\">\r\n          ghbdtn\r\n        </mat-option>\r\n      </mat-select>\r\n    </mat-form-field>\r\n  </div>\r\n  <div class=\"col-md-2 d-flex align-items-center\">\r\n    <a href=\"#\" class=\"cancel-button\">Cancel</a>\r\n    <a href=\"#\">Add</a>\r\n  </div>\r\n</div>\r\n<button type=\"button\" class=\"btn btn-default mt-3\" (click)=\"bsModalRef.hide()\">Close</button>"

/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/tasks/task-details/task-details.component.scss":
/*!*******************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/tasks/task-details/task-details.component.scss ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "select {\n  padding: 13px;\n  height: 44px;\n  width: 100%;\n  background: white;\n  border: 0.5px solid #707070; }\n\n.noborder-select {\n  border: none !important; }\n\n.cancel-button {\n  margin-right: 10px; }\n\na {\n  color: #0077c5;\n  font-size: 16px; }\n"

/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/tasks/task-details/task-details.component.ts":
/*!*****************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/tasks/task-details/task-details.component.ts ***!
  \*****************************************************************************************************/
/*! exports provided: TaskDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskDetailsComponent", function() { return TaskDetailsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-bootstrap */ "./node_modules/ngx-bootstrap/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var TaskDetailsComponent = /** @class */ (function () {
    function TaskDetailsComponent(bsModalRef) {
        this.bsModalRef = bsModalRef;
    }
    TaskDetailsComponent.prototype.ngOnInit = function () {
    };
    TaskDetailsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-task-details',
            template: __webpack_require__(/*! ./task-details.component.html */ "./src/app/projects-and-orders/project-and-orders/tasks/task-details/task-details.component.html"),
            styles: [__webpack_require__(/*! ./task-details.component.scss */ "./src/app/projects-and-orders/project-and-orders/tasks/task-details/task-details.component.scss")]
        }),
        __metadata("design:paramtypes", [ngx_bootstrap__WEBPACK_IMPORTED_MODULE_1__["BsModalRef"]])
    ], TaskDetailsComponent);
    return TaskDetailsComponent;
}());



/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/tasks/tasks-kanban/tasks-kanban.component.html":
/*!*******************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/tasks/tasks-kanban/tasks-kanban.component.html ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row align-items-center\" style=\"margin-top:70px;\">\r\n  <h3 class=\"orders-name col-md-8\">\r\n    Tasks (21)\r\n  </h3>\r\n<div class=\"col-md-4 d-flex justify-content-end\">\r\n    <button class=\"btn new-task\"><span style=\"margin-right:12px\">+</span> New task</button>\r\n</div>\r\n</div>\r\n<div class=\"row col-md-12\">\r\n  <div class=\"col-md-3 d-flex align-items-center pl-0\">\r\n   <div class=\"form-group w-100 position-relative search-input\">\r\n      <i class=\"fa fa-search search-icon\"></i>\r\n      <input type=\"text\" class=\"form-control\"/>\r\n   </div>\r\n  </div>\r\n  <div class=\"filters-button col-md-7\">\r\n    <i class=\"fa fa-filter mr-2\"></i>    \r\n    <mat-form-field class=\"custom-selects\">\r\n      <mat-select [(value)]=\"selected\">\r\n        <mat-option value=\"option1\">All Customers</mat-option>\r\n        <mat-option value=\"option2\">Option 2</mat-option>\r\n        <mat-option value=\"option3\">Option 3</mat-option>\r\n      </mat-select>\r\n    </mat-form-field>\r\n    <mat-form-field class=\"custom-selects\">\r\n      <mat-select [(value)]=\"selected\">\r\n        <mat-option value=\"option1\">All Users</mat-option>\r\n        <mat-option value=\"option2\">Option 2</mat-option>\r\n        <mat-option value=\"option3\">Option 3</mat-option>\r\n      </mat-select>\r\n    </mat-form-field>\r\n    <mat-form-field class=\"custom-selects\">\r\n      <mat-select [(value)]=\"selected\">\r\n        <mat-option value=\"option1\">All Teams</mat-option>\r\n        <mat-option value=\"option2\">Option 2</mat-option>\r\n        <mat-option value=\"option3\">Option 3</mat-option>\r\n      </mat-select>\r\n    </mat-form-field>\r\n    <mat-form-field class=\"custom-selects\">\r\n      <mat-select [(value)]=\"selected\">\r\n        <mat-option value=\"option1\">Due Date</mat-option>\r\n        <mat-option value=\"option2\">Option 2</mat-option>\r\n        <mat-option value=\"option3\">Option 3</mat-option>\r\n      </mat-select>\r\n    </mat-form-field>\r\n    <mat-slide-toggle></mat-slide-toggle><span class=\"switch-close\">Closed</span>\r\n  </div>\r\n  <div class=\"btn-group col-md-2 d-flex justify-content-end pr-0\" role=\"group\" aria-label=\"Basic example\">\r\n    <button type=\"button\" class=\"btn active\"><i class=\"fa fa-list-ul\" aria-hidden=\"true\"></i></button>\r\n    <button type=\"button\" class=\"btn\" (click)=\"showListView()\"><i class=\"fa fa-users\" aria-hidden=\"true\"></i>.</button>\r\n  </div>\r\n</div>\r\n<div class=\"row mt-5 kanban-full mb-3\">\r\n <div class=\"col-md-4\">\r\n   <p class=\"teams\">asdasdasd</p>\r\n   <div class=\"kanban-block\">\r\n      <p class=\"kanban-title\">Due Today</p>\r\n      <div class=\"kanban-info\">\r\n       <div class=\"d-flex justify-content-between align-items-center\">\r\n          <div>\r\n            <img src=\"../../../../../../assets/img/clipboard-1.svg\" alt=\"\">\r\n            <span class=\"kanban-digits\">50</span>\r\n          </div>\r\n          <div>\r\n            <span class=\"kanban-due\">Due Date</span>\r\n            <span class=\"kanban-date\">asdasdasd</span>\r\n          </div>\r\n       </div>\r\n       <p class=\"task-details\">adasdasd</p>\r\n       <div class=\"clients\">\r\n          <div>\r\n            <span>Status</span>\r\n            <a>asdasdasd</a>\r\n          </div>\r\n          <div>\r\n            <span>Customer</span>\r\n            <a>Archer Hotel</a>\r\n          </div>\r\n        </div>\r\n        <div class=\"assign\">\r\n          <span>Assigned</span>\r\n          <a>asdasd</a>\r\n          <a>asdasdasd</a>\r\n        </div>\r\n        <div class=\"kanban-footer\">\r\n          <div class=\"d-flex align-items-center\">\r\n              <img src=\"../../../../../../assets/img/order-2.svg\" alt=\"\">\r\n              <span class=\"kanban-digits\">50</span>\r\n              <p class=\"m-0\">2/4</p>\r\n          </div>\r\n          <div>\r\n              <img src=\"assets/img/project-active.svg\">\r\n              <span class=\"kanban-digits\">50</span>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"kanban-info\">\r\n       <div class=\"d-flex justify-content-between align-items-center\">\r\n          <div>\r\n            <img src=\"../../../../../../assets/img/clipboard-1.svg\" alt=\"\">\r\n            <span class=\"kanban-digits\">50</span>\r\n          </div>\r\n          <div>\r\n            <span class=\"kanban-due\">Due Date</span>\r\n            <span class=\"kanban-date\">asdasdasd</span>\r\n          </div>\r\n       </div>\r\n       <p class=\"task-details\">adasdasd</p>\r\n       <div class=\"clients\">\r\n          <div>\r\n            <span>Status</span>\r\n            <a>asdasdasd</a>\r\n          </div>\r\n          <div>\r\n            <span>Customer</span>\r\n            <a>Archer Hotel</a>\r\n          </div>\r\n        </div>\r\n        <div class=\"assign\">\r\n          <span>Assigned</span>\r\n          <a>asdasd</a>\r\n          <a>asdasdasd</a>\r\n        </div>\r\n        <div class=\"kanban-footer\">\r\n          <div class=\"d-flex align-items-center\">\r\n              <img src=\"../../../../../../assets/img/order-2.svg\" alt=\"\">\r\n              <span class=\"kanban-digits\">50</span>\r\n              <p class=\"m-0\">2/4</p>\r\n          </div>\r\n          <div>\r\n              <img src=\"assets/img/project-active.svg\">\r\n              <span class=\"kanban-digits\">50</span>\r\n          </div>\r\n        </div>\r\n      </div>\r\n   </div>\r\n </div>\r\n <div class=\"col-md-4\">\r\n   <p class=\"teams\">asdasdasd</p>\r\n   <div class=\"kanban-block\">\r\n      <p class=\"kanban-title\">Due Today</p>\r\n      <div class=\"kanban-info\">\r\n       <div class=\"d-flex justify-content-between align-items-center\">\r\n          <div>\r\n            <img src=\"../../../../../../assets/img/clipboard-1.svg\" alt=\"\">\r\n            <span class=\"kanban-digits\">50</span>\r\n          </div>\r\n          <div>\r\n            <span class=\"kanban-due\">Due Date</span>\r\n            <span class=\"kanban-date\">asdasdasd</span>\r\n          </div>\r\n       </div>\r\n       <p class=\"task-details\">adasdasd</p>\r\n       <div class=\"clients\">\r\n          <div>\r\n            <span>Status</span>\r\n            <a>asdasdasd</a>\r\n          </div>\r\n          <div>\r\n            <span>Customer</span>\r\n            <a>Archer Hotel</a>\r\n          </div>\r\n        </div>\r\n        <div class=\"assign\">\r\n          <span>Assigned</span>\r\n          <a>asdasd</a>\r\n          <a>asdasdasd</a>\r\n        </div>\r\n        <div class=\"kanban-footer\">\r\n          <div class=\"d-flex align-items-center\">\r\n              <img src=\"../../../../../../assets/img/order-2.svg\" alt=\"\">\r\n              <span class=\"kanban-digits\">50</span>\r\n              <p class=\"m-0\">2/4</p>\r\n          </div>\r\n          <div>\r\n              <img src=\"assets/img/project-active.svg\">\r\n              <span class=\"kanban-digits\">50</span>\r\n          </div>\r\n        </div>\r\n      </div>\r\n   </div>\r\n </div>\r\n <div class=\"col-md-4\">\r\n   <p class=\"teams\">asdasdasd</p>\r\n   <div class=\"kanban-block\">\r\n      <p class=\"kanban-title\">Due Today</p>\r\n      <div class=\"kanban-info\">\r\n       <div class=\"d-flex justify-content-between align-items-center\">\r\n          <div>\r\n            <img src=\"../../../../../../assets/img/clipboard-1.svg\" alt=\"\">\r\n            <span class=\"kanban-digits\">50</span>\r\n          </div>\r\n          <div>\r\n            <span class=\"kanban-due\">Due Date</span>\r\n            <span class=\"kanban-date\">asdasdasd</span>\r\n          </div>\r\n       </div>\r\n       <p class=\"task-details\">adasdasd</p>\r\n       <div class=\"clients\">\r\n          <div>\r\n            <span>Status</span>\r\n            <a>asdasdasd</a>\r\n          </div>\r\n          <div>\r\n            <span>Customer</span>\r\n            <a>Archer Hotel</a>\r\n          </div>\r\n        </div>\r\n        <div class=\"assign\">\r\n          <span>Assigned</span>\r\n          <a>asdasd</a>\r\n          <a>asdasdasd</a>\r\n        </div>\r\n        <div class=\"kanban-footer\">\r\n          <div class=\"d-flex align-items-center\">\r\n              <img src=\"../../../../../../assets/img/order-2.svg\" alt=\"\">\r\n              <span class=\"kanban-digits\">50</span>\r\n              <p class=\"m-0\">2/4</p>\r\n          </div>\r\n          <div>\r\n              <img src=\"assets/img/project-active.svg\">\r\n              <span class=\"kanban-digits\">50</span>\r\n          </div>\r\n        </div>\r\n      </div>\r\n   </div>\r\n </div>\r\n <div class=\"col-md-4\">\r\n   <p class=\"teams\">asdasdasd</p>\r\n   <div class=\"kanban-block\">\r\n      <p class=\"kanban-title\">Due Today</p>\r\n      <div class=\"kanban-info\">\r\n       <div class=\"d-flex justify-content-between align-items-center\">\r\n          <div>\r\n            <img src=\"../../../../../../assets/img/clipboard-1.svg\" alt=\"\">\r\n            <span class=\"kanban-digits\">50</span>\r\n          </div>\r\n          <div>\r\n            <span class=\"kanban-due\">Due Date</span>\r\n            <span class=\"kanban-date\">asdasdasd</span>\r\n          </div>\r\n       </div>\r\n       <p class=\"task-details\">adasdasd</p>\r\n       <div class=\"clients\">\r\n          <div>\r\n            <span>Status</span>\r\n            <a>asdasdasd</a>\r\n          </div>\r\n          <div>\r\n            <span>Customer</span>\r\n            <a>Archer Hotel</a>\r\n          </div>\r\n        </div>\r\n        <div class=\"assign\">\r\n          <span>Assigned</span>\r\n          <a>asdasd</a>\r\n          <a>asdasdasd</a>\r\n        </div>\r\n        <div class=\"kanban-footer\">\r\n          <div class=\"d-flex align-items-center\">\r\n              <img src=\"../../../../../../assets/img/order-2.svg\" alt=\"\">\r\n              <span class=\"kanban-digits\">50</span>\r\n              <p class=\"m-0\">2/4</p>\r\n          </div>\r\n          <div>\r\n              <img src=\"assets/img/project-active.svg\">\r\n              <span class=\"kanban-digits\">50</span>\r\n          </div>\r\n        </div>\r\n      </div>\r\n   </div>\r\n </div>\r\n</div>\r\n\r\n<button class=\"button\" (click)=\"openTaskDetails(1,$event)\">Modal Task Details</button>\r\n<button class=\"button\" (click)=\"openCreateTask($event)\">Modal Create Task</button>\r\n"

/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/tasks/tasks-kanban/tasks-kanban.component.scss":
/*!*******************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/tasks/tasks-kanban/tasks-kanban.component.scss ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".kanban-full {\n  flex-wrap: nowrap !important;\n  overflow: auto; }\n\n#newOrders {\n  font-size: 14px;\n  font-weight: bold;\n  color: #636363;\n  background: transparent;\n  border: 1px solid #707070;\n  text-transform: uppercase; }\n\n.new-task {\n  margin-right: 28.5px;\n  font-size: 14px;\n  font-weight: bold;\n  color: #000;\n  background: transparent;\n  border: 1px solid #000;\n  text-transform: uppercase; }\n\n.switch-close {\n  font-size: 14px;\n  font-weight: bold;\n  color: #000;\n  margin-left: 14px; }\n\n.search-input .search-icon {\n  position: absolute;\n  top: 12px;\n  left: 10px; }\n\n.search-input input {\n  height: 38px;\n  padding-left: 30px;\n  border: 1px solid #707070 !important; }\n\n.orders-name {\n  font-size: 24px;\n  font-weight: bold;\n  margin-bottom: 36px;\n  margin-top: 25px; }\n\n.btn-group {\n  height: 38px; }\n\n.btn-group button {\n    background: white;\n    width: 50px;\n    border: 1px solid #707070;\n    display: flex;\n    justify-content: center;\n    align-items: center; }\n\n.btn-group button i {\n      color: #636363;\n      font-size: 20px; }\n\n.btn-group .active {\n    background: #636363; }\n\n.btn-group .active i {\n      color: white; }\n\n.filters-button {\n  display: flex;\n  align-items: center;\n  background: white;\n  height: 38px;\n  border: 1px solid #707070; }\n\n.filters-button mat-form-field {\n    margin-right: 5px; }\n\n.filters-button mat-form-field .mat-form-field-underline {\n      height: 0px !important; }\n\n.filters-button mat-form-field .mat-form-field-underline .mat-form-field-ripple {\n        background: none !important; }\n\n.orders-buttons button {\n  margin-right: 14.5px;\n  border-radius: 17px;\n  background-color: #636363;\n  color: white;\n  font-size: 14px;\n  font-weight: bold;\n  padding: 8px 28px; }\n\n.orders-buttons .active {\n  background: transparent;\n  border: 0.5px solid #707070;\n  color: #636363;\n  font-weight: normal; }\n\n.active-order {\n  margin-top: 21px;\n  height: 100%;\n  width: 100%;\n  border: 0.5px solid #707070;\n  background: white;\n  padding: 27px 20.7px 50px 118px; }\n\n.active-order .title {\n    font-size: 22px;\n    font-weight: bold;\n    color: #19233b;\n    margin-bottom: 16px; }\n\n.active-order .info-order .name-order {\n    height: 44px;\n    background-color: #e9e9e9;\n    display: flex;\n    align-items: center;\n    justify-content: space-between;\n    padding: 0px 30px; }\n\n.active-order .info-order .name-order .pre-title {\n      font-size: 16px;\n      font-weight: bold;\n      color: #19233b; }\n\n.active-order .info-order .info-card {\n    margin-top: 31px;\n    display: flex;\n    align-items: center;\n    justify-content: space-between; }\n\n.active-order .info-order .info-card .order-image {\n      width: 63.5px;\n      height: 63.5px; }\n\n.active-order .info-order .info-card .card-name {\n      display: flex;\n      flex-direction: column;\n      margin-left: 55px; }\n\n.active-order .info-order .info-card .card-name a {\n        font-size: 16px;\n        font-weight: normal;\n        color: #0077c5; }\n\n.active-order .info-order .info-card .card-name a:hover {\n          text-decoration: none; }\n\n.active-order .info-order .info-card .card-name span {\n        font-size: 14px;\n        font-weight: normal;\n        color: #000; }\n\n.active-order .info-order .info-card .card-name .card-button {\n        border-radius: 4px;\n        background-color: #9198ac;\n        padding: 0px 8.5px;\n        color: white;\n        margin-right: 10px; }\n\n.active-order .info-order .info-card .vendors {\n      width: 25%; }\n\n.active-order .info-order .info-card .vendors img {\n        width: 17.8px;\n        height: 17.8px;\n        margin-right: 14.8px; }\n\n.active-order .info-order .info-card .vendors a {\n        font-size: 14px;\n        font-weight: bold;\n        color: #0077c5;\n        white-space: nowrap; }\n\n.active-order .info-order .info-card .vendors a:hover {\n          text-decoration: none; }\n\n.active-order .info-order .info-card .vendors span {\n        font-size: 14px;\n        font-weight: normal;\n        color: #000;\n        text-align: right;\n        width: 100%; }\n\n.active-order .order-details {\n    align-items: center;\n    display: flex;\n    justify-content: center;\n    flex-direction: column; }\n\n.active-order .order-details .details-order-button {\n      padding: 12px 37px;\n      color: #000;\n      font-size: 14px;\n      font-weight: normal;\n      text-transform: uppercase;\n      border: 1px solid black;\n      background: transparent;\n      margin-bottom: 25px; }\n\n.active-order .order-details .order-details-name {\n      text-align: center; }\n\n.active-order .order-details .order-details-name img {\n        width: 20px;\n        height: 22.1px; }\n\n.active-order .order-details .order-details-name .project-name {\n        margin-bottom: 10px; }\n\n.active-order .order-details .order-details-name .project-name span {\n          font-size: 16px;\n          font-weight: bold;\n          color: #000;\n          margin-left: 10px; }\n\n.active-order .order-details .order-details-name .order-client {\n        font-size: 16px;\n        font-weight: normal;\n        color: #000; }\n\n.active-order .order-details .order-details-name .tasks {\n        display: flex;\n        align-items: center;\n        margin-top: 10px;\n        justify-content: space-between; }\n\n.active-order .order-details .order-details-name .tasks span {\n          font-size: 16px;\n          font-weight: normal;\n          color: #000; }\n\n.clickable-title-link {\n  color: rgba(var(--palette-neutral-100, 0, 0, 0), 1); }\n\n.underline-on-hover:hover {\n  text-decoration: underline !important; }\n\n.underline-on-hover {\n  font-size: 16px; }\n\n.section-heading {\n  padding: 5px 10px 11px 0px;\n  font-size: 18px;\n  font-weight: bold; }\n\n.teams {\n  font-size: 22px;\n  font-weight: bold;\n  color: #000000; }\n\n.kanban-block {\n  background: #e9e9e9;\n  padding: 35px; }\n\n.kanban-block .kanban-title {\n    font-size: 17px;\n    font-weight: bold; }\n\n.kanban-block .kanban-info {\n    background-color: #ffffff;\n    padding: 20px;\n    margin-bottom: 30px; }\n\n.kanban-block .kanban-info img {\n      width: 20px;\n      height: 22.1px;\n      margin-right: 5px; }\n\n.kanban-block .kanban-info .kanban-due {\n      font-size: 12px;\n      font-weight: normal;\n      color: #636363; }\n\n.kanban-block .kanban-info .kanban-date {\n      font-size: 12px;\n      font-weight: normal;\n      color: #0077c5;\n      margin-left: 20px; }\n\n.kanban-block .kanban-info .kanban-digits {\n      font-size: 13px;\n      font-weight: bold;\n      color: #000000; }\n\n.kanban-block .kanban-info .task-details {\n      font-size: 12px;\n      font-weight: bold;\n      color: #000000;\n      margin-top: 24px; }\n\n.kanban-block .kanban-info .clients div {\n      margin-bottom: 20px; }\n\n.kanban-block .kanban-info .clients div span {\n        color: #707070;\n        margin-right: 12px; }\n\n.kanban-block .kanban-info .clients div a {\n        color: #0077c5; }\n\n.kanban-block .kanban-info .assign {\n      display: flex;\n      justify-content: space-between; }\n\n.kanban-block .kanban-info .assign span {\n        color: #707070; }\n\n.kanban-block .kanban-info .assign a {\n        color: #0077c5; }\n\n.kanban-block .kanban-info .kanban-footer {\n      display: flex;\n      justify-content: space-between;\n      margin-top: 23px; }\n\n.kanban-block .kanban-info .kanban-footer .kanban-digits {\n        margin-right: 12px; }\n\n.kanban-block .kanban-info .kanban-footer .kanban-digits + p {\n        font-size: 13px; }\n"

/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/tasks/tasks-kanban/tasks-kanban.component.ts":
/*!*****************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/tasks/tasks-kanban/tasks-kanban.component.ts ***!
  \*****************************************************************************************************/
/*! exports provided: TasksKanbanComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TasksKanbanComponent", function() { return TasksKanbanComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-bootstrap */ "./node_modules/ngx-bootstrap/index.js");
/* harmony import */ var _task_details_task_details_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../task-details/task-details.component */ "./src/app/projects-and-orders/project-and-orders/tasks/task-details/task-details.component.ts");
/* harmony import */ var _create_task_create_task_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../create-task/create-task.component */ "./src/app/projects-and-orders/project-and-orders/tasks/create-task/create-task.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var TasksKanbanComponent = /** @class */ (function () {
    function TasksKanbanComponent(modalService, route, activateRoute) {
        this.modalService = modalService;
        this.route = route;
        this.activateRoute = activateRoute;
        this.selected = 'option1';
    }
    TasksKanbanComponent.prototype.ngOnInit = function () {
    };
    TasksKanbanComponent.prototype.openTaskDetails = function (id, event) {
        event.preventDefault();
        var initialState = {
            taskDetails: "task details" //task details view
        };
        this.modalRef = this.modalService.show(_task_details_task_details_component__WEBPACK_IMPORTED_MODULE_2__["TaskDetailsComponent"], Object.assign({}, { class: 'gray modal-lg', initialState: initialState }));
        this.modalRef.content.closeBtnName = 'Close';
    };
    TasksKanbanComponent.prototype.openCreateTask = function (event) {
        event.preventDefault();
        this.modalRef = this.modalService.show(_create_task_create_task_component__WEBPACK_IMPORTED_MODULE_3__["CreateTaskComponent"], Object.assign({}, { class: 'gray modal-lg' }));
        this.modalRef.content.closeBtnName = 'Close';
    };
    TasksKanbanComponent.prototype.showListView = function () {
        this.route.navigate(['../tasksList'], { relativeTo: this.activateRoute });
    };
    TasksKanbanComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-tasks-kanban',
            template: __webpack_require__(/*! ./tasks-kanban.component.html */ "./src/app/projects-and-orders/project-and-orders/tasks/tasks-kanban/tasks-kanban.component.html"),
            styles: [__webpack_require__(/*! ./tasks-kanban.component.scss */ "./src/app/projects-and-orders/project-and-orders/tasks/tasks-kanban/tasks-kanban.component.scss")]
        }),
        __metadata("design:paramtypes", [ngx_bootstrap__WEBPACK_IMPORTED_MODULE_1__["BsModalService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]])
    ], TasksKanbanComponent);
    return TasksKanbanComponent;
}());



/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/tasks/tasks-list/tasks-list.component.html":
/*!***************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/tasks/tasks-list/tasks-list.component.html ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row align-items-center\" style=\"margin-top:70px;\">\r\n  <h3 class=\"orders-name col-md-8\">\r\n    Tasks (21)\r\n  </h3>\r\n  <div class=\"col-md-4 d-flex justify-content-end\">\r\n    <button class=\"btn new-task\"><span style=\"margin-right:12px\">+</span> New task</button>\r\n  </div>\r\n</div>\r\n<div class=\"row col-md-12\">\r\n  <div class=\"col-md-3 d-flex align-items-center pl-0\">\r\n    <div class=\"form-group w-100 position-relative search-input\">\r\n      <i class=\"fa fa-search search-icon\"></i>\r\n      <input type=\"text\" class=\"form-control\" />\r\n    </div>\r\n  </div>\r\n  <div class=\"filters-button col-md-7\">\r\n    <i class=\"fa fa-filter mr-2\"></i>\r\n    <mat-form-field class=\"custom-selects\">\r\n      <mat-select>\r\n        <mat-option>\r\n          ghbdtn\r\n        </mat-option>\r\n        <mat-option>\r\n          ghbdtn\r\n        </mat-option>\r\n      </mat-select>\r\n    </mat-form-field>\r\n    <mat-form-field class=\"custom-selects\">\r\n      <mat-select>\r\n        <mat-option>\r\n          ghbdtn\r\n        </mat-option>\r\n        <mat-option>\r\n          ghbdtn\r\n        </mat-option>\r\n      </mat-select>\r\n    </mat-form-field>\r\n    <mat-form-field class=\"custom-selects\">\r\n      <mat-select>\r\n        <mat-option>\r\n          ghbdtn\r\n        </mat-option>\r\n        <mat-option>\r\n          ghbdtn\r\n        </mat-option>\r\n      </mat-select>\r\n    </mat-form-field>\r\n    <mat-form-field class=\"custom-selects\">\r\n      <mat-select [(value)]=\"selected\">\r\n        <mat-option value=\"\">\r\n          ghbdtn  12342\r\n        </mat-option>\r\n        <mat-option value=\"option1\">\r\n          ghbdtn\r\n        </mat-option>\r\n        <mat-option value=\"option2\">\r\n          ghbdtn\r\n        </mat-option>\r\n      </mat-select>\r\n    </mat-form-field>\r\n    <mat-slide-toggle></mat-slide-toggle><span class=\"switch-close\">Closed</span>\r\n  </div>\r\n  <div class=\"btn-group col-md-2 d-flex justify-content-end pr-0\" role=\"group\" aria-label=\"Basic example\">\r\n    <button type=\"button\" class=\"btn\" (click)=\"showKanbanView()\"><i class=\"fa fa-list-ul\" aria-hidden=\"true\"></i></button>\r\n    <button type=\"button\" class=\"btn active\" ><i class=\"fa fa-users\" aria-hidden=\"true\"></i></button>\r\n  </div>\r\n</div>\r\n<div class=\"row col-md-12 mb-3\">\r\n  <div class=\"col-md-12 p-0\">\r\n    <h2 class=\"title-list\">Past Due</h2>\r\n  </div>\r\n  <div class=\"col-md-12 p-0\">\r\n    <div class=\"list d-flex justify-content-between\">\r\n      <div class=\"list-clipboard d-flex\">\r\n        <img src=\"../../../../../assets/img/clipboard-1.svg\" alt=\"\">\r\n        <span>50</span>\r\n      </div>\r\n      <div class=\"status-list\">\r\n        <p class=\"place-details mb-3\">This is the place for the task details</p>\r\n        <div class=\"d-flex\">\r\n          <span class=\"names-list\">Status</span>\r\n          <a href=\"#\" class=\"links-list\">This is the status</a>\r\n        </div>\r\n      </div>\r\n      <div class=\"customer-list\">\r\n        <div class=\"d-flex mb-3\">\r\n          <span class=\"names-list\">Customer</span>\r\n          <a href=\"#\" class=\"links-list\">Archer Hotel</a>\r\n        </div>\r\n        <div class=\"d-flex\">\r\n          <span class=\"names-list\">Due Date</span>\r\n          <a href=\"#\" class=\"links-list\">8/20/18</a>\r\n        </div>\r\n      </div>\r\n      <div class=\"assigned-list\">\r\n          <div class=\"d-flex mb-3\">\r\n            <span class=\"names-list\">Team assigned</span>\r\n            <a href=\"#\" class=\"links-list\">This is the status</a>\r\n          </div>\r\n          <div class=\"d-flex\">\r\n            <span class=\"names-list\">User assigned</span>\r\n            <a href=\"#\" class=\"links-list\">Jonathan Eisenback</a>\r\n          </div>\r\n        </div>\r\n        <div class=\"icons-list\">\r\n            <div class=\"d-flex mb-3\">\r\n              <img src=\"../../../../../assets/img/project-active.svg\" alt=\"\">\r\n              <span class=\"place-details\">81</span>\r\n            </div>\r\n            <div class=\"d-flex\">\r\n              <img src=\"../../../../../assets/img/order-2.svg\" alt=\"\">\r\n              <span class=\"place-details mr-3\">560</span>\r\n              <span style=\"font-size:16px;\">2/4</span>\r\n            </div>\r\n          </div>\r\n    </div>\r\n    <div class=\"list d-flex justify-content-between\">\r\n      <div class=\"list-clipboard d-flex\">\r\n        <img src=\"../../../../../assets/img/clipboard-1.svg\" alt=\"\">\r\n        <span>50</span>\r\n      </div>\r\n      <div class=\"status-list\">\r\n        <p class=\"place-details mb-3\">This is the place for the task details</p>\r\n        <div class=\"d-flex m\">\r\n          <span class=\"names-list\">Status</span>\r\n          <a href=\"#\" class=\"links-list\">This is the status</a>\r\n        </div>\r\n      </div>\r\n      <div class=\"customer-list\">\r\n        <div class=\"d-flex mb-3\">\r\n          <span class=\"names-list\">Customer</span>\r\n          <a href=\"#\" class=\"links-list\">Archer Hotel</a>\r\n        </div>\r\n        <div class=\"d-flex\">\r\n          <span class=\"names-list\">Due Date</span>\r\n          <a href=\"#\" class=\"links-list\">8/20/18</a>\r\n        </div>\r\n      </div>\r\n      <div class=\"assigned-list\">\r\n          <div class=\"d-flex mb-3\">\r\n            <span class=\"names-list\">Team assigned</span>\r\n            <a href=\"#\" class=\"links-list\">This is the status</a>\r\n          </div>\r\n          <div class=\"d-flex\">\r\n            <span class=\"names-list\">User assigned</span>\r\n            <a href=\"#\" class=\"links-list\">Jonathan Eisenback</a>\r\n          </div>\r\n        </div>\r\n        <div class=\"icons-list\">\r\n            <div class=\"d-flex mb-3\">\r\n              <img src=\"../../../../../assets/img/project-active.svg\" alt=\"\">\r\n              <span class=\"place-details\">81</span>\r\n            </div>\r\n            <div class=\"d-flex\">\r\n              <img src=\"../../../../../assets/img/order-2.svg\" alt=\"\">\r\n              <span class=\"place-details mr-3\">560</span>\r\n              <span style=\"font-size:16px;\">2/4</span>\r\n            </div>\r\n          </div>\r\n    </div>\r\n    <div class=\"list d-flex justify-content-between\">\r\n      <div class=\"list-clipboard d-flex\">\r\n        <img src=\"../../../../../assets/img/clipboard-1.svg\" alt=\"\">\r\n        <span>50</span>\r\n      </div>\r\n      <div class=\"status-list\">\r\n        <p class=\"place-details mb-3\">This is the place for the task details</p>\r\n        <div class=\"d-flex m\">\r\n          <span class=\"names-list\">Status</span>\r\n          <a href=\"#\" class=\"links-list\">This is the status</a>\r\n        </div>\r\n      </div>\r\n      <div class=\"customer-list\">\r\n        <div class=\"d-flex mb-3\">\r\n          <span class=\"names-list\">Customer</span>\r\n          <a href=\"#\" class=\"links-list\">Archer Hotel</a>\r\n        </div>\r\n        <div class=\"d-flex\">\r\n          <span class=\"names-list\">Due Date</span>\r\n          <a href=\"#\" class=\"links-list\">8/20/18</a>\r\n        </div>\r\n      </div>\r\n      <div class=\"assigned-list\">\r\n          <div class=\"d-flex mb-3\">\r\n            <span class=\"names-list\">Team assigned</span>\r\n            <a href=\"#\" class=\"links-list\">This is the status</a>\r\n          </div>\r\n          <div class=\"d-flex\">\r\n            <span class=\"names-list\">User assigned</span>\r\n            <a href=\"#\" class=\"links-list\">Jonathan Eisenback</a>\r\n          </div>\r\n        </div>\r\n        <div class=\"icons-list\">\r\n            <div class=\"d-flex mb-3\">\r\n              <img src=\"../../../../../assets/img/project-active.svg\" alt=\"\">\r\n              <span class=\"place-details\">81</span>\r\n            </div>\r\n            <div class=\"d-flex\">\r\n              <img src=\"../../../../../assets/img/order-2.svg\" alt=\"\">\r\n              <span class=\"place-details mr-3\">560</span>\r\n              <span style=\"font-size:16px;\">2/4</span>\r\n            </div>\r\n          </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n<button class=\"button\" (click)=\"openTaskDetails(1,$event)\">Modal Test</button>\r\n<button class=\"button\" (click)=\"openCreateTask($event)\">Modal Create Task</button>"

/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/tasks/tasks-list/tasks-list.component.scss":
/*!***************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/tasks/tasks-list/tasks-list.component.scss ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".kanban-full {\n  flex-wrap: nowrap !important;\n  overflow: auto; }\n\n#newOrders {\n  font-size: 14px;\n  font-weight: bold;\n  color: #636363;\n  background: transparent;\n  border: 1px solid #707070;\n  text-transform: uppercase; }\n\n.new-task {\n  margin-right: 28.5px;\n  font-size: 14px;\n  font-weight: bold;\n  color: #000;\n  background: transparent;\n  border: 1px solid #000;\n  text-transform: uppercase; }\n\n.switch-close {\n  font-size: 14px;\n  font-weight: bold;\n  color: #000;\n  margin-left: 14px; }\n\n.search-input .search-icon {\n  position: absolute;\n  top: 12px;\n  left: 10px; }\n\n.search-input input {\n  height: 38px;\n  padding-left: 30px;\n  border: 1px solid #707070 !important; }\n\n.orders-name {\n  font-size: 24px;\n  font-weight: bold;\n  margin-bottom: 36px;\n  margin-top: 25px; }\n\n.btn-group {\n  height: 38px; }\n\n.btn-group button {\n    background: white;\n    width: 50px;\n    border: 1px solid #707070;\n    display: flex;\n    justify-content: center;\n    align-items: center; }\n\n.btn-group button i {\n      color: #636363;\n      font-size: 20px; }\n\n.btn-group .active {\n    background: #636363; }\n\n.btn-group .active i {\n      color: white; }\n\n.filters-button {\n  display: flex;\n  align-items: center;\n  background: white;\n  height: 38px;\n  border: 1px solid #707070; }\n\n.filters-button mat-form-field {\n    margin-right: 5px; }\n\n.filters-button mat-form-field .mat-form-field-underline {\n      height: 0px !important; }\n\n.filters-button mat-form-field .mat-form-field-underline .mat-form-field-ripple {\n        background: none !important; }\n\n.orders-buttons button {\n  margin-right: 14.5px;\n  border-radius: 17px;\n  background-color: #636363;\n  color: white;\n  font-size: 14px;\n  font-weight: bold;\n  padding: 8px 28px; }\n\n.orders-buttons .active {\n  background: transparent;\n  border: 0.5px solid #707070;\n  color: #636363;\n  font-weight: normal; }\n\n.active-order {\n  margin-top: 21px;\n  height: 100%;\n  width: 100%;\n  border: 0.5px solid #707070;\n  background: white;\n  padding: 27px 20.7px 50px 118px; }\n\n.active-order .title {\n    font-size: 22px;\n    font-weight: bold;\n    color: #19233b;\n    margin-bottom: 16px; }\n\n.active-order .info-order .name-order {\n    height: 44px;\n    background-color: #e9e9e9;\n    display: flex;\n    align-items: center;\n    justify-content: space-between;\n    padding: 0px 30px; }\n\n.active-order .info-order .name-order .pre-title {\n      font-size: 16px;\n      font-weight: bold;\n      color: #19233b; }\n\n.active-order .info-order .info-card {\n    margin-top: 31px;\n    display: flex;\n    align-items: center;\n    justify-content: space-between; }\n\n.active-order .info-order .info-card .order-image {\n      width: 63.5px;\n      height: 63.5px; }\n\n.active-order .info-order .info-card .card-name {\n      display: flex;\n      flex-direction: column;\n      margin-left: 55px; }\n\n.active-order .info-order .info-card .card-name a {\n        font-size: 16px;\n        font-weight: normal;\n        color: #0077c5; }\n\n.active-order .info-order .info-card .card-name a:hover {\n          text-decoration: none; }\n\n.active-order .info-order .info-card .card-name span {\n        font-size: 14px;\n        font-weight: normal;\n        color: #000; }\n\n.active-order .info-order .info-card .card-name .card-button {\n        border-radius: 4px;\n        background-color: #9198ac;\n        padding: 0px 8.5px;\n        color: white;\n        margin-right: 10px; }\n\n.active-order .info-order .info-card .vendors {\n      width: 25%; }\n\n.active-order .info-order .info-card .vendors img {\n        width: 17.8px;\n        height: 17.8px;\n        margin-right: 14.8px; }\n\n.active-order .info-order .info-card .vendors a {\n        font-size: 14px;\n        font-weight: bold;\n        color: #0077c5;\n        white-space: nowrap; }\n\n.active-order .info-order .info-card .vendors a:hover {\n          text-decoration: none; }\n\n.active-order .info-order .info-card .vendors span {\n        font-size: 14px;\n        font-weight: normal;\n        color: #000;\n        text-align: right;\n        width: 100%; }\n\n.active-order .order-details {\n    align-items: center;\n    display: flex;\n    justify-content: center;\n    flex-direction: column; }\n\n.active-order .order-details .details-order-button {\n      padding: 12px 37px;\n      color: #000;\n      font-size: 14px;\n      font-weight: normal;\n      text-transform: uppercase;\n      border: 1px solid black;\n      background: transparent;\n      margin-bottom: 25px; }\n\n.active-order .order-details .order-details-name {\n      text-align: center; }\n\n.active-order .order-details .order-details-name img {\n        width: 20px;\n        height: 22.1px; }\n\n.active-order .order-details .order-details-name .project-name {\n        margin-bottom: 10px; }\n\n.active-order .order-details .order-details-name .project-name span {\n          font-size: 16px;\n          font-weight: bold;\n          color: #000;\n          margin-left: 10px; }\n\n.active-order .order-details .order-details-name .order-client {\n        font-size: 16px;\n        font-weight: normal;\n        color: #000; }\n\n.active-order .order-details .order-details-name .tasks {\n        display: flex;\n        align-items: center;\n        margin-top: 10px;\n        justify-content: space-between; }\n\n.active-order .order-details .order-details-name .tasks span {\n          font-size: 16px;\n          font-weight: normal;\n          color: #000; }\n\n.title-list {\n  font-size: 22px;\n  font-weight: bold; }\n\n.list {\n  height: 112px;\n  border: 0.5px solid #707070;\n  padding: 19px 104px 24px 37px;\n  background: white;\n  border-bottom: none; }\n\n.list:hover {\n    cursor: pointer;\n    background: #e9e9e9;\n    transition: .2s; }\n\n.list:last-child {\n    border-bottom: 0.5px solid #707070; }\n\n.list .list-clipboard img {\n    width: 20px;\n    height: 22px;\n    margin-right: 10px; }\n\n.list .list-clipboard span {\n    font-size: 16px;\n    font-weight: bold; }\n\n.list .place-details {\n    font-size: 16px;\n    font-weight: bold; }\n\n.list .names-list {\n    font-size: 16px;\n    font-weight: normal;\n    color: #636363;\n    margin-right: 15px; }\n\n.list .links-list {\n    font-size: 16px;\n    font-weight: normal;\n    color: #0077c5;\n    text-decoration: none; }\n\n.list .icons-list img {\n    width: 22px;\n    height: 22px;\n    margin-right: 10px; }\n"

/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/tasks/tasks-list/tasks-list.component.ts":
/*!*************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/tasks/tasks-list/tasks-list.component.ts ***!
  \*************************************************************************************************/
/*! exports provided: TasksListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TasksListComponent", function() { return TasksListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-bootstrap */ "./node_modules/ngx-bootstrap/index.js");
/* harmony import */ var _task_details_task_details_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../task-details/task-details.component */ "./src/app/projects-and-orders/project-and-orders/tasks/task-details/task-details.component.ts");
/* harmony import */ var _create_task_create_task_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../create-task/create-task.component */ "./src/app/projects-and-orders/project-and-orders/tasks/create-task/create-task.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var TasksListComponent = /** @class */ (function () {
    function TasksListComponent(modalService, route, activateRoute) {
        this.modalService = modalService;
        this.route = route;
        this.activateRoute = activateRoute;
    }
    TasksListComponent.prototype.ngOnInit = function () {
    };
    TasksListComponent.prototype.openTaskDetails = function (id, event) {
        event.preventDefault();
        var initialState = {
            taskDetails: "task details" //task details view
        };
        this.modalRef = this.modalService.show(_task_details_task_details_component__WEBPACK_IMPORTED_MODULE_2__["TaskDetailsComponent"], Object.assign({}, { class: 'gray modal-lg', initialState: initialState }));
        this.modalRef.content.closeBtnName = 'Close';
    };
    TasksListComponent.prototype.openCreateTask = function (event) {
        event.preventDefault();
        this.modalRef = this.modalService.show(_create_task_create_task_component__WEBPACK_IMPORTED_MODULE_3__["CreateTaskComponent"], Object.assign({}, { class: 'gray modal-lg' }));
        this.modalRef.content.closeBtnName = 'Close';
    };
    TasksListComponent.prototype.showKanbanView = function () {
        this.route.navigate(['../tasksKanban'], { relativeTo: this.activateRoute });
    };
    TasksListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-tasks-list',
            template: __webpack_require__(/*! ./tasks-list.component.html */ "./src/app/projects-and-orders/project-and-orders/tasks/tasks-list/tasks-list.component.html"),
            styles: [__webpack_require__(/*! ./tasks-list.component.scss */ "./src/app/projects-and-orders/project-and-orders/tasks/tasks-list/tasks-list.component.scss")]
        }),
        __metadata("design:paramtypes", [ngx_bootstrap__WEBPACK_IMPORTED_MODULE_1__["BsModalService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]])
    ], TasksListComponent);
    return TasksListComponent;
}());



/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/tasks/tasks-module-routing.module.ts":
/*!*********************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/tasks/tasks-module-routing.module.ts ***!
  \*********************************************************************************************/
/*! exports provided: TasksModuleRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TasksModuleRoutingModule", function() { return TasksModuleRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _tasks_kanban_tasks_kanban_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./tasks-kanban/tasks-kanban.component */ "./src/app/projects-and-orders/project-and-orders/tasks/tasks-kanban/tasks-kanban.component.ts");
/* harmony import */ var _tasks_list_tasks_list_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./tasks-list/tasks-list.component */ "./src/app/projects-and-orders/project-and-orders/tasks/tasks-list/tasks-list.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var routes = [
    {
        path: '',
        redirectTo: 'tasksKanban',
        pathMatch: 'full'
    },
    {
        path: 'tasksKanban',
        component: _tasks_kanban_tasks_kanban_component__WEBPACK_IMPORTED_MODULE_2__["TasksKanbanComponent"]
    },
    {
        path: 'tasksList',
        component: _tasks_list_tasks_list_component__WEBPACK_IMPORTED_MODULE_3__["TasksListComponent"]
    }
];
var TasksModuleRoutingModule = /** @class */ (function () {
    function TasksModuleRoutingModule() {
    }
    TasksModuleRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], TasksModuleRoutingModule);
    return TasksModuleRoutingModule;
}());



/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/tasks/tasks-module.module.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/tasks/tasks-module.module.ts ***!
  \*************************************************************************************/
/*! exports provided: TasksModuleModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TasksModuleModule", function() { return TasksModuleModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var ngx_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-bootstrap */ "./node_modules/ngx-bootstrap/index.js");
/* harmony import */ var _tasks_module_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./tasks-module-routing.module */ "./src/app/projects-and-orders/project-and-orders/tasks/tasks-module-routing.module.ts");
/* harmony import */ var _tasks_kanban_tasks_kanban_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./tasks-kanban/tasks-kanban.component */ "./src/app/projects-and-orders/project-and-orders/tasks/tasks-kanban/tasks-kanban.component.ts");
/* harmony import */ var _tasks_list_tasks_list_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./tasks-list/tasks-list.component */ "./src/app/projects-and-orders/project-and-orders/tasks/tasks-list/tasks-list.component.ts");
/* harmony import */ var _task_details_task_details_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./task-details/task-details.component */ "./src/app/projects-and-orders/project-and-orders/tasks/task-details/task-details.component.ts");
/* harmony import */ var _create_task_create_task_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./create-task/create-task.component */ "./src/app/projects-and-orders/project-and-orders/tasks/create-task/create-task.component.ts");
/* harmony import */ var app_shared_modules_material_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! app/shared/modules/material.module */ "./src/app/shared/modules/material.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var TasksModuleModule = /** @class */ (function () {
    function TasksModuleModule() {
    }
    TasksModuleModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _tasks_module_routing_module__WEBPACK_IMPORTED_MODULE_3__["TasksModuleRoutingModule"],
                ngx_bootstrap__WEBPACK_IMPORTED_MODULE_2__["ModalModule"].forRoot(),
                app_shared_modules_material_module__WEBPACK_IMPORTED_MODULE_8__["MaterialModule"]
            ],
            declarations: [
                _tasks_kanban_tasks_kanban_component__WEBPACK_IMPORTED_MODULE_4__["TasksKanbanComponent"],
                _tasks_list_tasks_list_component__WEBPACK_IMPORTED_MODULE_5__["TasksListComponent"],
                _task_details_task_details_component__WEBPACK_IMPORTED_MODULE_6__["TaskDetailsComponent"],
                _create_task_create_task_component__WEBPACK_IMPORTED_MODULE_7__["CreateTaskComponent"]
            ],
            exports: [
                _task_details_task_details_component__WEBPACK_IMPORTED_MODULE_6__["TaskDetailsComponent"]
            ],
            entryComponents: [
                _task_details_task_details_component__WEBPACK_IMPORTED_MODULE_6__["TaskDetailsComponent"],
                _create_task_create_task_component__WEBPACK_IMPORTED_MODULE_7__["CreateTaskComponent"]
            ],
        })
    ], TasksModuleModule);
    return TasksModuleModule;
}());



/***/ })

}]);
//# sourceMappingURL=project-and-orders-tasks-tasks-module-module.js.map