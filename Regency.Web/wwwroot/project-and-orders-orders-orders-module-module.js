(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["project-and-orders-orders-orders-module-module"],{

/***/ "./src/app/projects-and-orders/project-and-orders/orders/create-order/create-order.component.html":
/*!********************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/orders/create-order/create-order.component.html ***!
  \********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\r\n  <button class=\"btn btn-outline-secondary\" (click)=\"navigateToListOrders()\">\r\n      <i class=\"fas fa-angle-left\"></i>\r\n  </button>\r\n</div>\r\n<div class=\"row card-row\">\r\n  <div class=\"col-12 \">\r\n    <br>\r\n    <a href=\"#\" class=\"details-project-link\">Project #170</a>\r\n    <div class=\"order-title-wrapper\">\r\n      <h6 class=\"order-title\">Order #15</h6>\r\n      <button class=\"edit-order\">\r\n        <i class=\"fas fa-pen\"></i>\r\n      </button>\r\n    </div>\r\n    \r\n    <mat-card class=\"details-card\">\r\n\r\n      <mat-form-field class=\" example-full-width col-md-4\">\r\n        <input matInput\r\n               placeholder=\"Customer\"\r\n               value=\"\">\r\n      </mat-form-field>\r\n      <mat-form-field class=\"example-full-width col-md-4\">\r\n        <input matInput\r\n               [matDatepicker]=\"pickerDate\"\r\n               placeholder=\"Date\">\r\n\r\n        <mat-datepicker-toggle matSuffix [for]=\"pickerDate\"></mat-datepicker-toggle>\r\n        <mat-datepicker #pickerDate></mat-datepicker>\r\n      </mat-form-field>\r\n      <mat-form-field class=\"example-full-width col-md-4\">\r\n        <input matInput\r\n               [matDatepicker]=\"pickerDue\"\r\n               placeholder=\"Due Date\">\r\n\r\n        <mat-datepicker-toggle matSuffix [for]=\"pickerDue\"></mat-datepicker-toggle>\r\n        <mat-datepicker #pickerDue></mat-datepicker>\r\n      </mat-form-field>\r\n      <mat-form-field class=\"example-full-width  col-md-12 \">\r\n        <textarea matInput matTextareaAutosize placeholder=\"Memmo\"></textarea>\r\n      </mat-form-field>\r\n      <div class=\"edit-button-wrapper\">\r\n        <button class=\"cancel-btn btn btn-block btn-outline-primary\">\r\n          Cancel\r\n        </button>\r\n        <button class=\"save-btn btn btn-block btn-outline-primary\">\r\n          Save\r\n        </button>\r\n      </div>\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"row card-row\">\r\n  <div class=\"col-12 \">\r\n    <br>\r\n    <h6>Item Details</h6>\r\n    <mat-card class=\"item-detail\">\r\n      <div class=\"row\">\r\n        <div class=\"item-detail-info col-md-3\">\r\n          <mat-form-field class=\"example-full-width col-md-9\">\r\n            <input matInput\r\n                   placeholder=\"Item\"\r\n                   value=\"Business Card\">\r\n          </mat-form-field>\r\n          <mat-form-field class=\"example-full-width col-md-3\">\r\n            <input matInput\r\n                   placeholder=\"Qty\"\r\n                   value=\"165\">\r\n          </mat-form-field>\r\n          <div class=\"col-md-12\">\r\n            <span class=\"table-header\">Specs</span>\r\n            <div class=\"table-box\">\r\n              <table class=\"table  datatable  attachment-table\">\r\n                <tbody>\r\n                  <tr>\r\n                    <th class=\"text-left\">Type</th>\r\n                    <th class=\"text-left\">Value</th>\r\n                    <th class=\"text-left\"></th>\r\n                  </tr>\r\n                  <tr>\r\n                    <td class=\"text-left pl-0\">Width</td>\r\n                    <td class=\"text-left pl-0\">20</td>\r\n                    <td class=\"text-right pr-0\">\r\n                      <div class=\"attachment-button mr-3\"><i class=\"fas fa-pen\"></i></div>\r\n                      <div class=\"attachment-button\"><i class=\"fas fa-times\"></i></div>\r\n                    </td>\r\n                  </tr>\r\n                  <tr>\r\n                    <td class=\"text-left pl-0\">Length</td>\r\n                    <td class=\"text-left pl-0\">30</td>\r\n                    <td class=\"text-right pr-0\">\r\n                      <div class=\"attachment-button mr-3\"><i class=\"fas fa-pen\"></i></div>\r\n                      <div class=\"attachment-button\"><i class=\"fas fa-times\"></i></div>\r\n                    </td>\r\n                  </tr>\r\n                  <tr>\r\n                    <td class=\"text-left pl-0\">Paper</td>\r\n                    <td class=\"text-left pl-0\">10</td>\r\n                    <td class=\"text-right pr-0\">\r\n                      <div class=\"attachment-button mr-3\"><i class=\"fas fa-pen\"></i></div>\r\n                      <div class=\"attachment-button\"><i class=\"fas fa-times\"></i></div>\r\n                    </td>\r\n                  </tr>\r\n                  <tr class=\"add-new-row\">\r\n                    <td></td>\r\n                    <td class=\"text-left pl-0\" colspan=\"2\">\r\n                      <div class=\"attachment-button\">\r\n                        <i class=\"fas fa-plus\"></i>\r\n                      </div>\r\n                    </td>\r\n                  </tr>\r\n                </tbody>\r\n              </table>\r\n            </div>\r\n          </div>\r\n\r\n        </div>\r\n        <div class=\"item-detail-price col-md-9\">\r\n          <mat-form-field class=\"example-full-width col-md-3\">\r\n            <input matInput\r\n                   placeholder=\"Confirmed Price\"\r\n                   value=\"$1.00\">\r\n          </mat-form-field>\r\n          <mat-form-field class=\"example-full-width col-md-9\">\r\n            <input matInput\r\n                   placeholder=\"Confirmed Total\"\r\n                   value=\"$247.50\">\r\n          </mat-form-field>\r\n          <div class=\"col-md-12\">\r\n            <span class=\"table-header\">Sales Price Options</span>\r\n            <div class=\"table-box\">\r\n              <table class=\"table  datatable  attachment-table\">\r\n                <tbody>\r\n                  <tr>\r\n                    <th class=\"text-left\"></th>\r\n                    <th class=\"text-left\">ETA</th>\r\n                    <th class=\"text-left\">Qtu</th>\r\n                    <th class=\"text-left\">Price</th>\r\n                    <th class=\"text-left\">Total</th>\r\n                    <th class=\"text-left\">Spec2</th>\r\n                    <th class=\"text-left\">Spec3</th>\r\n                    <th class=\"text-left\">Spec4</th>\r\n                    <th class=\"text-left\"></th>\r\n                  </tr>\r\n                  <tr>\r\n                    <td class=\"text-left pl-0 checkbox-cell checked\">\r\n                      <i class=\"fas fa-check-circle\"></i>\r\n                    </td>\r\n                    <td class=\"text-left pl-0\">15 Days</td>\r\n                    <td class=\"text-left pl-0\">165</td>\r\n                    <td class=\"text-left pl-0\">$1.50</td>\r\n                    <td class=\"text-left pl-0\">$247.5</td>\r\n                    <td class=\"text-left pl-0\">test</td>\r\n                    <td class=\"text-left pl-0\">test</td>\r\n                    <td class=\"text-left pl-0\">test</td>\r\n                    <td class=\"text-right pr-0\">\r\n                      <div class=\"attachment-button mr-3\"><i class=\"fas fa-plus-square\"></i></div>\r\n                      <div class=\"attachment-button mr-3\"><i class=\"fas fa-pen\"></i></div>\r\n                      <div class=\"attachment-button\"><i class=\"fas fa-times\"></i></div>\r\n                    </td>\r\n                  </tr>\r\n                  <tr>\r\n                    <td class=\"text-left pl-0 checkbox-cell\">\r\n                      <i class=\"fas fa-check-circle\"></i>\r\n                    </td>\r\n                    <td class=\"text-left pl-0\">15 Days</td>\r\n                    <td class=\"text-left pl-0\">165</td>\r\n                    <td class=\"text-left pl-0\">$1.50</td>\r\n                    <td class=\"text-left pl-0\">$247.5</td>\r\n                    <td class=\"text-left pl-0\">test</td>\r\n                    <td class=\"text-left pl-0\">test</td>\r\n                    <td class=\"text-left pl-0\">test</td>\r\n                    <td class=\"text-right pr-0\">\r\n                      <div class=\"attachment-button mr-3\"><i class=\"fas fa-plus-square\"></i></div>\r\n                      <div class=\"attachment-button mr-3\"><i class=\"fas fa-pen\"></i></div>\r\n                      <div class=\"attachment-button\"><i class=\"fas fa-times\"></i></div>\r\n                    </td>\r\n                  </tr>\r\n                  <tr>\r\n                    <td class=\"text-left pl-0 checkbox-cell\">\r\n                      <i class=\"fas fa-check-circle\"></i>\r\n                    </td>\r\n                    <td class=\"text-left pl-0\">15 Days</td>\r\n                    <td class=\"text-left pl-0\">165</td>\r\n                    <td class=\"text-left pl-0\">$1.50</td>\r\n                    <td class=\"text-left pl-0\">$247.5</td>\r\n                    <td class=\"text-left pl-0\">test</td>\r\n                    <td class=\"text-left pl-0\">test</td>\r\n                    <td class=\"text-left pl-0\">test</td>\r\n                    <td class=\"text-right pr-0\">\r\n                      <div class=\"attachment-button mr-3\"><i class=\"fas fa-plus-square\"></i></div>\r\n                      <div class=\"attachment-button mr-3\"><i class=\"fas fa-pen\"></i></div>\r\n                      <div class=\"attachment-button\"><i class=\"fas fa-times\"></i></div>\r\n                    </td>\r\n                  </tr>\r\n                  <tr class=\"add-new-row\">\r\n                    <td></td>\r\n                    <td class=\"text-left pl-0\" colspan=\"8\">\r\n                      <div class=\"attachment-button\">\r\n                        <i class=\"fas fa-plus\"></i>\r\n                      </div>\r\n                    </td>\r\n                  </tr>\r\n                </tbody>\r\n              </table>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"row card-row\">\r\n  <div class=\"col-12\">\r\n    <br>\r\n    <h6>Vendor Costs</h6>\r\n    <mat-card class=\"details-vendors-card\">\r\n      <div class=\"row\">\r\n        <div class=\"vendor-list-wrapper col-md-2 p-0\">\r\n          <button class=\"attachment-button select-all-vendors\">\r\n            Select All\r\n          </button>\r\n          <ul class=\"vendor-list\">\r\n            <li class=\"vendor-item\">Vendor 3</li>\r\n            <li class=\"vendor-item selected\">Vendor Hotel</li>\r\n            <li class=\"vendor-item\">Vendor 4</li>\r\n            <li class=\"vendor-item\">Vendor 2</li>\r\n          </ul>\r\n          <button class=\"attachment-button add-new-vendor\">\r\n            <i class=\"fas fa-plus\"></i>\r\n          </button>\r\n        </div>\r\n        <div class=\"vendor-info col-md-10\">\r\n          <mat-form-field class=\"example-full-width col-md-4 pl-0\">\r\n            <input matInput\r\n                   placeholder=\"Confirmed Vendor\"\r\n                   value=\"Vendor 4\">\r\n          </mat-form-field>\r\n          <mat-form-field class=\"example-full-width col-md-4\">\r\n            <input matInput\r\n                   placeholder=\"Confirmed Cost\"\r\n                   value=\"$1.00\">\r\n          </mat-form-field>\r\n          <mat-form-field class=\"example-full-width col-md-4\">\r\n            <input matInput\r\n                   placeholder=\"Confirmed Total Cost\"\r\n                   value=\"$165.00\">\r\n          </mat-form-field>\r\n          <div class=\"table-header\">Cost Price Options</div>\r\n          <div class=\"table-box\">\r\n            <table class=\"table  datatable  attachment-table\">\r\n              <tbody>\r\n                <tr>\r\n                  <th class=\"text-left\"></th>\r\n                  <th class=\"text-left\">Vendor</th>\r\n                  <th class=\"text-left\">ETA</th>\r\n                  <th class=\"text-left\">Quty</th>\r\n                  <th class=\"text-left\">Price</th>\r\n                  <th class=\"text-left\">Total</th>\r\n                  <th class=\"text-left\">Spec2</th>\r\n                  <th class=\"text-left\">Spec3</th>\r\n                  <th class=\"text-left\">Spec4</th>\r\n                  <th class=\"text-left\"></th>\r\n                </tr>\r\n                <tr>\r\n                  <td class=\"text-left pl-0 checkbox-cell checked\">\r\n                    <i class=\"fas fa-check-circle\"></i>\r\n                  </td>\r\n                  <td class=\"text-left pl-0\">Vendor 4</td>\r\n                  <td class=\"text-left pl-0\">15 Days</td>\r\n                  <td class=\"text-left pl-0\">165</td>\r\n                  <td class=\"text-left pl-0\">$1.50</td>\r\n                  <td class=\"text-left pl-0\">$247.5</td>\r\n                  <td class=\"text-left pl-0\">test</td>\r\n                  <td class=\"text-left pl-0\">test</td>\r\n                  <td class=\"text-left pl-0\">test</td>\r\n                  <td class=\"text-right pr-0\">\r\n                    <div class=\"attachment-button mr-3\"><i class=\"fas fa-plus-square\"></i></div>\r\n                    <div class=\"attachment-button mr-3\"><i class=\"fas fa-pen\"></i></div>\r\n                    <div class=\"attachment-button\"><i class=\"fas fa-times\"></i></div>\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <td class=\"text-left pl-0 checkbox-cell\">\r\n                    <i class=\"fas fa-check-circle\"></i>\r\n                  </td>\r\n                  <td class=\"text-left pl-0\">Vendor 4</td>\r\n                  <td class=\"text-left pl-0\">15 Days</td>\r\n                  <td class=\"text-left pl-0\">165</td>\r\n                  <td class=\"text-left pl-0\">$1.50</td>\r\n                  <td class=\"text-left pl-0\">$247.5</td>\r\n                  <td class=\"text-left pl-0\">test</td>\r\n                  <td class=\"text-left pl-0\">test</td>\r\n                  <td class=\"text-left pl-0\">test</td>\r\n                  <td class=\"text-right pr-0\">\r\n                    <div class=\"attachment-button mr-3\"><i class=\"fas fa-plus-square\"></i></div>\r\n                    <div class=\"attachment-button mr-3\"><i class=\"fas fa-pen\"></i></div>\r\n                    <div class=\"attachment-button\"><i class=\"fas fa-times\"></i></div>\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <td class=\"text-left pl-0 checkbox-cell\">\r\n                    <i class=\"fas fa-check-circle\"></i>\r\n                  </td>\r\n                  <td class=\"text-left pl-0\">Vendor 4</td>\r\n                  <td class=\"text-left pl-0\">15 Days</td>\r\n                  <td class=\"text-left pl-0\">165</td>\r\n                  <td class=\"text-left pl-0\">$1.50</td>\r\n                  <td class=\"text-left pl-0\">$247.5</td>\r\n                  <td class=\"text-left pl-0\">test</td>\r\n                  <td class=\"text-left pl-0\">test</td>\r\n                  <td class=\"text-left pl-0\">test</td>\r\n                  <td class=\"text-right pr-0\">\r\n                    <div class=\"attachment-button mr-3\"><i class=\"fas fa-plus-square\"></i></div>\r\n                    <div class=\"attachment-button mr-3\"><i class=\"fas fa-pen\"></i></div>\r\n                    <div class=\"attachment-button\"><i class=\"fas fa-times\"></i></div>\r\n                  </td>\r\n                </tr>\r\n                <tr class=\"add-new-row\">\r\n                  <td></td>\r\n                  <td class=\"text-left pl-0\" colspan=\"9\">\r\n                    <div class=\"attachment-button\">\r\n                      <i class=\"fas fa-plus\"></i>\r\n                    </div>\r\n                  </td>\r\n                </tr>\r\n              </tbody>\r\n            </table>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n<div class=\"card-row hide\">\r\n  <div class=\"row\">\r\n    <div class=\"col-md-6\">\r\n      <br>\r\n      <h6>Vendor Costs</h6>\r\n      <mat-card class=\"attachments-card\">\r\n        <ul class=\"file-list\">\r\n          <li class=\"file-item\">\r\n            <div class=\"file-name\">Archer Hotel signed doc.pdg</div>\r\n            <div class=\"control-buttons\">\r\n              <button class=\"download control-btn mr-1\"><i class=\"fas fa-long-arrow-alt-down\"></i></button>\r\n              <button class=\"remove control-btn\"><i class=\"fas fa-times\"></i></button>\r\n            </div>\r\n          </li>\r\n          <li class=\"file-item\">\r\n            <div class=\"file-name\">this is g doc es.img</div>\r\n            <div class=\"control-buttons\">\r\n              <button class=\"download control-btn mr-1\"><i class=\"fas fa-long-arrow-alt-down\"></i></button>\r\n              <button class=\"remove control-btn\"><i class=\"fas fa-times\"></i></button>\r\n            </div>\r\n          </li>\r\n        </ul>\r\n        <div class=\"drag-zone\">\r\n          Drag and drop files here\r\n        </div>\r\n      </mat-card>\r\n    </div>\r\n    <div class=\"col-md-6\">\r\n      <br>\r\n      <h6>Activity</h6>\r\n      <mat-card class=\"activity-card\">\r\n      </mat-card>\r\n    </div>\r\n  </div>\r\n</div>\r\n<div class=\"card-row\">\r\n  <div class=\"row\">\r\n    <div class=\"col-md-12\">\r\n      <br>\r\n      <h6>Tasks</h6>\r\n      <mat-card class=\"tasks-card\">\r\n        \r\n      </mat-card>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/orders/create-order/create-order.component.scss":
/*!********************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/orders/create-order/create-order.component.scss ***!
  \********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn-outline-secondary {\n  border: 0;\n  background: transparent;\n  color: #0077c5;\n  font-size: 18px;\n  margin-top: 20px; }\n\n.detail-project-link {\n  color: #0077c5;\n  font-size: 14px;\n  text-decoration: none; }\n\n.detail-project-link:visited {\n    color: #0077c5; }\n\n.detail-project-link:hover {\n    text-decoration: none;\n    color: #0077c5; }\n\n.order-title-wrapper {\n  display: flex;\n  justify-content: space-between;\n  margin-bottom: 22px; }\n\n.order-title-wrapper .order-title {\n    font-size: 24px;\n    font-weight: bold;\n    margin-bottom: 0; }\n\n.order-title-wrapper .edit-order {\n    border: 0;\n    background: transparent;\n    font-size: 18px; }\n\n.custom-file-upload {\n  border: 1px solid #ccc;\n  display: inline-block;\n  color: #fff;\n  background-color: #20a8d8;\n  border-color: #20a8d8;\n  padding: 6px 12px;\n  cursor: pointer; }\n\nlabel {\n  margin-bottom: 0px; }\n\ninput[type=\"file\"] {\n  display: none; }\n\np {\n  margin-top: 1rem !important;\n  margin-bottom: 1rem; }\n\n.custom-table {\n  border-spacing: 0px 15px;\n  border-collapse: separate !important; }\n\n.custom-table thead tr th {\n    border-bottom: none !important;\n    border-top: none !important; }\n\n.custom-table tbody tr {\n    border-bottom: 20px solid white !important;\n    background: #F2F4F8 !important; }\n\n.custom-table tbody tr:after {\n      content: '';\n      border-bottom: 1px solid black;\n      position: absolute;\n      bottom: 0; }\n\n.custom-table tbody tr .info-task .success {\n      background: #4CAF50;\n      height: 48px;\n      width: 48px;\n      position: relative;\n      border-radius: 50%; }\n\n.custom-table tbody tr .info-task .success .counter {\n        position: absolute;\n        height: 15px;\n        width: 15px;\n        border: 1px solid #4CAF50;\n        border-radius: 50%;\n        top: 0;\n        right: 0;\n        background: white;\n        display: flex;\n        justify-content: center;\n        align-items: center;\n        font-size: 10px;\n        font-weight: bold; }\n\n.custom-table tbody tr .info-task .success:before {\n        content: \"\";\n        position: absolute;\n        left: 20px;\n        top: 12px;\n        width: 10px;\n        height: 18px;\n        border: solid white;\n        border-width: 0 3px 3px 0;\n        -webkit-transform: rotate(45deg);\n        transform: rotate(45deg); }\n\n.custom-table tbody tr .info-task .error {\n      background: #CD3232;\n      height: 48px;\n      width: 48px;\n      position: relative;\n      border-radius: 50%;\n      text-align: center;\n      vertical-align: middle;\n      display: table-cell;\n      color: white; }\n\n.custom-table tbody tr .info-task .error .counter-error {\n        position: absolute;\n        height: 15px;\n        width: 15px;\n        border: 1px solid #CD3232;\n        border-radius: 50%;\n        top: 0;\n        right: 0;\n        background: white;\n        display: flex;\n        justify-content: center;\n        align-items: center;\n        font-size: 10px;\n        font-weight: bold;\n        color: black; }\n\n.custom-table tbody tr td {\n      border-top: 1px solid #D0D0D0;\n      border-bottom: 1px solid #D0D0D0;\n      vertical-align: middle; }\n\n.custom-table tbody tr td:first-child {\n        border-left: 1px solid #D0D0D0; }\n\n.custom-table tbody tr td:last-child {\n        border-right: 1px solid #D0D0D0; }\n\n.custom-table tbody tr:hover {\n      background: #d5d5d5 !important; }\n\n.custom-table tfoot tr td {\n    border-top: none !important; }\n\n.selected-game {\n  display: inline-block;\n  margin-right: 15px;\n  font-size: 15px;\n  background: #B9CFEA;\n  margin-bottom: 10px;\n  padding: 2px 11px 4px 11px;\n  border-radius: 8px; }\n\n.delete-game {\n  font-size: 10px;\n  padding: 3px 6px;\n  vertical-align: middle;\n  cursor: pointer; }\n\n#newProject {\n  width: 180px;\n  height: 45px;\n  font-size: large; }\n\n.add-project .overlay {\n  position: fixed;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  background: rgba(0, 0, 0, 0.4);\n  z-index: 10000; }\n\n.add-project .modal-add-project {\n  position: fixed;\n  background: #fff;\n  z-index: 10000;\n  top: -100%;\n  left: calc(50% - 200px);\n  width: 400px; }\n\n.add-project .modal-add-project.active {\n    top: 200px;\n    left: calc(50% - 200px);\n    transition: top .5s; }\n\n.add-project .modal-add-project .row {\n    width: 100%; }\n\n.add-project .modal-add-project .row .mt-10 {\n      margin-top: 15px; }\n\n.add-project .modal-add-project .button-wrapper {\n    display: flex;\n    justify-content: flex-end;\n    margin-top: 10px; }\n\n.add-project .modal-add-project .button-wrapper .btn {\n      width: 100px;\n      margin: 0;\n      margin-left: 10px; }\n\n.add-project .modal-show-pdf {\n  position: fixed;\n  background: #fff;\n  z-index: 10000;\n  top: -100%;\n  left: calc(50% - 400px);\n  width: 800px; }\n\n.add-project .modal-show-pdf .modal-body {\n    padding-right: 0; }\n\n.add-project .modal-show-pdf .modal-body .pdf-container {\n      max-height: 400px;\n      overflow-y: auto; }\n\n.add-project .modal-show-pdf .modal-body .text-container {\n      max-height: 400px;\n      overflow-y: auto; }\n\n.add-project .modal-show-pdf.active {\n    top: 200px;\n    left: calc(50% - 400px);\n    transition: top .5s; }\n\n.add-project .modal-show-pdf .row {\n    width: 100%; }\n\n.add-project .modal-show-pdf .row .mt-10 {\n      margin-top: 15px; }\n\n.add-project .modal-show-pdf .button-wrapper {\n    display: flex;\n    justify-content: flex-end;\n    margin-top: 10px; }\n\n.add-project .modal-show-pdf .button-wrapper .btn {\n      width: 100px;\n      margin: 0;\n      margin-left: 10px; }\n\n.save-attachment .overlay {\n  position: fixed;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  background: rgba(0, 0, 0, 0.4);\n  z-index: 10000; }\n\n.save-attachment .modal-save-attachment {\n  position: fixed;\n  background: #fff;\n  z-index: 10000;\n  top: -100%;\n  left: calc(50% - 400px);\n  width: 900px; }\n\n.save-attachment .modal-save-attachment.active {\n    top: 200px;\n    left: calc(50% - 400px);\n    transition: top .5s; }\n\n.save-attachment .modal-save-attachment .row {\n    width: 100%; }\n\n.save-attachment .modal-save-attachment .row .mt-10 {\n      margin-top: 15px; }\n\n.save-attachment .modal-save-attachment .button-wrapper {\n    display: flex;\n    justify-content: flex-end;\n    margin-top: 10px; }\n\n.save-attachment .modal-save-attachment .button-wrapper .btn {\n      width: 100px;\n      margin: 0;\n      margin-left: 10px; }\n\n.project-list-button {\n  border: none;\n  background: none;\n  color: #20A8D8;\n  padding: 0px;\n  margin-left: 16px;\n  margin-bottom: 20px; }\n\n.close-project-button {\n  margin-left: auto; }\n\n.attachment-button {\n  display: inline;\n  margin: 0;\n  padding: 0;\n  border: 0;\n  background: transparent;\n  color: #0077c5;\n  cursor: pointer; }\n\n.button-wrapper {\n  vertical-align: bottom;\n  margin-top: auto; }\n\n.card-body.project-details.project {\n  border: 1px solid #e1e6ef; }\n\n.card-body {\n  padding: 16px !important; }\n\n.card-header {\n  padding: 0.75rem 1.25rem;\n  margin-bottom: 0;\n  background-color: #f9f9fa;\n  display: flex;\n  border-bottom: 0px;\n  border: 1px solid #e1e6ef; }\n\n.project-details-input-wrapper.project {\n  display: flex; }\n\n.row {\n  display: flex;\n  flex-wrap: wrap;\n  margin-left: 0 !important; }\n\n.project-name-description-wrapper {\n  display: flex; }\n\n.card {\n  border: 0px !important; }\n\n.edit-project-button {\n  margin-left: auto; }\n\n.attachment-table {\n  text-align: center; }\n\n.attachment-table thead th {\n    border-top: none !important;\n    border-bottom: none !important; }\n\n.attachment-table tbody td {\n    border-top: none !important;\n    border-bottom: none !important; }\n\n.attachment-table tbody .link {\n    color: deepskyblue !important; }\n\n.edit-button-wrapper {\n  display: flex;\n  justify-content: flex-end;\n  margin-top: 20px; }\n\n.edit-button-wrapper .btn {\n    width: 100px;\n    margin: 0;\n    margin-left: 10px; }\n\n.mat-form-field.mat-focused.mat-form-field-ripple {\n  background-color: #20a8d8 !important; }\n\n.mat-form-field-ripple {\n  background-color: #20a8d8 !important; }\n\ntextarea.mat-autosize {\n  resize: auto !important;\n  overflow: hidden !important; }\n\n.item-detail {\n  padding: 0; }\n\n.item-detail-info, .item-detail-price {\n  padding: 44px 34px 66px 21.5px;\n  height: 100%; }\n\n.item-detail-info {\n  border-right: 1px solid #d4d4d4; }\n\n.table-header {\n  color: #636363;\n  font-size: 12px; }\n\n.table-box {\n  border: 1px solid #d4d4d4;\n  padding: 37px 45px 48.7px 35px; }\n\n.table-box table {\n    margin: 0; }\n\n.table-box th {\n    padding-left: 0px;\n    border-top: none;\n    font-size: 14px;\n    color: #636363; }\n\n.table-box td {\n    font-size: 16px; }\n\n.table-box td.checkbox-cell {\n      font-size: 16px;\n      color: #707070; }\n\n.table-box td.checkbox-cell.checked {\n        color: #ffba00; }\n\n.table-box td.add-new-row {\n      padding-top: 36px; }\n\n.details-vendors-card {\n  padding: 0; }\n\n.details-vendors-card .select-all-vendors {\n    width: 100%;\n    padding: 10px;\n    text-align: left;\n    border-bottom: 1px solid #d4d4d4;\n    padding-top: 16.5px;\n    padding-left: 63px;\n    padding-bottom: 18px;\n    font-size: 14px;\n    color: #0077c5; }\n\n.details-vendors-card .vendor-list-wrapper {\n    border-right: 1px solid #d4d4d4; }\n\n.details-vendors-card .vendor-list {\n    list-style: none;\n    margin: 0;\n    padding: 0; }\n\n.details-vendors-card .vendor-list .vendor-item {\n      padding-left: 63px;\n      padding-top: 17px;\n      padding-bottom: 18px;\n      font-size: 14px;\n      cursor: pointer; }\n\n.details-vendors-card .vendor-list .vendor-item.selected {\n        position: relative;\n        background: #e9e9e9; }\n\n.details-vendors-card .vendor-list .vendor-item.selected::after {\n          position: absolute;\n          content: '\\f058';\n          font-family: \"Font Awesome 5 Free\";\n          left: 20px;\n          color: #ffba00;\n          top: 18px; }\n\n.details-vendors-card .vendor-list .vendor-item:hover {\n        background: #e9e9e9; }\n\n.details-vendors-card .add-new-vendor {\n    width: 100%;\n    text-align: left;\n    padding: 12px 0;\n    padding-left: 63px; }\n\n.details-vendors-card .vendor-info {\n    padding: 52px 52px 38px 66.5px; }\n\n.attachments-card, .activity-card {\n  padding: 39px 66px 48.5px 59px;\n  height: 100%; }\n\n.attachments-card .file-list {\n  list-style: none;\n  padding: 0; }\n\n.attachments-card .file-list .file-item {\n    display: flex;\n    justify-content: space-between; }\n\n.attachments-card .file-list .file-name {\n    font-size: 16px;\n    color: #0077c5; }\n\n.attachments-card .file-list .control-btn {\n    background: transparent;\n    margin: 0;\n    padding: 5px;\n    border: none; }\n\n.attachments-card .drag-zone {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  margin-top: 61.5px;\n  width: 100%;\n  height: 72px;\n  font-size: 16px;\n  color: #636363;\n  border: 2px dashed #707070;\n  border-right-color: #D0D0D0;\n  border-left-color: #D0D0D0;\n  border-top-color: #d4d4d4;\n  border-bottom-color: #d4d4d4; }\n\n.hide {\n  overflow: hidden; }\n\n.tasks-card {\n  height: 806px;\n  background: #e9e9e9; }\n"

/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/orders/create-order/create-order.component.ts":
/*!******************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/orders/create-order/create-order.component.ts ***!
  \******************************************************************************************************/
/*! exports provided: CreateOrderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateOrderComponent", function() { return CreateOrderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CreateOrderComponent = /** @class */ (function () {
    function CreateOrderComponent(route) {
        this.route = route;
    }
    CreateOrderComponent.prototype.navigateToListOrders = function () {
        this.route.navigate(['projects-and-orders/orders/ordersList']);
    };
    CreateOrderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'create-order',
            template: __webpack_require__(/*! ./create-order.component.html */ "./src/app/projects-and-orders/project-and-orders/orders/create-order/create-order.component.html"),
            styles: [__webpack_require__(/*! ./create-order.component.scss */ "./src/app/projects-and-orders/project-and-orders/orders/create-order/create-order.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], CreateOrderComponent);
    return CreateOrderComponent;
}());



/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/orders/order-details/order-details.component.html":
/*!**********************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/orders/order-details/order-details.component.html ***!
  \**********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\r\n  <button class=\"btn btn-outline-secondary\" (click)=\"navigateToListOrders()\">\r\n    <i class=\"fas fa-angle-left\"></i>\r\n  </button>\r\n</div>\r\n<div class=\"row\">\r\n  <div class=\"col-md-12\">\r\n    <a [routerLink]=\"\" class=\"details-project-link\" *ngIf=\"alreadyGetOrderDetails\" (click)=\"showProjectDetailsById(currentOrder.project.id)\">Project #{{currentOrder.project.numericId}}</a>\r\n    <div class=\"order-title-wrapper\">\r\n      <h6 class=\"order-title\">Order #{{currentOrder.numericId}}</h6>\r\n      <!--<button class=\"edit-order\" (click)=\"activeEditableOrder()\" *ngIf=\"!activeEditOrder\">\r\n        <i class=\"fas fa-pen\"></i>\r\n      </button>-->\r\n    </div>\r\n  </div>\r\n</div>\r\n<div class=\"row card-row order-detail-block\">\r\n  <div class=\"col-md-8\">\r\n\r\n    <h6>Order Details</h6>\r\n\r\n    <mat-card class=\"details-card height-100\">\r\n      <div class=\"order-title-wrapper col-md-12\">\r\n        <h6>Basic Info</h6>\r\n        <img class=\"edit-order\" src=\"../../../../../assets/img/pencil-edit-button.svg\" (click)=\"activeEditableOrder()\" *ngIf=\"!activeEditOrder\" />\r\n        <div class=\"edit-button-wrapper\" *ngIf=\"activeEditOrder\">\r\n          <button class=\"btn order-control-btn cancel\" (click)=\"cancelUpdateOrder()\" *ngIf=\"activeEditOrder\">\r\n            Cancel\r\n          </button>\r\n          <button class=\"btn order-control-btn\" (click)=\"saveChanges()\" *ngIf=\"activeEditOrder\">\r\n            Save\r\n          </button>\r\n        </div>\r\n      </div>\r\n      <mat-form-field *ngIf=\"alreadyGetOrderDetails\" class=\" example-full-width col-md-6\">\r\n        <mat-select placeholder=\"Customer\" [(ngModel)]=\"currentOrder.customer.id\" [disabled]=\"!activeEditOrder\">\r\n          <mat-option *ngFor=\"let cust of customers.customers\" [value]=\"cust.id\">\r\n            {{cust.name}}\r\n          </mat-option>\r\n        </mat-select>\r\n      </mat-form-field>\r\n      <mat-form-field *ngIf=\"alreadyGetOrderDetails\" class=\" example-full-width col-md-6\">\r\n        <mat-select placeholder=\"Project Manager\" [(ngModel)]=\"currentOrder.project.user.id\" [disabled]=\"!activeEditOrder\">\r\n          <mat-option *ngFor=\"let user of projectManagers.users\" [value]=\"user.id\">\r\n            {{user.firstName}} {{user.lastName}}\r\n          </mat-option>\r\n        </mat-select>\r\n      </mat-form-field>\r\n      <mat-form-field class=\"example-full-width col-md-6\">\r\n        <input matInput\r\n               [matDatepicker]=\"pickerDue\"\r\n               placeholder=\"Due Date\"\r\n               [disabled]=\"!activeEditOrder\"\r\n               [(ngModel)]=\"currentOrder.dueDate\">\r\n        <mat-datepicker-toggle matSuffix [for]=\"pickerDue\"></mat-datepicker-toggle>\r\n        <mat-datepicker #pickerDue></mat-datepicker>\r\n      </mat-form-field>\r\n\r\n      <mat-form-field class=\"example-full-width col-md-6\">\r\n        <input matInput\r\n               [matDatepicker]=\"pickerDate\"\r\n               placeholder=\"Date\"\r\n               [disabled]=\"!activeEditOrder\"\r\n               [(ngModel)]=\"currentOrder.date\">\r\n        <mat-datepicker-toggle matSuffix [for]=\"pickerDate\"></mat-datepicker-toggle>\r\n        <mat-datepicker #pickerDate></mat-datepicker>\r\n      </mat-form-field>\r\n\r\n      <mat-form-field class=\"example-full-width  col-md-12 \">\r\n        <textarea matInput matTextareaAutosize placeholder=\"Memmo\"\r\n                  [disabled]=\"!activeEditOrder\" [(ngModel)]=\"currentOrder.memo\"></textarea>\r\n      </mat-form-field>\r\n    </mat-card>\r\n  </div>\r\n  <div class=\"col-md-4\">\r\n    <h6>Status</h6>\r\n    <mat-card class=\"details-card height-100\">\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n<div class=\"row card-row\">\r\n  <div class=\"col-12 \">\r\n    <br>\r\n    <h6>Item Details</h6>\r\n    <mat-card class=\"item-detail\">\r\n      <div class=\"row\">\r\n        <div class=\"item-detail-info col-md-3\">\r\n          <mat-form-field class=\"example-full-width col-md-9\">\r\n            <input matInput\r\n                   placeholder=\"Item\"\r\n                   [(ngModel)]=\"currentOrder.item.itemName\">\r\n          </mat-form-field>\r\n          <mat-form-field class=\"example-full-width col-md-3\">\r\n            <input matInput\r\n                   placeholder=\"Qty\"\r\n                   [(ngModel)]=\"currentOrder.confirmedCustomerQuote.qty\">\r\n          </mat-form-field>\r\n          <div class=\"col-md-12\">\r\n            <span class=\"table-header\">Specs</span>\r\n            <div class=\"table-box\">\r\n              <table class=\"table  datatable  attachment-table\">\r\n                <tbody>\r\n                  <tr>\r\n                    <th class=\"text-left\">Type</th>\r\n                    <th class=\"text-left\">Value</th>\r\n                    <th class=\"text-left\"></th>\r\n                  </tr>\r\n                  <tr *ngFor=\"let spec of currentOrder.orderItemSpecs\">\r\n                    <td class=\"text-left pl-0\">{{spec.specName}}</td>\r\n                    <td class=\"text-left pl-0\">{{spec.specValue}}</td>\r\n                    <td class=\"text-right pr-0\">\r\n                      <div class=\"attachment-button mr-3\"><i class=\"fas fa-pen\"></i></div>\r\n                      <div class=\"attachment-button\"><i class=\"fas fa-times\"></i></div>\r\n                    </td>\r\n                  </tr>               \r\n                  <tr class=\"add-new-row\">\r\n                    <td></td>\r\n                    <td class=\"text-left pl-0\" colspan=\"2\">\r\n                      <div class=\"attachment-button\">\r\n                        <i class=\"fas fa-plus\"></i>\r\n                      </div>\r\n                    </td>\r\n                  </tr>\r\n                </tbody>\r\n              </table>\r\n            </div>\r\n          </div>\r\n\r\n        </div>\r\n        <div class=\"item-detail-price col-md-9\">\r\n          <div class=\"col-md-12 space-between\">\r\n            <mat-form-field class=\"example-full-width\">\r\n              <input matInput\r\n                     placeholder=\"Confirmed Price\"\r\n                     [(ngModel)]=\"currentOrder.confirmedCustomerQuote.amount\">\r\n            </mat-form-field>\r\n            <mat-form-field class=\"example-full-width text-right\">\r\n              <input matInput\r\n                     placeholder=\"Confirmed Total\"\r\n                     [(ngModel)]=\"currentOrder.confirmedCustomerQuote.totalAmount\">\r\n            </mat-form-field>\r\n          </div>\r\n          <div class=\"col-md-12\">\r\n            <span class=\"table-header\">Sales Price Options</span>\r\n            <div class=\"table-box\">\r\n              <table class=\"table  datatable  attachment-table\">\r\n                <tbody>\r\n                  <tr>\r\n                    <th class=\"text-left\"></th>\r\n                    <th class=\"text-left\">ETA</th>\r\n                    <th class=\"text-left\">Qtu</th>\r\n                    <th class=\"text-left\">Price</th>\r\n                    <th class=\"text-left\">Total</th>\r\n                    <!--<th *ngFor=\"let i of currentOrder.orderCustomerQuoteItemSpecs\" class=\"text-left\">\r\n                      Spec\r\n                    </th>-->                   \r\n                  </tr>\r\n                  <tr *ngFor=\"let orderQuotes of currentOrder.orderCustomerQuotes\">\r\n                    <td class=\"text-left pl-0 checkbox-cell checked\">\r\n                      <i class=\"fas fa-check-circle\"></i>\r\n                    </td>\r\n                    <td class=\"text-left pl-0\">{{orderQuotes.eta}} Days</td>\r\n                    <td class=\"text-left pl-0\">{{orderQuotes.qty}}</td>\r\n                    <td class=\"text-left pl-0\">${{orderQuotes.amount}}</td>\r\n                    <td class=\"text-left pl-0\">${{orderQuotes.totalAmount}}</td>\r\n                    <td *ngFor=\"let orderQuotesSpec of orderQuotes.orderCustomerQuoteItemSpecs\" class=\"text-left pl-0\">\r\n                      {{orderQuotesSpec.specValue}}\r\n                    </td>\r\n                    \r\n                    <td class=\"text-right pr-0\">\r\n                      <div class=\"attachment-button mr-3\"><i class=\"fas fa-plus-square\"></i></div>\r\n                      <div class=\"attachment-button mr-3\"><i class=\"fas fa-pen\"></i></div>\r\n                      <div class=\"attachment-button\"><i class=\"fas fa-times\"></i></div>\r\n                    </td>\r\n                  </tr>\r\n                  <tr class=\"add-new-row\">\r\n                    <td></td>\r\n                    <td class=\"text-left pl-0\" colspan=\"8\">\r\n                      <div class=\"attachment-button\">\r\n                        <i class=\"fas fa-plus\"></i>\r\n                      </div>\r\n                    </td>\r\n                  </tr>\r\n                </tbody>\r\n              </table>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"row card-row\">\r\n  <div class=\"col-12\">\r\n    <br>\r\n    <h6>Vendor Costs</h6>\r\n    <mat-card class=\"details-vendors-card\">\r\n      <div class=\"row\">\r\n        <div class=\"vendor-list-wrapper col-md-2 p-0\">\r\n          <button class=\"attachment-button select-all-vendors\">\r\n            Select All\r\n          </button>\r\n          <ul class=\"vendor-list\" *ngFor=\"let orderVendorQuotes of currentOrder.orderVendorQuotes\">\r\n            <li class=\"vendor-item\">{{orderVendorQuotes.vendor.vendorName}}</li>\r\n          </ul>\r\n          <button class=\"attachment-button add-new-vendor\">\r\n            <i class=\"fas fa-plus\"></i>\r\n          </button>\r\n        </div>\r\n        <div class=\"vendor-info col-md-10\">\r\n          <mat-form-field class=\"example-full-width col-md-4 pl-0\">\r\n            <input matInput\r\n                   placeholder=\"Confirmed Vendor\"\r\n                   [(ngModel)]=\"currentOrder.confirmedVendorQuote.vendor.vendorName\">\r\n          </mat-form-field>\r\n          <mat-form-field class=\"example-full-width col-md-4\">\r\n            <input matInput\r\n                   placeholder=\"Confirmed Cost\"\r\n                   [(ngModel)]=\"currentOrder.confirmedVendorQuote.amount\">\r\n          </mat-form-field>\r\n          <mat-form-field class=\"example-full-width col-md-4\">\r\n            <input matInput\r\n                   placeholder=\"Confirmed Total Cost\"\r\n                   [(ngModel)]=\"currentOrder.confirmedVendorQuote.totalAmount\">\r\n          </mat-form-field>\r\n          <div class=\"table-header mt-4\">Cost Price Options</div>\r\n          <div class=\"table-box\">\r\n            <table class=\"table  datatable  attachment-table\">\r\n              <tbody>\r\n                <tr>\r\n                  <th class=\"text-left\"></th>\r\n                  <th class=\"text-left\">Vendor</th>\r\n                  <th class=\"text-left\">ETA</th>\r\n                  <th class=\"text-left\">Quty</th>\r\n                  <th class=\"text-left\">Price</th>\r\n                  <th class=\"text-left\">Total</th>\r\n                  <th class=\"text-left\">Spec2</th>\r\n                  <th class=\"text-left\">Spec3</th>\r\n                  <th class=\"text-left\">Spec4</th>\r\n                  <th class=\"text-left\"></th>\r\n                </tr>\r\n                <tr *ngFor=\"let orderQuote of currentOrder.orderVendorQuotes\">\r\n                  <td *ngIf=\"orderQuote.isConfirmed\" class=\"text-left pl-0 checkbox-cell checked\">\r\n                    <i class=\"fas fa-check-circle\"></i>\r\n                  </td>\r\n                  <td *ngIf=\"!orderQuote.isConfirmed\" (click)=\"openOrderVendorQuoteAttachmentModal(orderQuote)\" class=\"text-left pl-0 checkbox-cell\">\r\n                    <i class=\"fas fa-check-circle\"></i>\r\n                  </td>\r\n                  <td class=\"text-left pl-0\">{{orderQuote.vendor.vendorName}}</td>\r\n                  <td class=\"text-left pl-0\">{{orderQuote.eta}}</td>\r\n                  <td class=\"text-left pl-0\">{{orderQuote.qty}}</td>\r\n                  <td class=\"text-left pl-0\">{{orderQuote.amount}}</td>\r\n                  <td class=\"text-left pl-0\">{{orderQuote.totalAmount}}</td>\r\n                  <td *ngFor=\"let orderQuotesSpec of orderQuote.orderVendorQuoteItemSpecs\" class=\"text-left pl-0\">\r\n                    {{orderQuotesSpec.specValue}}\r\n                  </td>\r\n                  <td class=\"text-right pr-0\">\r\n                    <div class=\"attachment-button mr-3\"><i class=\"fas fa-plus-square\"></i></div>\r\n                    <div class=\"attachment-button mr-3\"><i class=\"fas fa-pen\"></i></div>\r\n                    <div class=\"attachment-button\"><i class=\"fas fa-times\"></i></div>\r\n                  </td>\r\n                </tr>\r\n                <tr class=\"add-new-row\">\r\n                  <td></td>\r\n                  <td class=\"text-left pl-0\" colspan=\"9\">\r\n                    <div class=\"attachment-button\">\r\n                      <i class=\"fas fa-plus\"></i>\r\n                    </div>\r\n                  </td>\r\n                </tr>\r\n              </tbody>\r\n            </table>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n<div class=\"card-row hide\">\r\n  <div class=\"row\">\r\n    <div class=\"col-md-6\">\r\n      <br>\r\n      <h6>Attachments</h6>\r\n      <!--<mat-card class=\"attachments-card\">\r\n      <ul class=\"file-list\">\r\n        <li class=\"file-item\">\r\n          <div class=\"file-name\">Archer Hotel signed doc.pdg</div>\r\n          <div class=\"control-buttons\">\r\n            <button class=\"download control-btn mr-1\"><i class=\"fas fa-long-arrow-alt-down\"></i></button>\r\n            <button class=\"remove control-btn\"><i class=\"fas fa-times\"></i></button>\r\n          </div>\r\n        </li>\r\n        <li class=\"file-item\">\r\n          <div class=\"file-name\">this is g doc es.img</div>\r\n          <div class=\"control-buttons\">\r\n            <button class=\"download control-btn mr-1\"><i class=\"fas fa-long-arrow-alt-down\"></i></button>\r\n            <button class=\"remove control-btn\"><i class=\"fas fa-times\"></i></button>\r\n          </div>\r\n        </li>\r\n      </ul>-->\r\n      <!--Drag and drop zone -start--->\r\n      <div class=\"add-project\">\r\n        <div class=\"overlay\" *ngIf='openDeleteAttachmentModal'></div>\r\n        <div class=\"modal-add-project\" [ngClass]=\"{'active':openDeleteAttachmentModal}\">\r\n          <div class=\"card-header\">\r\n            <span>\r\n              <strong>\r\n                <h5>Are you sure you want to delete the project attachment?</h5>\r\n              </strong>\r\n            </span>\r\n            <div class=\"close-project-button\" (click)=\"cancelDeleteAttachment()\">\r\n              <i class=\"fas fa-times\"></i>\r\n            </div>\r\n          </div>\r\n          <div class=\"modal-body\">\r\n            <div class=\"row button-wrapper\">\r\n              <button class=\"btn btn-block btn-outline-primary\" (click)=\"cancelDeleteAttachment()\">Cancel</button>\r\n              <button class=\"btn btn-block btn-outline-primary\" (click)=\"yesDeleteAttachment()\">Yes</button>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n\r\n      <mat-card class=\"attachments-card\">\r\n\r\n        <table class=\"table  datatable  attachment-table\" [mfData]=\"orderAttachments.orderAttachments\"\r\n               #mf=\"mfDataTable\" [mfRowsOnPage]=\"10\">\r\n          <tbody>\r\n            <tr *ngFor=\"let item of mf.data\">\r\n              <td class=\"link text-left pl-0\" (click)=\"openShowAttachment(item.id)\">{{item.fileName}}{{item.fileExtantion}}</td>\r\n              <td class=\"text-right pr-0\">\r\n                <div class=\"attachment-button mr-3\" (click)=\"downloadOrderAttachment(item.id)\"><i class=\"fa fa-arrow-down\"></i></div>\r\n                <div class=\"attachment-button text-right\" (click)=\"openDeleteAttachment(item.id)\"><i class=\"fas fa-times\"></i></div>\r\n              </td>\r\n            </tr>\r\n          </tbody>\r\n        </table>\r\n        <div ng2FileDrop [ngClass]=\"{'nv-file-over': hasBaseDropZoneOver}\" (fileOver)=\"fileOverBase($event)\" (onFileDrop)=\"dropped($event)\"\r\n             [uploader]=\"uploader\" class=\"table-responsive col-md-12 p-0\">\r\n          <span *ngIf=\"showSpinner\">\r\n            <mat-progress-bar mode=\"indeterminate\"></mat-progress-bar>\r\n          </span>\r\n          <div class=\"drag drag-zone\">\r\n            <span *ngIf=\"!showSpinner\">Drag and drop files here</span>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-md-12 d-flex space-between p-0 align-items-center control-btn\">\r\n          <button class=\"btn mr-3 download-button\" (click)=\"downloadAllOrderAttachments(currentOrder.id)\">Download All</button>\r\n          <label class=\"custom-file-upload\">\r\n            <input (change)=\"inputAdd($event)\" type=\"file\" multiple />\r\n            Add New Files\r\n          </label>\r\n        </div>\r\n      </mat-card>\r\n      <!--Drag and drop zone -end--->\r\n      <!--<div class=\"drag-zone\">\r\n        Drag and drop files here\r\n      </div>-->\r\n      <div class=\"add-project\">\r\n        <div class=\"overlay\" *ngIf='showAttachmentImg'></div>\r\n        <div class=\"modal-add-project\" [ngClass]=\"{'active':showAttachmentImg}\">\r\n          <div class=\"card-header\">\r\n            <span>\r\n              <strong>\r\n                <h5>Attachment: {{currentOrderAttachment.fileName}}</h5>\r\n              </strong>\r\n            </span>\r\n            <div class=\"close-project-button\" (click)=\"closeShowAttachment()\">\r\n              <i class=\"fas fa-times\"></i>\r\n            </div>\r\n          </div>\r\n          <div class=\"modal-body\">\r\n            <div>\r\n              <img src=\"{{currentOrderAttachment.link}}\" width=\"200px;\" height=\"200px;\" />\r\n            </div>\r\n            <div class=\"row button-wrapper\">\r\n              <button class=\"btn btn-block btn-outline-primary\" (click)=\"closeShowAttachment()\">Close</button>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"add-project\">\r\n        <div class=\"overlay\" *ngIf='showAttachedTxt'></div>\r\n        <div class=\"modal-show-pdf\" [ngClass]=\"{'active':showAttachedTxt}\">\r\n          <div class=\"card-header\">\r\n            <span>\r\n              <strong>\r\n                <h5>Attachment: {{currentOrderAttachment.fileName}}</h5>\r\n              </strong>\r\n            </span>\r\n            <div class=\"close-project-button\" (click)=\"closeShowAttachment()\">\r\n              <i class=\"fas fa-times\"></i>\r\n            </div>\r\n          </div>\r\n          <div class=\"modal-body\">\r\n            <div class=\"text-container\">\r\n              <p>{{attachmentTxtText}}</p>\r\n            </div>\r\n            <div class=\"row button-wrapper\">\r\n              <button class=\"btn btn-block btn-outline-primary\" (click)=\"closeShowAttachment()\">Close</button>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"add-project\">\r\n        <div class=\"overlay\" *ngIf='showAttachedPdf'></div>\r\n        <div class=\"modal-show-pdf\" [ngClass]=\"{'active':showAttachedPdf}\">\r\n          <div class=\"card-header\">\r\n            <span>\r\n              <strong>\r\n                <h5>Attachment: {{currentOrderAttachment.fileName}}</h5>\r\n              </strong>\r\n            </span>\r\n            <div class=\"close-project-button\" (click)=\"closeShowAttachment()\">\r\n              <i class=\"fas fa-times\"></i>\r\n            </div>\r\n          </div>\r\n          <div class=\"modal-body\">\r\n            <div class=\"pdf-container\">\r\n              <pdf-viewer [(src)]=\"attachmentPdf\" [original-size]=\"false\" [show-all]=\"true\" [render-text-mode]=\"true\"\r\n                          [autoresize]=\"true\">\r\n              </pdf-viewer>\r\n            </div>\r\n            <div class=\"row button-wrapper\">\r\n              <button class=\"btn btn-block btn-outline-primary\" (click)=\"closeShowAttachment()\">Close</button>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n\r\n    </div>\r\n    <div class=\"col-md-6\">\r\n      <br>\r\n      <h6>Activity</h6>\r\n      <mat-card class=\"activity-card\">\r\n        <div class=\"row height-100\">\r\n          <ul class=\"activity-list col-md-3\">\r\n            <li class=\"activity-item selected\">\r\n              Order Activity\r\n            </li>\r\n            <li class=\"activity-item\">\r\n              Task 1 Activity\r\n            </li>\r\n            <li class=\"activity-item\">\r\n              Task 2 Activity\r\n            </li>\r\n          </ul>\r\n          <div class=\"system-log-wrapper col-md-9\">\r\n            <ul class=\"system-log-list\">\r\n              <li *ngFor=\"let item of logs.orderLogActivities; let i = index\" class=\"log-item\">\r\n                <div class=\"log-description\">\r\n                  {{item.log}}\r\n                </div>\r\n                <div class=\"send-from\">{{item.firstName}} {{item.lastName}}</div>\r\n                <div class=\"separator\">•</div>\r\n                <div class=\"time\"> {{item.dateModified}}</div>\r\n              </li>\r\n            </ul>\r\n            <button class=\"hide-log-btn\">Hide system logs</button>\r\n            <div class=\"report-wrapper\">\r\n              <textarea class=\"report-text\"></textarea>\r\n              <button class=\"send\"><i class=\"fab fa-telegram-plane\"></i></button>\r\n            </div>\r\n\r\n          </div>\r\n        </div>\r\n      </mat-card>\r\n    </div>\r\n  </div>\r\n</div>\r\n<div class=\"card-row\">\r\n  <div class=\"row\">\r\n    <div class=\"col-md-12\">\r\n      <br>\r\n      <h6>Tasks</h6>\r\n      <mat-card class=\"tasks-card\">\r\n\r\n      </mat-card>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"add-project\">\r\n  <div class=\"overlay\" *ngIf='openOrderVendorQuoteModal'></div>\r\n  <div class=\"modal-add-project\" [ngClass]=\"{'active':openOrderVendorQuoteModal}\">\r\n    <div class=\"card-header\">\r\n      <span>\r\n        <strong>\r\n          <h5>Are you sure you want to confirm this vendor quote ?</h5>\r\n        </strong>\r\n      </span>\r\n      <div class=\"close-project-button\" (click)=\"cancelOrderVendorQuoteAttachment()\">\r\n        <i class=\"fas fa-times\"></i>\r\n      </div>\r\n    </div>\r\n    <div class=\"modal-body\">\r\n      <div class=\"row button-wrapper\">\r\n        <button class=\"btn btn-block btn-outline-primary\" (click)=\"cancelOrderVendorQuoteAttachment()\">Cancel</button>\r\n        <button class=\"btn btn-block btn-outline-primary\" (click)=\"confirmOrderVendorQuoteAttachment()\">Yes</button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/orders/order-details/order-details.component.scss":
/*!**********************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/orders/order-details/order-details.component.scss ***!
  \**********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".mt-4 {\n  margin-top: 40px; }\n\n.height-100 {\n  height: 100%; }\n\n.modal-add-project img {\n  width: 100%; }\n\n.btn-outline-secondary {\n  border: 0;\n  background: transparent;\n  color: #0077c5;\n  font-size: 18px;\n  margin-top: 20px; }\n\n.detail-project-link {\n  color: #0077c5;\n  font-size: 14px;\n  text-decoration: none; }\n\n.detail-project-link:visited {\n    color: #0077c5; }\n\n.detail-project-link:hover {\n    text-decoration: none;\n    color: #0077c5; }\n\n.order-title-wrapper {\n  display: flex;\n  justify-content: space-between;\n  margin-bottom: 5px; }\n\n.order-title-wrapper .order-title {\n    font-size: 24px;\n    font-weight: bold;\n    margin-bottom: 0; }\n\n.order-title-wrapper .edit-order {\n    height: 24px;\n    width: 24px;\n    cursor: pointer; }\n\n.custom-file-upload {\n  border: 1px solid #ccc;\n  display: inline-block;\n  color: #fff;\n  background-color: #20a8d8;\n  border-color: #20a8d8;\n  padding: 6px 12px;\n  cursor: pointer; }\n\nlabel {\n  margin-bottom: 0px; }\n\ninput[type=\"file\"] {\n  display: none; }\n\np {\n  margin-top: 1rem !important;\n  margin-bottom: 1rem; }\n\n.custom-table {\n  border-spacing: 0px 15px;\n  border-collapse: separate !important; }\n\n.custom-table thead tr th {\n    border-bottom: none !important;\n    border-top: none !important; }\n\n.custom-table tbody tr {\n    border-bottom: 20px solid white !important;\n    background: #F2F4F8 !important; }\n\n.custom-table tbody tr:after {\n      content: '';\n      border-bottom: 1px solid black;\n      position: absolute;\n      bottom: 0; }\n\n.custom-table tbody tr .info-task .success {\n      background: #4CAF50;\n      height: 48px;\n      width: 48px;\n      position: relative;\n      border-radius: 50%; }\n\n.custom-table tbody tr .info-task .success .counter {\n        position: absolute;\n        height: 15px;\n        width: 15px;\n        border: 1px solid #4CAF50;\n        border-radius: 50%;\n        top: 0;\n        right: 0;\n        background: white;\n        display: flex;\n        justify-content: center;\n        align-items: center;\n        font-size: 10px;\n        font-weight: bold; }\n\n.custom-table tbody tr .info-task .success:before {\n        content: \"\";\n        position: absolute;\n        left: 20px;\n        top: 12px;\n        width: 10px;\n        height: 18px;\n        border: solid white;\n        border-width: 0 3px 3px 0;\n        -webkit-transform: rotate(45deg);\n        transform: rotate(45deg); }\n\n.custom-table tbody tr .info-task .error {\n      background: #CD3232;\n      height: 48px;\n      width: 48px;\n      position: relative;\n      border-radius: 50%;\n      text-align: center;\n      vertical-align: middle;\n      display: table-cell;\n      color: white; }\n\n.custom-table tbody tr .info-task .error .counter-error {\n        position: absolute;\n        height: 15px;\n        width: 15px;\n        border: 1px solid #CD3232;\n        border-radius: 50%;\n        top: 0;\n        right: 0;\n        background: white;\n        display: flex;\n        justify-content: center;\n        align-items: center;\n        font-size: 10px;\n        font-weight: bold;\n        color: black; }\n\n.custom-table tbody tr td {\n      border-top: 1px solid #D0D0D0;\n      border-bottom: 1px solid #D0D0D0;\n      vertical-align: middle; }\n\n.custom-table tbody tr td:first-child {\n        border-left: 1px solid #D0D0D0; }\n\n.custom-table tbody tr td:last-child {\n        border-right: 1px solid #D0D0D0; }\n\n.custom-table tbody tr:hover {\n      background: #d5d5d5 !important; }\n\n.custom-table tfoot tr td {\n    border-top: none !important; }\n\n.selected-game {\n  display: inline-block;\n  margin-right: 15px;\n  font-size: 15px;\n  background: #B9CFEA;\n  margin-bottom: 10px;\n  padding: 2px 11px 4px 11px;\n  border-radius: 8px; }\n\n.delete-game {\n  font-size: 10px;\n  padding: 3px 6px;\n  vertical-align: middle;\n  cursor: pointer; }\n\n#newProject {\n  width: 180px;\n  height: 45px;\n  font-size: large; }\n\n.add-project .overlay {\n  position: fixed;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  background: rgba(0, 0, 0, 0.4);\n  z-index: 10000; }\n\n.add-project .modal-add-project {\n  position: fixed;\n  background: #fff;\n  z-index: 10000;\n  top: -100%;\n  left: calc(50% - 200px);\n  width: 400px; }\n\n.add-project .modal-add-project.active {\n    top: 200px;\n    left: calc(50% - 200px);\n    transition: top .5s; }\n\n.add-project .modal-add-project .row {\n    width: 100%; }\n\n.add-project .modal-add-project .row .mt-10 {\n      margin-top: 15px; }\n\n.add-project .modal-add-project .button-wrapper {\n    display: flex;\n    justify-content: flex-end;\n    margin-top: 10px; }\n\n.add-project .modal-add-project .button-wrapper .btn {\n      width: 100px;\n      margin: 0;\n      margin-left: 10px; }\n\n.add-project .modal-show-pdf {\n  position: fixed;\n  background: #fff;\n  z-index: 10000;\n  top: -100%;\n  left: calc(50% - 400px);\n  width: 800px; }\n\n.add-project .modal-show-pdf .modal-body {\n    padding-right: 0; }\n\n.add-project .modal-show-pdf .modal-body .pdf-container {\n      max-height: 400px;\n      overflow-y: auto; }\n\n.add-project .modal-show-pdf .modal-body .text-container {\n      max-height: 400px;\n      overflow-y: auto; }\n\n.add-project .modal-show-pdf.active {\n    top: 200px;\n    left: calc(50% - 400px);\n    transition: top .5s; }\n\n.add-project .modal-show-pdf .row {\n    width: 100%; }\n\n.add-project .modal-show-pdf .row .mt-10 {\n      margin-top: 15px; }\n\n.add-project .modal-show-pdf .button-wrapper {\n    display: flex;\n    justify-content: flex-end;\n    margin-top: 10px; }\n\n.add-project .modal-show-pdf .button-wrapper .btn {\n      width: 100px;\n      margin: 0;\n      margin-left: 10px; }\n\n.save-attachment .overlay {\n  position: fixed;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  background: rgba(0, 0, 0, 0.4);\n  z-index: 10000; }\n\n.save-attachment .modal-save-attachment {\n  position: fixed;\n  background: #fff;\n  z-index: 10000;\n  top: -100%;\n  left: calc(50% - 400px);\n  width: 900px; }\n\n.save-attachment .modal-save-attachment.active {\n    top: 200px;\n    left: calc(50% - 400px);\n    transition: top .5s; }\n\n.save-attachment .modal-save-attachment .row {\n    width: 100%; }\n\n.save-attachment .modal-save-attachment .row .mt-10 {\n      margin-top: 15px; }\n\n.save-attachment .modal-save-attachment .button-wrapper {\n    display: flex;\n    justify-content: flex-end;\n    margin-top: 10px; }\n\n.save-attachment .modal-save-attachment .button-wrapper .btn {\n      width: 100px;\n      margin: 0;\n      margin-left: 10px; }\n\n.project-list-button {\n  border: none;\n  background: none;\n  color: #20A8D8;\n  padding: 0px;\n  margin-left: 16px;\n  margin-bottom: 20px; }\n\n.close-project-button {\n  margin-left: auto; }\n\n.attachment-button {\n  display: inline;\n  margin: 0;\n  padding: 0;\n  border: 0;\n  background: transparent;\n  color: #0077c5;\n  cursor: pointer; }\n\n.button-wrapper {\n  vertical-align: bottom;\n  margin-top: auto; }\n\n.card-body.project-details.project {\n  border: 1px solid #e1e6ef; }\n\n.card-body {\n  padding: 16px !important; }\n\n.card-header {\n  padding: 0.75rem 1.25rem;\n  margin-bottom: 0;\n  background-color: #f9f9fa;\n  display: flex;\n  border-bottom: 0px;\n  border: 1px solid #e1e6ef; }\n\n.project-details-input-wrapper.project {\n  display: flex; }\n\n.row {\n  display: flex;\n  flex-wrap: wrap;\n  margin-left: 0 !important; }\n\n.project-name-description-wrapper {\n  display: flex; }\n\n.card {\n  border: 0px !important; }\n\n.edit-project-button {\n  margin-left: auto; }\n\n.attachment-table {\n  text-align: center; }\n\n.attachment-table thead th {\n    border-top: none !important;\n    border-bottom: none !important; }\n\n.attachment-table tbody td {\n    border-top: none !important;\n    border-bottom: none !important; }\n\n.attachment-table tbody .link {\n    color: deepskyblue !important; }\n\n.edit-button-wrapper {\n  display: flex;\n  justify-content: flex-end;\n  margin-top: 20px; }\n\n.edit-button-wrapper .btn {\n    width: 100px;\n    margin: 0;\n    margin-left: 10px; }\n\n.mat-form-field.mat-focused.mat-form-field-ripple {\n  background-color: #20a8d8 !important; }\n\n.mat-form-field-ripple {\n  background-color: #20a8d8 !important; }\n\ntextarea.mat-autosize {\n  resize: auto !important;\n  overflow: hidden !important; }\n\n.item-detail {\n  padding: 0; }\n\n.item-detail-info, .item-detail-price {\n  height: 100%; }\n\n@media (max-width: 1636px) {\n    .item-detail-info, .item-detail-price {\n      padding: 15px 0; }\n      .item-detail-info .table-box, .item-detail-price .table-box {\n        padding: 37px 25px 48.7px 15px; } }\n\n@media (max-width: 1636px) and (max-width: 1636px) {\n      .item-detail-info .table-box, .item-detail-price .table-box {\n        padding: 15px; } }\n\n.item-detail-price {\n  padding: 51px 119px 53px 121.9px; }\n\n@media (max-width: 1636px) {\n    .item-detail-price {\n      padding: 15px; } }\n\n.item-detail-info {\n  padding: 51px 0px 53px 105px; }\n\n@media (max-width: 1636px) {\n    .item-detail-info {\n      padding: 15px; } }\n\n.table-header {\n  color: #636363;\n  font-size: 12px; }\n\n.table-box {\n  border: 1px solid #d4d4d4;\n  padding: 37px 45px 48.7px 35px; }\n\n@media (max-width: 1830px) {\n    .table-box {\n      padding: 10px; } }\n\n.table-box table {\n    margin: 0; }\n\n.table-box th {\n    padding-left: 0px;\n    border-top: none;\n    font-size: 14px;\n    color: #636363; }\n\n.table-box td {\n    font-size: 16px; }\n\n.table-box td.checkbox-cell {\n      font-size: 16px;\n      color: #707070; }\n\n.table-box td.checkbox-cell.checked {\n        color: #ffba00; }\n\n.table-box td.add-new-row {\n      padding-top: 36px; }\n\n.details-vendors-card {\n  padding: 0; }\n\n.details-vendors-card .select-all-vendors {\n    width: 100%;\n    padding: 10px;\n    text-align: left;\n    border-bottom: 1px solid #d4d4d4;\n    padding-top: 16.5px;\n    padding-left: 63px;\n    padding-bottom: 18px;\n    font-size: 14px;\n    color: #0077c5; }\n\n.details-vendors-card .select-all-vendors:hover {\n      background: #e9e9e9;\n      transition: .2s; }\n\n.details-vendors-card .vendor-list-wrapper {\n    border-right: 1px solid #d4d4d4; }\n\n.details-vendors-card .vendor-list {\n    list-style: none;\n    margin: 0;\n    padding: 0; }\n\n.details-vendors-card .vendor-list .vendor-item {\n      padding-left: 63px;\n      padding-top: 17px;\n      padding-bottom: 18px;\n      font-size: 14px;\n      cursor: pointer; }\n\n.details-vendors-card .vendor-list .vendor-item.selected {\n        position: relative;\n        background: #e9e9e9; }\n\n.details-vendors-card .vendor-list .vendor-item.selected::after {\n          position: absolute;\n          content: '\\f058';\n          font-family: \"Font Awesome 5 Free\";\n          left: 20px;\n          color: #ffba00;\n          top: 18px; }\n\n.details-vendors-card .vendor-list .vendor-item:hover {\n        background: #e9e9e9;\n        transition: .2s; }\n\n.details-vendors-card .add-new-vendor {\n    width: 100%;\n    text-align: left;\n    padding: 12px 0;\n    padding-left: 63px; }\n\n.details-vendors-card .vendor-info {\n    padding: 52px 134px 54px 115px; }\n\n@media (max-width: 1636px) {\n      .details-vendors-card .vendor-info {\n        padding: 15px; }\n        .details-vendors-card .vendor-info .table-box {\n          margin-right: 15px; } }\n\n.attachments-card, .activity-card {\n  padding: 43.9px 120px 85px 116.7px;\n  height: calc(100% - 65px); }\n\n@media (max-width: 1636px) {\n  .attachments-card {\n    padding: 15px; } }\n\n.attachments-card .file-list {\n  list-style: none;\n  padding: 0; }\n\n.attachments-card .file-list .file-item {\n    display: flex;\n    justify-content: space-between; }\n\n.attachments-card .file-list .file-name {\n    font-size: 16px;\n    color: #0077c5; }\n\n.attachments-card .control-btn {\n  margin-top: 15px; }\n\n.attachments-card .control-btn .btn {\n    padding: 0;\n    background: transparent;\n    border: none;\n    font-size: 16px;\n    color: #0077c5; }\n\n.attachments-card .control-btn .custom-file-upload {\n    padding: 0;\n    background: transparent;\n    border: none;\n    font-size: 16px;\n    color: #0077c5; }\n\n.attachments-card .drag-zone {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  width: 100%;\n  height: 72px;\n  font-size: 16px;\n  color: #636363;\n  border: 2px dashed #707070;\n  border-right-color: #D0D0D0;\n  border-left-color: #D0D0D0;\n  border-top-color: #d4d4d4;\n  border-bottom-color: #d4d4d4; }\n\n.hide {\n  overflow: hidden; }\n\n.tasks-card {\n  height: 806px;\n  background: #e9e9e9; }\n\n.order-detail-block .details-card {\n  padding-top: 38px !important;\n  padding-bottom: 0px !important; }\n\n@media (max-width: 1636px) {\n    .order-detail-block .details-card {\n      padding: 15px !important; } }\n\n.order-control-btn {\n  width: auto !important;\n  font-size: 14px;\n  border: none;\n  background: transparent;\n  color: #0077c5;\n  padding: 0;\n  margin: 0; }\n\n.order-control-btn.cancel {\n    margin-right: 18px !important; }\n\n.activity-card {\n  padding: 0px !important; }\n\n.activity-card .activity-list {\n    list-style: none;\n    margin: 0;\n    padding: 0;\n    height: 100%;\n    border-right: 1px solid #d4d4d4; }\n\n.activity-card .activity-list .activity-item {\n      padding-left: 63px;\n      padding-top: 17px;\n      padding-bottom: 18px;\n      font-size: 14px;\n      cursor: pointer; }\n\n@media (max-width: 1636px) {\n        .activity-card .activity-list .activity-item {\n          padding-left: 20px; } }\n\n.activity-card .activity-list .activity-item.selected {\n        position: relative;\n        background: #e9e9e9; }\n\n.activity-card .activity-list .activity-item:hover {\n        background: #e9e9e9;\n        transition: .2s; }\n\n.activity-card .system-log-wrapper {\n    padding: 64px 120px 32px 154px; }\n\n@media (max-width: 1636px) {\n      .activity-card .system-log-wrapper {\n        padding: 15px;\n        padding-right: 30px; } }\n\n.activity-card .system-log-wrapper .system-log-list {\n      overflow: auto;\n      max-height: 267px;\n      list-style: none;\n      padding: 0;\n      margin: 0; }\n\n.activity-card .system-log-wrapper .log-item {\n      display: flex;\n      flex-wrap: wrap; }\n\n.activity-card .system-log-wrapper .log-item:not(:last-child) {\n        margin-bottom: 13px; }\n\n.activity-card .system-log-wrapper .log-item .log-description {\n        width: 100%;\n        font-size: 12px;\n        line-height: 1.25; }\n\n.activity-card .system-log-wrapper .log-item .send-from, .activity-card .system-log-wrapper .log-item .separator, .activity-card .system-log-wrapper .log-item .time {\n        font-size: 8px;\n        color: #757575; }\n\n.activity-card .system-log-wrapper .log-item .separator {\n        margin: 0 5px; }\n\n.activity-card .system-log-wrapper .hide-log-btn {\n      background: none;\n      border: none;\n      padding: 0;\n      margin: 0;\n      margin-top: 29px;\n      font-size: 8px;\n      color: #0077c5; }\n\n.activity-card .system-log-wrapper .report-wrapper {\n      position: relative; }\n\n.activity-card .system-log-wrapper .report-wrapper .report-text {\n        resize: vertical;\n        width: 100%;\n        height: 61px;\n        min-height: 61px;\n        padding: 10px;\n        font-size: 12px;\n        padding-right: 40px;\n        border: 1px solid #757575; }\n\n.activity-card .system-log-wrapper .report-wrapper .send {\n        position: absolute;\n        color: #0077c5;\n        right: 6px;\n        top: 50%;\n        font-size: 24px;\n        margin-top: -19px;\n        border: 0px;\n        background: transparent; }\n"

/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/orders/order-details/order-details.component.ts":
/*!********************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/orders/order-details/order-details.component.ts ***!
  \********************************************************************************************************/
/*! exports provided: OrderDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderDetailsComponent", function() { return OrderDetailsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var app_shared_constans_projects_and_orders_constans__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/shared/constans/projects-and-orders.constans */ "./src/app/shared/constans/projects-and-orders.constans.ts");
/* harmony import */ var file_saver__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! file-saver */ "./node_modules/file-saver/dist/FileSaver.min.js");
/* harmony import */ var file_saver__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(file_saver__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var app_shared_providers_services_order_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/shared/providers/services/order.service */ "./src/app/shared/providers/services/order.service.ts");
/* harmony import */ var app_shared_providers_services_customer_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! app/shared/providers/services/customer.service */ "./src/app/shared/providers/services/customer.service.ts");
/* harmony import */ var app_shared_providers_services_noitification_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! app/shared/providers/services/noitification.service */ "./src/app/shared/providers/services/noitification.service.ts");
/* harmony import */ var app_shared_providers_services_project_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! app/shared/providers/services/project.service */ "./src/app/shared/providers/services/project.service.ts");
/* harmony import */ var app_shared_models_order_get_order_view__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! app/shared/models/order/get-order-view */ "./src/app/shared/models/order/get-order-view.ts");
/* harmony import */ var app_shared_models_customer_get_all_customer_view__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! app/shared/models/customer/get-all-customer.view */ "./src/app/shared/models/customer/get-all-customer.view.ts");
/* harmony import */ var app_shared_models_order_update_order_view__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! app/shared/models/order/update-order.view */ "./src/app/shared/models/order/update-order.view.ts");
/* harmony import */ var app_shared_models_users_get_all_users_view__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! app/shared/models/users/get-all-users.view */ "./src/app/shared/models/users/get-all-users.view.ts");
/* harmony import */ var app_shared_models_project_reassign_project_manager_view__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! app/shared/models/project/reassign-project-manager.view */ "./src/app/shared/models/project/reassign-project-manager.view.ts");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ng2-file-upload */ "./node_modules/ng2-file-upload/index.js");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(ng2_file_upload__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var app_shared_providers_services_order_attachment_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! app/shared/providers/services/order-attachment.service */ "./src/app/shared/providers/services/order-attachment.service.ts");
/* harmony import */ var app_shared_models_order_attachment_get_order_attachments_by_order_id_view__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! app/shared/models/order-attachment/get-order-attachments-by-order-id.view */ "./src/app/shared/models/order-attachment/get-order-attachments-by-order-id.view.ts");
/* harmony import */ var app_shared_models_order_attachment_delete_order_attachment_view__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! app/shared/models/order-attachment/delete-order-attachment.view */ "./src/app/shared/models/order-attachment/delete-order-attachment.view.ts");
/* harmony import */ var app_shared_models_order_attachment_get_order_attachment_by_orderId_view__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! app/shared/models/order-attachment/get-order-attachment-by-orderId.view */ "./src/app/shared/models/order-attachment/get-order-attachment-by-orderId.view.ts");
/* harmony import */ var app_shared_models_order_log_activity_get_all_order_log_activities_by_orderId_view__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! app/shared/models/order-log-activity/get-all-order-log-activities-by-orderId.view */ "./src/app/shared/models/order-log-activity/get-all-order-log-activities-by-orderId.view.ts");
/* harmony import */ var app_shared_providers_services_order_log_activity_service__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! app/shared/providers/services/order-log-activity.service */ "./src/app/shared/providers/services/order-log-activity.service.ts");
/* harmony import */ var _shared_models_order_update_order_confirmed_order_vendor_quote_view__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ../../../../shared/models/order/update-order-confirmed-order-vendor-quote.view */ "./src/app/shared/models/order/update-order-confirmed-order-vendor-quote.view.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





















var OrderDetailsComponent = /** @class */ (function () {
    function OrderDetailsComponent(activateRoute, orderService, route, customerService, notificationService, constans, projectService, orderAttachmentService, orderLogActivitiesService) {
        this.activateRoute = activateRoute;
        this.orderService = orderService;
        this.route = route;
        this.customerService = customerService;
        this.notificationService = notificationService;
        this.constans = constans;
        this.projectService = projectService;
        this.orderAttachmentService = orderAttachmentService;
        this.orderLogActivitiesService = orderLogActivitiesService;
        //drag and drop variables
        this.hasBaseDropZoneOver = false;
        this.uploader = new ng2_file_upload__WEBPACK_IMPORTED_MODULE_13__["FileUploader"]({ url: this.constans.URL });
        this.showAttachmentImg = false;
        this.showAttachedPdf = false;
        this.showAttachedTxt = false;
        this.showAttachment = false;
        //spinner
        this.showSpinner = false;
        this.formValidation = false;
        //popup
        this.openSaveAttachment = false;
        this.openDeleteAttachmentModal = false;
        this.openOrderVendorQuoteModal = false;
        this.alreadyGetOrderDetails = false;
        this.activeEditOrder = false;
        this.logs = new app_shared_models_order_log_activity_get_all_order_log_activities_by_orderId_view__WEBPACK_IMPORTED_MODULE_18__["GetAllOrderLogActivitiesByOrderIdView"]();
        this.currentOrder = new app_shared_models_order_get_order_view__WEBPACK_IMPORTED_MODULE_8__["GetOrderView"]();
        this.currentOrderAttachment = new app_shared_models_order_attachment_get_order_attachment_by_orderId_view__WEBPACK_IMPORTED_MODULE_17__["GetOrderAttachmentByOrderIdView"]();
        this.orderAttachments = new app_shared_models_order_attachment_get_order_attachments_by_order_id_view__WEBPACK_IMPORTED_MODULE_15__["GetOrderAttachmentsByOrderIdView"]();
        this.deletedAttachment = new app_shared_models_order_attachment_delete_order_attachment_view__WEBPACK_IMPORTED_MODULE_16__["DeleteOrderAttachmentView"]();
        this.customers = new app_shared_models_customer_get_all_customer_view__WEBPACK_IMPORTED_MODULE_9__["GetAllCustomerView"]();
        this.projectManagers = new app_shared_models_users_get_all_users_view__WEBPACK_IMPORTED_MODULE_11__["GetAllUserView"]();
        this.tempOrderQuote = new app_shared_models_order_get_order_view__WEBPACK_IMPORTED_MODULE_8__["GetOrderOrderVendorQuoteViewItem"]();
        this.updateOrder = new app_shared_models_order_update_order_view__WEBPACK_IMPORTED_MODULE_10__["UpdateOrderView"]();
        this.updateProjectManager = new app_shared_models_project_reassign_project_manager_view__WEBPACK_IMPORTED_MODULE_12__["ReassignProjectManagerView"]();
        this.orderId = this.activateRoute.snapshot.params['id'];
        this.getOrderDetails(this.orderId);
        this.getCustomers();
        this.getProjectManager();
        this.getAllOrderLogActivities();
    }
    OrderDetailsComponent.prototype.ngOnInit = function () {
    };
    OrderDetailsComponent.prototype.cancelOrderVendorQuoteAttachment = function () {
        this.openOrderVendorQuoteModal = false;
    };
    OrderDetailsComponent.prototype.openOrderVendorQuoteAttachmentModal = function (orderQuote) {
        this.tempOrderQuote = orderQuote;
        this.openOrderVendorQuoteModal = true;
    };
    OrderDetailsComponent.prototype.confirmOrderVendorQuoteAttachment = function () {
        this.updateConfirmedOrderVendorQuote(this.tempOrderQuote);
        this.openOrderVendorQuoteModal = false;
    };
    //Update confirmed vendor quote
    OrderDetailsComponent.prototype.updateConfirmedOrderVendorQuote = function (orderQuote) {
        var _this = this;
        var updateOrderconfirmedVendorQuote = new _shared_models_order_update_order_confirmed_order_vendor_quote_view__WEBPACK_IMPORTED_MODULE_20__["UpdateOrderConfirmedOrderVendorQuoteView"]();
        updateOrderconfirmedVendorQuote.id = this.currentOrder.id;
        updateOrderconfirmedVendorQuote.confirmedOrderVendorQuoteId = orderQuote.id;
        orderQuote.isConfirmed = true;
        this.currentOrder.orderVendorQuotes.find(function (x) { return x.id == _this.currentOrder.confirmedVendorQuote.id; }).isConfirmed = false;
        this.orderService.updateConfirmedOrderVendoQuote(updateOrderconfirmedVendorQuote)
            .subscribe(function (response) {
            _this.notificationService.showSuccess(_this.constans.updateConfirmedOrderVenorQuoteMessage);
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
        this.currentOrder.confirmedVendorQuote = this.currentOrder.orderVendorQuotes.find(function (x) { return x.id == orderQuote.id; });
    };
    OrderDetailsComponent.prototype.getAllOrderLogActivities = function () {
        var _this = this;
        this.orderLogActivitiesService.getAllOrderLogActivities(this.orderId).subscribe(function (response) {
            _this.logs = response;
        });
    };
    OrderDetailsComponent.prototype.fileOverBase = function (event) {
        this.hasBaseDropZoneOver = event;
        console.log(event);
    };
    OrderDetailsComponent.prototype.dropped = function (fileList) {
        var invalidFiles = this.checkAttachmentExtensions(fileList);
        if (invalidFiles.length > 0) {
            this.notificationService.showWarning(this.constans.fileExtensionNotAllowedMessage);
        }
        this.showSpinner = false;
    };
    OrderDetailsComponent.prototype.openDeleteAttachment = function (id) {
        this.deletedAttachmentId = id;
        this.openDeleteAttachmentModal = true;
    };
    OrderDetailsComponent.prototype.cancelDeleteAttachment = function () {
        this.deletedAttachmentId = null;
        this.openDeleteAttachmentModal = false;
    };
    OrderDetailsComponent.prototype.yesDeleteAttachment = function () {
        this.deleteAttachment();
        this.openDeleteAttachmentModal = false;
    };
    OrderDetailsComponent.prototype.deleteAttachment = function () {
        var _this = this;
        this.deletedAttachment.id = this.deletedAttachmentId;
        this.orderAttachmentService.deleteAttachment(this.deletedAttachment).subscribe(function (response) {
            _this.notificationService.showSuccess(_this.constans.atachmentDeleteMessage);
            _this.getOrderAttachments(_this.currentOrder.id);
        });
        this.deletedAttachmentId = null;
    };
    OrderDetailsComponent.prototype.closeShowAttachment = function () {
        this.showAttachedTxt = false;
        this.showAttachment = false;
        this.showAttachmentImg = false;
        this.showAttachedPdf = false;
        this.currentOrderAttachment = new app_shared_models_order_attachment_get_order_attachment_by_orderId_view__WEBPACK_IMPORTED_MODULE_17__["GetOrderAttachmentByOrderIdView"]();
        this.currentPdfLink = "";
        this.attachmentTxtText = "";
    };
    OrderDetailsComponent.prototype.checkAttachmentExtensions = function (files) {
        var invalidFiles = [];
        for (var i = 0; i < files.length; i++) {
            this.showSpinner = true;
            var success = false;
            this.constans.allowableExtensions.forEach(function (ex) {
                var index = files[i].name.lastIndexOf(".");
                var extensions = files[i].name.substr(index + 1);
                if (ex == extensions) {
                    success = true;
                    return;
                }
            });
            if (!success) {
                invalidFiles.push(files[i].name);
                this.showSpinner = false;
            }
            if (success) {
                this.saveAttachments(files[i]);
            }
        }
        ;
        return invalidFiles;
    };
    //preview attachments
    OrderDetailsComponent.prototype.openShowAttachment = function (id) {
        var _this = this;
        this.orderAttachmentService.getOrderAttachment(id).subscribe(function (response) {
            _this.currentOrderAttachment = response;
            console.log(_this.currentOrderAttachment);
            if (_this.currentOrderAttachment.fileName.length > 35) {
                _this.currentOrderAttachment.fileName = _this.currentOrderAttachment.fileName.substring(0, 35) + "...";
            }
            _this.constans.allowableImageExtensions.forEach(function (x) {
                if (x === _this.currentOrderAttachment.fileExtantion) {
                    _this.showAttachmentImg = true;
                }
            });
            if (_this.currentOrderAttachment.fileExtantion === _this.constans.attacmentsData.pdf.extension) {
                _this.orderAttachmentService.downloadAttachmentToArray(id).subscribe(function (response) {
                    _this.attachmentPdf = window.URL.createObjectURL(response);
                });
                _this.showAttachedPdf = true;
            }
            if (_this.currentOrderAttachment.fileExtantion === _this.constans.attacmentsData.text.extension) {
                _this.orderAttachmentService.downloadAttachment(id).subscribe(function (data) {
                    _this.attachmentTxtText = data;
                });
                _this.showAttachedTxt = true;
            }
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    //download all attachments
    OrderDetailsComponent.prototype.downloadAllOrderAttachments = function (id) {
        var _this = this;
        this.orderAttachmentService.downloadAllOrderAttachment(id).subscribe(function (response) {
            var zip = response;
            var filename = _this.constans.attacmentsData.zip.filename;
            var blob = new Blob([zip], { type: _this.constans.attacmentsData.zip.type });
            file_saver__WEBPACK_IMPORTED_MODULE_3___default()(blob, filename);
        });
    };
    OrderDetailsComponent.prototype.inputAdd = function (event) {
        var fileList = event.target.files;
        var invalidFiles = this.checkAttachmentExtensions(fileList);
        if (invalidFiles.length > 0) {
            this.notificationService.showWarning(this.constans.fileExtensionNotAllowedMessage);
        }
    };
    //download files
    OrderDetailsComponent.prototype.downloadOrderAttachment = function (id) {
        var _this = this;
        this.orderAttachmentService.getOrderAttachment(id).subscribe(function (response) {
            _this.currentOrderAttachment = response;
            _this.orderAttachmentService.downloadAttachmentToBlob(id).subscribe(function (result) {
                var file = result;
                var filename = _this.currentOrderAttachment.fileName + _this.currentOrderAttachment.fileExtantion;
                if (_this.currentOrderAttachment.fileExtantion === _this.constans.attacmentsData.pdf.extension) {
                    var blob = new Blob([file], { type: _this.constans.attacmentsData.pdf.type });
                    file_saver__WEBPACK_IMPORTED_MODULE_3___default()(blob, filename);
                }
                if (_this.currentOrderAttachment.fileExtantion === _this.constans.attacmentsData.imageJpeg.extension) {
                    var blob = new Blob([file], { type: _this.constans.attacmentsData.imageJpeg.type });
                    file_saver__WEBPACK_IMPORTED_MODULE_3___default()(blob, filename);
                }
                if (_this.currentOrderAttachment.fileExtantion === _this.constans.attacmentsData.imagePng.extension) {
                    var blob = new Blob([file], { type: _this.constans.attacmentsData.imagePng.type });
                    file_saver__WEBPACK_IMPORTED_MODULE_3___default()(blob, filename);
                }
                if (_this.currentOrderAttachment.fileExtantion === _this.constans.attacmentsData.text.extension) {
                    var blob = new Blob([file], { type: _this.constans.attacmentsData.text.type });
                    file_saver__WEBPACK_IMPORTED_MODULE_3___default()(blob, filename);
                }
                if (_this.currentOrderAttachment.fileExtantion === _this.constans.attacmentsData.docx.extension) {
                    var blob = new Blob([file], { type: _this.constans.attacmentsData.docx.type });
                    file_saver__WEBPACK_IMPORTED_MODULE_3___default()(blob, filename);
                }
                if (_this.currentOrderAttachment.fileExtantion === _this.constans.attacmentsData.xlsx.extension) {
                    var blob = new Blob([file], { type: _this.constans.attacmentsData.xlsx.type });
                    file_saver__WEBPACK_IMPORTED_MODULE_3___default()(blob, filename);
                }
                if (_this.currentOrderAttachment.fileExtantion === _this.constans.attacmentsData.msg.extension) {
                    var blob = new Blob([file], { type: _this.constans.attacmentsData.msg.type });
                    file_saver__WEBPACK_IMPORTED_MODULE_3___default()(blob, filename);
                }
            });
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    OrderDetailsComponent.prototype.saveAttachments = function (file) {
        var _this = this;
        var formData = new FormData();
        formData.append("file", file, file.name);
        formData.append("orderId", this.currentOrder.id);
        this.orderAttachmentService.createOrderAttachment(formData).subscribe(function (response) {
            _this.notificationService.showSuccess(_this.constans.fileUploadeMessage + " " + file.name);
            _this.getOrderAttachments(_this.currentOrder.id);
        });
        this.openSaveAttachment = false;
        this.formValidation = true;
    };
    OrderDetailsComponent.prototype.getOrderAttachments = function (id) {
        var _this = this;
        console.log(id);
        this.orderAttachmentService.getAllOrderAttachmentsByOrderId(id).subscribe(function (response) {
            _this.orderAttachments = response;
            console.log(_this.orderAttachments);
            _this.showSpinner = false;
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    OrderDetailsComponent.prototype.getOrderDetails = function (orderId) {
        var _this = this;
        this.orderService.getOrderDetails(orderId).subscribe(function (response) {
            _this.currentOrder = response;
            console.log(_this.currentOrder);
            _this.activeEditOrder = false;
            _this.alreadyGetOrderDetails = true;
            console.log(_this.currentOrder);
            _this.getOrderAttachments(_this.currentOrder.id);
        });
    };
    OrderDetailsComponent.prototype.navigateToListOrders = function () {
        this.route.navigate(['projects-and-orders/orders/ordersKanban']);
    };
    OrderDetailsComponent.prototype.showProjectDetailsById = function (id) {
        this.route.navigate(['projects-and-orders/projects/projectdetails', id]);
    };
    OrderDetailsComponent.prototype.activeEditableOrder = function () {
        this.activeEditOrder = true;
    };
    OrderDetailsComponent.prototype.cancelUpdateOrder = function () {
        this.getOrderDetails(this.currentOrder.id);
    };
    OrderDetailsComponent.prototype.getCustomers = function () {
        var _this = this;
        this.customerService.getCustomers().subscribe(function (response) {
            _this.customers = response;
        });
    };
    OrderDetailsComponent.prototype.getProjectManager = function () {
        var _this = this;
        this.projectService.getProjectManager().subscribe(function (response) {
            _this.projectManagers = response;
            console.log(_this.projectManagers);
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    OrderDetailsComponent.prototype.saveChanges = function () {
        var _this = this;
        this.updateOrder.id = this.currentOrder.id;
        this.updateOrder.itemId = this.currentOrder.item.id;
        this.updateOrder.customerId = this.currentOrder.customer.id;
        this.updateOrder.projectId = this.currentOrder.project.id;
        this.updateOrder.date = this.currentOrder.date;
        this.updateOrder.dueDate = this.currentOrder.dueDate;
        this.updateOrder.memo = this.currentOrder.memo;
        this.updateOrder.status = this.currentOrder.status;
        this.updateProjectManager.projectId = this.currentOrder.project.id;
        //this.updateProjectManager.projectManagerId = this.currentOrder.project.user.id;
        this.projectService.reassignProjectManeger(this.updateProjectManager).subscribe(function (response) {
            _this.orderService.updateOrder(_this.updateOrder).subscribe(function (response) {
                _this.getOrderDetails(_this.currentOrder.id);
                _this.notificationService.showSuccess(_this.constans.orderUpdateMessage);
            });
        });
    };
    OrderDetailsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-order-details',
            template: __webpack_require__(/*! ./order-details.component.html */ "./src/app/projects-and-orders/project-and-orders/orders/order-details/order-details.component.html"),
            styles: [__webpack_require__(/*! ./order-details.component.scss */ "./src/app/projects-and-orders/project-and-orders/orders/order-details/order-details.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            app_shared_providers_services_order_service__WEBPACK_IMPORTED_MODULE_4__["OrderService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            app_shared_providers_services_customer_service__WEBPACK_IMPORTED_MODULE_5__["CustomerService"],
            app_shared_providers_services_noitification_service__WEBPACK_IMPORTED_MODULE_6__["NotificationService"],
            app_shared_constans_projects_and_orders_constans__WEBPACK_IMPORTED_MODULE_2__["ProjectsAndOrdersConstans"],
            app_shared_providers_services_project_service__WEBPACK_IMPORTED_MODULE_7__["ProjectService"],
            app_shared_providers_services_order_attachment_service__WEBPACK_IMPORTED_MODULE_14__["OrderAttachmentService"],
            app_shared_providers_services_order_log_activity_service__WEBPACK_IMPORTED_MODULE_19__["OrderLogActivityService"]])
    ], OrderDetailsComponent);
    return OrderDetailsComponent;
}());



/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/orders/order-kanban/order-kanban.component.html":
/*!********************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/orders/order-kanban/order-kanban.component.html ***!
  \********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<div class=\"content\">\r\n  <div class=\"row col-md-12 d-flex justify-content-between align-items-center\">\r\n      <h3 class=\"orders-name\">\r\n          Orders ({{ordersCount}})\r\n        </h3>\r\n        <div>\r\n          <button id=\"newOrders\" class=\"btn\"><span style=\"margin-right:12px\">+</span> New Order</button>\r\n        </div>\r\n      </div>\r\n      <div class=\"row col-md-12\">\r\n        <div class=\"col-md-3 d-flex align-items-center pl-0\">\r\n         <div class=\"form-group w-100 position-relative search-input\">\r\n            <i class=\"fa fa-search search-icon\"></i>\r\n            <input type=\"text\" class=\"form-control\"/>\r\n         </div>\r\n        </div>\r\n        <div class=\"filters-button col-md-1\">\r\n            <i class=\"fa fa-filter mr-2\"></i>\r\n          Filters\r\n        </div>\r\n        <div class=\"btn-group col-md-8 d-flex justify-content-end pr-0\" role=\"group\" aria-label=\"Basic example\">\r\n            <button type=\"button\" class=\"btn \" (click)=\"showListView()\"><i class=\"fa fa-list-ul\" aria-hidden=\"true\"></i></button>\r\n            <button type=\"button\" class=\"btn active\"><i class=\"fa fa-bars\" aria-hidden=\"true\"></i></button>\r\n          </div>\r\n      </div>\r\n  <div class=\"kanban\">\r\n    <div class=\"row\">\r\n\r\n      <div class=\"col-md-3\">\r\n        <div class=\"section-heading\">New</div>\r\n        <div class=\"drag-container\">\r\n          <div cdkDropList\r\n               #newList=\"cdkDropList\"\r\n               [cdkDropListData]=\"newOrder\"\r\n               [cdkDropListConnectedTo]=\"[activeList,processingList,doneList]\"\r\n               class=\"item-list\" (cdkDropListDropped)=\"drop($event)\">\r\n            <div class=\"item-box\" *ngFor=\"let item of newOrder\" (click)=\"showOrdersDetails(item.id)\" cdkDrag>\r\n\r\n                <div class=\"d-flex align-items-center mb-1\">\r\n                  <a (click)=\"callBack($event)\">\r\n                    <img class=\"order-blue\" src=\"../../../../../assets/img/order-2.svg\" alt=\"\">\r\n                  </a>\r\n                  <a class=\"underline-on-hover\" (click)=\"callBack($event)\">\r\n                    <span class=\"order-count\">{{item.numericId}}</span>\r\n                  </a>\r\n                  <a class=\"underline-on-hover\" (click)=\"callBack($event)\">{{item.date | date:'shortDate'}}</a>\r\n                </div>\r\n\r\n                <div class=\"clients mb-1\">\r\n                  <div>\r\n                    <span>Customer</span>\r\n                    <a class=\"underline-on-hover\" (click)=\"callBack($event)\">{{item.customer.name}}</a>\r\n                  </div>\r\n                  <div>\r\n                    <span>Item</span>\r\n                    <a class=\"underline-on-hover\" (click)=\"callBack($event)\">{{item.item.name}}</a>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"d-flex justify-content-between align-items-center mb-1\" style=\"font-size:16px;\">\r\n                  <div class=\"red-circle\">\r\n                    <a (click)=\"callBack($event)\">{{item.project.user.firstName[0]}}</a>\r\n                  </div>\r\n                  <span class=\"name-order\">\r\n                    <a class=\"underline-on-hover\" (click)=\"callBack($event)\"> {{item.project.user.firstName}}   {{item.project.user.lastName}}</a>\r\n                  </span>\r\n                  <div class=\"clipboard\">\r\n                    <a class=\"callback-icon\" (click)=\"callBack($event)\">\r\n                      <img src=\"../../../../../assets/img/clipboard-1.svg\" alt=\"\">\r\n                    </a>\r\n                    <a class=\"underline-on-hover\" (click)=\"callBack($event)\">0/1</a>\r\n                  </div>\r\n                </div>\r\n\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"col-md-3\">\r\n        <div class=\"section-heading\">Active</div>\r\n        <div class=\"drag-container\">\r\n          <div cdkDropList\r\n               #activeList=\"cdkDropList\"\r\n               [cdkDropListData]=\"activeOrder\"\r\n               [cdkDropListConnectedTo]=\"[newList,processingList,doneList]\"\r\n               class=\"item-list\" (cdkDropListDropped)=\"drop($event)\">\r\n            <div class=\"item-box\" *ngFor=\"let item of activeOrder\" (click)=\"showOrdersDetails(item.id)\" cdkDrag>\r\n\r\n                <div class=\"d-flex align-items-center mb-1\">\r\n                  <a (click)=\"callBack($event)\">\r\n                    <img class=\"order-blue\" src=\"../../../../../assets/img/order-2.svg\" alt=\"\">\r\n                  </a>\r\n                  <a class=\"underline-on-hover\" (click)=\"callBack($event)\">\r\n                    <span class=\"order-count\">{{item.numericId}}</span>\r\n                  </a>\r\n                  <a class=\"underline-on-hover\" (click)=\"callBack($event)\">{{item.date | date:'shortDate'}}</a>\r\n                </div>\r\n\r\n                <div class=\"clients mb-1\">\r\n                  <div>\r\n                    <span>Customer</span>\r\n                    <a class=\"underline-on-hover\" (click)=\"callBack($event)\">{{item.customer.name}}</a>\r\n                  </div>\r\n                  <div>\r\n                    <span>Item</span>\r\n                    <a class=\"underline-on-hover\" (click)=\"callBack($event)\">{{item.item.name}}</a>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"d-flex justify-content-between align-items-center mb-1\" style=\"font-size:16px;\">\r\n                  <div class=\"red-circle\">\r\n                    <a (click)=\"callBack($event)\">{{item.project.user.firstName[0]}}</a>\r\n                  </div>\r\n                  <span class=\"name-order\">\r\n                    <a class=\"underline-on-hover\" (click)=\"callBack($event)\"> {{item.project.user.firstName}}   {{item.project.user.lastName}}</a>\r\n                  </span>\r\n                  <div class=\"clipboard\">\r\n                    <a class=\"callback-icon\" (click)=\"callBack($event)\">\r\n                      <img src=\"../../../../../assets/img/clipboard-1.svg\" alt=\"\">\r\n                    </a>\r\n                    <a class=\"underline-on-hover\" (click)=\"callBack($event)\">0/1</a>\r\n                  </div>\r\n                </div>\r\n\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"col-md-3\">\r\n        <div class=\"section-heading\">Processing</div>\r\n        <div class=\"drag-container\">\r\n          <div cdkDropList\r\n               #processingList=\"cdkDropList\"\r\n               [cdkDropListData]=\"processingOrder\"\r\n               [cdkDropListConnectedTo]=\"[activeList,newList,doneList]\"\r\n               class=\"item-list\" (cdkDropListDropped)=\"drop($event)\">\r\n            <div class=\"item-box\" *ngFor=\"let item of processingOrder\" (click)=\"showOrdersDetails(item.id)\" cdkDrag>\r\n\r\n                <div class=\"d-flex align-items-center mb-1\">\r\n                  <a (click)=\"callBack($event)\">\r\n                    <img class=\"order-blue\" src=\"../../../../../assets/img/order-2.svg\" alt=\"\">\r\n                  </a>\r\n                  <a class=\"underline-on-hover\" (click)=\"callBack($event)\">\r\n                    <span class=\"order-count\">{{item.numericId}}</span>\r\n                  </a>\r\n                  <a class=\"underline-on-hover\" (click)=\"callBack($event)\">{{item.date | date:'shortDate'}}</a>\r\n                </div>\r\n\r\n                <div class=\"clients mb-1\">\r\n                  <div>\r\n                    <span>Customer</span>\r\n                    <a class=\"underline-on-hover\" (click)=\"callBack($event)\">{{item.customer.name}}</a>\r\n                  </div>\r\n                  <div>\r\n                    <span>Item</span>\r\n                    <a class=\"underline-on-hover\" (click)=\"callBack($event)\">{{item.item.name}}</a>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"d-flex justify-content-between align-items-center mb-1\" style=\"font-size:16px;\">\r\n                  <div class=\"red-circle\">\r\n                    <a (click)=\"callBack($event)\">{{item.project.user.firstName[0]}}</a>\r\n                  </div>\r\n                  <span class=\"name-order\">\r\n                    <a class=\"underline-on-hover\" (click)=\"callBack($event)\"> {{item.project.user.firstName}}   {{item.project.user.lastName}}</a>\r\n                  </span>\r\n                  <div class=\"clipboard\">\r\n                    <a class=\"callback-icon\" (click)=\"callBack($event)\">\r\n                      <img src=\"../../../../../assets/img/clipboard-1.svg\" alt=\"\">\r\n                    </a>\r\n                    <a class=\"underline-on-hover\" (click)=\"callBack($event)\">0/1</a>\r\n                  </div>\r\n                </div>\r\n\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"col-md-3\">\r\n        <div class=\"section-heading\">Done</div>\r\n        <div class=\"drag-container\">\r\n          <div cdkDropList #doneList=\"cdkDropList\"\r\n               [cdkDropListData]=\"doneOrder\"\r\n               [cdkDropListConnectedTo]=\"[processingList,activeList,newList]\"\r\n               class=\"item-list\" (cdkDropListDropped)=\"drop($event)\">\r\n            <div class=\"item-box\" *ngFor=\"let item of doneOrder\" (click)=\"showOrdersDetails(item.id)\" cdkDrag>\r\n\r\n                <div class=\"d-flex align-items-center mb-1\">\r\n                  <a (click)=\"callBack($event)\">\r\n                    <img class=\"order-blue\" src=\"../../../../../assets/img/order-2.svg\" alt=\"\">\r\n                  </a>\r\n                  <a class=\"underline-on-hover\" (click)=\"callBack($event)\">\r\n                    <span class=\"order-count\">{{item.numericId}}</span>\r\n                  </a>\r\n                  <a class=\"underline-on-hover\" (click)=\"callBack($event)\">{{item.date | date:'shortDate'}}</a>\r\n                </div>\r\n\r\n                <div class=\"clients mb-1\">\r\n                  <div>\r\n                    <span>Customer</span>\r\n                    <a class=\"underline-on-hover\" (click)=\"callBack($event)\">{{item.customer.name}}</a>\r\n                  </div>\r\n                  <div>\r\n                    <span>Item</span>\r\n                    <a class=\"underline-on-hover\" (click)=\"callBack($event)\">{{item.item.name}}</a>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"d-flex justify-content-between align-items-center mb-1\" style=\"font-size:16px;\">\r\n                  <div class=\"red-circle\">\r\n                    <a (click)=\"callBack($event)\">{{item.project.user.firstName[0]}}</a>\r\n                  </div>\r\n                  <span class=\"name-order\">\r\n                    <a class=\"underline-on-hover\" (click)=\"callBack($event)\"> {{item.project.user.firstName}}   {{item.project.user.lastName}}</a>\r\n                  </span>\r\n                  <div class=\"clipboard\">\r\n                    <a class=\"callback-icon\" (click)=\"callBack($event)\">\r\n                      <img src=\"../../../../../assets/img/clipboard-1.svg\" alt=\"\">\r\n                    </a>\r\n                    <a class=\"underline-on-hover\" (click)=\"callBack($event)\">0/1</a>\r\n                  </div>\r\n                </div>\r\n\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<!--<div class=\"d-flex align-items-center mb-1\">\r\n  <img class=\"order-blue\" src=\"../../../../../assets/img/order-2.svg\" alt=\"\">\r\n  <span class=\"order-count\">50</span>\r\n  <span class=\"order-date\">{{item.date | date:'shortDate'  }}</span>\r\n</div>\r\n<div class=\"clients mb-1\">\r\n  <div><span>Customer</span>{{item.customer.name}}</div>\r\n  <div><span>Item</span>{{item.item.name}}</div>\r\n</div>\r\n<div class=\"d-flex justify-content-between align-items-center mb-1\" style=\"font-size:16px;\">\r\n  <div class=\"red-circle\">\r\n    {{item.project.user.firstName[0]}}\r\n  </div>\r\n  <span class=\"name-order\">\r\n    {{item.project.user.firstName}}   {{item.project.user.lastName}}\r\n  </span>\r\n  <div class=\"clipboard\">\r\n    <img src=\"../../../../../assets/img/clipboard-1.svg\" alt=\"\">\r\n    <span>0/1</span>\r\n  </div>\r\n</div>-->\r\n"

/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/orders/order-kanban/order-kanban.component.scss":
/*!********************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/orders/order-kanban/order-kanban.component.scss ***!
  \********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".clickable-title-link {\n  color: rgba(var(--palette-neutral-100, 0, 0, 0), 1); }\n\n.underline-on-hover:hover {\n  text-decoration: underline !important; }\n\n.underline-on-hover {\n  font-size: 16px; }\n\n.section-heading {\n  padding: 5px 10px 11px 0px;\n  font-size: 18px;\n  font-weight: bold; }\n\n.drag-container {\n  width: 100%;\n  height: auto;\n  max-width: 100%;\n  margin: 0 25px 25px 0;\n  display: inline-block;\n  vertical-align: top;\n  background-color: #E9ECEF;\n  padding: 15px; }\n\n.item-list {\n  min-height: 60px;\n  border-radius: 4px;\n  display: block; }\n\n.item-box:hover {\n  cursor: pointer; }\n\n.item-box {\n  border: solid 1px #808080;\n  margin-bottom: 15px;\n  color: rgba(0, 0, 0, 0.87);\n  box-sizing: border-box;\n  cursor: move;\n  background: white;\n  font-size: 13px;\n  padding: 15px; }\n\n.item-box p {\n    margin-bottom: 5px;\n    color: #636363; }\n\n.item-box p span {\n      color: black; }\n\n.item-box p .date-proj {\n      margin-left: 25px; }\n\n.item-box p .red {\n      background: #CD3232;\n      display: block;\n      height: 25px;\n      width: 25px;\n      margin-right: 10px;\n      border-radius: 50%; }\n\n.item-box p .blue {\n      background: #0077c5;\n      display: block;\n      width: 15px;\n      height: 15px; }\n\n.item-box p .yellow {\n      background: #ffba00;\n      display: block;\n      width: 20px;\n      height: 22.1px; }\n\n.cdk-drag-preview {\n  box-sizing: border-box;\n  border-radius: 4px;\n  box-shadow: 0 5px 5px -3px rgba(0, 0, 0, 0.2), 0 8px 10px 1px rgba(0, 0, 0, 0.14), 0 3px 14px 2px rgba(0, 0, 0, 0.12);\n  border-radius: 20px; }\n\n#newOrders {\n  font-size: 14px;\n  font-weight: bold;\n  color: #636363;\n  background: transparent;\n  border: 1px solid #707070; }\n\n.item-box .order-blue {\n  width: 16.5px;\n  height: 20px;\n  margin-right: 10px; }\n\n.item-box .order-count {\n  margin-right: 31px;\n  font-size: 16px;\n  font-weight: bold; }\n\n.item-box .order-date {\n  font-size: 16px; }\n\n.item-box .clipboard {\n  display: flex;\n  align-items: center;\n  margin-left: 10px; }\n\n.item-box .clipboard img {\n    width: 20px;\n    height: 22.1px;\n    margin-right: 10px; }\n\n.item-box .red-circle {\n  min-width: 30px;\n  min-height: 30px;\n  background-color: #cd3232;\n  border-radius: 50%;\n  color: white;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  text-transform: uppercase;\n  margin-right: 10px; }\n\n.item-box .clients {\n  font-size: 16px; }\n\n.item-box .clients span {\n    color: #636363;\n    margin-right: 9px; }\n\n.orders-name {\n  font-size: 24px;\n  font-weight: bold;\n  margin-bottom: 36px;\n  margin-top: 25px; }\n\n.kanban {\n  margin-top: 105px; }\n\n.name-order {\n  white-space: nowrap;\n  text-overflow: ellipsis;\n  overflow: hidden; }\n\n.callback-icon {\n  display: flex;\n  align-items: center; }\n\n.search-input .search-icon {\n  position: absolute;\n  top: 12px;\n  left: 10px; }\n\n.search-input input {\n  height: 38px;\n  padding-left: 30px;\n  border: 1px solid #707070 !important; }\n\n.orders-name {\n  font-size: 24px;\n  font-weight: bold;\n  margin-bottom: 36px;\n  margin-top: 25px; }\n\n.btn-group {\n  height: 38px; }\n\n.btn-group button {\n    background: white;\n    width: 50px;\n    border: 1px solid #707070;\n    display: flex;\n    justify-content: center;\n    align-items: center; }\n\n.btn-group button i {\n      color: #636363;\n      font-size: 20px; }\n\n.btn-group .active {\n    background: #636363; }\n\n.btn-group .active i {\n      color: white; }\n\n.filters-button {\n  justify-content: center;\n  width: 118.5px;\n  height: 38px;\n  background-color: #ffffff;\n  border: 0.5px solid #707070;\n  display: flex;\n  align-items: center;\n  cursor: pointer; }\n"

/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/orders/order-kanban/order-kanban.component.ts":
/*!******************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/orders/order-kanban/order-kanban.component.ts ***!
  \******************************************************************************************************/
/*! exports provided: OrderKanbanComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderKanbanComponent", function() { return OrderKanbanComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/cdk/drag-drop */ "./node_modules/@angular/cdk/esm5/drag-drop.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var app_shared_constans_projects_and_orders_constans__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/shared/constans/projects-and-orders.constans */ "./src/app/shared/constans/projects-and-orders.constans.ts");
/* harmony import */ var app_shared_providers_services_order_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/shared/providers/services/order.service */ "./src/app/shared/providers/services/order.service.ts");
/* harmony import */ var app_shared_providers_services_load_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! app/shared/providers/services/load.service */ "./src/app/shared/providers/services/load.service.ts");
/* harmony import */ var app_shared_providers_services_noitification_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! app/shared/providers/services/noitification.service */ "./src/app/shared/providers/services/noitification.service.ts");
/* harmony import */ var app_shared_models_order_get_all_order_view__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! app/shared/models/order/get-all-order.view */ "./src/app/shared/models/order/get-all-order.view.ts");
/* harmony import */ var app_shared_models_order_update_order_view__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! app/shared/models/order/update-order.view */ "./src/app/shared/models/order/update-order.view.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var OrderKanbanComponent = /** @class */ (function () {
    function OrderKanbanComponent(route, orderService, notificationService, loadService, constans) {
        this.route = route;
        this.orderService = orderService;
        this.notificationService = notificationService;
        this.loadService = loadService;
        this.constans = constans;
        this.newOrder = [];
        this.activeOrder = [];
        this.processingOrder = [];
        this.doneOrder = [];
        this.allOrders = new app_shared_models_order_get_all_order_view__WEBPACK_IMPORTED_MODULE_7__["GetAllOrderView"]();
        this.updateOrder = new app_shared_models_order_update_order_view__WEBPACK_IMPORTED_MODULE_8__["UpdateOrderView"]();
    }
    OrderKanbanComponent.prototype.ngOnInit = function () {
        this.loadService.set(true);
        this.getAllOrders();
    };
    OrderKanbanComponent.prototype.showOrdersDetails = function (id) {
        this.route.navigate(['projects-and-orders/orders/orderDetails', id]);
    };
    OrderKanbanComponent.prototype.drop = function (event) {
        var _this = this;
        if (event.previousContainer === event.container) {
            Object(_angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_1__["moveItemInArray"])(event.container.data, event.previousIndex, event.currentIndex);
        }
        else {
            Object(_angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_1__["transferArrayItem"])(event.previousContainer.data, event.container.data, event.previousIndex, event.currentIndex);
            this.curentOrder = event.container.data[event.currentIndex];
            if (event.container.id == this.constans.kanbanBlockStatus.new) {
                this.curentOrder.status = this.constans.status.new;
            }
            if (event.container.id == this.constans.kanbanBlockStatus.active) {
                this.curentOrder.status = this.constans.status.active;
            }
            if (event.container.id == this.constans.kanbanBlockStatus.processing) {
                this.curentOrder.status = this.constans.status.processing;
            }
            if (event.container.id == this.constans.kanbanBlockStatus.done) {
                this.curentOrder.status = this.constans.status.done;
            }
            this.updateOrder.id = this.curentOrder.id;
            this.updateOrder.date = this.curentOrder.date;
            this.updateOrder.dueDate = this.curentOrder.dueDate;
            this.updateOrder.memo = this.curentOrder.memo;
            this.updateOrder.status = this.curentOrder.status;
            this.updateOrder.customerId = this.curentOrder.customer.id;
            this.updateOrder.projectId = this.curentOrder.project.id;
            this.updateOrder.itemId = this.curentOrder.item.id;
            this.updateOrder.confirmedCustomerQuoteId = this.curentOrder.confirmedCustomerQuoteId;
            this.updateOrder.confirmedVendorQuoteId = this.curentOrder.confirmedVendorQuoteId;
            console.log(this.updateOrder);
            this.orderService.updateOrder(this.updateOrder).subscribe(function (response) {
                _this.notificationService.showSuccess(_this.constans.orderUpdateMessage);
            });
        }
    };
    OrderKanbanComponent.prototype.getAllOrders = function () {
        var _this = this;
        this.orderService.getAllOrders().subscribe(function (response) {
            _this.allOrders = response;
            _this.ordersCount = response.orders.length;
            console.log(_this.allOrders);
            _this.sortOrderByStatus(_this.allOrders.orders);
            _this.loadService.set(false);
        }, function (error) {
            _this.errors = error;
            _this.loadService.set(false);
        });
    };
    OrderKanbanComponent.prototype.sortOrderByStatus = function (allOrders) {
        var _this = this;
        this.newOrder = allOrders.filter(function (x) { return x.status == _this.constans.status.new; });
        this.activeOrder = allOrders.filter(function (x) { return x.status == _this.constans.status.active; });
        this.processingOrder = allOrders.filter(function (x) { return x.status == _this.constans.status.processing; });
        this.doneOrder = allOrders.filter(function (x) { return x.status == _this.constans.status.done; });
    };
    OrderKanbanComponent.prototype.navigateToCreateOrder = function () {
        this.route.navigate(['projects-and-orders/orders/createOrder']);
    };
    OrderKanbanComponent.prototype.showListView = function () {
        this.route.navigate(["projects-and-orders/orders/ordersList"]);
    };
    OrderKanbanComponent.prototype.callBack = function (event) {
        event.stopPropagation();
        alert('hello');
    };
    OrderKanbanComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-order-kanban',
            template: __webpack_require__(/*! ./order-kanban.component.html */ "./src/app/projects-and-orders/project-and-orders/orders/order-kanban/order-kanban.component.html"),
            styles: [__webpack_require__(/*! ./order-kanban.component.scss */ "./src/app/projects-and-orders/project-and-orders/orders/order-kanban/order-kanban.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            app_shared_providers_services_order_service__WEBPACK_IMPORTED_MODULE_4__["OrderService"],
            app_shared_providers_services_noitification_service__WEBPACK_IMPORTED_MODULE_6__["NotificationService"],
            app_shared_providers_services_load_service__WEBPACK_IMPORTED_MODULE_5__["LoadService"],
            app_shared_constans_projects_and_orders_constans__WEBPACK_IMPORTED_MODULE_3__["ProjectsAndOrdersConstans"]])
    ], OrderKanbanComponent);
    return OrderKanbanComponent;
}());



/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/orders/order-list/order-list.component.html":
/*!****************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/orders/order-list/order-list.component.html ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "      <div class=\"row col-md-12 d-flex justify-content-between align-items-center\">\r\n        <h3 class=\"orders-name\">\r\n          Orders ({{ordersCount}})\r\n        </h3>\r\n        <div>\r\n          <button id=\"newOrders\" class=\"btn\"><span style=\"margin-right:12px\">+</span> New Order</button>\r\n        </div>\r\n      </div>\r\n      <div class=\"row col-md-12\">\r\n        <div class=\"col-md-3 d-flex align-items-center pl-0\">\r\n         <div class=\"form-group w-100 position-relative search-input\">\r\n            <i class=\"fa fa-search search-icon\"></i>\r\n            <input type=\"text\" class=\"form-control\"/>\r\n         </div>\r\n        </div>\r\n        <div class=\"filters-button col-md-1\">\r\n            <i class=\"fa fa-filter mr-2\"></i>\r\n          Filters\r\n        </div>\r\n        <div class=\"btn-group col-md-8 d-flex justify-content-end pr-0\" role=\"group\" aria-label=\"Basic example\">\r\n            <button type=\"button\" class=\"btn active\"><i class=\"fa fa-list-ul\" aria-hidden=\"true\"></i></button>\r\n            <button type=\"button\" class=\"btn\" (click)=\"showKanbanView()\"><i class=\"fa fa-bars\" aria-hidden=\"true\"></i></button>\r\n          </div>\r\n      </div>\r\n      <div class=\"row mt-4 col-md-12  orders-buttons\">\r\n        <button type=\"button\" class=\"btn \">New</button>\r\n        <button type=\"button\" class=\"btn\">Active</button>\r\n        <button type=\"button\" class=\"btn\">Processing</button>\r\n        <button type=\"button\" class=\"btn active\">Closed</button>\r\n      </div>\r\n      <div class=\"row col-md-12\" *ngFor=\"let item of allOrders.orders\" >\r\n          <div class=\"active-order\">\r\n              <h2 class=\"title\">{{item.status}} Order</h2>\r\n             <div class=\"row\">\r\n                <div class=\"info-order col-md-8\">\r\n                   <div class=\"name-order\">\r\n                     <span class=\"pre-title\">{{item.customer.name}}</span>\r\n                     <span class=\"pre-title\">{{item.date | date:'shortDate'}}</span>\r\n                   </div>\r\n                   <div class=\"info-card\">\r\n                     <div class=\"d-flex\">\r\n                       <img src=\"../../../../../../assets/img/favicon.png\" class=\"order-image\" alt=\"\">\r\n                       <div class=\"card-name\">\r\n                         <a href=\"#\">Bussines Card</a>\r\n                         <span>Qty: 150</span>\r\n                         <div class=\"mt-1\">\r\n                           <span class=\"card-button\">size: 500</span>\r\n                           <span class=\"card-button\">Hight: 17</span>\r\n                           <span class=\"card-button\">Paper type Plain</span>\r\n                         </div>\r\n                       </div>\r\n                     </div>\r\n                     <div class=\"vendors\">\r\n                       <div class=\"d-flex mb-1\">\r\n                         <img src=\"../../../../../../assets/img/success-yellow.svg\" alt=\"\">\r\n                         <a href=\"#\">Vendor</a>\r\n                         <span>Vendor Name</span>\r\n                       </div>\r\n                       <div class=\"d-flex mb-1\">\r\n                           <img src=\"../../../../../../assets/img/success-yellow.svg\" alt=\"\">\r\n                         <a href=\"#\">Cost</a>\r\n                         <span>$225.00</span>\r\n                       </div>\r\n                       <div class=\"d-flex\">\r\n                           <img src=\"../../../../../../assets/img/success-yellow.svg\" alt=\"\">\r\n                         <a href=\"#\">Sales Price</a>\r\n                         <span>$375.00</span>\r\n                       </div>\r\n                     </div>\r\n                   </div>\r\n                </div>\r\n                <div class=\"order-details col-md-4\">\r\n                 <button type=\"button\" class=\"details-order-button\" (click)=\"showOrdersDetails(item.id)\">see order details</button>\r\n                 <div class=\"order-details-name\">\r\n                   <div class=\"project-name\">\r\n                       <img  src=\"assets/img/project-active.svg\">\r\n                     <span>Project 81</span>\r\n                   </div>\r\n                   <span class=\"order-client\"> {{item.project.user.firstName}}   {{item.project.user.lastName}}</span>\r\n                   <div  class=\"tasks\">\r\n                       <img src=\"../../../../../../assets/img/clipboard-1.svg\" alt=\"\">\r\n                       <span>Tasks 5/8</span>\r\n                       <i class=\"fa fa-chevron-down\" *ngIf=\"!orderTaskKanbanIsShow || item.id != currentOrderId\" (click)=\"showOrderTask(item.id)\" aria-hidden=\"true\" ></i>\r\n                       <i class=\"fa fa-chevron-up\" *ngIf=\"orderTaskKanbanIsShow && item.id == currentOrderId\" (click)=\"hideOrderTasks()\" aria-hidden=\"true\" ></i>\r\n                   </div>\r\n                 </div>\r\n                </div>\r\n             </div>\r\n             <app-order-tasks-kanban\r\n             *ngIf=\"orderTaskKanbanIsShow && item.id == currentOrderId\"\r\n             [orderId]=\"currentOrderId \"></app-order-tasks-kanban>\r\n\r\n             <app-order-tasks-list\r\n             *ngIf=\"orderTaskListIsShow\"></app-order-tasks-list>\r\n           </div>\r\n      </div>\r\n\r\n"

/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/orders/order-list/order-list.component.scss":
/*!****************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/orders/order-list/order-list.component.scss ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#newOrders {\n  font-size: 14px;\n  font-weight: bold;\n  color: #636363;\n  background: transparent;\n  border: 1px solid #707070;\n  text-transform: uppercase; }\n\n.new-task {\n  margin-right: 28.5px;\n  font-size: 14px;\n  font-weight: bold;\n  color: #000;\n  background: transparent;\n  border: 1px solid #000;\n  text-transform: uppercase; }\n\n.switch-close {\n  font-size: 14px;\n  font-weight: bold;\n  color: #000;\n  margin-left: 14px; }\n\n.search-input .search-icon {\n  position: absolute;\n  top: 12px;\n  left: 10px; }\n\n.search-input input {\n  height: 38px;\n  padding-left: 30px;\n  border: 1px solid #707070 !important; }\n\n.orders-name {\n  font-size: 24px;\n  font-weight: bold;\n  margin-bottom: 36px;\n  margin-top: 25px; }\n\n.btn-group {\n  height: 38px; }\n\n.btn-group button {\n    background: white;\n    width: 50px;\n    border: 1px solid #707070;\n    display: flex;\n    justify-content: center;\n    align-items: center; }\n\n.btn-group button i {\n      color: #636363;\n      font-size: 20px; }\n\n.btn-group .active {\n    background: #636363; }\n\n.btn-group .active i {\n      color: white; }\n\n.filters-button {\n  justify-content: center;\n  width: 118.5px;\n  height: 38px;\n  background-color: #ffffff;\n  border: 0.5px solid #707070;\n  display: flex;\n  align-items: center;\n  cursor: pointer; }\n\n.orders-buttons button {\n  margin-right: 14.5px;\n  border-radius: 17px;\n  background-color: #636363;\n  color: white;\n  font-size: 14px;\n  font-weight: bold;\n  padding: 8px 28px; }\n\n.orders-buttons .active {\n  background: transparent;\n  border: 0.5px solid #707070;\n  color: #636363;\n  font-weight: normal; }\n\n.active-order {\n  margin-top: 21px;\n  height: 100%;\n  width: 100%;\n  border: 0.5px solid #707070;\n  background: white;\n  padding: 27px 110.7px 50px 118px; }\n\n.active-order .title {\n    font-size: 22px;\n    font-weight: bold;\n    color: #19233b;\n    margin-bottom: 16px; }\n\n.active-order .info-order .name-order {\n    height: 44px;\n    background-color: #e9e9e9;\n    display: flex;\n    align-items: center;\n    justify-content: space-between;\n    padding: 0px 30px; }\n\n.active-order .info-order .name-order .pre-title {\n      font-size: 16px;\n      font-weight: bold;\n      color: #19233b; }\n\n.active-order .info-order .info-card {\n    margin-top: 31px;\n    display: flex;\n    align-items: center;\n    justify-content: space-between; }\n\n.active-order .info-order .info-card .order-image {\n      width: 63.5px;\n      height: 63.5px; }\n\n.active-order .info-order .info-card .card-name {\n      display: flex;\n      flex-direction: column;\n      margin-left: 55px; }\n\n.active-order .info-order .info-card .card-name a {\n        font-size: 16px;\n        font-weight: normal;\n        color: #0077c5; }\n\n.active-order .info-order .info-card .card-name a:hover {\n          text-decoration: none; }\n\n.active-order .info-order .info-card .card-name span {\n        font-size: 14px;\n        font-weight: normal;\n        color: #000; }\n\n.active-order .info-order .info-card .card-name .card-button {\n        border-radius: 4px;\n        background-color: #9198ac;\n        padding: 0px 8.5px;\n        color: white;\n        margin-right: 10px; }\n\n.active-order .info-order .info-card .vendors {\n      width: 25%; }\n\n.active-order .info-order .info-card .vendors img {\n        width: 17.8px;\n        height: 17.8px;\n        margin-right: 14.8px; }\n\n.active-order .info-order .info-card .vendors a {\n        font-size: 14px;\n        font-weight: bold;\n        color: #0077c5;\n        white-space: nowrap; }\n\n.active-order .info-order .info-card .vendors a:hover {\n          text-decoration: none; }\n\n.active-order .info-order .info-card .vendors span {\n        font-size: 14px;\n        font-weight: normal;\n        color: #000;\n        text-align: right;\n        width: 100%; }\n\n.active-order .order-details {\n    align-items: flex-end;\n    display: flex;\n    justify-content: center;\n    flex-direction: column; }\n\n.active-order .order-details .details-order-button {\n      padding: 12px 37px;\n      color: #000;\n      font-size: 14px;\n      font-weight: normal;\n      text-transform: uppercase;\n      border: 1px solid black;\n      background: transparent;\n      margin-bottom: 25px; }\n\n.active-order .order-details .order-details-name {\n      text-align: center;\n      width: 207px; }\n\n.active-order .order-details .order-details-name img {\n        width: 20px;\n        height: 22.1px; }\n\n.active-order .order-details .order-details-name .project-name {\n        margin-bottom: 10px; }\n\n.active-order .order-details .order-details-name .project-name span {\n          font-size: 16px;\n          font-weight: bold;\n          color: #000;\n          margin-left: 10px; }\n\n.active-order .order-details .order-details-name .order-client {\n        font-size: 16px;\n        font-weight: normal;\n        color: #000; }\n\n.active-order .order-details .order-details-name .tasks {\n        display: flex;\n        align-items: center;\n        margin-top: 10px;\n        justify-content: space-between; }\n\n.active-order .order-details .order-details-name .tasks span {\n          font-size: 16px;\n          font-weight: normal;\n          color: #000; }\n\n.clickable-title-link {\n  color: rgba(var(--palette-neutral-100, 0, 0, 0), 1); }\n\n.underline-on-hover:hover {\n  text-decoration: underline !important; }\n\n.underline-on-hover {\n  font-size: 16px; }\n\n.section-heading {\n  padding: 5px 10px 11px 0px;\n  font-size: 18px;\n  font-weight: bold; }\n"

/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/orders/order-list/order-list.component.ts":
/*!**************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/orders/order-list/order-list.component.ts ***!
  \**************************************************************************************************/
/*! exports provided: OrderListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderListComponent", function() { return OrderListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var app_shared_constans_projects_and_orders_constans__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/shared/constans/projects-and-orders.constans */ "./src/app/shared/constans/projects-and-orders.constans.ts");
/* harmony import */ var app_shared_providers_services_order_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/shared/providers/services/order.service */ "./src/app/shared/providers/services/order.service.ts");
/* harmony import */ var app_shared_providers_services_load_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/shared/providers/services/load.service */ "./src/app/shared/providers/services/load.service.ts");
/* harmony import */ var app_shared_providers_services_noitification_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! app/shared/providers/services/noitification.service */ "./src/app/shared/providers/services/noitification.service.ts");
/* harmony import */ var app_shared_models_order_get_all_order_view__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! app/shared/models/order/get-all-order.view */ "./src/app/shared/models/order/get-all-order.view.ts");
/* harmony import */ var app_shared_models_order_update_order_view__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! app/shared/models/order/update-order.view */ "./src/app/shared/models/order/update-order.view.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var OrderListComponent = /** @class */ (function () {
    function OrderListComponent(route, orderService, notificationService, loadService, constans) {
        this.route = route;
        this.orderService = orderService;
        this.notificationService = notificationService;
        this.loadService = loadService;
        this.constans = constans;
        this.newOrder = [];
        this.activeOrder = [];
        this.processingOrder = [];
        this.doneOrder = [];
        this.allOrders = new app_shared_models_order_get_all_order_view__WEBPACK_IMPORTED_MODULE_6__["GetAllOrderView"]();
        this.updateOrder = new app_shared_models_order_update_order_view__WEBPACK_IMPORTED_MODULE_7__["UpdateOrderView"]();
        this.orderTaskKanbanIsShow = false;
        this.orderTaskListIsShow = false;
    }
    OrderListComponent.prototype.ngOnInit = function () {
        this.loadService.set(true);
        this.getAllOrders();
    };
    OrderListComponent.prototype.getAllOrders = function () {
        var _this = this;
        this.orderService.getAllOrders().subscribe(function (response) {
            _this.allOrders = response;
            _this.ordersCount = response.orders.length;
            console.log(_this.allOrders);
            _this.sortOrderByStatus(_this.allOrders.orders);
            _this.loadService.set(false);
        }, function (error) {
            _this.errors = error;
            _this.loadService.set(false);
        });
    };
    OrderListComponent.prototype.sortOrderByStatus = function (allOrders) {
        var _this = this;
        this.newOrder = allOrders.filter(function (x) { return x.status == _this.constans.status.new; });
        this.activeOrder = allOrders.filter(function (x) { return x.status == _this.constans.status.active; });
        this.processingOrder = allOrders.filter(function (x) { return x.status == _this.constans.status.processing; });
        this.doneOrder = allOrders.filter(function (x) { return x.status == _this.constans.status.done; });
    };
    OrderListComponent.prototype.navigateToCreateOrder = function () {
        this.route.navigate(['projects-and-orders/orders/createOrder']);
    };
    OrderListComponent.prototype.showOrdersDetails = function (id) {
        this.route.navigate(['projects-and-orders/orders/orderDetails', id]);
    };
    OrderListComponent.prototype.showKanbanView = function () {
        this.route.navigate(["projects-and-orders/orders/ordersKanban"]);
    };
    OrderListComponent.prototype.showOrderTask = function (orderId) {
        this.currentOrderId = orderId;
        console.log("item id");
        console.log(orderId);
        this.orderTaskKanbanIsShow = true;
    };
    OrderListComponent.prototype.hideOrderTasks = function () {
        this.currentOrderId = null;
        this.orderTaskKanbanIsShow = false;
    };
    OrderListComponent.prototype.callBack = function (event) {
        event.stopPropagation();
        alert('hello');
    };
    OrderListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'orderList',
            template: __webpack_require__(/*! ./order-list.component.html */ "./src/app/projects-and-orders/project-and-orders/orders/order-list/order-list.component.html"),
            styles: [__webpack_require__(/*! ./order-list.component.scss */ "./src/app/projects-and-orders/project-and-orders/orders/order-list/order-list.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            app_shared_providers_services_order_service__WEBPACK_IMPORTED_MODULE_3__["OrderService"],
            app_shared_providers_services_noitification_service__WEBPACK_IMPORTED_MODULE_5__["NotificationService"],
            app_shared_providers_services_load_service__WEBPACK_IMPORTED_MODULE_4__["LoadService"],
            app_shared_constans_projects_and_orders_constans__WEBPACK_IMPORTED_MODULE_2__["ProjectsAndOrdersConstans"]])
    ], OrderListComponent);
    return OrderListComponent;
}());



/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/orders/order-list/order-tasks-kanban/order-tasks-kanban.component.html":
/*!*******************************************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/orders/order-list/order-tasks-kanban/order-tasks-kanban.component.html ***!
  \*******************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row align-items-center\" style=\"margin-top:70px;\">\r\n  <h3 class=\"orders-name col-md-8 pl-0\">\r\n    Tasks (10)\r\n  </h3>\r\n  <div class=\"col-md-4 d-flex justify-content-end\">\r\n    <button class=\"btn new-task\"><span style=\"margin-right:12px\">+</span> New task</button>\r\n    <div class=\"btn-group d-flex justify-content-end pr-0\" role=\"group\" aria-label=\"Basic example\">\r\n      <button type=\"button\" class=\"btn active\"><i class=\"fa fa-list-ul\" aria-hidden=\"true\"></i></button>\r\n      <button type=\"button\" class=\"btn\"><i class=\"fa fa-bars\" aria-hidden=\"true\"></i></button>\r\n    </div>\r\n  </div>\r\n</div>\r\n<div class=\"row col-md-12\">\r\n  <mat-slide-toggle></mat-slide-toggle><span class=\"switch-close\">Closed</span>\r\n</div>\r\n<div class=\"row mt-5 kanban-full\">\r\n  <div class=\"col-md-4\" *ngFor=\"let orderTask of allOrderTasksByOrderId.orderTasks\">\r\n    <p class=\"teams\">{{orderTask.teamName}}</p>\r\n    <div *ngFor=\"let orderTaskByName of orderTask.orderTasksGroupByName\">\r\n      <div class=\"kanban-block\">\r\n        <p class=\"kanban-title\">{{orderTaskByName.dueDate | date:'shortDate'}}</p>\r\n          <div class=\"kanban-info\" *ngFor=\"let task of orderTaskByName.orderTasksGroupByDueTime\">\r\n            <div class=\"d-flex justify-content-between align-items-center\">\r\n              <div>\r\n                <img src=\"../../../../../../assets/img/clipboard-1.svg\" alt=\"\">\r\n                <span class=\"kanban-digits\">50</span>\r\n              </div>\r\n              <div>\r\n                <span class=\"kanban-due\">Due Date</span>\r\n                <span class=\"kanban-date\">{{task.dueDate | date:'shortDate'}}</span>\r\n              </div>\r\n            </div>\r\n            <p class=\"task-details\">{{task.description}}</p>\r\n            <div class=\"clients\">\r\n              <div>\r\n                <span>Status</span>\r\n                <a>{{task.taskStatus.statusName}}</a>\r\n              </div>\r\n              <div>\r\n                <span>Customer</span>\r\n                <a>Archer Hotel</a>\r\n              </div>\r\n            </div>\r\n            <div class=\"assign\">\r\n              <span>Assigned</span>\r\n               <a>{{task.assignedTeam.teamName}}</a>\r\n               <a>{{task.assignedUser.firstName}} {{task.assignedUser.lastName}}</a>\r\n            </div>\r\n            <div class=\"kanban-footer\">\r\n              <div class=\"d-flex align-items-center\">\r\n                <img src=\"../../../../../../assets/img/order-2.svg\" alt=\"\">\r\n                <span class=\"kanban-digits\">50</span>\r\n                <p class=\"m-0\">2/4</p>\r\n              </div>\r\n              <div>\r\n                <img src=\"assets/img/project-active.svg\">\r\n                <span class=\"kanban-digits\">50</span>\r\n              </div>\r\n            </div>\r\n          </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/orders/order-list/order-tasks-kanban/order-tasks-kanban.component.scss":
/*!*******************************************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/orders/order-list/order-tasks-kanban/order-tasks-kanban.component.scss ***!
  \*******************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".kanban-full {\n  flex-wrap: nowrap !important;\n  overflow: auto; }\n\n#newOrders {\n  font-size: 14px;\n  font-weight: bold;\n  color: #636363;\n  background: transparent;\n  border: 1px solid #707070;\n  text-transform: uppercase; }\n\n.new-task {\n  margin-right: 28.5px;\n  font-size: 14px;\n  font-weight: bold;\n  color: #000;\n  background: transparent;\n  border: 1px solid #000;\n  text-transform: uppercase; }\n\n.switch-close {\n  font-size: 14px;\n  font-weight: bold;\n  color: #000;\n  margin-left: 14px; }\n\n.search-input .search-icon {\n  position: absolute;\n  top: 12px;\n  left: 10px; }\n\n.search-input input {\n  height: 38px;\n  padding-left: 30px;\n  border: 1px solid #707070 !important; }\n\n.orders-name {\n  font-size: 24px;\n  font-weight: bold;\n  margin-bottom: 36px;\n  margin-top: 25px; }\n\n.btn-group {\n  height: 38px; }\n\n.btn-group button {\n    background: white;\n    width: 50px;\n    border: 1px solid #707070;\n    display: flex;\n    justify-content: center;\n    align-items: center; }\n\n.btn-group button i {\n      color: #636363;\n      font-size: 20px; }\n\n.btn-group .active {\n    background: #636363; }\n\n.btn-group .active i {\n      color: white; }\n\n.filters-button {\n  justify-content: center;\n  width: 118.5px;\n  height: 38px;\n  background-color: #ffffff;\n  border: 0.5px solid #707070;\n  display: flex;\n  align-items: center;\n  cursor: pointer; }\n\n.orders-buttons button {\n  margin-right: 14.5px;\n  border-radius: 17px;\n  background-color: #636363;\n  color: white;\n  font-size: 14px;\n  font-weight: bold;\n  padding: 8px 28px; }\n\n.orders-buttons .active {\n  background: transparent;\n  border: 0.5px solid #707070;\n  color: #636363;\n  font-weight: normal; }\n\n.active-order {\n  margin-top: 21px;\n  height: 100%;\n  width: 100%;\n  border: 0.5px solid #707070;\n  background: white;\n  padding: 27px 20.7px 50px 118px; }\n\n.active-order .title {\n    font-size: 22px;\n    font-weight: bold;\n    color: #19233b;\n    margin-bottom: 16px; }\n\n.active-order .info-order .name-order {\n    height: 44px;\n    background-color: #e9e9e9;\n    display: flex;\n    align-items: center;\n    justify-content: space-between;\n    padding: 0px 30px; }\n\n.active-order .info-order .name-order .pre-title {\n      font-size: 16px;\n      font-weight: bold;\n      color: #19233b; }\n\n.active-order .info-order .info-card {\n    margin-top: 31px;\n    display: flex;\n    align-items: center;\n    justify-content: space-between; }\n\n.active-order .info-order .info-card .order-image {\n      width: 63.5px;\n      height: 63.5px; }\n\n.active-order .info-order .info-card .card-name {\n      display: flex;\n      flex-direction: column;\n      margin-left: 55px; }\n\n.active-order .info-order .info-card .card-name a {\n        font-size: 16px;\n        font-weight: normal;\n        color: #0077c5; }\n\n.active-order .info-order .info-card .card-name a:hover {\n          text-decoration: none; }\n\n.active-order .info-order .info-card .card-name span {\n        font-size: 14px;\n        font-weight: normal;\n        color: #000; }\n\n.active-order .info-order .info-card .card-name .card-button {\n        border-radius: 4px;\n        background-color: #9198ac;\n        padding: 0px 8.5px;\n        color: white;\n        margin-right: 10px; }\n\n.active-order .info-order .info-card .vendors {\n      width: 25%; }\n\n.active-order .info-order .info-card .vendors img {\n        width: 17.8px;\n        height: 17.8px;\n        margin-right: 14.8px; }\n\n.active-order .info-order .info-card .vendors a {\n        font-size: 14px;\n        font-weight: bold;\n        color: #0077c5;\n        white-space: nowrap; }\n\n.active-order .info-order .info-card .vendors a:hover {\n          text-decoration: none; }\n\n.active-order .info-order .info-card .vendors span {\n        font-size: 14px;\n        font-weight: normal;\n        color: #000;\n        text-align: right;\n        width: 100%; }\n\n.active-order .order-details {\n    align-items: center;\n    display: flex;\n    justify-content: center;\n    flex-direction: column; }\n\n.active-order .order-details .details-order-button {\n      padding: 12px 37px;\n      color: #000;\n      font-size: 14px;\n      font-weight: normal;\n      text-transform: uppercase;\n      border: 1px solid black;\n      background: transparent;\n      margin-bottom: 25px; }\n\n.active-order .order-details .order-details-name {\n      text-align: center; }\n\n.active-order .order-details .order-details-name img {\n        width: 20px;\n        height: 22.1px; }\n\n.active-order .order-details .order-details-name .project-name {\n        margin-bottom: 10px; }\n\n.active-order .order-details .order-details-name .project-name span {\n          font-size: 16px;\n          font-weight: bold;\n          color: #000;\n          margin-left: 10px; }\n\n.active-order .order-details .order-details-name .order-client {\n        font-size: 16px;\n        font-weight: normal;\n        color: #000; }\n\n.active-order .order-details .order-details-name .tasks {\n        display: flex;\n        align-items: center;\n        margin-top: 10px;\n        justify-content: space-between; }\n\n.active-order .order-details .order-details-name .tasks span {\n          font-size: 16px;\n          font-weight: normal;\n          color: #000; }\n\n.clickable-title-link {\n  color: rgba(var(--palette-neutral-100, 0, 0, 0), 1); }\n\n.underline-on-hover:hover {\n  text-decoration: underline !important; }\n\n.underline-on-hover {\n  font-size: 16px; }\n\n.section-heading {\n  padding: 5px 10px 11px 0px;\n  font-size: 18px;\n  font-weight: bold; }\n\n.teams {\n  font-size: 22px;\n  font-weight: bold;\n  color: #000000; }\n\n.kanban-block {\n  background: #e9e9e9;\n  padding: 35px; }\n\n.kanban-block .kanban-title {\n    font-size: 17px;\n    font-weight: bold; }\n\n.kanban-block .kanban-info {\n    background-color: #ffffff;\n    padding: 20px;\n    margin-bottom: 30px; }\n\n.kanban-block .kanban-info img {\n      width: 20px;\n      height: 22.1px;\n      margin-right: 5px; }\n\n.kanban-block .kanban-info .kanban-due {\n      font-size: 12px;\n      font-weight: normal;\n      color: #636363; }\n\n.kanban-block .kanban-info .kanban-date {\n      font-size: 12px;\n      font-weight: normal;\n      color: #0077c5;\n      margin-left: 20px; }\n\n.kanban-block .kanban-info .kanban-digits {\n      font-size: 13px;\n      font-weight: bold;\n      color: #000000; }\n\n.kanban-block .kanban-info .task-details {\n      font-size: 12px;\n      font-weight: bold;\n      color: #000000;\n      margin-top: 24px; }\n\n.kanban-block .kanban-info .clients div {\n      margin-bottom: 20px; }\n\n.kanban-block .kanban-info .clients div span {\n        color: #707070;\n        margin-right: 12px; }\n\n.kanban-block .kanban-info .clients div a {\n        color: #0077c5; }\n\n.kanban-block .kanban-info .assign {\n      display: flex;\n      justify-content: space-between; }\n\n.kanban-block .kanban-info .assign span {\n        color: #707070; }\n\n.kanban-block .kanban-info .assign a {\n        color: #0077c5; }\n\n.kanban-block .kanban-info .kanban-footer {\n      display: flex;\n      justify-content: space-between;\n      margin-top: 23px; }\n\n.kanban-block .kanban-info .kanban-footer .kanban-digits {\n        margin-right: 12px; }\n\n.kanban-block .kanban-info .kanban-footer .kanban-digits + p {\n        font-size: 13px; }\n"

/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/orders/order-list/order-tasks-kanban/order-tasks-kanban.component.ts":
/*!*****************************************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/orders/order-list/order-tasks-kanban/order-tasks-kanban.component.ts ***!
  \*****************************************************************************************************************************/
/*! exports provided: OrderTasksKanbanComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderTasksKanbanComponent", function() { return OrderTasksKanbanComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_shared_models_order_tasks_get_all_order_tasks_by_order_id_view__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/shared/models/order-tasks/get-all-order-tasks-by-order-id.view */ "./src/app/shared/models/order-tasks/get-all-order-tasks-by-order-id.view.ts");
/* harmony import */ var app_shared_providers_services_order_tasks_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/shared/providers/services/order-tasks.service */ "./src/app/shared/providers/services/order-tasks.service.ts");
/* harmony import */ var app_shared_providers_services_load_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/shared/providers/services/load.service */ "./src/app/shared/providers/services/load.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var OrderTasksKanbanComponent = /** @class */ (function () {
    function OrderTasksKanbanComponent(orderTastService, loaderService) {
        this.orderTastService = orderTastService;
        this.loaderService = loaderService;
        this.allOrderTasksByOrderId = new app_shared_models_order_tasks_get_all_order_tasks_by_order_id_view__WEBPACK_IMPORTED_MODULE_1__["GetAllOrderTasksByOrderIdView"]();
    }
    OrderTasksKanbanComponent.prototype.ngOnChanges = function () {
        this.loaderService.set(true);
        this.getAllOrderTaskByOrderId(this.orderId);
    };
    OrderTasksKanbanComponent.prototype.getAllOrderTaskByOrderId = function (orderId) {
        var _this = this;
        this.orderTastService.getAllOrderTasksByOrderId(orderId).subscribe(function (response) {
            _this.allOrderTasksByOrderId = response;
            _this.loaderService.set(false);
            console.log(_this.allOrderTasksByOrderId);
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('orderId'),
        __metadata("design:type", String)
    ], OrderTasksKanbanComponent.prototype, "orderId", void 0);
    OrderTasksKanbanComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-order-tasks-kanban',
            template: __webpack_require__(/*! ./order-tasks-kanban.component.html */ "./src/app/projects-and-orders/project-and-orders/orders/order-list/order-tasks-kanban/order-tasks-kanban.component.html"),
            styles: [__webpack_require__(/*! ./order-tasks-kanban.component.scss */ "./src/app/projects-and-orders/project-and-orders/orders/order-list/order-tasks-kanban/order-tasks-kanban.component.scss")]
        }),
        __metadata("design:paramtypes", [app_shared_providers_services_order_tasks_service__WEBPACK_IMPORTED_MODULE_2__["OrderTasksService"], app_shared_providers_services_load_service__WEBPACK_IMPORTED_MODULE_3__["LoadService"]])
    ], OrderTasksKanbanComponent);
    return OrderTasksKanbanComponent;
}());



/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/orders/order-list/order-tasks-list/order-tasks-list.component.html":
/*!***************************************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/orders/order-list/order-tasks-list/order-tasks-list.component.html ***!
  \***************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/orders/order-list/order-tasks-list/order-tasks-list.component.scss":
/*!***************************************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/orders/order-list/order-tasks-list/order-tasks-list.component.scss ***!
  \***************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/orders/order-list/order-tasks-list/order-tasks-list.component.ts":
/*!*************************************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/orders/order-list/order-tasks-list/order-tasks-list.component.ts ***!
  \*************************************************************************************************************************/
/*! exports provided: OrderTasksListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderTasksListComponent", function() { return OrderTasksListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var OrderTasksListComponent = /** @class */ (function () {
    function OrderTasksListComponent() {
    }
    OrderTasksListComponent.prototype.ngOnChanges = function () {
    };
    OrderTasksListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-order-tasks-list',
            template: __webpack_require__(/*! ./order-tasks-list.component.html */ "./src/app/projects-and-orders/project-and-orders/orders/order-list/order-tasks-list/order-tasks-list.component.html"),
            styles: [__webpack_require__(/*! ./order-tasks-list.component.scss */ "./src/app/projects-and-orders/project-and-orders/orders/order-list/order-tasks-list/order-tasks-list.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], OrderTasksListComponent);
    return OrderTasksListComponent;
}());



/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/orders/orders-module-routing.module.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/orders/orders-module-routing.module.ts ***!
  \***********************************************************************************************/
/*! exports provided: OrdersModuleRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrdersModuleRoutingModule", function() { return OrdersModuleRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _order_details_order_details_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./order-details/order-details.component */ "./src/app/projects-and-orders/project-and-orders/orders/order-details/order-details.component.ts");
/* harmony import */ var _create_order_create_order_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./create-order/create-order.component */ "./src/app/projects-and-orders/project-and-orders/orders/create-order/create-order.component.ts");
/* harmony import */ var _order_list_order_list_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./order-list/order-list.component */ "./src/app/projects-and-orders/project-and-orders/orders/order-list/order-list.component.ts");
/* harmony import */ var _order_kanban_order_kanban_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./order-kanban/order-kanban.component */ "./src/app/projects-and-orders/project-and-orders/orders/order-kanban/order-kanban.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        redirectTo: 'ordersKanban',
    },
    {
        path: 'ordersKanban',
        component: _order_kanban_order_kanban_component__WEBPACK_IMPORTED_MODULE_5__["OrderKanbanComponent"]
    },
    {
        path: 'ordersList',
        component: _order_list_order_list_component__WEBPACK_IMPORTED_MODULE_4__["OrderListComponent"]
    },
    {
        path: 'orderDetails/:id',
        component: _order_details_order_details_component__WEBPACK_IMPORTED_MODULE_2__["OrderDetailsComponent"]
    },
    {
        path: 'createOrder',
        component: _create_order_create_order_component__WEBPACK_IMPORTED_MODULE_3__["CreateOrderComponent"]
    }
];
var OrdersModuleRoutingModule = /** @class */ (function () {
    function OrdersModuleRoutingModule() {
    }
    OrdersModuleRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], OrdersModuleRoutingModule);
    return OrdersModuleRoutingModule;
}());



/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/orders/orders-module.module.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/orders/orders-module.module.ts ***!
  \***************************************************************************************/
/*! exports provided: OrdersModuleModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrdersModuleModule", function() { return OrdersModuleModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _orders_module_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./orders-module-routing.module */ "./src/app/projects-and-orders/project-and-orders/orders/orders-module-routing.module.ts");
/* harmony import */ var _order_list_order_list_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./order-list/order-list.component */ "./src/app/projects-and-orders/project-and-orders/orders/order-list/order-list.component.ts");
/* harmony import */ var _order_details_order_details_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./order-details/order-details.component */ "./src/app/projects-and-orders/project-and-orders/orders/order-details/order-details.component.ts");
/* harmony import */ var _create_order_create_order_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./create-order/create-order.component */ "./src/app/projects-and-orders/project-and-orders/orders/create-order/create-order.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var angular_6_datatable__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! angular-6-datatable */ "./node_modules/angular-6-datatable/index.js");
/* harmony import */ var angular_6_datatable__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(angular_6_datatable__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-bootstrap/datepicker */ "./node_modules/ngx-bootstrap/datepicker/index.js");
/* harmony import */ var ng_busy__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ng-busy */ "./node_modules/ng-busy/fesm5/ng-busy.js");
/* harmony import */ var ngx_uploader__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-uploader */ "./node_modules/ngx-uploader/fesm5/ngx-uploader.js");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ng2-file-upload */ "./node_modules/ng2-file-upload/index.js");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(ng2_file_upload__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var ng_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ng-multiselect-dropdown */ "./node_modules/ng-multiselect-dropdown/fesm5/ng-multiselect-dropdown.js");
/* harmony import */ var _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/slide-toggle */ "./node_modules/@angular/material/esm5/slide-toggle.es5.js");
/* harmony import */ var ng2_pdf_viewer__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ng2-pdf-viewer */ "./node_modules/ng2-pdf-viewer/ng2-pdf-viewer.es5.js");
/* harmony import */ var _progress_kendo_angular_grid__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @progress/kendo-angular-grid */ "./node_modules/@progress/kendo-angular-grid/dist/es/index.js");
/* harmony import */ var app_shared_modules_material_module__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! app/shared/modules/material.module */ "./src/app/shared/modules/material.module.ts");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/esm5/table.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var devextreme_angular__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! devextreme-angular */ "./node_modules/devextreme-angular/index.js");
/* harmony import */ var devextreme_angular__WEBPACK_IMPORTED_MODULE_19___default = /*#__PURE__*/__webpack_require__.n(devextreme_angular__WEBPACK_IMPORTED_MODULE_19__);
/* harmony import */ var _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @angular/cdk/drag-drop */ "./node_modules/@angular/cdk/esm5/drag-drop.es5.js");
/* harmony import */ var _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @angular/material/grid-list */ "./node_modules/@angular/material/esm5/grid-list.es5.js");
/* harmony import */ var _order_kanban_order_kanban_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./order-kanban/order-kanban.component */ "./src/app/projects-and-orders/project-and-orders/orders/order-kanban/order-kanban.component.ts");
/* harmony import */ var _order_list_order_tasks_list_order_tasks_list_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./order-list/order-tasks-list/order-tasks-list.component */ "./src/app/projects-and-orders/project-and-orders/orders/order-list/order-tasks-list/order-tasks-list.component.ts");
/* harmony import */ var _order_list_order_tasks_kanban_order_tasks_kanban_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./order-list/order-tasks-kanban/order-tasks-kanban.component */ "./src/app/projects-and-orders/project-and-orders/orders/order-list/order-tasks-kanban/order-tasks-kanban.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

























var OrdersModuleModule = /** @class */ (function () {
    function OrdersModuleModule() {
    }
    OrdersModuleModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _orders_module_routing_module__WEBPACK_IMPORTED_MODULE_2__["OrdersModuleRoutingModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                devextreme_angular__WEBPACK_IMPORTED_MODULE_19__["DxDataGridModule"],
                devextreme_angular__WEBPACK_IMPORTED_MODULE_19__["DxSelectBoxModule"],
                devextreme_angular__WEBPACK_IMPORTED_MODULE_19__["DxCheckBoxModule"],
                app_shared_modules_material_module__WEBPACK_IMPORTED_MODULE_16__["MaterialModule"],
                angular_6_datatable__WEBPACK_IMPORTED_MODULE_7__["DataTableModule"],
                ng2_file_upload__WEBPACK_IMPORTED_MODULE_11__["FileUploadModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"],
                _progress_kendo_angular_grid__WEBPACK_IMPORTED_MODULE_15__["GridModule"],
                _angular_material_table__WEBPACK_IMPORTED_MODULE_17__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_18__["MatPaginatorModule"],
                _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_13__["MatSlideToggleModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ReactiveFormsModule"],
                ng_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_12__["NgMultiSelectDropDownModule"].forRoot(),
                ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_8__["BsDatepickerModule"].forRoot(),
                ng_busy__WEBPACK_IMPORTED_MODULE_9__["NgBusyModule"],
                ngx_uploader__WEBPACK_IMPORTED_MODULE_10__["NgxUploaderModule"],
                ng2_pdf_viewer__WEBPACK_IMPORTED_MODULE_14__["PdfViewerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_18__["MatSortModule"],
                _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_20__["DragDropModule"],
                _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_21__["MatGridListModule"]
            ],
            declarations: [
                _order_list_order_list_component__WEBPACK_IMPORTED_MODULE_3__["OrderListComponent"],
                _order_details_order_details_component__WEBPACK_IMPORTED_MODULE_4__["OrderDetailsComponent"],
                _create_order_create_order_component__WEBPACK_IMPORTED_MODULE_5__["CreateOrderComponent"],
                _order_kanban_order_kanban_component__WEBPACK_IMPORTED_MODULE_22__["OrderKanbanComponent"],
                _order_list_order_tasks_list_order_tasks_list_component__WEBPACK_IMPORTED_MODULE_23__["OrderTasksListComponent"],
                _order_list_order_tasks_kanban_order_tasks_kanban_component__WEBPACK_IMPORTED_MODULE_24__["OrderTasksKanbanComponent"]
            ]
        })
    ], OrdersModuleModule);
    return OrdersModuleModule;
}());



/***/ }),

/***/ "./src/app/shared/models/order-attachment/delete-order-attachment.view.ts":
/*!********************************************************************************!*\
  !*** ./src/app/shared/models/order-attachment/delete-order-attachment.view.ts ***!
  \********************************************************************************/
/*! exports provided: DeleteOrderAttachmentView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeleteOrderAttachmentView", function() { return DeleteOrderAttachmentView; });
var DeleteOrderAttachmentView = /** @class */ (function () {
    function DeleteOrderAttachmentView() {
    }
    return DeleteOrderAttachmentView;
}());



/***/ }),

/***/ "./src/app/shared/models/order-attachment/get-order-attachment-by-orderId.view.ts":
/*!****************************************************************************************!*\
  !*** ./src/app/shared/models/order-attachment/get-order-attachment-by-orderId.view.ts ***!
  \****************************************************************************************/
/*! exports provided: GetOrderAttachmentByOrderIdView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetOrderAttachmentByOrderIdView", function() { return GetOrderAttachmentByOrderIdView; });
var GetOrderAttachmentByOrderIdView = /** @class */ (function () {
    function GetOrderAttachmentByOrderIdView() {
    }
    return GetOrderAttachmentByOrderIdView;
}());



/***/ }),

/***/ "./src/app/shared/models/order-attachment/get-order-attachments-by-order-id.view.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/shared/models/order-attachment/get-order-attachments-by-order-id.view.ts ***!
  \******************************************************************************************/
/*! exports provided: GetOrderAttachmentsByOrderIdView, GetOrderAttachmentsByOrderIdViewItem */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetOrderAttachmentsByOrderIdView", function() { return GetOrderAttachmentsByOrderIdView; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetOrderAttachmentsByOrderIdViewItem", function() { return GetOrderAttachmentsByOrderIdViewItem; });
var GetOrderAttachmentsByOrderIdView = /** @class */ (function () {
    function GetOrderAttachmentsByOrderIdView() {
        this.orderAttachments = [];
    }
    return GetOrderAttachmentsByOrderIdView;
}());

var GetOrderAttachmentsByOrderIdViewItem = /** @class */ (function () {
    function GetOrderAttachmentsByOrderIdViewItem() {
    }
    return GetOrderAttachmentsByOrderIdViewItem;
}());



/***/ }),

/***/ "./src/app/shared/models/order-log-activity/get-all-order-log-activities-by-orderId.view.ts":
/*!**************************************************************************************************!*\
  !*** ./src/app/shared/models/order-log-activity/get-all-order-log-activities-by-orderId.view.ts ***!
  \**************************************************************************************************/
/*! exports provided: GetAllOrderLogActivitiesByOrderIdView, GetAllOrderLogActivitiesByOrderIdViewItem */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetAllOrderLogActivitiesByOrderIdView", function() { return GetAllOrderLogActivitiesByOrderIdView; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetAllOrderLogActivitiesByOrderIdViewItem", function() { return GetAllOrderLogActivitiesByOrderIdViewItem; });
var GetAllOrderLogActivitiesByOrderIdView = /** @class */ (function () {
    function GetAllOrderLogActivitiesByOrderIdView() {
        this.orderLogActivities = [];
    }
    return GetAllOrderLogActivitiesByOrderIdView;
}());

var GetAllOrderLogActivitiesByOrderIdViewItem = /** @class */ (function () {
    function GetAllOrderLogActivitiesByOrderIdViewItem() {
    }
    return GetAllOrderLogActivitiesByOrderIdViewItem;
}());



/***/ }),

/***/ "./src/app/shared/models/order-tasks/get-all-order-tasks-by-order-id.view.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/shared/models/order-tasks/get-all-order-tasks-by-order-id.view.ts ***!
  \***********************************************************************************/
/*! exports provided: GetAllOrderTasksByOrderIdView, GetAllOrderTasksByOrderIdGroupByTeamNameView, GetAllOrderTasksByOrderIdGroupByDueTimeView, OrderTaskGetAllOrderTasksByOrderIdViewItem, OrderGetAllOrderTasksByOrderIdViewItem, TeamGetAllOrderTasksByOrderIdViewItem, UserGetAllOrderTasksByOrderIdViewItem, MyTasksGetAllOrderTasksByOrderIdViewItem, TaskStatusGetAllOrderTasksByOrderIdViewItem, ProjectOrderGetAllOrderTasksByOrderIdViewItem */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetAllOrderTasksByOrderIdView", function() { return GetAllOrderTasksByOrderIdView; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetAllOrderTasksByOrderIdGroupByTeamNameView", function() { return GetAllOrderTasksByOrderIdGroupByTeamNameView; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetAllOrderTasksByOrderIdGroupByDueTimeView", function() { return GetAllOrderTasksByOrderIdGroupByDueTimeView; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderTaskGetAllOrderTasksByOrderIdViewItem", function() { return OrderTaskGetAllOrderTasksByOrderIdViewItem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderGetAllOrderTasksByOrderIdViewItem", function() { return OrderGetAllOrderTasksByOrderIdViewItem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TeamGetAllOrderTasksByOrderIdViewItem", function() { return TeamGetAllOrderTasksByOrderIdViewItem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserGetAllOrderTasksByOrderIdViewItem", function() { return UserGetAllOrderTasksByOrderIdViewItem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyTasksGetAllOrderTasksByOrderIdViewItem", function() { return MyTasksGetAllOrderTasksByOrderIdViewItem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskStatusGetAllOrderTasksByOrderIdViewItem", function() { return TaskStatusGetAllOrderTasksByOrderIdViewItem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProjectOrderGetAllOrderTasksByOrderIdViewItem", function() { return ProjectOrderGetAllOrderTasksByOrderIdViewItem; });
var GetAllOrderTasksByOrderIdView = /** @class */ (function () {
    function GetAllOrderTasksByOrderIdView() {
        this.orderTasks = [];
    }
    return GetAllOrderTasksByOrderIdView;
}());

var GetAllOrderTasksByOrderIdGroupByTeamNameView = /** @class */ (function () {
    function GetAllOrderTasksByOrderIdGroupByTeamNameView() {
        this.orderTasksGroupByName = [];
    }
    return GetAllOrderTasksByOrderIdGroupByTeamNameView;
}());

var GetAllOrderTasksByOrderIdGroupByDueTimeView = /** @class */ (function () {
    function GetAllOrderTasksByOrderIdGroupByDueTimeView() {
        this.orderTasksGroupByDueTime = [];
    }
    return GetAllOrderTasksByOrderIdGroupByDueTimeView;
}());

var OrderTaskGetAllOrderTasksByOrderIdViewItem = /** @class */ (function () {
    function OrderTaskGetAllOrderTasksByOrderIdViewItem() {
    }
    return OrderTaskGetAllOrderTasksByOrderIdViewItem;
}());

var OrderGetAllOrderTasksByOrderIdViewItem = /** @class */ (function () {
    function OrderGetAllOrderTasksByOrderIdViewItem() {
    }
    return OrderGetAllOrderTasksByOrderIdViewItem;
}());

var TeamGetAllOrderTasksByOrderIdViewItem = /** @class */ (function () {
    function TeamGetAllOrderTasksByOrderIdViewItem() {
    }
    return TeamGetAllOrderTasksByOrderIdViewItem;
}());

var UserGetAllOrderTasksByOrderIdViewItem = /** @class */ (function () {
    function UserGetAllOrderTasksByOrderIdViewItem() {
    }
    return UserGetAllOrderTasksByOrderIdViewItem;
}());

var MyTasksGetAllOrderTasksByOrderIdViewItem = /** @class */ (function () {
    function MyTasksGetAllOrderTasksByOrderIdViewItem() {
    }
    return MyTasksGetAllOrderTasksByOrderIdViewItem;
}());

var TaskStatusGetAllOrderTasksByOrderIdViewItem = /** @class */ (function () {
    function TaskStatusGetAllOrderTasksByOrderIdViewItem() {
    }
    return TaskStatusGetAllOrderTasksByOrderIdViewItem;
}());

var ProjectOrderGetAllOrderTasksByOrderIdViewItem = /** @class */ (function () {
    function ProjectOrderGetAllOrderTasksByOrderIdViewItem() {
    }
    return ProjectOrderGetAllOrderTasksByOrderIdViewItem;
}());



/***/ }),

/***/ "./src/app/shared/models/order/get-all-order.view.ts":
/*!***********************************************************!*\
  !*** ./src/app/shared/models/order/get-all-order.view.ts ***!
  \***********************************************************/
/*! exports provided: GetAllOrderView, GetAllOrderViewItem, GetAllOrderProjectViewItem, GetUserViewItem, GetAllOrderCustomerViewItem, GetAllOrderItemViewItem */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetAllOrderView", function() { return GetAllOrderView; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetAllOrderViewItem", function() { return GetAllOrderViewItem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetAllOrderProjectViewItem", function() { return GetAllOrderProjectViewItem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetUserViewItem", function() { return GetUserViewItem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetAllOrderCustomerViewItem", function() { return GetAllOrderCustomerViewItem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetAllOrderItemViewItem", function() { return GetAllOrderItemViewItem; });
var GetAllOrderView = /** @class */ (function () {
    function GetAllOrderView() {
        this.orders = [];
    }
    return GetAllOrderView;
}());

var GetAllOrderViewItem = /** @class */ (function () {
    function GetAllOrderViewItem() {
    }
    return GetAllOrderViewItem;
}());

var GetAllOrderProjectViewItem = /** @class */ (function () {
    function GetAllOrderProjectViewItem() {
    }
    return GetAllOrderProjectViewItem;
}());

var GetUserViewItem = /** @class */ (function () {
    function GetUserViewItem() {
    }
    return GetUserViewItem;
}());

var GetAllOrderCustomerViewItem = /** @class */ (function () {
    function GetAllOrderCustomerViewItem() {
    }
    return GetAllOrderCustomerViewItem;
}());

var GetAllOrderItemViewItem = /** @class */ (function () {
    function GetAllOrderItemViewItem() {
    }
    return GetAllOrderItemViewItem;
}());



/***/ }),

/***/ "./src/app/shared/models/order/get-order-view.ts":
/*!*******************************************************!*\
  !*** ./src/app/shared/models/order/get-order-view.ts ***!
  \*******************************************************/
/*! exports provided: GetOrderView, GetOrderOrderVendorViewItem, GetOrderOrderVendorQuoteViewItem, GetOrderOrderVendorQuoteItemSpecViewItem, GetOrderOrderCustomerQuoteViewItem, GetOrderOrderCustomerQuoteItemSpecViewItem, GetOrderProjectViewItem, GetOrderUserViewItem, GetOrderCustomerViewItem, GetOrderItemViewItem, GetOrderOrderItemSpecViewItem */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetOrderView", function() { return GetOrderView; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetOrderOrderVendorViewItem", function() { return GetOrderOrderVendorViewItem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetOrderOrderVendorQuoteViewItem", function() { return GetOrderOrderVendorQuoteViewItem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetOrderOrderVendorQuoteItemSpecViewItem", function() { return GetOrderOrderVendorQuoteItemSpecViewItem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetOrderOrderCustomerQuoteViewItem", function() { return GetOrderOrderCustomerQuoteViewItem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetOrderOrderCustomerQuoteItemSpecViewItem", function() { return GetOrderOrderCustomerQuoteItemSpecViewItem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetOrderProjectViewItem", function() { return GetOrderProjectViewItem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetOrderUserViewItem", function() { return GetOrderUserViewItem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetOrderCustomerViewItem", function() { return GetOrderCustomerViewItem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetOrderItemViewItem", function() { return GetOrderItemViewItem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetOrderOrderItemSpecViewItem", function() { return GetOrderOrderItemSpecViewItem; });
var GetOrderView = /** @class */ (function () {
    function GetOrderView() {
        this.confirmedCustomerQuote = new GetOrderOrderCustomerQuoteViewItem();
        this.confirmedVendorQuote = new GetOrderOrderVendorQuoteViewItem();
        this.item = new GetOrderItemViewItem();
        this.orderItemSpecs = [];
        this.orderCustomerQuotes = [];
        this.orderVendorQuotes = [];
    }
    return GetOrderView;
}());

var GetOrderOrderVendorViewItem = /** @class */ (function () {
    function GetOrderOrderVendorViewItem() {
    }
    return GetOrderOrderVendorViewItem;
}());

var GetOrderOrderVendorQuoteViewItem = /** @class */ (function () {
    function GetOrderOrderVendorQuoteViewItem() {
        this.orderCustomerQuoteItemSpecs = [];
        this.vendor = new GetOrderOrderVendorViewItem();
    }
    return GetOrderOrderVendorQuoteViewItem;
}());

var GetOrderOrderVendorQuoteItemSpecViewItem = /** @class */ (function () {
    function GetOrderOrderVendorQuoteItemSpecViewItem() {
    }
    return GetOrderOrderVendorQuoteItemSpecViewItem;
}());

var GetOrderOrderCustomerQuoteViewItem = /** @class */ (function () {
    function GetOrderOrderCustomerQuoteViewItem() {
        this.orderCustomerQuoteItemSpecs = [];
    }
    return GetOrderOrderCustomerQuoteViewItem;
}());

var GetOrderOrderCustomerQuoteItemSpecViewItem = /** @class */ (function () {
    function GetOrderOrderCustomerQuoteItemSpecViewItem() {
    }
    return GetOrderOrderCustomerQuoteItemSpecViewItem;
}());

var GetOrderProjectViewItem = /** @class */ (function () {
    function GetOrderProjectViewItem() {
    }
    return GetOrderProjectViewItem;
}());

var GetOrderUserViewItem = /** @class */ (function () {
    function GetOrderUserViewItem() {
    }
    return GetOrderUserViewItem;
}());

var GetOrderCustomerViewItem = /** @class */ (function () {
    function GetOrderCustomerViewItem() {
    }
    return GetOrderCustomerViewItem;
}());

var GetOrderItemViewItem = /** @class */ (function () {
    function GetOrderItemViewItem() {
    }
    return GetOrderItemViewItem;
}());

var GetOrderOrderItemSpecViewItem = /** @class */ (function () {
    function GetOrderOrderItemSpecViewItem() {
    }
    return GetOrderOrderItemSpecViewItem;
}());



/***/ }),

/***/ "./src/app/shared/models/order/update-order-confirmed-order-vendor-quote.view.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/shared/models/order/update-order-confirmed-order-vendor-quote.view.ts ***!
  \***************************************************************************************/
/*! exports provided: UpdateOrderConfirmedOrderVendorQuoteView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateOrderConfirmedOrderVendorQuoteView", function() { return UpdateOrderConfirmedOrderVendorQuoteView; });
var UpdateOrderConfirmedOrderVendorQuoteView = /** @class */ (function () {
    function UpdateOrderConfirmedOrderVendorQuoteView() {
    }
    return UpdateOrderConfirmedOrderVendorQuoteView;
}());



/***/ }),

/***/ "./src/app/shared/providers/services/order-attachment.service.ts":
/*!***********************************************************************!*\
  !*** ./src/app/shared/providers/services/order-attachment.service.ts ***!
  \***********************************************************************/
/*! exports provided: OrderAttachmentService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderAttachmentService", function() { return OrderAttachmentService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var OrderAttachmentService = /** @class */ (function () {
    function OrderAttachmentService(http) {
        this.http = http;
        this.apiControllerURL = environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl;
    }
    OrderAttachmentService.prototype.createOrderAttachment = function (attachment) {
        return this.http.post(this.apiControllerURL + "api/OrderAttachment/Create", attachment);
    };
    OrderAttachmentService.prototype.getAllOrderAttachmentsByOrderId = function (id) {
        return this.http.get(this.apiControllerURL + ("api/OrderAttachment/GetAllByOrderId/?id=" + id));
    };
    OrderAttachmentService.prototype.getOrderAttachment = function (id) {
        return this.http.get(this.apiControllerURL + "api/OrderAttachment/Get?id=" + id);
    };
    OrderAttachmentService.prototype.deleteAttachment = function (orderAttachment) {
        return this.http.post(this.apiControllerURL + "api/OrderAttachment/Delete", orderAttachment);
    };
    OrderAttachmentService.prototype.downloadAttachmentToBlob = function (id) {
        return this.http.get(this.apiControllerURL + "api/OrderAttachment/DownloadAttachment?id=" + id, { responseType: 'blob' });
    };
    OrderAttachmentService.prototype.downloadAttachmentToArray = function (id) {
        return this.http.get(this.apiControllerURL + "api/OrderAttachment/DownloadAttachment?id=" + id, { responseType: 'blob' });
    };
    OrderAttachmentService.prototype.downloadAttachment = function (id) {
        return this.http.get(this.apiControllerURL + "api/OrderAttachment/DownloadAttachment?id=" + id, { responseType: 'text' });
    };
    OrderAttachmentService.prototype.downloadAllOrderAttachment = function (id) {
        return this.http.get(this.apiControllerURL + "api/OrderAttachment/DownloadAllForOrder?id=" + id, { responseType: 'blob' });
    };
    OrderAttachmentService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], OrderAttachmentService);
    return OrderAttachmentService;
}());



/***/ }),

/***/ "./src/app/shared/providers/services/order-log-activity.service.ts":
/*!*************************************************************************!*\
  !*** ./src/app/shared/providers/services/order-log-activity.service.ts ***!
  \*************************************************************************/
/*! exports provided: OrderLogActivityService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderLogActivityService", function() { return OrderLogActivityService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var OrderLogActivityService = /** @class */ (function () {
    function OrderLogActivityService(http) {
        this.http = http;
        this.apiControllerURL = environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl;
    }
    OrderLogActivityService.prototype.getAllOrderLogActivities = function (id) {
        return this.http.get(this.apiControllerURL + ("api/OrderLogActivity/GetAllOrderLogActivitiesByOrderId/?id=" + id));
    };
    OrderLogActivityService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], OrderLogActivityService);
    return OrderLogActivityService;
}());



/***/ }),

/***/ "./src/app/shared/providers/services/order-tasks.service.ts":
/*!******************************************************************!*\
  !*** ./src/app/shared/providers/services/order-tasks.service.ts ***!
  \******************************************************************/
/*! exports provided: OrderTasksService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderTasksService", function() { return OrderTasksService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var OrderTasksService = /** @class */ (function () {
    function OrderTasksService(http) {
        this.http = http;
        this.apiControllerURL = environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].apiUrl;
    }
    OrderTasksService.prototype.getAllOrderTasksByOrderId = function (orderId) {
        return this.http.get(this.apiControllerURL + ("api/OrderTask/GetAllOrderTasksByOrderId/?id=" + orderId));
    };
    OrderTasksService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], OrderTasksService);
    return OrderTasksService;
}());



/***/ })

}]);
//# sourceMappingURL=project-and-orders-orders-orders-module-module.js.map