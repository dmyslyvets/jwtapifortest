(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["common"],{

/***/ "./src/app/shared/models/order/get-all-order-by-projectId.view.ts":
/*!************************************************************************!*\
  !*** ./src/app/shared/models/order/get-all-order-by-projectId.view.ts ***!
  \************************************************************************/
/*! exports provided: GetAllOrdersByProjectIdView, GetAllOrdersByProjectIdViewItem, GetAllOrdersByProjectIdProjectViewItem, GetAllOrderByProjectIdUserViewItem, GetAllOrdersByProjectIdCustomerViewItem, GetAllOrdersByProjectIdItemViewItem */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetAllOrdersByProjectIdView", function() { return GetAllOrdersByProjectIdView; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetAllOrdersByProjectIdViewItem", function() { return GetAllOrdersByProjectIdViewItem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetAllOrdersByProjectIdProjectViewItem", function() { return GetAllOrdersByProjectIdProjectViewItem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetAllOrderByProjectIdUserViewItem", function() { return GetAllOrderByProjectIdUserViewItem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetAllOrdersByProjectIdCustomerViewItem", function() { return GetAllOrdersByProjectIdCustomerViewItem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetAllOrdersByProjectIdItemViewItem", function() { return GetAllOrdersByProjectIdItemViewItem; });
var GetAllOrdersByProjectIdView = /** @class */ (function () {
    function GetAllOrdersByProjectIdView() {
        this.orders = [];
    }
    return GetAllOrdersByProjectIdView;
}());

var GetAllOrdersByProjectIdViewItem = /** @class */ (function () {
    function GetAllOrdersByProjectIdViewItem() {
    }
    return GetAllOrdersByProjectIdViewItem;
}());

var GetAllOrdersByProjectIdProjectViewItem = /** @class */ (function () {
    function GetAllOrdersByProjectIdProjectViewItem() {
    }
    return GetAllOrdersByProjectIdProjectViewItem;
}());

var GetAllOrderByProjectIdUserViewItem = /** @class */ (function () {
    function GetAllOrderByProjectIdUserViewItem() {
    }
    return GetAllOrderByProjectIdUserViewItem;
}());

var GetAllOrdersByProjectIdCustomerViewItem = /** @class */ (function () {
    function GetAllOrdersByProjectIdCustomerViewItem() {
    }
    return GetAllOrdersByProjectIdCustomerViewItem;
}());

var GetAllOrdersByProjectIdItemViewItem = /** @class */ (function () {
    function GetAllOrdersByProjectIdItemViewItem() {
    }
    return GetAllOrdersByProjectIdItemViewItem;
}());



/***/ }),

/***/ "./src/app/shared/models/project-attachment/delete-project-attachment.view.ts":
/*!************************************************************************************!*\
  !*** ./src/app/shared/models/project-attachment/delete-project-attachment.view.ts ***!
  \************************************************************************************/
/*! exports provided: DeleteProjectAttachmentView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeleteProjectAttachmentView", function() { return DeleteProjectAttachmentView; });
var DeleteProjectAttachmentView = /** @class */ (function () {
    function DeleteProjectAttachmentView() {
    }
    return DeleteProjectAttachmentView;
}());



/***/ }),

/***/ "./src/app/shared/models/project-attachment/get-all-by-project-id-projects-attachments-view.ts":
/*!*****************************************************************************************************!*\
  !*** ./src/app/shared/models/project-attachment/get-all-by-project-id-projects-attachments-view.ts ***!
  \*****************************************************************************************************/
/*! exports provided: GetAllByProjectIdProjectAttachmentsView, GetAllByProjectIdProjectAttachmentsViewItem */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetAllByProjectIdProjectAttachmentsView", function() { return GetAllByProjectIdProjectAttachmentsView; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetAllByProjectIdProjectAttachmentsViewItem", function() { return GetAllByProjectIdProjectAttachmentsViewItem; });
var GetAllByProjectIdProjectAttachmentsView = /** @class */ (function () {
    function GetAllByProjectIdProjectAttachmentsView() {
    }
    return GetAllByProjectIdProjectAttachmentsView;
}());

var GetAllByProjectIdProjectAttachmentsViewItem = /** @class */ (function () {
    function GetAllByProjectIdProjectAttachmentsViewItem() {
    }
    return GetAllByProjectIdProjectAttachmentsViewItem;
}());



/***/ }),

/***/ "./src/app/shared/models/project-attachment/get-project-attachment.view.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/shared/models/project-attachment/get-project-attachment.view.ts ***!
  \*********************************************************************************/
/*! exports provided: GetProjectAttachmentView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetProjectAttachmentView", function() { return GetProjectAttachmentView; });
var GetProjectAttachmentView = /** @class */ (function () {
    function GetProjectAttachmentView() {
    }
    return GetProjectAttachmentView;
}());



/***/ }),

/***/ "./src/app/shared/models/project/create-project.view.ts":
/*!**************************************************************!*\
  !*** ./src/app/shared/models/project/create-project.view.ts ***!
  \**************************************************************/
/*! exports provided: CreateProjectView, CreateProjectProjectTypeViewItem */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateProjectView", function() { return CreateProjectView; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateProjectProjectTypeViewItem", function() { return CreateProjectProjectTypeViewItem; });
var CreateProjectView = /** @class */ (function () {
    function CreateProjectView() {
        this.projectType = new CreateProjectProjectTypeViewItem();
    }
    return CreateProjectView;
}());

var CreateProjectProjectTypeViewItem = /** @class */ (function () {
    function CreateProjectProjectTypeViewItem() {
    }
    return CreateProjectProjectTypeViewItem;
}());



/***/ }),

/***/ "./src/app/shared/models/project/get-all-active-project.view.ts":
/*!**********************************************************************!*\
  !*** ./src/app/shared/models/project/get-all-active-project.view.ts ***!
  \**********************************************************************/
/*! exports provided: GetAllActiveProjectView, GetAllActiveProjectViewItem, GetAllActiveProjectProjectTypeViewItem, GetAllActiveProjectUserViewItem, GetAllActiveProjectCustomerViewItem */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetAllActiveProjectView", function() { return GetAllActiveProjectView; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetAllActiveProjectViewItem", function() { return GetAllActiveProjectViewItem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetAllActiveProjectProjectTypeViewItem", function() { return GetAllActiveProjectProjectTypeViewItem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetAllActiveProjectUserViewItem", function() { return GetAllActiveProjectUserViewItem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetAllActiveProjectCustomerViewItem", function() { return GetAllActiveProjectCustomerViewItem; });
var GetAllActiveProjectView = /** @class */ (function () {
    function GetAllActiveProjectView() {
        this.projects = [];
    }
    return GetAllActiveProjectView;
}());

var GetAllActiveProjectViewItem = /** @class */ (function () {
    function GetAllActiveProjectViewItem() {
    }
    return GetAllActiveProjectViewItem;
}());

var GetAllActiveProjectProjectTypeViewItem = /** @class */ (function () {
    function GetAllActiveProjectProjectTypeViewItem() {
    }
    return GetAllActiveProjectProjectTypeViewItem;
}());

var GetAllActiveProjectUserViewItem = /** @class */ (function () {
    function GetAllActiveProjectUserViewItem() {
    }
    return GetAllActiveProjectUserViewItem;
}());

var GetAllActiveProjectCustomerViewItem = /** @class */ (function () {
    function GetAllActiveProjectCustomerViewItem() {
    }
    return GetAllActiveProjectCustomerViewItem;
}());



/***/ }),

/***/ "./src/app/shared/models/project/get-all-project-types.view.ts":
/*!*********************************************************************!*\
  !*** ./src/app/shared/models/project/get-all-project-types.view.ts ***!
  \*********************************************************************/
/*! exports provided: GetAllProjectTypesView, GetAllProjectTypesViewItem */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetAllProjectTypesView", function() { return GetAllProjectTypesView; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetAllProjectTypesViewItem", function() { return GetAllProjectTypesViewItem; });
var GetAllProjectTypesView = /** @class */ (function () {
    function GetAllProjectTypesView() {
        this.projectTypes = [];
    }
    return GetAllProjectTypesView;
}());

var GetAllProjectTypesViewItem = /** @class */ (function () {
    function GetAllProjectTypesViewItem() {
    }
    return GetAllProjectTypesViewItem;
}());



/***/ }),

/***/ "./src/app/shared/models/project/update-project.view.ts":
/*!**************************************************************!*\
  !*** ./src/app/shared/models/project/update-project.view.ts ***!
  \**************************************************************/
/*! exports provided: UpdateProjectView, UpdateProjectProjectTypeViewItem */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateProjectView", function() { return UpdateProjectView; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateProjectProjectTypeViewItem", function() { return UpdateProjectProjectTypeViewItem; });
var UpdateProjectView = /** @class */ (function () {
    function UpdateProjectView() {
    }
    return UpdateProjectView;
}());

var UpdateProjectProjectTypeViewItem = /** @class */ (function () {
    function UpdateProjectProjectTypeViewItem() {
    }
    return UpdateProjectProjectTypeViewItem;
}());



/***/ }),

/***/ "./src/app/shared/providers/services/project-attachment.service.ts":
/*!*************************************************************************!*\
  !*** ./src/app/shared/providers/services/project-attachment.service.ts ***!
  \*************************************************************************/
/*! exports provided: ProjectAttachmentservice */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProjectAttachmentservice", function() { return ProjectAttachmentservice; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ProjectAttachmentservice = /** @class */ (function () {
    function ProjectAttachmentservice(http) {
        this.http = http;
        this.apiUrl = environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl;
    }
    ProjectAttachmentservice.prototype.createProjectAttachment = function (attachment) {
        return this.http.post(this.apiUrl + "api/ProjectAttachment/Create", attachment);
    };
    ProjectAttachmentservice.prototype.getAllProjectsAttachmentsByProjectId = function (id) {
        return this.http.get(this.apiUrl + "api/ProjectAttachment/GetAllByProjectId?id=" + id);
    };
    ProjectAttachmentservice.prototype.deleteAttachment = function (attachmentView) {
        return this.http.post(this.apiUrl + "api/ProjectAttachment/Delete", attachmentView);
    };
    ProjectAttachmentservice.prototype.getProjectAttachment = function (id) {
        return this.http.get(this.apiUrl + "api/ProjectAttachment/Get?id=" + id);
    };
    ProjectAttachmentservice.prototype.downloadAttachmentToBlob = function (id) {
        return this.http.get(this.apiUrl + "api/ProjectAttachment/DownloadAttachment?id=" + id, { responseType: 'blob' });
    };
    ProjectAttachmentservice.prototype.downloadAttachmentToArray = function (id) {
        return this.http.get(this.apiUrl + "api/ProjectAttachment/DownloadAttachment?id=" + id, { responseType: 'blob' });
    };
    ProjectAttachmentservice.prototype.downloadAttachment = function (id) {
        return this.http.get(this.apiUrl + "api/ProjectAttachment/DownloadAttachment?id=" + id, { responseType: 'text' });
    };
    ProjectAttachmentservice.prototype.downloadAllProjectsAttachment = function (id) {
        return this.http.get(this.apiUrl + "api/ProjectAttachment/DownloadAllForProject?id=" + id, { responseType: 'blob' });
    };
    ProjectAttachmentservice = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], ProjectAttachmentservice);
    return ProjectAttachmentservice;
}());



/***/ })

}]);
//# sourceMappingURL=common.js.map