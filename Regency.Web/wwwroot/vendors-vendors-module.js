(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["vendors-vendors-module"],{

/***/ "./src/app/vendors/vendors-routing.module.ts":
/*!***************************************************!*\
  !*** ./src/app/vendors/vendors-routing.module.ts ***!
  \***************************************************/
/*! exports provided: VendorsRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VendorsRoutingModule", function() { return VendorsRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _vendors_vendors_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./vendors/vendors.component */ "./src/app/vendors/vendors/vendors.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: _vendors_vendors_component__WEBPACK_IMPORTED_MODULE_2__["VendorsComponent"],
        data: {
            title: 'Vendors'
        }
    }
];
var VendorsRoutingModule = /** @class */ (function () {
    function VendorsRoutingModule() {
    }
    VendorsRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], VendorsRoutingModule);
    return VendorsRoutingModule;
}());



/***/ }),

/***/ "./src/app/vendors/vendors.module.ts":
/*!*******************************************!*\
  !*** ./src/app/vendors/vendors.module.ts ***!
  \*******************************************/
/*! exports provided: VendorsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VendorsModule", function() { return VendorsModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _vendors_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./vendors-routing.module */ "./src/app/vendors/vendors-routing.module.ts");
/* harmony import */ var _vendors_vendors_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./vendors/vendors.component */ "./src/app/vendors/vendors/vendors.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var VendorsModule = /** @class */ (function () {
    function VendorsModule() {
    }
    VendorsModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _vendors_routing_module__WEBPACK_IMPORTED_MODULE_2__["VendorsRoutingModule"]
            ],
            declarations: [_vendors_vendors_component__WEBPACK_IMPORTED_MODULE_3__["VendorsComponent"]]
        })
    ], VendorsModule);
    return VendorsModule;
}());



/***/ }),

/***/ "./src/app/vendors/vendors/vendors.component.html":
/*!********************************************************!*\
  !*** ./src/app/vendors/vendors/vendors.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  vendors works!\r\n</p>\r\n"

/***/ }),

/***/ "./src/app/vendors/vendors/vendors.component.scss":
/*!********************************************************!*\
  !*** ./src/app/vendors/vendors/vendors.component.scss ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/vendors/vendors/vendors.component.ts":
/*!******************************************************!*\
  !*** ./src/app/vendors/vendors/vendors.component.ts ***!
  \******************************************************/
/*! exports provided: VendorsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VendorsComponent", function() { return VendorsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var VendorsComponent = /** @class */ (function () {
    function VendorsComponent() {
    }
    VendorsComponent.prototype.ngOnInit = function () {
    };
    VendorsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-vendors',
            template: __webpack_require__(/*! ./vendors.component.html */ "./src/app/vendors/vendors/vendors.component.html"),
            styles: [__webpack_require__(/*! ./vendors.component.scss */ "./src/app/vendors/vendors/vendors.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], VendorsComponent);
    return VendorsComponent;
}());



/***/ })

}]);
//# sourceMappingURL=vendors-vendors-module.js.map