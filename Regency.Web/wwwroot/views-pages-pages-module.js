(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["views-pages-pages-module"],{

/***/ "./src/app/shared/models/account/auth-data.model.ts":
/*!**********************************************************!*\
  !*** ./src/app/shared/models/account/auth-data.model.ts ***!
  \**********************************************************/
/*! exports provided: AuthData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthData", function() { return AuthData; });
var AuthData = /** @class */ (function () {
    function AuthData() {
    }
    return AuthData;
}());



/***/ }),

/***/ "./src/app/shared/models/account/forgot-password.model.ts":
/*!****************************************************************!*\
  !*** ./src/app/shared/models/account/forgot-password.model.ts ***!
  \****************************************************************/
/*! exports provided: ForgotPasswordModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForgotPasswordModel", function() { return ForgotPasswordModel; });
var ForgotPasswordModel = /** @class */ (function () {
    function ForgotPasswordModel() {
    }
    return ForgotPasswordModel;
}());



/***/ }),

/***/ "./src/app/shared/models/account/index.ts":
/*!************************************************!*\
  !*** ./src/app/shared/models/account/index.ts ***!
  \************************************************/
/*! exports provided: ResetPasswordModel, ForgotPasswordModel, LoginModel, AuthData, UserRole */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _reset_password_model__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./reset-password.model */ "./src/app/shared/models/account/reset-password.model.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ResetPasswordModel", function() { return _reset_password_model__WEBPACK_IMPORTED_MODULE_0__["ResetPasswordModel"]; });

/* harmony import */ var _forgot_password_model__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./forgot-password.model */ "./src/app/shared/models/account/forgot-password.model.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ForgotPasswordModel", function() { return _forgot_password_model__WEBPACK_IMPORTED_MODULE_1__["ForgotPasswordModel"]; });

/* harmony import */ var _login_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./login.model */ "./src/app/shared/models/account/login.model.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "LoginModel", function() { return _login_model__WEBPACK_IMPORTED_MODULE_2__["LoginModel"]; });

/* harmony import */ var _auth_data_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./auth-data.model */ "./src/app/shared/models/account/auth-data.model.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AuthData", function() { return _auth_data_model__WEBPACK_IMPORTED_MODULE_3__["AuthData"]; });

/* harmony import */ var _user_role_enum__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./user-role.enum */ "./src/app/shared/models/account/user-role.enum.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "UserRole", function() { return _user_role_enum__WEBPACK_IMPORTED_MODULE_4__["UserRole"]; });








/***/ }),

/***/ "./src/app/shared/models/account/login.model.ts":
/*!******************************************************!*\
  !*** ./src/app/shared/models/account/login.model.ts ***!
  \******************************************************/
/*! exports provided: LoginModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginModel", function() { return LoginModel; });
var LoginModel = /** @class */ (function () {
    function LoginModel() {
    }
    return LoginModel;
}());



/***/ }),

/***/ "./src/app/shared/models/account/reset-password.model.ts":
/*!***************************************************************!*\
  !*** ./src/app/shared/models/account/reset-password.model.ts ***!
  \***************************************************************/
/*! exports provided: ResetPasswordModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResetPasswordModel", function() { return ResetPasswordModel; });
var ResetPasswordModel = /** @class */ (function () {
    function ResetPasswordModel() {
    }
    return ResetPasswordModel;
}());



/***/ }),

/***/ "./src/app/shared/models/account/user-role.enum.ts":
/*!*********************************************************!*\
  !*** ./src/app/shared/models/account/user-role.enum.ts ***!
  \*********************************************************/
/*! exports provided: UserRole */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserRole", function() { return UserRole; });
var UserRole;
(function (UserRole) {
    UserRole[UserRole["None"] = 0] = "None";
    UserRole[UserRole["Admin"] = 1] = "Admin";
    UserRole[UserRole["User"] = 2] = "User";
})(UserRole || (UserRole = {}));


/***/ }),

/***/ "./src/app/shared/providers/services/noitification.service.ts":
/*!********************************************************************!*\
  !*** ./src/app/shared/providers/services/noitification.service.ts ***!
  \********************************************************************/
/*! exports provided: NotificationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationService", function() { return NotificationService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NotificationService = /** @class */ (function () {
    function NotificationService(toastr) {
        this.toastr = toastr;
    }
    NotificationService.prototype.showSuccess = function (message) {
        this.toastr.success(message, 'Success!');
    };
    NotificationService.prototype.showError = function (message) {
        this.toastr.error(message, 'Error!');
    };
    NotificationService.prototype.showErrors = function (data) {
        if (data.length == 0 || data == null) {
            return;
        }
        var errorMessage = "";
        data.forEach(function (file) {
            errorMessage = errorMessage.concat(file.concat("\n"));
        });
        this.toastr.error(errorMessage, 'Error!');
    };
    NotificationService.prototype.showWarning = function (message) {
        this.toastr.warning(message);
    };
    NotificationService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [ngx_toastr__WEBPACK_IMPORTED_MODULE_1__["ToastrService"]])
    ], NotificationService);
    return NotificationService;
}());



/***/ }),

/***/ "./src/app/views/pages/404.component.html":
/*!************************************************!*\
  !*** ./src/app/views/pages/404.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"app flex-row align-items-center\">\n  <div class=\"container\">\n    <div class=\"row justify-content-center\">\n      <div class=\"col-md-6\">\n        <div class=\"clearfix\">\n          <h1 class=\"float-left display-3 mr-4\">404</h1>\n          <h4 class=\"pt-3\">Oops! You're lost.</h4>\n          <p class=\"text-muted\">The page you are looking for was not found.</p>\n        </div>\n        <div class=\"input-prepend input-group\">\n          <div class=\"input-group-prepend\">\n            <span class=\"input-group-text\"><i class=\"fa fa-search\"></i></span>\n          </div>\n          <input id=\"prependedInput\" class=\"form-control\" size=\"16\" type=\"text\" placeholder=\"What are you looking for?\">\n          <span class=\"input-group-append\">\n            <button class=\"btn btn-info\" type=\"button\">Search</button>\n          </span>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/views/pages/404.component.ts":
/*!**********************************************!*\
  !*** ./src/app/views/pages/404.component.ts ***!
  \**********************************************/
/*! exports provided: P404Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "P404Component", function() { return P404Component; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var P404Component = /** @class */ (function () {
    function P404Component() {
    }
    P404Component = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            template: __webpack_require__(/*! ./404.component.html */ "./src/app/views/pages/404.component.html")
        }),
        __metadata("design:paramtypes", [])
    ], P404Component);
    return P404Component;
}());



/***/ }),

/***/ "./src/app/views/pages/500.component.html":
/*!************************************************!*\
  !*** ./src/app/views/pages/500.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"app flex-row align-items-center\">\n  <div class=\"container\">\n    <div class=\"row justify-content-center\">\n      <div class=\"col-md-6\">\n        <div class=\"clearfix\">\n          <h1 class=\"float-left display-3 mr-4\">500</h1>\n          <h4 class=\"pt-3\">Houston, we have a problem!</h4>\n          <p class=\"text-muted\">The page you are looking for is temporarily unavailable.</p>\n        </div>\n        <div class=\"input-prepend input-group\">\n          <div class=\"input-group-prepend\">\n            <span class=\"input-group-text\"><i class=\"fa fa-search\"></i></span>\n          </div>\n          <input id=\"prependedInput\" class=\"form-control\" size=\"16\" type=\"text\" placeholder=\"What are you looking for?\">\n          <span class=\"input-group-append\">\n            <button class=\"btn btn-info\" type=\"button\">Search</button>\n          </span>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/views/pages/500.component.ts":
/*!**********************************************!*\
  !*** ./src/app/views/pages/500.component.ts ***!
  \**********************************************/
/*! exports provided: P500Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "P500Component", function() { return P500Component; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var P500Component = /** @class */ (function () {
    function P500Component() {
    }
    P500Component = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            template: __webpack_require__(/*! ./500.component.html */ "./src/app/views/pages/500.component.html")
        }),
        __metadata("design:paramtypes", [])
    ], P500Component);
    return P500Component;
}());



/***/ }),

/***/ "./src/app/views/pages/forgot-password.component.html":
/*!************************************************************!*\
  !*** ./src/app/views/pages/forgot-password.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"app flex-row align-items-center\">\r\n  <div class=\"container\">\r\n    <div class=\"row justify-content-center\">\r\n      <div class=\"col-md-6\">\r\n        <div class=\"card mx-4\">\r\n          <div class=\"card-body p-4\">\r\n            <h1>Forgot password?</h1>\r\n            <p class=\"text-muted\">Request password reset link</p>            \r\n            <div class=\"input-group mb-3\">\r\n              <div class=\"input-group-prepend\">\r\n                <span class=\"input-group-text\">@</span>\r\n              </div>\r\n              <input type=\"text\" class=\"form-control\" placeholder=\"Email\" [(ngModel)]=\"model.email\">\r\n            </div>\r\n            <button type=\"button\" class=\"btn btn-block btn-success\" (click)=\"forgotPassword()\">Send</button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/views/pages/forgot-password.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/views/pages/forgot-password.component.ts ***!
  \**********************************************************/
/*! exports provided: ForgotPasswordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForgotPasswordComponent", function() { return ForgotPasswordComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_shared_providers_providers__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/shared/providers/providers */ "./src/app/shared/providers/providers/index.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_models_account__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../shared/models/account */ "./src/app/shared/models/account/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ForgotPasswordComponent = /** @class */ (function () {
    function ForgotPasswordComponent(accountProvider, router) {
        this.accountProvider = accountProvider;
        this.router = router;
        this.model = new _shared_models_account__WEBPACK_IMPORTED_MODULE_3__["ForgotPasswordModel"]();
    }
    ForgotPasswordComponent.prototype.forgotPassword = function () {
        var _this = this;
        this.isLoading = true;
        this.accountProvider.forgotPassword(this.model).subscribe(function () {
            _this.router.navigateByUrl('pages/login');
        }, function (err) {
            _this.isLoading = false;
        });
    };
    ForgotPasswordComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            template: __webpack_require__(/*! ./forgot-password.component.html */ "./src/app/views/pages/forgot-password.component.html")
        }),
        __metadata("design:paramtypes", [app_shared_providers_providers__WEBPACK_IMPORTED_MODULE_1__["AccountProvider"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], ForgotPasswordComponent);
    return ForgotPasswordComponent;
}());



/***/ }),

/***/ "./src/app/views/pages/login.component.html":
/*!**************************************************!*\
  !*** ./src/app/views/pages/login.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"app flex-row align-items-center\">\r\n  <div class=\"container\">\r\n    <div class=\"row justify-content-center\">\r\n      <div class=\"login-block\">\r\n        <div class=\"card-group mb-0\">\r\n          <div class=\"card p-4\">\r\n            <div class=\"card-body\">\r\n              <h1>Login</h1>\r\n              <p class=\"text-muted\">Sign In to your account</p>\r\n              <div class=\"input-group mb-3\">\r\n                <div class=\"input-group-prepend\">\r\n                  <span class=\"input-group-text\"><i class=\"icon-user\"></i></span>\r\n                </div>\r\n                <input type=\"text\" class=\"form-control\" placeholder=\"Username\" [(ngModel)]=\"account.email\">\r\n              </div>\r\n              <div class=\"input-group mb-4\">\r\n                <div class=\"input-group-prepend\">\r\n                  <span class=\"input-group-text\"><i class=\"icon-lock\"></i></span>\r\n                </div>\r\n                <input type=\"password\" class=\"form-control\" placeholder=\"Password\" (keyup.enter)=\"login()\" [(ngModel)]=\"account.password\">\r\n              </div>\r\n              <div class=\"row\">\r\n                <div class=\"col-6\">\r\n                  <button type=\"button\" class=\"btn btn-primary px-4\" [disabled]=\"isLoading\" (click)=\"login()\">Login</button>\r\n                </div>\r\n                <div class=\"col-6 text-right\">\r\n                  <button type=\"button\" class=\"btn btn-link px-0\" (click)=\"forgotPassword()\">Forgot password?</button>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <!--<div class=\"card text-white bg-primary py-5 d-md-down-none\" style=\"width:44%\">\r\n            <div class=\"card-body text-center\">\r\n              <div>\r\n                <h2>Sign up</h2>\r\n                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\r\n                <button type=\"button\" class=\"btn btn-primary active mt-3\">Register Now!</button>\r\n              </div>\r\n            </div>\r\n          </div>-->\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/views/pages/login.component.scss":
/*!**************************************************!*\
  !*** ./src/app/views/pages/login.component.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".login-block {\n  max-width: 450px;\n  width: 100%; }\n"

/***/ }),

/***/ "./src/app/views/pages/login.component.ts":
/*!************************************************!*\
  !*** ./src/app/views/pages/login.component.ts ***!
  \************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var app_shared_providers_providers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/shared/providers/providers */ "./src/app/shared/providers/providers/index.ts");
/* harmony import */ var app_shared_providers_services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/shared/providers/services */ "./src/app/shared/providers/services/index.ts");
/* harmony import */ var app_shared_providers_services_noitification_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/shared/providers/services/noitification.service */ "./src/app/shared/providers/services/noitification.service.ts");
/* harmony import */ var app_shared_models_account__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! app/shared/models/account */ "./src/app/shared/models/account/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var LoginComponent = /** @class */ (function () {
    function LoginComponent(accountService, accountProvider, router, notificationService) {
        this.accountService = accountService;
        this.accountProvider = accountProvider;
        this.router = router;
        this.notificationService = notificationService;
        this.account = new app_shared_models_account__WEBPACK_IMPORTED_MODULE_5__["LoginModel"]();
        this.isLoading = false;
    }
    LoginComponent.prototype.login = function () {
        var _this = this;
        this.isLoading = true;
        this.accountProvider.login(this.account).subscribe(function (res) {
            _this.accountService.onLogin(res);
            _this.router.navigate(['dashboard']);
        }, function (err) {
            _this.notificationService.showError("Incorrect login or password");
            _this.isLoading = false;
        });
    };
    LoginComponent.prototype.forgotPassword = function () {
        this.router.navigateByUrl('pages/forgot-password');
    };
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/views/pages/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.scss */ "./src/app/views/pages/login.component.scss")]
        }),
        __metadata("design:paramtypes", [app_shared_providers_services__WEBPACK_IMPORTED_MODULE_3__["AccountService"],
            app_shared_providers_providers__WEBPACK_IMPORTED_MODULE_2__["AccountProvider"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            app_shared_providers_services_noitification_service__WEBPACK_IMPORTED_MODULE_4__["NotificationService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/views/pages/pages-routing.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/views/pages/pages-routing.module.ts ***!
  \*****************************************************/
/*! exports provided: PagesRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PagesRoutingModule", function() { return PagesRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _404_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./404.component */ "./src/app/views/pages/404.component.ts");
/* harmony import */ var _500_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./500.component */ "./src/app/views/pages/500.component.ts");
/* harmony import */ var _login_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./login.component */ "./src/app/views/pages/login.component.ts");
/* harmony import */ var _forgot_password_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./forgot-password.component */ "./src/app/views/pages/forgot-password.component.ts");
/* harmony import */ var _reset_password_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./reset-password.component */ "./src/app/views/pages/reset-password.component.ts");
/* harmony import */ var _reset_forgot_password_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./reset-forgot-password.component */ "./src/app/views/pages/reset-forgot-password.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var routes = [
    {
        path: '',
        data: {
            title: 'Example Pages'
        },
        children: [
            {
                path: '404',
                component: _404_component__WEBPACK_IMPORTED_MODULE_2__["P404Component"],
                data: {
                    title: 'Page 404'
                }
            },
            {
                path: '500',
                component: _500_component__WEBPACK_IMPORTED_MODULE_3__["P500Component"],
                data: {
                    title: 'Page 500'
                }
            },
            {
                path: 'login',
                component: _login_component__WEBPACK_IMPORTED_MODULE_4__["LoginComponent"],
                data: {
                    title: 'Login Page'
                }
            },
            {
                path: 'forgot-password',
                component: _forgot_password_component__WEBPACK_IMPORTED_MODULE_5__["ForgotPasswordComponent"],
                data: {
                    title: 'Forgot Password Page'
                }
            },
            {
                path: 'reset-password/:token',
                component: _reset_password_component__WEBPACK_IMPORTED_MODULE_6__["ResetPasswordComponent"],
                data: {
                    title: 'Reset Password Page'
                }
            },
            {
                path: 'reset-forgot-password/:token/:email',
                component: _reset_forgot_password_component__WEBPACK_IMPORTED_MODULE_7__["ResetForgotPasswordComponent"],
                data: {
                    title: 'Reset Password Page'
                }
            }
            //{
            //  path: 'register',
            //  component: RegisterComponent,
            //  data: {
            //    title: 'Register Page'
            //  }
            //}
        ]
    }
];
var PagesRoutingModule = /** @class */ (function () {
    function PagesRoutingModule() {
    }
    PagesRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], PagesRoutingModule);
    return PagesRoutingModule;
}());



/***/ }),

/***/ "./src/app/views/pages/pages.module.ts":
/*!*********************************************!*\
  !*** ./src/app/views/pages/pages.module.ts ***!
  \*********************************************/
/*! exports provided: PagesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PagesModule", function() { return PagesModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _404_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./404.component */ "./src/app/views/pages/404.component.ts");
/* harmony import */ var _500_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./500.component */ "./src/app/views/pages/500.component.ts");
/* harmony import */ var _login_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./login.component */ "./src/app/views/pages/login.component.ts");
/* harmony import */ var _register_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./register.component */ "./src/app/views/pages/register.component.ts");
/* harmony import */ var _pages_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./pages-routing.module */ "./src/app/views/pages/pages-routing.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _forgot_password_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./forgot-password.component */ "./src/app/views/pages/forgot-password.component.ts");
/* harmony import */ var _reset_password_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./reset-password.component */ "./src/app/views/pages/reset-password.component.ts");
/* harmony import */ var _reset_forgot_password_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./reset-forgot-password.component */ "./src/app/views/pages/reset-forgot-password.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var PagesModule = /** @class */ (function () {
    function PagesModule() {
    }
    PagesModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"], _pages_routing_module__WEBPACK_IMPORTED_MODULE_5__["PagesRoutingModule"]],
            declarations: [
                _404_component__WEBPACK_IMPORTED_MODULE_1__["P404Component"],
                _500_component__WEBPACK_IMPORTED_MODULE_2__["P500Component"],
                _login_component__WEBPACK_IMPORTED_MODULE_3__["LoginComponent"],
                _register_component__WEBPACK_IMPORTED_MODULE_4__["RegisterComponent"],
                _forgot_password_component__WEBPACK_IMPORTED_MODULE_7__["ForgotPasswordComponent"],
                _reset_password_component__WEBPACK_IMPORTED_MODULE_8__["ResetPasswordComponent"],
                _reset_forgot_password_component__WEBPACK_IMPORTED_MODULE_9__["ResetForgotPasswordComponent"]
            ]
        })
    ], PagesModule);
    return PagesModule;
}());



/***/ }),

/***/ "./src/app/views/pages/register.component.html":
/*!*****************************************************!*\
  !*** ./src/app/views/pages/register.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"app flex-row align-items-center\">\n  <div class=\"container\">\n    <div class=\"row justify-content-center\">\n      <div class=\"col-md-6\">\n        <div class=\"card mx-4\">\n          <div class=\"card-body p-4\">\n            <h1>Register</h1>\n            <p class=\"text-muted\">Create your account</p>\n            <div class=\"input-group mb-3\">\n              <div class=\"input-group-prepend\">\n                <span class=\"input-group-text\"><i class=\"icon-user\"></i></span>\n              </div>\n              <input type=\"text\" class=\"form-control\" placeholder=\"Username\">\n            </div>\n\n            <div class=\"input-group mb-3\">\n              <div class=\"input-group-prepend\">\n                <span class=\"input-group-text\">@</span>\n              </div>\n              <input type=\"text\" class=\"form-control\" placeholder=\"Email\">\n            </div>\n\n            <div class=\"input-group mb-3\">\n              <div class=\"input-group-prepend\">\n                <span class=\"input-group-text\"><i class=\"icon-lock\"></i></span>\n              </div>\n              <input type=\"password\" class=\"form-control\" placeholder=\"Password\">\n            </div>\n\n            <div class=\"input-group mb-4\">\n              <div class=\"input-group-prepend\">\n                <span class=\"input-group-text\"><i class=\"icon-lock\"></i></span>\n              </div>\n              <input type=\"password\" class=\"form-control\" placeholder=\"Repeat password\">\n            </div>\n\n            <button type=\"button\" class=\"btn btn-block btn-success\">Create Account</button>\n          </div>\n          <div class=\"card-footer p-4\">\n            <div class=\"row\">\n              <div class=\"col-6\">\n                <button class=\"btn btn-block btn-facebook\" type=\"button\"><span>facebook</span></button>\n              </div>\n              <div class=\"col-6\">\n                <button class=\"btn btn-block btn-twitter\" type=\"button\"><span>twitter</span></button>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/views/pages/register.component.ts":
/*!***************************************************!*\
  !*** ./src/app/views/pages/register.component.ts ***!
  \***************************************************/
/*! exports provided: RegisterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterComponent", function() { return RegisterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var RegisterComponent = /** @class */ (function () {
    function RegisterComponent() {
    }
    RegisterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            template: __webpack_require__(/*! ./register.component.html */ "./src/app/views/pages/register.component.html")
        }),
        __metadata("design:paramtypes", [])
    ], RegisterComponent);
    return RegisterComponent;
}());



/***/ }),

/***/ "./src/app/views/pages/reset-forgot-password.component.html":
/*!******************************************************************!*\
  !*** ./src/app/views/pages/reset-forgot-password.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"app flex-row align-items-center\">\r\n  <div class=\"container\">\r\n    <div class=\"row justify-content-center\">\r\n      <div class=\"col-md-6\">\r\n        <div class=\"card mx-4\">\r\n          <div class=\"card-body p-4\">\r\n            <h1>Reset password</h1>\r\n            <p class=\"text-muted\">Set new password</p>\r\n\r\n            <div class=\"input-group mb-3\">\r\n              <div class=\"input-group-prepend\">\r\n                <span class=\"input-group-text\"><i class=\"icon-lock\"></i></span>\r\n              </div>\r\n              <input type=\"password\" class=\"form-control\" placeholder=\"New password\" [(ngModel)]=\"model.password\">\r\n            </div>\r\n\r\n            <div class=\"input-group mb-4\">\r\n              <div class=\"input-group-prepend\">\r\n                <span class=\"input-group-text\"><i class=\"icon-lock\"></i></span>\r\n              </div>\r\n              <input type=\"password\" class=\"form-control\" placeholder=\"Confirm password\" [(ngModel)]=\"model.confirmPassword\">\r\n            </div>\r\n\r\n            <button type=\"button\" class=\"btn btn-block btn-success\" (click)=\"resetPassword()\">Set</button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/views/pages/reset-forgot-password.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/views/pages/reset-forgot-password.component.ts ***!
  \****************************************************************/
/*! exports provided: ResetForgotPasswordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResetForgotPasswordComponent", function() { return ResetForgotPasswordComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_shared_providers_providers__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/shared/providers/providers */ "./src/app/shared/providers/providers/index.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var app_shared_providers_services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/shared/providers/services */ "./src/app/shared/providers/services/index.ts");
/* harmony import */ var _shared_models_account__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../shared/models/account */ "./src/app/shared/models/account/index.ts");
/* harmony import */ var _shared_providers_services_noitification_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../shared/providers/services/noitification.service */ "./src/app/shared/providers/services/noitification.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ResetForgotPasswordComponent = /** @class */ (function () {
    function ResetForgotPasswordComponent(accountProvider, route, router, accountService, notificationService) {
        this.accountProvider = accountProvider;
        this.route = route;
        this.router = router;
        this.accountService = accountService;
        this.notificationService = notificationService;
        this.model = new _shared_models_account__WEBPACK_IMPORTED_MODULE_4__["ResetPasswordModel"]();
        this.regexForUpperCase = /[A-Z]+/;
        this.regexForLowerCase = /[a-z]+/;
        this.regexForDigit = /[\d]+/;
        this.containsOneUpperCase = false;
        this.containsOneDigit = false;
        this.containsOneLowerCase = false;
        this.containsSixChars = false;
        this.matchConfirmPassword = false;
        this.validPassword = true;
    }
    ResetForgotPasswordComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.model.token = params['token'];
            _this.model.email = params['email'];
        });
    };
    ResetForgotPasswordComponent.prototype.validatePassword = function () {
        this.validateSixChars();
        this.validateForDigit();
        this.validateLowerCase();
        this.validateUpperCase();
    };
    ResetForgotPasswordComponent.prototype.validateConfirmPassword = function () {
        this.matchConfirmPassword = (this.model.confirmPassword === this.model.password);
        if (!this.matchConfirmPassword) {
            this.validPassword = false;
            this.notificationService.showError("Confirm password doesn't match!");
        }
    };
    ResetForgotPasswordComponent.prototype.validateForDigit = function () {
        this.containsOneDigit = this.regexForDigit.test(this.model.password);
        if (!this.containsOneDigit) {
            this.validPassword = false;
            this.notificationService.showError("Should contain at least 1 digit!");
        }
    };
    ResetForgotPasswordComponent.prototype.validateLowerCase = function () {
        if (!(this.model.password === undefined)) {
            this.containsOneLowerCase = this.regexForLowerCase.test(this.model.password);
        }
        if (this.model.password === undefined) {
            this.containsOneLowerCase = false;
        }
        if (!this.containsOneLowerCase) {
            this.validPassword = false;
            this.notificationService.showError("Should contain at least 1 latin lowercase letter!");
        }
    };
    ResetForgotPasswordComponent.prototype.validateUpperCase = function () {
        this.containsOneUpperCase = this.regexForUpperCase.test(this.model.password);
        if (!this.containsOneUpperCase) {
            this.validPassword = false;
            this.notificationService.showError("Should contain at least 1 latin uppercase letter!");
        }
    };
    ResetForgotPasswordComponent.prototype.validateSixChars = function () {
        if (!(this.model.password === undefined)) {
            this.containsSixChars = (this.model.password.length >= 6);
        }
        if (this.model.password === undefined) {
            this.containsSixChars = false;
        }
        if (!this.containsSixChars) {
            this.validPassword = false;
            this.notificationService.showError("Should contain at least 6 charters!");
        }
    };
    ResetForgotPasswordComponent.prototype.resetPassword = function () {
        var _this = this;
        this.validPassword = true;
        this.validatePassword();
        this.validateConfirmPassword();
        if (this.validPassword) {
            this.isLoading = true;
            this.accountProvider.resetPassword(this.model).subscribe(function () {
                _this.router.navigateByUrl('pages/login');
            }, function (err) {
                _this.notificationService.showError("Change password failed! Invalid token!");
                _this.isLoading = false;
            });
        }
    };
    ResetForgotPasswordComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            template: __webpack_require__(/*! ./reset-forgot-password.component.html */ "./src/app/views/pages/reset-forgot-password.component.html")
        }),
        __metadata("design:paramtypes", [app_shared_providers_providers__WEBPACK_IMPORTED_MODULE_1__["AccountProvider"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            app_shared_providers_services__WEBPACK_IMPORTED_MODULE_3__["AccountService"],
            _shared_providers_services_noitification_service__WEBPACK_IMPORTED_MODULE_5__["NotificationService"]])
    ], ResetForgotPasswordComponent);
    return ResetForgotPasswordComponent;
}());



/***/ }),

/***/ "./src/app/views/pages/reset-password.component.html":
/*!***********************************************************!*\
  !*** ./src/app/views/pages/reset-password.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"app flex-row align-items-center\">\r\n  <div class=\"container\">\r\n    <div class=\"row justify-content-center\">\r\n      <div class=\"col-md-6\">\r\n        <div class=\"card mx-4\">\r\n          <div class=\"card-body p-4\">\r\n            <h1>Reset password</h1>\r\n            <p class=\"text-muted\">Set new password</p>\r\n\r\n            <div class=\"input-group mb-3\">\r\n              <div class=\"input-group-prepend\">\r\n                <span class=\"input-group-text\">@</span>\r\n              </div>\r\n              <input type=\"text\" class=\"form-control\" placeholder=\"Email\" [(ngModel)]=\"model.email\">\r\n            </div>\r\n\r\n            <div class=\"input-group mb-3\">\r\n              <div class=\"input-group-prepend\">\r\n                <span class=\"input-group-text\"><i class=\"icon-lock\"></i></span>\r\n              </div>\r\n              <input type=\"password\" class=\"form-control\" placeholder=\"New password\" [(ngModel)]=\"model.password\">\r\n            </div>\r\n\r\n            <div class=\"input-group mb-4\">\r\n              <div class=\"input-group-prepend\">\r\n                <span class=\"input-group-text\"><i class=\"icon-lock\"></i></span>\r\n              </div>\r\n              <input type=\"password\" class=\"form-control\" placeholder=\"Confirm password\" [(ngModel)]=\"model.confirmPassword\">\r\n            </div>\r\n\r\n            <button type=\"button\" class=\"btn btn-block btn-success\" (click)=\"resetPassword()\">Set</button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/views/pages/reset-password.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/views/pages/reset-password.component.ts ***!
  \*********************************************************/
/*! exports provided: ResetPasswordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResetPasswordComponent", function() { return ResetPasswordComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_shared_providers_providers__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/shared/providers/providers */ "./src/app/shared/providers/providers/index.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var app_shared_providers_services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/shared/providers/services */ "./src/app/shared/providers/services/index.ts");
/* harmony import */ var app_shared_models_account__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/shared/models/account */ "./src/app/shared/models/account/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ResetPasswordComponent = /** @class */ (function () {
    function ResetPasswordComponent(accountProvider, route, router, accountService) {
        this.accountProvider = accountProvider;
        this.route = route;
        this.router = router;
        this.accountService = accountService;
        this.model = new app_shared_models_account__WEBPACK_IMPORTED_MODULE_4__["ResetPasswordModel"]();
    }
    ResetPasswordComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            //this.model.token = params['token'];
            _this.model.token = _this.accountService.getToken();
        });
    };
    ResetPasswordComponent.prototype.resetPassword = function () {
        var _this = this;
        this.isLoading = true;
        this.accountProvider.resetPassword(this.model).subscribe(function () {
            _this.router.navigateByUrl('pages/login');
        }, function (err) {
            _this.isLoading = false;
        });
    };
    ResetPasswordComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            template: __webpack_require__(/*! ./reset-password.component.html */ "./src/app/views/pages/reset-password.component.html")
        }),
        __metadata("design:paramtypes", [app_shared_providers_providers__WEBPACK_IMPORTED_MODULE_1__["AccountProvider"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            app_shared_providers_services__WEBPACK_IMPORTED_MODULE_3__["AccountService"]])
    ], ResetPasswordComponent);
    return ResetPasswordComponent;
}());



/***/ })

}]);
//# sourceMappingURL=views-pages-pages-module.js.map