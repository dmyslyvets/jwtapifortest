(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["project-and-orders-new-projects-new-projects-module"],{

/***/ "./src/app/projects-and-orders/project-and-orders/new-projects/new-projects-routing.module.ts":
/*!****************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/new-projects/new-projects-routing.module.ts ***!
  \****************************************************************************************************/
/*! exports provided: NewProjectsRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewProjectsRoutingModule", function() { return NewProjectsRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _new_projects_new_projects_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./new-projects/new-projects.component */ "./src/app/projects-and-orders/project-and-orders/new-projects/new-projects/new-projects.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        redirectTo: 'newProjects',
        pathMatch: 'full'
    },
    {
        path: 'newProjects',
        component: _new_projects_new_projects_component__WEBPACK_IMPORTED_MODULE_2__["NewProjectsComponent"]
    }
];
var NewProjectsRoutingModule = /** @class */ (function () {
    function NewProjectsRoutingModule() {
    }
    NewProjectsRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], NewProjectsRoutingModule);
    return NewProjectsRoutingModule;
}());



/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/new-projects/new-projects.module.ts":
/*!********************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/new-projects/new-projects.module.ts ***!
  \********************************************************************************************/
/*! exports provided: NewProjectsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewProjectsModule", function() { return NewProjectsModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _new_projects_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./new-projects-routing.module */ "./src/app/projects-and-orders/project-and-orders/new-projects/new-projects-routing.module.ts");
/* harmony import */ var _new_projects_new_projects_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./new-projects/new-projects.component */ "./src/app/projects-and-orders/project-and-orders/new-projects/new-projects/new-projects.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var angular_6_datatable__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! angular-6-datatable */ "./node_modules/angular-6-datatable/index.js");
/* harmony import */ var angular_6_datatable__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(angular_6_datatable__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-bootstrap/datepicker */ "./node_modules/ngx-bootstrap/datepicker/index.js");
/* harmony import */ var ng_busy__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ng-busy */ "./node_modules/ng-busy/fesm5/ng-busy.js");
/* harmony import */ var ngx_uploader__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-uploader */ "./node_modules/ngx-uploader/fesm5/ngx-uploader.js");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ng2-file-upload */ "./node_modules/ng2-file-upload/index.js");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(ng2_file_upload__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var ng_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ng-multiselect-dropdown */ "./node_modules/ng-multiselect-dropdown/fesm5/ng-multiselect-dropdown.js");
/* harmony import */ var _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/slide-toggle */ "./node_modules/@angular/material/esm5/slide-toggle.es5.js");
/* harmony import */ var ng2_pdf_viewer__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ng2-pdf-viewer */ "./node_modules/ng2-pdf-viewer/ng2-pdf-viewer.es5.js");
/* harmony import */ var _progress_kendo_angular_grid__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @progress/kendo-angular-grid */ "./node_modules/@progress/kendo-angular-grid/dist/es/index.js");
/* harmony import */ var app_shared_modules_material_module__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! app/shared/modules/material.module */ "./src/app/shared/modules/material.module.ts");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/esm5/table.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var devextreme_angular__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! devextreme-angular */ "./node_modules/devextreme-angular/index.js");
/* harmony import */ var devextreme_angular__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(devextreme_angular__WEBPACK_IMPORTED_MODULE_17__);
/* harmony import */ var _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/cdk/drag-drop */ "./node_modules/@angular/cdk/esm5/drag-drop.es5.js");
/* harmony import */ var _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/material/grid-list */ "./node_modules/@angular/material/esm5/grid-list.es5.js");
/* harmony import */ var _new_projects_project_details_project_details_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./new-projects/project-details/project-details.component */ "./src/app/projects-and-orders/project-and-orders/new-projects/new-projects/project-details/project-details.component.ts");
/* harmony import */ var _new_projects_create_project_create_project_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./new-projects/create-project/create-project.component */ "./src/app/projects-and-orders/project-and-orders/new-projects/new-projects/create-project/create-project.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






















var NewProjectsModule = /** @class */ (function () {
    function NewProjectsModule() {
    }
    NewProjectsModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _new_projects_routing_module__WEBPACK_IMPORTED_MODULE_2__["NewProjectsRoutingModule"],
                devextreme_angular__WEBPACK_IMPORTED_MODULE_17__["DxDataGridModule"],
                devextreme_angular__WEBPACK_IMPORTED_MODULE_17__["DxSelectBoxModule"],
                devextreme_angular__WEBPACK_IMPORTED_MODULE_17__["DxCheckBoxModule"],
                app_shared_modules_material_module__WEBPACK_IMPORTED_MODULE_14__["MaterialModule"],
                angular_6_datatable__WEBPACK_IMPORTED_MODULE_5__["DataTableModule"],
                ng2_file_upload__WEBPACK_IMPORTED_MODULE_9__["FileUploadModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _progress_kendo_angular_grid__WEBPACK_IMPORTED_MODULE_13__["GridModule"],
                _angular_material_table__WEBPACK_IMPORTED_MODULE_15__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_16__["MatPaginatorModule"],
                _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_11__["MatSlideToggleModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
                ng_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_10__["NgMultiSelectDropDownModule"].forRoot(),
                ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_6__["BsDatepickerModule"].forRoot(),
                ng_busy__WEBPACK_IMPORTED_MODULE_7__["NgBusyModule"],
                ngx_uploader__WEBPACK_IMPORTED_MODULE_8__["NgxUploaderModule"],
                ng2_pdf_viewer__WEBPACK_IMPORTED_MODULE_12__["PdfViewerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_16__["MatSortModule"],
                _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_18__["DragDropModule"],
                _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_19__["MatGridListModule"]
            ],
            declarations: [_new_projects_new_projects_component__WEBPACK_IMPORTED_MODULE_3__["NewProjectsComponent"], _new_projects_project_details_project_details_component__WEBPACK_IMPORTED_MODULE_20__["ProjectDetailsComponent"], _new_projects_create_project_create_project_component__WEBPACK_IMPORTED_MODULE_21__["CreateProjectComponent"]]
        })
    ], NewProjectsModule);
    return NewProjectsModule;
}());



/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/new-projects/new-projects/create-project/create-project.component.html":
/*!*******************************************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/new-projects/new-projects/create-project/create-project.component.html ***!
  \*******************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-card class=\"project-card\">\r\n    <div class=\"project-header col-md-12\">\r\n      <h4 class=\"name-project col-md-12\">Create Project</h4>\r\n      <button class=\"close-project btn\" (click)=\"closeCreateProject()\"><i class=\"fas fa-times\"></i></button>\r\n    </div>\r\n        <mat-card class=\"details-card project\">\r\n          <div class=\"edit-project-button col-md-12\">\r\n            <div class=\"edit-button-wrapper\">\r\n              <a class=\"proj-button\" (click)=\"cancelCreateProject()\">\r\n                Cancel\r\n              </a>\r\n              <a class=\"proj-button\"  (click)=\"saveProjectChange()\">\r\n                Save\r\n              </a>\r\n            </div>\r\n          </div>\r\n          <mat-form-field class=\"example-full-width col-md-4\">\r\n            <input matInput  placeholder=\"Project Name\" [(ngModel)]=\"createProject.name\"\r\n                   value=\"\">\r\n          </mat-form-field>\r\n          <mat-form-field class=\"example-full-width col-md-4\">\r\n            <mat-select placeholder=\"Project Manager\"  [(ngModel)]=\"createProject.userId\">\r\n              <mat-option *ngFor=\"let item of projectManagers.users\" [value]=\"item.id\">\r\n                {{item.firstName}} {{item.lastName}}\r\n              </mat-option>\r\n            </mat-select>\r\n          </mat-form-field>\r\n          <mat-form-field class=\"example-full-width col-md-4\">\r\n                  <input matInput\r\n                         [ngModel]=\"createProject.projectType\"\r\n                         (keyup)=\"typedProjectType($event)\"\r\n                         [matAutocomplete]=\"auto\"\r\n                         placeholder=\"Project Type\">\r\n            </mat-form-field>\r\n        \r\n            <mat-autocomplete #auto=\"matAutocomplete\"\r\n                              (optionSelected)=\"selectedProjectType($event)\"\r\n                              [displayWith]=\"displayFn\"\r\n                              >\r\n              <mat-option *ngFor=\"let item of projectTypesView.projectTypes\"\r\n                          [value]=\"item\"\r\n                          >\r\n                {{item.name}}\r\n              </mat-option>\r\n            </mat-autocomplete>\r\n          <mat-form-field class=\"example-full-width col-md-4\">\r\n            <mat-select placeholder=\"Customer\" [(ngModel)]=\"createProject.customerId\" >\r\n              <mat-option *ngFor=\"let cust of customers.customers\" [value]=\"cust.id\">\r\n                {{cust.name}}\r\n              </mat-option>\r\n            </mat-select>\r\n          </mat-form-field>\r\n          <mat-form-field class=\"example-full-width col-md-4\">\r\n              <input matInput\r\n                     [matDatepicker]=\"picker\"\r\n                     [(ngModel)]=\"createProject.date\"\r\n                     placeholder=\"Choose a date\">\r\n      \r\n              <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\r\n              <mat-datepicker #picker></mat-datepicker>\r\n            </mat-form-field>\r\n          <mat-form-field class=\"example-full-width col-md-4\">\r\n            <input matInput placeholder=\"Description\"  [(ngModel)]=\"createProject.description\">\r\n          </mat-form-field>\r\n          <mat-form-field class=\"example-full-width  col-md-12\">\r\n            <textarea matInput  matTextareaAutosize [(ngModel)]=\"createProject.memo\" placeholder=\"Memmo\"></textarea>\r\n          </mat-form-field>\r\n        </mat-card>\r\n\r\n\r\n        <h6 class=\"attachments-title\">Attachments</h6>\r\n        <mat-card class=\"attachments-card details-card col-md-6\">\r\n          <ul class=\"file-list\">\r\n            <li class=\"file-item\" *ngFor=\"let item of createProjectFileList; let i = index\" title=\"{{item.name}}{{item.type}}\">\r\n              <div class=\"file-name\">{{item.name}}{{item.type}}</div>\r\n              <div class=\"control-buttons\">\r\n                <button class=\"remove control-btn\" (click)=\"openDeleteAttachment(i)\"><i class=\"fas fa-times\"></i></button>\r\n              </div>\r\n            </li>\r\n          </ul>\r\n          <div ng2FileDrop [ngClass]=\"{'nv-file-over': hasBaseDropZoneOver}\" (fileOver)=\"fileOverBase($event)\" (onFileDrop)=\"dropped($event)\"\r\n          [uploader]=\"uploader\" class=\"table-responsive col-md-12 p-0\">\r\n          <span *ngIf=\"showSpinner\">\r\n            <mat-progress-bar mode=\"indeterminate\"></mat-progress-bar>\r\n          </span>\r\n          <div class=\"drag-zone\">\r\n            Drag and drop files here\r\n          </div>\r\n          </div>\r\n          <div class=\"button-block\">\r\n            <label class=\"custom-file-upload\">\r\n              <input (change)=\"inputAdd($event)\" #fileUploader type=\"file\" multiple />\r\n              Add New Files\r\n            </label>\r\n          </div>\r\n        </mat-card>\r\n  </mat-card>\r\n \r\n  \r\n\r\n  <div class=\"add-project\">\r\n      <div class=\"overlay\" *ngIf='openDeleteAttachmentModal'></div>\r\n      <div class=\"modal-add-project\" [ngClass]=\"{'active':openDeleteAttachmentModal}\">\r\n        <div class=\"card-header\">\r\n          <span>\r\n            <strong>\r\n              <h5>Are you sure you want to delete the project attachment?</h5>\r\n            </strong>\r\n          </span>\r\n          <div class=\"close-project-button\" (click)=\"cancelDeleteAttachment()\">\r\n            <i class=\"fas fa-times\"></i>\r\n          </div>\r\n        </div>\r\n        <div class=\"modal-body\">\r\n          <div class=\"row button-wrapper\">\r\n            <button class=\"btn btn-block btn-outline-primary\" (click)=\"cancelDeleteAttachment()\">Cancel</button>\r\n            <button class=\"btn btn-block btn-outline-primary\" (click)=\"yesDeleteAttachment()\">Yes</button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>"

/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/new-projects/new-projects/create-project/create-project.component.scss":
/*!*******************************************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/new-projects/new-projects/create-project/create-project.component.scss ***!
  \*******************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".edit-project-button {\n  display: flex;\n  justify-content: flex-end; }\n  .edit-project-button .edit-button {\n    height: 24px;\n    width: 24px;\n    cursor: pointer; }\n  .project-header {\n  display: flex;\n  justify-content: space-between;\n  padding: 25px 74px 0px 74px !important;\n  margin-bottom: 40px; }\n  .project-header .close-project {\n    position: absolute;\n    top: 20px;\n    right: 5px;\n    border: none;\n    background: transparent;\n    padding: 0;\n    margin: 0;\n    margin-right: 15px;\n    font-size: 18px; }\n  .proj-button {\n  color: #0077c5 !important;\n  cursor: pointer;\n  margin-right: 18px; }\n  .proj-button:last-child {\n    margin-right: 0px; }\n  .edit-button-wrapper {\n  display: flex;\n  justify-content: flex-end;\n  margin-bottom: 13px; }\n  .edit-button-wrapper .btn {\n    width: 100px;\n    margin: 0;\n    margin-left: 10px; }\n  .name-project {\n  font-weight: bold;\n  margin: 0; }\n  .project-card {\n  overflow: hidden;\n  height: calc(100% - 20px);\n  max-height: 845px;\n  margin-top: 20px;\n  padding: 0;\n  border: 1px solid #949494; }\n  .project-list-wrapper {\n  margin-top: 20px;\n  border: 1px solid #949494;\n  height: calc(100% - 20px);\n  background: white; }\n  .project-list-wrapper .search-bar {\n    position: relative;\n    margin-top: 15px; }\n  .project-list-wrapper .search-bar .search-icon {\n      position: absolute;\n      line-height: 37px;\n      font-size: 16px;\n      left: 15px;\n      top: 0;\n      bottom: 0; }\n  .project-list-wrapper .search-bar .search {\n      width: 100%;\n      border: 1px solid #949494;\n      padding: 7px;\n      padding-left: 40px; }\n  .project-list-wrapper .project-list-title {\n    padding: 15px;\n    border-bottom: 1px solid #949494; }\n  .project-list-wrapper .project-list-title .add-wrapper {\n      display: flex;\n      justify-content: space-between;\n      align-items: center; }\n  .project-list-wrapper .project-list-title .add-new-project {\n      border: 1px solid #949494;\n      text-transform: uppercase;\n      padding: 10px;\n      font-size: 16px;\n      background: transparent; }\n  .project-list-wrapper .project-list-title .add-new-project:hover {\n        background: #e1e6ef;\n        transition: .2s; }\n  .project-list-wrapper .project-list {\n    max-height: 714px;\n    overflow: auto;\n    list-style: none;\n    margin: 0;\n    padding: 0; }\n  .project-list-wrapper .project-list .project-item {\n      padding: 15px;\n      border-bottom: 1px solid #949494; }\n  .project-list-wrapper .project-list .project-item:hover {\n        cursor: pointer;\n        background: #dcdcdc;\n        transition: .2s; }\n  .project-list-wrapper .project-list .data-group {\n      display: flex;\n      justify-content: space-between; }\n  .project-list-wrapper .project-list .data-group .archer {\n        font-weight: bold;\n        font-size: 15px; }\n  .project-list-wrapper .project-list .data-group .project-name {\n        font-weight: bold;\n        font-size: 14px; }\n  .project-list-wrapper .project-list .data-group .type {\n        color: #948f8f;\n        margin-left: 15px; }\n  .project-list-wrapper .project-list .data-group .stat .icon {\n        font-size: 16px;\n        color: #009aff;\n        margin-right: 10px;\n        cursor: pointer; }\n  .details-card.project {\n  border-bottom: 1px solid #949494; }\n  textarea.mat-autosize {\n  resize: auto !important;\n  max-height: 80px; }\n  .attachments-title {\n  margin-left: 74px;\n  margin-top: 20px; }\n  @media (max-width: 1636px) {\n    .attachments-title {\n      margin-left: 15px;\n      margin-top: 15px; } }\n  .attachments-card {\n  padding-top: 20px !important; }\n  @media (max-width: 1636px) {\n    .attachments-card {\n      padding: 15px !important; } }\n  .attachments-card .file-list {\n    list-style: none;\n    padding: 0;\n    max-height: 100px;\n    overflow: auto; }\n  .attachments-card .file-list .file-item {\n      display: flex;\n      justify-content: space-between; }\n  .attachments-card .file-list .file-name {\n      white-space: nowrap;\n      overflow: hidden;\n      text-overflow: ellipsis;\n      font-size: 16px;\n      color: #0077c5;\n      cursor: pointer; }\n  .attachments-card .file-list .control-btn {\n      background: transparent;\n      margin: 0;\n      padding: 5px;\n      border: none; }\n  .attachments-card .drag-zone {\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    margin-top: 20px;\n    width: 100%;\n    height: 72px;\n    font-size: 16px;\n    color: #636363;\n    border: 2px dashed #707070;\n    border-right-color: #D0D0D0;\n    border-left-color: #D0D0D0;\n    border-top-color: #d4d4d4;\n    border-bottom-color: #d4d4d4; }\n  .attachments-card .button-block {\n    display: flex;\n    justify-content: space-between;\n    margin-top: 10px; }\n  .attachments-card .button-block .btn {\n      border: 0;\n      margin: 0;\n      padding: 0;\n      font-size: 14px;\n      color: #0077c5;\n      background: transparent; }\n  .control-buttons {\n  white-space: nowrap; }\n  .custom-file-upload {\n  color: #0077c5;\n  cursor: pointer; }\n  label {\n  margin-bottom: 0px; }\n  input[type=\"file\"] {\n  display: none; }\n  .modal-add-project {\n  position: fixed;\n  background: #fff;\n  z-index: 10000;\n  top: -100%;\n  left: calc(50% - 200px);\n  width: 400px; }\n  .modal-add-project.active {\n    top: 200px;\n    left: calc(50% - 200px);\n    transition: top .5s; }\n  .modal-add-project .row {\n    width: 100%; }\n  .modal-add-project .row .mt-10 {\n      margin-top: 15px; }\n  .modal-add-project .button-wrapper {\n    display: flex;\n    justify-content: flex-end;\n    margin-top: 10px; }\n  .modal-add-project .button-wrapper .btn {\n      width: 100px;\n      margin: 0;\n      margin-left: 10px; }\n  .overlay {\n  position: fixed;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  background: rgba(0, 0, 0, 0.4);\n  z-index: 10000; }\n  .clickable-title-link {\n  color: rgba(var(--palette-neutral-100, 0, 0, 0), 1); }\n  .underline-on-hover:hover {\n  text-decoration: underline !important; }\n  .underline-on-hover {\n  font-size: 16px; }\n  .section-heading {\n  padding: 5px 10px 11px 0px;\n  font-size: 18px;\n  font-weight: bold; }\n  .drag-container {\n  width: 100%;\n  height: auto;\n  max-width: 100%;\n  margin: 0 25px 25px 0;\n  display: inline-block;\n  vertical-align: top;\n  background-color: #E9ECEF;\n  padding: 15px; }\n  .item-list {\n  min-height: 60px;\n  border-radius: 4px;\n  display: block; }\n  .item-box:hover {\n  cursor: pointer; }\n  .item-box {\n  border: solid 1px #808080;\n  margin-bottom: 15px;\n  color: rgba(0, 0, 0, 0.87);\n  box-sizing: border-box;\n  cursor: move;\n  background: white;\n  font-size: 13px;\n  padding: 15px; }\n  .item-box p {\n    margin-bottom: 5px;\n    color: #636363; }\n  .item-box p span {\n      color: black; }\n  .item-box p .date-proj {\n      margin-left: 25px; }\n  .item-box p .red {\n      background: #CD3232;\n      display: block;\n      height: 25px;\n      width: 25px;\n      margin-right: 10px;\n      border-radius: 50%; }\n  .item-box p .blue {\n      background: #0077c5;\n      display: block;\n      width: 15px;\n      height: 15px; }\n  .item-box p .yellow {\n      background: #ffba00;\n      display: block;\n      width: 20px;\n      height: 22.1px; }\n  .cdk-drag-preview {\n  box-sizing: border-box;\n  border-radius: 4px;\n  box-shadow: 0 5px 5px -3px rgba(0, 0, 0, 0.2), 0 8px 10px 1px rgba(0, 0, 0, 0.14), 0 3px 14px 2px rgba(0, 0, 0, 0.12);\n  border-radius: 20px; }\n  #newOrders {\n  font-size: 14px;\n  font-weight: bold;\n  color: #636363;\n  background: transparent;\n  border: 1px solid #707070; }\n  .item-box .order-blue {\n  width: 16.5px;\n  height: 20px;\n  margin-right: 10px; }\n  .item-box .order-count {\n  margin-right: 31px;\n  font-size: 16px;\n  font-weight: bold; }\n  .item-box .order-date {\n  font-size: 16px; }\n  .item-box .clipboard {\n  display: flex;\n  align-items: center;\n  margin-left: 10px; }\n  .item-box .clipboard img {\n    width: 20px;\n    height: 22.1px;\n    margin-right: 10px; }\n  .item-box .red-circle {\n  min-width: 30px;\n  min-height: 30px;\n  background-color: #cd3232;\n  border-radius: 50%;\n  color: white;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  text-transform: uppercase;\n  margin-right: 10px; }\n  .item-box .clients {\n  font-size: 16px; }\n  .item-box .clients span {\n    color: #636363;\n    margin-right: 9px; }\n  .orders-name {\n  font-size: 24px;\n  font-weight: bold;\n  margin-bottom: 36px;\n  margin-top: 25px; }\n  .kanban {\n  margin-top: 105px;\n  width: 96%;\n  margin-left: 9px; }\n  .name-order {\n  white-space: nowrap;\n  text-overflow: ellipsis;\n  overflow: hidden; }\n  .callback-icon {\n  display: flex;\n  align-items: center; }\n"

/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/new-projects/new-projects/create-project/create-project.component.ts":
/*!*****************************************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/new-projects/new-projects/create-project/create-project.component.ts ***!
  \*****************************************************************************************************************************/
/*! exports provided: CreateProjectComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateProjectComponent", function() { return CreateProjectComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ng2-file-upload */ "./node_modules/ng2-file-upload/index.js");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(ng2_file_upload__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var app_shared_constans_projects_and_orders_constans__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/shared/constans/projects-and-orders.constans */ "./src/app/shared/constans/projects-and-orders.constans.ts");
/* harmony import */ var app_shared_providers_services_project_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/shared/providers/services/project.service */ "./src/app/shared/providers/services/project.service.ts");
/* harmony import */ var app_shared_providers_services_customer_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! app/shared/providers/services/customer.service */ "./src/app/shared/providers/services/customer.service.ts");
/* harmony import */ var app_shared_providers_services_noitification_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! app/shared/providers/services/noitification.service */ "./src/app/shared/providers/services/noitification.service.ts");
/* harmony import */ var app_shared_providers_services_project_attachment_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! app/shared/providers/services/project-attachment.service */ "./src/app/shared/providers/services/project-attachment.service.ts");
/* harmony import */ var app_shared_providers_services_load_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! app/shared/providers/services/load.service */ "./src/app/shared/providers/services/load.service.ts");
/* harmony import */ var app_shared_models_users_get_all_users_view__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! app/shared/models/users/get-all-users.view */ "./src/app/shared/models/users/get-all-users.view.ts");
/* harmony import */ var app_shared_models_customer_get_all_customer_view__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! app/shared/models/customer/get-all-customer.view */ "./src/app/shared/models/customer/get-all-customer.view.ts");
/* harmony import */ var app_shared_models_project_create_project_view__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! app/shared/models/project/create-project.view */ "./src/app/shared/models/project/create-project.view.ts");
/* harmony import */ var app_shared_models_project_get_all_project_types_view__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! app/shared/models/project/get-all-project-types.view */ "./src/app/shared/models/project/get-all-project-types.view.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













var CreateProjectComponent = /** @class */ (function () {
    function CreateProjectComponent(notificationService, projectService, customerService, projectAttachmentService, activateRoute, route, loadService, constans) {
        this.notificationService = notificationService;
        this.projectService = projectService;
        this.customerService = customerService;
        this.projectAttachmentService = projectAttachmentService;
        this.activateRoute = activateRoute;
        this.route = route;
        this.loadService = loadService;
        this.constans = constans;
        this.closeCreateProjectComponent = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.createProject = new app_shared_models_project_create_project_view__WEBPACK_IMPORTED_MODULE_11__["CreateProjectView"]();
        this.currentPdfLink = " ";
        //get request
        this.projectManagers = new app_shared_models_users_get_all_users_view__WEBPACK_IMPORTED_MODULE_9__["GetAllUserView"]();
        this.customers = new app_shared_models_customer_get_all_customer_view__WEBPACK_IMPORTED_MODULE_10__["GetAllCustomerView"]();
        this.projectTypesView = new app_shared_models_project_get_all_project_types_view__WEBPACK_IMPORTED_MODULE_12__["GetAllProjectTypesView"]();
        //form 
        // public myControl = new FormControl();
        //spinner
        this.showSpinner = false;
        //popups
        this.formValidation = false;
        this.openSaveAttachment = false;
        this.openDeleteAttachmentModal = false;
        this.droppedFiles = [];
        this.hasBaseDropZoneOver = false;
        //attachment
        this.uploader = new ng2_file_upload__WEBPACK_IMPORTED_MODULE_2__["FileUploader"]({ url: this.constans.URL });
        this.showAttachmentImg = false;
        this.showAttachedPdf = false;
        this.showAttachedTxt = false;
        this.showAttachment = false;
        this.createProjectFileList = [];
    }
    CreateProjectComponent.prototype.closeCreateProject = function () {
        this.closeCreateProjectComponent.emit(false);
    };
    CreateProjectComponent.prototype.ngOnInit = function () {
        this.loadService.set(true);
        this.getProjectManager();
        this.getCustomers();
        this.getProjectTypes();
    };
    //temp
    CreateProjectComponent.prototype.change = function () {
        console.log(this.createProject.projectType);
    };
    //get
    CreateProjectComponent.prototype.getCustomers = function () {
        var _this = this;
        this.customerService.getCustomers().subscribe(function (response) {
            _this.customers = response;
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    CreateProjectComponent.prototype.getProjectTypes = function () {
        var _this = this;
        this.projectService.getAllProjectTypes().subscribe(function (response) {
            _this.projectTypesView = response;
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    CreateProjectComponent.prototype.getProjectManager = function () {
        var _this = this;
        this.projectService.getProjectManager().subscribe(function (response) {
            _this.projectManagers = response;
            _this.loadService.set(false);
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    //functions for project types
    CreateProjectComponent.prototype.displayFn = function (val) {
        return val ? val.name : val;
    };
    CreateProjectComponent.prototype.selectedProjectType = function (item) {
        this.createProject.projectType.id = item.option.value.id;
        this.createProject.projectType.name = item.option.value.name;
    };
    CreateProjectComponent.prototype.typedProjectType = function (event) {
        this.createProject.projectType.name = '';
        this.createProject.projectType.id = null;
        this.createProject.projectType.name = event.target.value;
    };
    //back on page
    CreateProjectComponent.prototype.showingProjectsList = function () {
        this.route.navigate(['projects-and-orders/projects/projectslist']);
    };
    CreateProjectComponent.prototype.saveProjectChange = function () {
        var _this = this;
        if (!this.createProject.userId) {
            this.notificationService.showError(this.constans.cantCreateProjectWithoutPMMessage);
        }
        if (!this.createProject.customerId) {
            this.notificationService.showError(this.constans.cantCreateProjectWithoutCustomerMessage);
        }
        if (!this.createProject.date) {
            this.notificationService.showError(this.constans.cantCreateProjectWitoutDate);
        }
        if (this.createProject.customerId && this.createProject.userId && this.createProject.date) {
            var invalidFiles = [];
            var formData = new FormData();
            if (!this.createProject.name) {
                this.createProject.name = "";
            }
            for (var i = 0; i < this.createProjectFileList.length; i++) {
                var success = false;
                this.constans.allowableExtensions.forEach(function (ex) {
                    var index = _this.createProjectFileList[i].name.lastIndexOf(".");
                    var extensions = _this.createProjectFileList[i].name.substr(index + 1);
                    if (ex == extensions) {
                        success = true;
                        return;
                    }
                });
                if (success) {
                    formData.append("file", this.createProjectFileList[i], this.createProjectFileList[i].name);
                }
            }
            ;
            formData.append("name", this.createProject.name);
            formData.append("description", this.createProject.description);
            formData.append("memo", this.createProject.memo);
            formData.append("projectType[id]", this.createProject.projectType.id);
            formData.append("projectType[name]", this.createProject.projectType.name);
            formData.append("customerId", this.createProject.customerId);
            formData.append("userId", this.createProject.userId);
            formData.append("date", this.createProject.date.toUTCString());
            this.projectService.createProject(formData).subscribe(function (response) {
                _this.notificationService.showSuccess(_this.constans.projectCreateMessage);
                _this.closeCreateProject();
            }),
                function (error) {
                    _this.notificationService.showError(error.error);
                };
        }
    };
    CreateProjectComponent.prototype.cancelCreateProject = function () {
        this.createProject = new app_shared_models_project_create_project_view__WEBPACK_IMPORTED_MODULE_11__["CreateProjectView"]();
    };
    //attacments
    CreateProjectComponent.prototype.openDeleteAttachment = function (index) {
        this.deletedAttachmentId = index;
        this.openDeleteAttachmentModal = true;
    };
    CreateProjectComponent.prototype.cancelDeleteAttachment = function () {
        this.deletedAttachmentId = null;
        this.openDeleteAttachmentModal = false;
    };
    CreateProjectComponent.prototype.yesDeleteAttachment = function () {
        this.deleteAttachment();
        this.openDeleteAttachmentModal = false;
    };
    CreateProjectComponent.prototype.deleteAttachment = function () {
        this.createProjectFileList.splice(this.deletedAttachmentId, 1);
        this.deletedAttachmentId = null;
    };
    CreateProjectComponent.prototype.inputAdd = function (event) {
        var fileList = event.target.files;
        var invalidFiles = this.checkAttachmentExtensions(fileList);
        if (invalidFiles.length > 0) {
            this.notificationService.showWarning(this.constans.fileExtensionNotAllowedMessage);
        }
    };
    CreateProjectComponent.prototype.fileLeave = function (file) {
        var index = this.droppedFiles.indexOf(file);
        if (index > -1) {
            this.droppedFiles.splice(index, 1);
        }
    };
    CreateProjectComponent.prototype.fileOver = function (event) { };
    CreateProjectComponent.prototype.fileOverBase = function (event) {
        this.hasBaseDropZoneOver = event;
    };
    CreateProjectComponent.prototype.dropped = function (fileList) {
        var invalidFiles = this.checkAttachmentExtensions(fileList);
        if (invalidFiles.length > 0) {
            this.notificationService.showWarning(this.constans.fileExtensionNotAllowedMessage);
        }
    };
    CreateProjectComponent.prototype.checkAttachmentExtensions = function (files) {
        var invalidFiles = [];
        for (var i = 0; i < files.length; i++) {
            this.showSpinner = true;
            var success = false;
            this.constans.allowableExtensions.forEach(function (ex) {
                var index = files[i].name.lastIndexOf(".");
                var extensions = files[i].name.substr(index + 1);
                if (ex == extensions) {
                    success = true;
                    return;
                }
            });
            if (!success) {
                invalidFiles.push(files[i].name);
            }
            if (success) {
                this.createProjectFileList.push(files[i]);
                this.inputFileElement.nativeElement.value = "";
            }
        }
        ;
        this.showSpinner = false;
        return invalidFiles;
    };
    CreateProjectComponent.prototype.closeShowAttachment = function () {
        this.showAttachmentImg = false;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"])
    ], CreateProjectComponent.prototype, "closeCreateProjectComponent", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('fileUploader'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], CreateProjectComponent.prototype, "inputFileElement", void 0);
    CreateProjectComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-create-project',
            template: __webpack_require__(/*! ./create-project.component.html */ "./src/app/projects-and-orders/project-and-orders/new-projects/new-projects/create-project/create-project.component.html"),
            styles: [__webpack_require__(/*! ./create-project.component.scss */ "./src/app/projects-and-orders/project-and-orders/new-projects/new-projects/create-project/create-project.component.scss")]
        }),
        __metadata("design:paramtypes", [app_shared_providers_services_noitification_service__WEBPACK_IMPORTED_MODULE_6__["NotificationService"],
            app_shared_providers_services_project_service__WEBPACK_IMPORTED_MODULE_4__["ProjectService"],
            app_shared_providers_services_customer_service__WEBPACK_IMPORTED_MODULE_5__["CustomerService"],
            app_shared_providers_services_project_attachment_service__WEBPACK_IMPORTED_MODULE_7__["ProjectAttachmentservice"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            app_shared_providers_services_load_service__WEBPACK_IMPORTED_MODULE_8__["LoadService"],
            app_shared_constans_projects_and_orders_constans__WEBPACK_IMPORTED_MODULE_3__["ProjectsAndOrdersConstans"]])
    ], CreateProjectComponent);
    return CreateProjectComponent;
}());



/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/new-projects/new-projects/new-projects.component.html":
/*!**************************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/new-projects/new-projects/new-projects.component.html ***!
  \**************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row card-row\">\r\n  <div class=\"col-12\">\r\n    <div class=\"row\">\r\n      <div class=\"col-md-4\">\r\n\r\n        <div class=\"project-list-wrapper\">\r\n          <div class=\"project-list-title\">\r\n            <div class=\"add-wrapper\">\r\n              <h4 class=\"name-project\">Project ({{projectsCount}})</h4>\r\n              <button class=\"add-new-project btn\" (click)=\"saveNewProject()\">\r\n                <i class=\"fas fa-plus\"></i>  New project\r\n              </button>\r\n            </div>\r\n            <div>\r\n              <mat-slide-toggle [(ngModel)]=\"closeProjectShow\" (change)=\"showClosedProjects()\"></mat-slide-toggle> Closed\r\n            </div>\r\n            <div class=\"search-bar\">\r\n              <i class=\"search-icon fas fa-search\"></i>\r\n              <input class=\"search\" type=\"text\" name=\"name\" value=\"\" [(ngModel)]=\"searcText\" (keyup)=\"projectSearch()\" />\r\n              <button class=\"filter-btn\"><i class=\"fa fa-filter\"></i></button>\r\n            </div>\r\n            <div>\r\n\r\n            </div>\r\n          </div>\r\n          <div class=\"list-body\" *ngIf=\"closeProjectShow\">\r\n            <ul class=\"project-list\">\r\n              <li class=\"project-item\" [ngClass]=\"{'selected-row' : item.id==this.currentProjectId && projectDetailsIsShow}\" *ngFor=\"let item of activeProjects.projects\" (click)=\"showProjectDetailsById(item.id)\">\r\n                <div class=\"data-group\">\r\n                  <span class=\"archer\">{{item.customer.name}}</span>\r\n                  <span class=\"date\">{{ item.date | date:'shortDate' }}</span>\r\n                </div>\r\n                <div class=\"data-group\">\r\n                  <span class=\"project-name\">{{item.name}}</span>\r\n\r\n\r\n\r\n                  <mat-form-field class=\"example-full-width\" (click)=\"stopPropagation($event)\">\r\n                    <mat-select [(ngModel)]=\"item.userId\" (selectionChange)=\"reasignProjectManager(item.id, item.userId)\">\r\n                      <mat-option *ngFor=\"let manager of projectManagers.users\" [value]=\"manager.id\">\r\n                        {{manager.firstName}} {{manager.lastName}}\r\n                      </mat-option>\r\n                    </mat-select>\r\n                  </mat-form-field>\r\n\r\n                </div>\r\n                <div class=\"data-group\">\r\n                  <span class=\"type-field\">{{item.projectType.name}} <span class=\"type\">{{item.description}}</span></span>\r\n                  <span class=\"stat\"><span class=\"icon\"><i class=\"fas fa-trash\"></i></span> 0/1</span>\r\n                </div>\r\n              </li>\r\n            </ul>\r\n          </div>\r\n          <div class=\"list-body\" *ngIf=\"!closeProjectShow\">\r\n            <ul class=\"project-list\">\r\n              <li class=\"project-item\" [ngClass]=\"{'selected-row' : item.id==this.currentProjectId && projectDetailsIsShow}\" *ngFor=\"let item of activeProjectsWithOpenedOrders.projects\" (click)=\"showProjectDetailsById(item.id)\">\r\n                <div class=\"data-group\">\r\n                  <span class=\"archer\">{{item.customer.name}}</span>\r\n                  <span class=\"date\">{{ item.date | date:'shortDate' }}</span>\r\n                </div>\r\n                <div class=\"data-group\">\r\n                  <span class=\"project-name\">{{item.name}}</span>\r\n\r\n\r\n\r\n                  <mat-form-field class=\"example-full-width\" (click)=\"stopPropagation($event)\">\r\n                    <mat-select [(ngModel)]=\"item.userId\" (selectionChange)=\"reasignProjectManager(item.id, item.userId)\">\r\n                      <mat-option *ngFor=\"let manager of projectManagers.users\" [value]=\"manager.id\">\r\n                        {{manager.firstName}} {{manager.lastName}}\r\n                      </mat-option>\r\n                    </mat-select>\r\n                  </mat-form-field>\r\n\r\n                </div>\r\n                <div class=\"data-group\">\r\n                  <span class=\"type-field\">{{item.projectType.name}} <span class=\"type\">{{item.description}}</span></span>\r\n                  <span class=\"stat\"><span class=\"icon\"><i class=\"fas fa-trash\"></i></span> 0/1</span>\r\n                </div>\r\n              </li>\r\n            </ul>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <app-project-details \r\n      *ngIf=\"projectDetailsIsShow\"\r\n      class=\"col-md-8\"\r\n      [projectId]=\"currentProjectId\"\r\n      (closeDetailsComponent)=\"closeDetails()\"\r\n      (updateProjectEvent)=\"getActiveProject()\">\r\n    </app-project-details>\r\n      \r\n    <app-create-project\r\n    *ngIf=\"createProjectIsShow\"\r\n    class=\"col-md-8\"\r\n    (closeCreateProjectComponent)=\"closeCreatePorject()\">\r\n      </app-create-project>\r\n\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/new-projects/new-projects/new-projects.component.scss":
/*!**************************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/new-projects/new-projects/new-projects.component.scss ***!
  \**************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".edit-project-button {\n  display: flex;\n  justify-content: flex-end; }\n  .edit-project-button .edit-button {\n    height: 24px;\n    width: 24px;\n    cursor: pointer; }\n  .project-header {\n  display: flex;\n  justify-content: space-between;\n  padding: 25px 74px 0px 74px !important;\n  margin-bottom: 40px; }\n  .project-header .close-project {\n    position: absolute;\n    top: 20px;\n    right: 5px;\n    border: none;\n    background: transparent;\n    padding: 0;\n    margin: 0;\n    margin-right: 15px;\n    font-size: 18px; }\n  .proj-button {\n  color: #0077c5 !important;\n  cursor: pointer;\n  margin-right: 18px; }\n  .proj-button:last-child {\n    margin-right: 0px; }\n  .edit-button-wrapper {\n  display: flex;\n  justify-content: flex-end;\n  margin-bottom: 13px; }\n  .edit-button-wrapper .btn {\n    width: 100px;\n    margin: 0;\n    margin-left: 10px; }\n  .name-project {\n  font-weight: bold;\n  margin: 0; }\n  .project-card {\n  margin-top: 20px;\n  padding: 0;\n  border: 1px solid #949494; }\n  .project-list-wrapper {\n  margin-top: 20px;\n  border: 1px solid #949494;\n  height: calc(100% - 20px);\n  background: white; }\n  .project-list-wrapper .search-bar {\n    position: relative;\n    margin-top: 15px; }\n  .project-list-wrapper .search-bar .search-icon {\n      position: absolute;\n      line-height: 37px;\n      font-size: 16px;\n      left: 15px;\n      top: 0;\n      bottom: 0; }\n  .project-list-wrapper .search-bar .search {\n      width: 85%;\n      border: 1px solid #949494;\n      padding: 7px;\n      padding-left: 40px; }\n  .project-list-wrapper .search-bar .filter-btn {\n      margin-left: 5px;\n      line-height: 33px;\n      padding-bottom: 0px;\n      padding-top: 0px; }\n  .project-list-wrapper .project-list-title {\n    padding: 15px;\n    border-bottom: 1px solid #949494; }\n  .project-list-wrapper .project-list-title .add-wrapper {\n      display: flex;\n      justify-content: space-between;\n      align-items: center; }\n  .project-list-wrapper .project-list-title .add-new-project {\n      border: 1px solid #949494;\n      text-transform: uppercase;\n      padding: 10px;\n      font-size: 16px;\n      background: transparent; }\n  .project-list-wrapper .project-list-title .add-new-project:hover {\n        background: #e9e9e9;\n        transition: .2s; }\n  .project-list-wrapper .project-list {\n    max-height: 714px;\n    overflow-x: hidden;\n    list-style: none;\n    margin: 0;\n    padding: 0; }\n  .project-list-wrapper .project-list .project-item {\n      padding: 15px;\n      border-bottom: 1px solid #949494; }\n  .project-list-wrapper .project-list .project-item:hover {\n        cursor: pointer;\n        background: #e9e9e9;\n        transition: .2s; }\n  .project-list-wrapper .project-list .data-group {\n      display: flex;\n      justify-content: space-between;\n      align-items: center; }\n  .project-list-wrapper .project-list .data-group .archer {\n        font-weight: bold;\n        font-size: 15px; }\n  .project-list-wrapper .project-list .data-group .project-name {\n        width: 100%;\n        font-weight: bold;\n        font-size: 14px; }\n  .project-list-wrapper .project-list .data-group .type {\n        color: #948f8f;\n        margin-left: 15px; }\n  .project-list-wrapper .project-list .data-group .stat .icon {\n        font-size: 16px;\n        color: #009aff;\n        margin-right: 10px;\n        cursor: pointer; }\n  .details-card.project {\n  border-bottom: 1px solid #949494; }\n  textarea.mat-autosize {\n  resize: auto !important;\n  overflow: hidden !important; }\n  .attachments-title {\n  margin-left: 74px;\n  margin-top: 20px; }\n  .attachments-card {\n  padding-top: 20px !important; }\n  .attachments-card .file-list {\n    list-style: none;\n    padding: 0; }\n  .attachments-card .file-list .file-item {\n      display: flex;\n      justify-content: space-between; }\n  .attachments-card .file-list .file-name {\n      font-size: 16px;\n      color: #0077c5; }\n  .attachments-card .file-list .control-btn {\n      background: transparent;\n      margin: 0;\n      padding: 5px;\n      border: none; }\n  .attachments-card .drag-zone {\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    margin-top: 20px;\n    width: 100%;\n    height: 72px;\n    font-size: 16px;\n    color: #636363;\n    border: 2px dashed #707070;\n    border-right-color: #D0D0D0;\n    border-left-color: #D0D0D0;\n    border-top-color: #d4d4d4;\n    border-bottom-color: #d4d4d4; }\n  .attachments-card .button-block {\n    display: flex;\n    justify-content: space-between;\n    margin-top: 10px; }\n  .attachments-card .button-block .btn {\n      border: 0;\n      margin: 0;\n      padding: 0;\n      font-size: 14px;\n      color: #0077c5;\n      background: transparent; }\n  .selected-row {\n  background: #dcdcdc;\n  transition: .2s; }\n"

/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/new-projects/new-projects/new-projects.component.ts":
/*!************************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/new-projects/new-projects/new-projects.component.ts ***!
  \************************************************************************************************************/
/*! exports provided: NewProjectsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewProjectsComponent", function() { return NewProjectsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var app_shared_providers_services_project_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/shared/providers/services/project.service */ "./src/app/shared/providers/services/project.service.ts");
/* harmony import */ var app_shared_providers_services_noitification_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/shared/providers/services/noitification.service */ "./src/app/shared/providers/services/noitification.service.ts");
/* harmony import */ var app_shared_providers_services_load_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! app/shared/providers/services/load.service */ "./src/app/shared/providers/services/load.service.ts");
/* harmony import */ var app_shared_models_users_get_all_users_view__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! app/shared/models/users/get-all-users.view */ "./src/app/shared/models/users/get-all-users.view.ts");
/* harmony import */ var app_shared_models_project_get_all_active_project_view__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! app/shared/models/project/get-all-active-project.view */ "./src/app/shared/models/project/get-all-active-project.view.ts");
/* harmony import */ var app_shared_models_project_reassign_project_manager_view__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! app/shared/models/project/reassign-project-manager.view */ "./src/app/shared/models/project/reassign-project-manager.view.ts");
/* harmony import */ var app_shared_constans_projects_and_orders_constans__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! app/shared/constans/projects-and-orders.constans */ "./src/app/shared/constans/projects-and-orders.constans.ts");
/* harmony import */ var _shared_models_project_get_with_opened_orders_view__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../../shared/models/project/get-with-opened-orders.view */ "./src/app/shared/models/project/get-with-opened-orders.view.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var NewProjectsComponent = /** @class */ (function () {
    function NewProjectsComponent(route, notificationService, loadService, projectService, constans) {
        this.route = route;
        this.notificationService = notificationService;
        this.loadService = loadService;
        this.projectService = projectService;
        this.constans = constans;
        // public myControl = new FormControl();
        this.projectManagers = new app_shared_models_users_get_all_users_view__WEBPACK_IMPORTED_MODULE_6__["GetAllUserView"]();
        this.activeProjects = new app_shared_models_project_get_all_active_project_view__WEBPACK_IMPORTED_MODULE_7__["GetAllActiveProjectView"]();
        this.activeProjectsWithOpenedOrders = new _shared_models_project_get_with_opened_orders_view__WEBPACK_IMPORTED_MODULE_10__["GetWithOpenedOrdersProjectView"]();
        this.activeProjectsForSearch = new app_shared_models_project_get_all_active_project_view__WEBPACK_IMPORTED_MODULE_7__["GetAllActiveProjectView"]();
        this.activeProjectsWithOpenedOrdersForSearch = new _shared_models_project_get_with_opened_orders_view__WEBPACK_IMPORTED_MODULE_10__["GetWithOpenedOrdersProjectView"]();
        this.reassignProjectManager = new app_shared_models_project_reassign_project_manager_view__WEBPACK_IMPORTED_MODULE_8__["ReassignProjectManagerView"]();
        this.projectDetailsIsShow = false;
        this.createProjectIsShow = false;
        this.closeProjectShow = false;
    }
    NewProjectsComponent.prototype.ngOnInit = function () {
        this.loadService.set(true);
        //this.getActiveProject();
        this.getActiveProjectsWithOpenedOrders();
        this.getProjectManager();
    };
    NewProjectsComponent.prototype.getActiveProjectsWithOpenedOrders = function () {
        var _this = this;
        this.projectService.getAllActivePorjectsWithOpenedOrders().subscribe(function (response) {
            _this.activeProjectsWithOpenedOrders = response;
            _this.activeProjectsWithOpenedOrdersForSearch.projects = response.projects.slice(0);
            for (var i = 0; i < _this.activeProjectsWithOpenedOrdersForSearch.projects.length; i++) {
                if (!_this.activeProjectsWithOpenedOrdersForSearch.projects[i].name) {
                    _this.activeProjectsWithOpenedOrders.projects[i].name = _this.activeProjectsWithOpenedOrdersForSearch.projects[i].numericId.toString();
                    _this.activeProjectsWithOpenedOrdersForSearch.projects[i].name = _this.activeProjectsWithOpenedOrdersForSearch.projects[i].numericId.toString();
                }
            }
            _this.updateDate = null;
            _this.loadService.set(false);
            _this.projectsCount = _this.activeProjectsWithOpenedOrders.projects.length;
        }, function (error) {
            _this.loadService.set(false);
        });
    };
    NewProjectsComponent.prototype.getActiveProject = function () {
        var _this = this;
        this.projectService.getAllActivePorjects().subscribe(function (response) {
            _this.activeProjects = response;
            _this.activeProjectsForSearch.projects = response.projects.slice(0);
            for (var i = 0; i < _this.activeProjectsForSearch.projects.length; i++) {
                if (!_this.activeProjectsForSearch.projects[i].name) {
                    _this.activeProjects.projects[i].name = _this.activeProjectsForSearch.projects[i].numericId.toString();
                    _this.activeProjectsForSearch.projects[i].name = _this.activeProjectsForSearch.projects[i].numericId.toString();
                }
            }
            _this.updateDate = null;
            _this.loadService.set(false);
            _this.projectsCount = _this.activeProjects.projects.length;
        }, function (error) {
            _this.loadService.set(false);
        });
    };
    NewProjectsComponent.prototype.showClosedProjects = function () {
        if (this.closeProjectShow) {
            this.getActiveProject();
        }
        if (!this.closeProjectShow) {
            this.getActiveProjectsWithOpenedOrders();
        }
    };
    NewProjectsComponent.prototype.projectSearch = function () {
        var _this = this;
        if (this.showClosedProjects) {
            this.activeProjects.projects = this.activeProjectsForSearch.projects;
            this.activeProjects.projects = lodash__WEBPACK_IMPORTED_MODULE_1__["filter"](this.activeProjects.projects, function (row) { return ((row.name).toLowerCase()).indexOf(_this.searcText.toLowerCase()) > -1; });
            this.projectsCount = this.activeProjects.projects.length;
        }
        if (this.showClosedProjects) {
            this.activeProjectsWithOpenedOrders.projects = this.activeProjectsWithOpenedOrdersForSearch.projects;
            this.activeProjectsWithOpenedOrders.projects = lodash__WEBPACK_IMPORTED_MODULE_1__["filter"](this.activeProjectsWithOpenedOrders.projects, function (row) { return ((row.name).toLowerCase()).indexOf(_this.searcText.toLowerCase()) > -1; });
            this.projectsCount = this.activeProjectsWithOpenedOrders.projects.length;
        }
    };
    NewProjectsComponent.prototype.stopPropagation = function (event) {
        event.stopPropagation();
    };
    NewProjectsComponent.prototype.getProjectManager = function () {
        var _this = this;
        this.projectService.getProjectManager().subscribe(function (response) {
            _this.projectManagers = response;
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    NewProjectsComponent.prototype.saveNewProject = function () {
        this.createProjectIsShow = true;
        this.projectDetailsIsShow = false;
    };
    NewProjectsComponent.prototype.closeDetails = function () {
        this.projectDetailsIsShow = false;
    };
    NewProjectsComponent.prototype.closeCreatePorject = function () {
        this.createProjectIsShow = false;
        this.getActiveProject();
    };
    NewProjectsComponent.prototype.showProjectDetailsById = function (id) {
        this.createProjectIsShow = false;
        this.projectDetailsIsShow = true;
        this.currentProjectId = id;
    };
    NewProjectsComponent.prototype.reasignProjectManager = function (projectId, userId) {
        var _this = this;
        this.reassignProjectManager = new app_shared_models_project_reassign_project_manager_view__WEBPACK_IMPORTED_MODULE_8__["ReassignProjectManagerView"]();
        this.reassignProjectManager.projectId = projectId;
        this.reassignProjectManager.projectManagerId = userId;
        this.projectService.reassignProjectManeger(this.reassignProjectManager).subscribe(function (response) {
            _this.getActiveProject();
            _this.notificationService.showSuccess(_this.constans.projectManagerUpdateMessage);
        });
    };
    NewProjectsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-new-projects',
            template: __webpack_require__(/*! ./new-projects.component.html */ "./src/app/projects-and-orders/project-and-orders/new-projects/new-projects/new-projects.component.html"),
            styles: [__webpack_require__(/*! ./new-projects.component.scss */ "./src/app/projects-and-orders/project-and-orders/new-projects/new-projects/new-projects.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            app_shared_providers_services_noitification_service__WEBPACK_IMPORTED_MODULE_4__["NotificationService"],
            app_shared_providers_services_load_service__WEBPACK_IMPORTED_MODULE_5__["LoadService"],
            app_shared_providers_services_project_service__WEBPACK_IMPORTED_MODULE_3__["ProjectService"],
            app_shared_constans_projects_and_orders_constans__WEBPACK_IMPORTED_MODULE_9__["ProjectsAndOrdersConstans"]])
    ], NewProjectsComponent);
    return NewProjectsComponent;
}());



/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/new-projects/new-projects/project-details/project-details.component.html":
/*!*********************************************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/new-projects/new-projects/project-details/project-details.component.html ***!
  \*********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-card class=\"project-card\">\r\n  <div class=\"project-header col-md-12\">\r\n    <h4 class=\"name-project col-md-12\">Project #{{currentProject.numericId}}</h4>\r\n    <button class=\"close-project btn\" (click)=\"closeDetails()\"><i class=\"fas fa-times\"></i></button>\r\n  </div>\r\n  <mat-tab-group class=\"project-navigation\">\r\n    <mat-tab label=\"Project Details\">\r\n      <ng-template mat-tab-label>\r\n        <span>Projects</span>\r\n      </ng-template>\r\n      <mat-card class=\"details-card project\">\r\n        <div class=\"edit-project-button col-md-12\">\r\n          <img *ngIf=\"!activeEditProject\" src=\"../../../../../assets/img/pencil-edit-button.svg\" (click)=\"activateEditProject()\"\r\n               class=\"edit-button\" alt=\"\">\r\n          <div class=\"edit-button-wrapper\">\r\n            <a class=\"proj-button\" *ngIf=\"activeEditProject\" (click)=\"canceltProjectChange()\">\r\n              Cancel\r\n            </a>\r\n            <a class=\"proj-button\" *ngIf=\"activeEditProject\" (click)=\"saveProjectChange()\">\r\n              Save\r\n            </a>\r\n          </div>\r\n        </div>\r\n        <mat-form-field class=\"example-full-width col-md-4\">\r\n          <input matInput [disabled]=\"!activeEditProject\" placeholder=\"Project Name\" [(ngModel)]=\"currentProject.name\"\r\n                 value=\"\">\r\n        </mat-form-field>\r\n        <mat-form-field class=\"example-full-width col-md-4\">\r\n          <mat-select placeholder=\"Project Manager\" [disabled]=\"!activeEditProject\" [(ngModel)]=\"currentProject.userId\">\r\n            <mat-option *ngFor=\"let item of projectManagers.users\" [value]=\"item.id\">\r\n              {{item.firstName}} {{item.lastName}}\r\n            </mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n        <mat-form-field class=\"example-full-width col-md-4\">\r\n          <input matInput [disabled]=\"!activeEditProject\"\r\n                 [ngModel]=\"currentProject.projectType\"\r\n                 (keyup)=\"typedProjectType($event)\"\r\n                 [matAutocomplete]=\"auto\"\r\n                 placeholder=\"Project Type\">\r\n        </mat-form-field>\r\n\r\n        <mat-autocomplete #auto=\"matAutocomplete\"\r\n                          (optionSelected)=\"selectedProjectType($event)\"\r\n                          [displayWith]=\"displayFn\">\r\n          <mat-option *ngFor=\"let item of projectTypesView.projectTypes\"\r\n                      [value]=\"item\"\r\n                      [disabled]=\"!activeEditProject\">\r\n            {{item.name}}\r\n          </mat-option>\r\n        </mat-autocomplete>\r\n        <mat-form-field class=\"example-full-width col-md-4\">\r\n          <mat-select placeholder=\"Customer\" [(ngModel)]=\"currentProject.customerId\" [disabled]=\"!activeEditProject\">\r\n            <mat-option *ngFor=\"let cust of customers.customers\" [value]=\"cust.id\">\r\n              {{cust.name}}\r\n            </mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n        <mat-form-field class=\"example-full-width col-md-4\">\r\n          <input matInput placeholder=\"Description\" [disabled]=\"!activeEditProject\" [(ngModel)]=\"currentProject.description\">\r\n        </mat-form-field>\r\n        <mat-form-field class=\"example-full-width  col-md-12\">\r\n          <textarea matInput [disabled]=\"!activeEditProject\" matTextareaAutosize [(ngModel)]=\"currentProject.memo\" placeholder=\"Memmo\"></textarea>\r\n        </mat-form-field>\r\n      </mat-card>\r\n      <h6 class=\"attachments-title\">Attachments</h6>\r\n      <mat-card class=\"attachments-card details-card col-md-6\">\r\n        <ul class=\"file-list\">\r\n          <li class=\"file-item\" *ngFor=\"let item of projectAttachments.attachments\" title=\"{{item.fileName}}{{item.fileExtantion}}\">\r\n            <div class=\"file-name\" (click)=\"openShowAttachment(item.id)\">{{item.fileName}}{{item.fileExtantion}}</div>\r\n            <div class=\"control-buttons\">\r\n              <button class=\"download control-btn mr-1\" (click)=\"downloadProjectAttachment(item.id)\"><i class=\"fa fa-arrow-down\"></i></button>\r\n              <button class=\"remove control-btn\" (click)=\"openDeleteAttachment(item.id)\"><i class=\"fas fa-times\"></i></button>\r\n            </div>\r\n          </li>\r\n        </ul>\r\n        <div ng2FileDrop [ngClass]=\"{'nv-file-over': hasBaseDropZoneOver}\" (fileOver)=\"fileOverBase($event)\" (onFileDrop)=\"dropped($event)\"\r\n             [uploader]=\"uploader\" class=\"table-responsive col-md-12 p-0\">\r\n          <span *ngIf=\"showSpinner\">\r\n            <mat-progress-bar mode=\"indeterminate\"></mat-progress-bar>\r\n          </span>\r\n          <div class=\"drag-zone\">\r\n            Drag and drop files here\r\n          </div>\r\n        </div>\r\n        <div class=\"button-block\">\r\n          <button class=\"btn\" (click)=\"downloadAllProjectAttachment(currentProject.id)\">Download All</button>\r\n          <label class=\"custom-file-upload\">\r\n            <input (change)=\"inputAdd($event)\" #fileUploader type=\"file\" multiple />\r\n            Add New Files\r\n          </label>\r\n        </div>\r\n      </mat-card>\r\n    </mat-tab>\r\n    <mat-tab label=\"Orders\">\r\n      <ng-template mat-tab-label>\r\n        <span>Orders</span>\r\n      </ng-template>\r\n      <div class=\"kanban\">\r\n        <div class=\"row\">\r\n          <div class=\"col-md-3\">\r\n            <div class=\"section-heading\">New</div>\r\n            <div class=\"drag-container\">\r\n              <div cdkDropList\r\n                   #newList=\"cdkDropList\"\r\n                   [cdkDropListData]=\"newOrder\"\r\n                   [cdkDropListConnectedTo]=\"[activeList,processingList,doneList]\"\r\n                   class=\"item-list\" (cdkDropListDropped)=\"drop($event)\">\r\n                <div class=\"item-box\" *ngFor=\"let item of newOrder\" (click)=\"showOrdersDetails(item.id)\" cdkDrag>\r\n\r\n                  <div class=\"d-flex align-items-center space-between mb-1\">\r\n                    <a (click)=\"callBack($event)\">\r\n                      <img class=\"order-blue\" src=\"../../../../../assets/img/order-2.svg\" alt=\"\">\r\n                    </a>\r\n                    <a class=\"underline-on-hover\" (click)=\"callBack($event)\">\r\n                      <span class=\"order-count\">{{item.numericId}}</span>\r\n                    </a>\r\n                    <a class=\"underline-on-hover date\" (click)=\"callBack($event)\">{{item.date | date:'shortDate'}}</a>\r\n                  </div>\r\n\r\n                  <div class=\"clients mb-1\">\r\n                    <div class=\"customer\">\r\n                      <span>Customer</span>\r\n                      <a class=\"underline-on-hover\" (click)=\"callBack($event)\">{{item.customer.name}}</a>\r\n                    </div>\r\n                    <div class=\"space-between item\">\r\n                      <span>Item</span>\r\n                      <a class=\"underline-on-hover\" (click)=\"callBack($event)\">{{item.item.name}}</a>\r\n                    </div>\r\n                  </div>\r\n\r\n                  <div class=\"order-info d-flex justify-content-between align-items-center mb-1\" style=\"font-size:16px;\">\r\n                    <div class=\"space-between align-center\">\r\n                      <div class=\"red-circle\">\r\n                        <a (click)=\"callBack($event)\">{{item.project.user.firstName[0]}}</a>\r\n                      </div>\r\n                      <span class=\"name-order\">\r\n                        <a class=\"underline-on-hover\" (click)=\"callBack($event)\"> {{item.project.user.firstName}}   {{item.project.user.lastName}}</a>\r\n                      </span>\r\n                    </div>\r\n                    <div class=\"clipboard\">\r\n                      <a class=\"callback-icon\" (click)=\"callBack($event)\">\r\n                        <img src=\"../../../../../assets/img/clipboard-1.svg\" alt=\"\">\r\n                      </a>\r\n                      <a class=\"underline-on-hover\" (click)=\"callBack($event)\">0/1</a>\r\n                    </div>\r\n                  </div>\r\n\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n\r\n          <div class=\"col-md-3\">\r\n            <div class=\"section-heading\">Active</div>\r\n            <div class=\"drag-container\">\r\n              <div cdkDropList\r\n                   #activeList=\"cdkDropList\"\r\n                   [cdkDropListData]=\"activeOrder\"\r\n                   [cdkDropListConnectedTo]=\"[newList,processingList,doneList]\"\r\n                   class=\"item-list\" (cdkDropListDropped)=\"drop($event)\">\r\n                <div class=\"item-box\" *ngFor=\"let item of activeOrder\" (click)=\"showOrdersDetails(item.id)\" cdkDrag>\r\n\r\n                  <div class=\"d-flex align-items-center space-between mb-1\">\r\n                    <a (click)=\"callBack($event)\">\r\n                      <img class=\"order-blue\" src=\"../../../../../assets/img/order-2.svg\" alt=\"\">\r\n                    </a>\r\n                    <a class=\"underline-on-hover\" (click)=\"callBack($event)\">\r\n                      <span class=\"order-count\">{{item.numericId}}</span>\r\n                    </a>\r\n                    <a class=\"underline-on-hover date\" (click)=\"callBack($event)\">{{item.date | date:'shortDate'}}</a>\r\n                  </div>\r\n\r\n                  <div class=\"clients mb-1\">\r\n                    <div class=\"customer\">\r\n                      <span>Customer</span>\r\n                      <a class=\"underline-on-hover\" (click)=\"callBack($event)\">{{item.customer.name}}</a>\r\n                    </div>\r\n                    <div class=\"space-between item\">\r\n                      <span>Item</span>\r\n                      <a class=\"underline-on-hover\" (click)=\"callBack($event)\">{{item.item.name}}</a>\r\n                    </div>\r\n                  </div>\r\n\r\n                  <div class=\"order-info d-flex justify-content-between align-items-center mb-1\" style=\"font-size:16px;\">\r\n                    <div class=\"space-between align-center\">\r\n                      <div class=\"red-circle\">\r\n                        <a (click)=\"callBack($event)\">{{item.project.user.firstName[0]}}</a>\r\n                      </div>\r\n                      <span class=\"name-order\">\r\n                        <a class=\"underline-on-hover\" (click)=\"callBack($event)\"> {{item.project.user.firstName}}   {{item.project.user.lastName}}</a>\r\n                      </span>\r\n                    </div>\r\n                    <div class=\"clipboard\">\r\n                      <a class=\"callback-icon\" (click)=\"callBack($event)\">\r\n                        <img src=\"../../../../../assets/img/clipboard-1.svg\" alt=\"\">\r\n                      </a>\r\n                      <a class=\"underline-on-hover\" (click)=\"callBack($event)\">0/1</a>\r\n                    </div>\r\n                  </div>\r\n\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n\r\n          <div class=\"col-md-3\">\r\n            <div class=\"section-heading\">Processing</div>\r\n            <div class=\"drag-container\">\r\n              <div cdkDropList\r\n                   #processingList=\"cdkDropList\"\r\n                   [cdkDropListData]=\"processingOrder\"\r\n                   [cdkDropListConnectedTo]=\"[activeList,newList,doneList]\"\r\n                   class=\"item-list\" (cdkDropListDropped)=\"drop($event)\">\r\n                <div class=\"item-box\" *ngFor=\"let item of processingOrder\" (click)=\"showOrdersDetails(item.id)\" cdkDrag>\r\n\r\n                  <div class=\"d-flex align-items-center space-between mb-1\">\r\n                    <a (click)=\"callBack($event)\">\r\n                      <img class=\"order-blue\" src=\"../../../../../assets/img/order-2.svg\" alt=\"\">\r\n                    </a>\r\n                    <a class=\"underline-on-hover\" (click)=\"callBack($event)\">\r\n                      <span class=\"order-count\">{{item.numericId}}</span>\r\n                    </a>\r\n                    <a class=\"underline-on-hover date\" (click)=\"callBack($event)\">{{item.date | date:'shortDate'}}</a>\r\n                  </div>\r\n\r\n                  <div class=\"clients mb-1\">\r\n                    <div class=\"customer\">\r\n                      <span>Customer</span>\r\n                      <a class=\"underline-on-hover\" (click)=\"callBack($event)\">{{item.customer.name}}</a>\r\n                    </div>\r\n                    <div class=\"space-between item\">\r\n                      <span>Item</span>\r\n                      <a class=\"underline-on-hover\" (click)=\"callBack($event)\">{{item.item.name}}</a>\r\n                    </div>\r\n                  </div>\r\n\r\n                  <div class=\"order-info d-flex justify-content-between align-items-center mb-1\" style=\"font-size:16px;\">\r\n                    <div class=\"space-between align-center\">\r\n                      <div class=\"red-circle\">\r\n                        <a (click)=\"callBack($event)\">{{item.project.user.firstName[0]}}</a>\r\n                      </div>\r\n                      <span class=\"name-order\">\r\n                        <a class=\"underline-on-hover\" (click)=\"callBack($event)\"> {{item.project.user.firstName}}   {{item.project.user.lastName}}</a>\r\n                      </span>\r\n                    </div>\r\n                    <div class=\"clipboard\">\r\n                      <a class=\"callback-icon\" (click)=\"callBack($event)\">\r\n                        <img src=\"../../../../../assets/img/clipboard-1.svg\" alt=\"\">\r\n                      </a>\r\n                      <a class=\"underline-on-hover\" (click)=\"callBack($event)\">0/1</a>\r\n                    </div>\r\n                  </div>\r\n\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n\r\n          <div class=\"col-md-3\">\r\n            <div class=\"section-heading\">Done</div>\r\n            <div class=\"drag-container\">\r\n              <div cdkDropList #doneList=\"cdkDropList\"\r\n                   [cdkDropListData]=\"doneOrder\"\r\n                   [cdkDropListConnectedTo]=\"[processingList,activeList,newList]\"\r\n                   class=\"item-list\" (cdkDropListDropped)=\"drop($event)\">\r\n                <div class=\"item-box\" *ngFor=\"let item of doneOrder\" (click)=\"showOrdersDetails(item.id)\" cdkDrag>\r\n\r\n                  <div class=\"d-flex align-items-center space-between mb-1\">\r\n                    <a (click)=\"callBack($event)\">\r\n                      <img class=\"order-blue\" src=\"../../../../../assets/img/order-2.svg\" alt=\"\">\r\n                    </a>\r\n                    <a class=\"underline-on-hover\" (click)=\"callBack($event)\">\r\n                      <span class=\"order-count\">{{item.numericId}}</span>\r\n                    </a>\r\n                    <a class=\"underline-on-hover date\" (click)=\"callBack($event)\">{{item.date | date:'shortDate'}}</a>\r\n                  </div>\r\n\r\n                  <div class=\"clients mb-1\">\r\n                    <div class=\"customer\">\r\n                      <span>Customer</span>\r\n                      <a class=\"underline-on-hover\" (click)=\"callBack($event)\">{{item.customer.name}}</a>\r\n                    </div>\r\n                    <div class=\"space-between item\">\r\n                      <span>Item</span>\r\n                      <a class=\"underline-on-hover\" (click)=\"callBack($event)\">{{item.item.name}}</a>\r\n                    </div>\r\n                  </div>\r\n\r\n                  <div class=\"order-info d-flex justify-content-between align-items-center mb-1\" style=\"font-size:16px;\">\r\n                    <div class=\"space-between align-center\">\r\n                      <div class=\"red-circle\">\r\n                        <a (click)=\"callBack($event)\">{{item.project.user.firstName[0]}}</a>\r\n                      </div>\r\n                      <span class=\"name-order\">\r\n                        <a class=\"underline-on-hover\" (click)=\"callBack($event)\"> {{item.project.user.firstName}}   {{item.project.user.lastName}}</a>\r\n                      </span>\r\n                    </div>\r\n                    <div class=\"clipboard\">\r\n                      <a class=\"callback-icon\" (click)=\"callBack($event)\">\r\n                        <img src=\"../../../../../assets/img/clipboard-1.svg\" alt=\"\">\r\n                      </a>\r\n                      <a class=\"underline-on-hover\" (click)=\"callBack($event)\">0/1</a>\r\n                    </div>\r\n                  </div>\r\n\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </mat-tab>\r\n    <mat-tab label=\"Activity\">\r\n      <ng-template mat-tab-label>\r\n        <span>Activity</span>\r\n      </ng-template>\r\n      <div class=\"col-6 activity-card-wrapper\">\r\n        <h6>Activity</h6>\r\n        <mat-card class=\"activity-card\">\r\n        </mat-card>\r\n      </div>\r\n    </mat-tab>\r\n  </mat-tab-group>\r\n</mat-card>"

/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/new-projects/new-projects/project-details/project-details.component.scss":
/*!*********************************************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/new-projects/new-projects/project-details/project-details.component.scss ***!
  \*********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".edit-project-button {\n  display: flex;\n  justify-content: flex-end; }\n  .edit-project-button .edit-button {\n    height: 24px;\n    width: 24px;\n    cursor: pointer; }\n  .project-header {\n  display: flex;\n  justify-content: space-between;\n  padding: 25px 74px 0px 74px !important;\n  margin-bottom: 40px; }\n  .project-header .close-project {\n    position: absolute;\n    top: 20px;\n    right: 5px;\n    border: none;\n    background: transparent;\n    padding: 0;\n    margin: 0;\n    margin-right: 15px;\n    font-size: 18px; }\n  .proj-button {\n  color: #0077c5 !important;\n  cursor: pointer;\n  margin-right: 18px; }\n  .proj-button:last-child {\n    margin-right: 0px; }\n  .edit-button-wrapper {\n  display: flex;\n  justify-content: flex-end;\n  margin-bottom: 13px; }\n  .edit-button-wrapper .btn {\n    width: 100px;\n    margin: 0;\n    margin-left: 10px; }\n  .name-project {\n  font-weight: bold;\n  margin: 0; }\n  .project-card {\n  overflow: hidden;\n  height: calc(100% - 20px);\n  max-height: 845px;\n  margin-top: 20px;\n  padding: 0;\n  border: 1px solid #949494; }\n  .project-list-wrapper {\n  margin-top: 20px;\n  border: 1px solid #949494;\n  height: calc(100% - 20px);\n  background: white; }\n  .project-list-wrapper .search-bar {\n    position: relative;\n    margin-top: 15px; }\n  .project-list-wrapper .search-bar .search-icon {\n      position: absolute;\n      line-height: 37px;\n      font-size: 16px;\n      left: 15px;\n      top: 0;\n      bottom: 0; }\n  .project-list-wrapper .search-bar .search {\n      width: 100%;\n      border: 1px solid #949494;\n      padding: 7px;\n      padding-left: 40px; }\n  .project-list-wrapper .project-list-title {\n    padding: 15px;\n    border-bottom: 1px solid #949494; }\n  .project-list-wrapper .project-list-title .add-wrapper {\n      display: flex;\n      justify-content: space-between;\n      align-items: center; }\n  .project-list-wrapper .project-list-title .add-new-project {\n      border: 1px solid #949494;\n      text-transform: uppercase;\n      padding: 10px;\n      font-size: 16px;\n      background: transparent; }\n  .project-list-wrapper .project-list-title .add-new-project:hover {\n        background: #e1e6ef;\n        transition: .2s; }\n  .project-list-wrapper .project-list {\n    max-height: 714px;\n    overflow: auto;\n    list-style: none;\n    margin: 0;\n    padding: 0; }\n  .project-list-wrapper .project-list .project-item {\n      padding: 15px;\n      border-bottom: 1px solid #949494; }\n  .project-list-wrapper .project-list .project-item:hover {\n        cursor: pointer;\n        background: #dcdcdc;\n        transition: .2s; }\n  .project-list-wrapper .project-list .data-group {\n      display: flex;\n      justify-content: space-between; }\n  .project-list-wrapper .project-list .data-group .archer {\n        font-weight: bold;\n        font-size: 15px; }\n  .project-list-wrapper .project-list .data-group .project-name {\n        font-weight: bold;\n        font-size: 14px; }\n  .project-list-wrapper .project-list .data-group .type {\n        color: #948f8f;\n        margin-left: 15px; }\n  .project-list-wrapper .project-list .data-group .stat .icon {\n        font-size: 16px;\n        color: #009aff;\n        margin-right: 10px;\n        cursor: pointer; }\n  .details-card.project {\n  border-bottom: 1px solid #949494; }\n  textarea.mat-autosize {\n  resize: auto !important;\n  max-height: 80px; }\n  .attachments-title {\n  margin-left: 74px;\n  margin-top: 20px; }\n  @media (max-width: 1636px) {\n    .attachments-title {\n      margin-left: 15px;\n      margin-top: 15px; } }\n  .attachments-card {\n  padding-top: 20px !important; }\n  @media (max-width: 1636px) {\n    .attachments-card {\n      padding: 15px !important; } }\n  .attachments-card .file-list {\n    list-style: none;\n    padding: 0;\n    max-height: 100px;\n    overflow: auto; }\n  .attachments-card .file-list .file-item {\n      display: flex;\n      justify-content: space-between; }\n  .attachments-card .file-list .file-name {\n      white-space: nowrap;\n      overflow: hidden;\n      text-overflow: ellipsis;\n      font-size: 16px;\n      color: #0077c5;\n      cursor: pointer; }\n  .attachments-card .file-list .control-btn {\n      background: transparent;\n      margin: 0;\n      padding: 5px;\n      border: none; }\n  .attachments-card .drag-zone {\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    margin-top: 20px;\n    width: 100%;\n    height: 72px;\n    font-size: 16px;\n    color: #636363;\n    border: 2px dashed #707070;\n    border-right-color: #D0D0D0;\n    border-left-color: #D0D0D0;\n    border-top-color: #d4d4d4;\n    border-bottom-color: #d4d4d4; }\n  .attachments-card .button-block {\n    display: flex;\n    justify-content: space-between;\n    margin-top: 10px; }\n  .attachments-card .button-block .btn {\n      border: 0;\n      margin: 0;\n      padding: 0;\n      font-size: 14px;\n      color: #0077c5;\n      background: transparent; }\n  .control-buttons {\n  white-space: nowrap; }\n  .custom-file-upload {\n  color: #0077c5;\n  cursor: pointer; }\n  label {\n  margin-bottom: 0px; }\n  input[type=\"file\"] {\n  display: none; }\n  .modal-add-project {\n  position: fixed;\n  background: #fff;\n  z-index: 10000;\n  top: -100%;\n  left: calc(50% - 200px);\n  width: 400px; }\n  .modal-add-project.active {\n    top: 200px;\n    left: calc(50% - 200px);\n    transition: top .5s; }\n  .modal-add-project .row {\n    width: 100%; }\n  .modal-add-project .row .mt-10 {\n      margin-top: 15px; }\n  .modal-add-project .button-wrapper {\n    display: flex;\n    justify-content: flex-end;\n    margin-top: 10px; }\n  .modal-add-project .button-wrapper .btn {\n      width: 100px;\n      margin: 0;\n      margin-left: 10px; }\n  .overlay {\n  position: fixed;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  background: rgba(0, 0, 0, 0.4);\n  z-index: 10000; }\n  .clickable-title-link {\n  color: rgba(var(--palette-neutral-100, 0, 0, 0), 1); }\n  .underline-on-hover:hover {\n  text-decoration: underline !important; }\n  .underline-on-hover {\n  font-size: 16px; }\n  .section-heading {\n  padding: 5px 10px 11px 0px;\n  font-size: 18px;\n  font-weight: bold; }\n  .drag-container {\n  width: 100%;\n  height: auto;\n  max-width: 100%;\n  margin: 0 25px 25px 0;\n  display: inline-block;\n  vertical-align: top;\n  background-color: #E9ECEF;\n  padding: 15px; }\n  .item-list {\n  min-height: 60px;\n  border-radius: 4px;\n  display: block; }\n  .item-box:hover {\n  cursor: pointer; }\n  .item-box {\n  border: solid 1px #808080;\n  margin-bottom: 15px;\n  color: rgba(0, 0, 0, 0.87);\n  box-sizing: border-box;\n  cursor: move;\n  background: white;\n  font-size: 13px;\n  padding: 15px; }\n  .item-box p {\n    margin-bottom: 5px;\n    color: #636363; }\n  .item-box p span {\n      color: black; }\n  .item-box p .date-proj {\n      margin-left: 25px; }\n  .item-box p .red {\n      background: #CD3232;\n      display: block;\n      height: 25px;\n      width: 25px;\n      margin-right: 10px;\n      border-radius: 50%; }\n  .item-box p .blue {\n      background: #0077c5;\n      display: block;\n      width: 15px;\n      height: 15px; }\n  .item-box p .yellow {\n      background: #ffba00;\n      display: block;\n      width: 20px;\n      height: 22.1px; }\n  .cdk-drag-preview {\n  box-sizing: border-box;\n  border-radius: 4px;\n  box-shadow: 0 5px 5px -3px rgba(0, 0, 0, 0.2), 0 8px 10px 1px rgba(0, 0, 0, 0.14), 0 3px 14px 2px rgba(0, 0, 0, 0.12);\n  border-radius: 20px; }\n  #newOrders {\n  font-size: 14px;\n  font-weight: bold;\n  color: #636363;\n  background: transparent;\n  border: 1px solid #707070; }\n  .item-box .order-blue {\n  width: 16.5px;\n  height: 20px;\n  margin-right: 10px; }\n  .item-box .order-count {\n  margin-right: 31px;\n  font-size: 16px;\n  font-weight: bold; }\n  .item-box .order-date {\n  font-size: 16px; }\n  .item-box .clipboard {\n  display: flex;\n  align-items: center;\n  margin-left: 10px; }\n  .item-box .clipboard img {\n    width: 20px;\n    height: 22.1px;\n    margin-right: 10px; }\n  .item-box .red-circle {\n  min-width: 30px;\n  min-height: 30px;\n  background-color: #cd3232;\n  border-radius: 50%;\n  color: white;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  text-transform: uppercase;\n  margin-right: 10px; }\n  .item-box .clients {\n  font-size: 16px; }\n  .item-box .clients span {\n    color: #636363;\n    margin-right: 9px; }\n  .orders-name {\n  font-size: 24px;\n  font-weight: bold;\n  margin-bottom: 36px;\n  margin-top: 25px; }\n  .kanban {\n  margin-top: 105px;\n  width: 96%;\n  margin-left: 9px; }\n  .name-order {\n  white-space: nowrap;\n  text-overflow: ellipsis;\n  overflow: hidden; }\n  .callback-icon {\n  display: flex;\n  align-items: center; }\n  .card-header {\n  display: flex; }\n  .drag-container {\n  margin: 0; }\n  .item-box {\n  padding: 10px; }\n  .name-order {\n  width: 75px; }\n  @media screen and (max-width: 1900px) {\n  .drag-container {\n    padding: 5px; }\n  .customer, .item {\n    display: block !important; }\n    .customer span, .item span {\n      font-size: 12px; }\n    .customer a, .item a {\n      display: block; }\n  .order-info {\n    display: block !important; }\n  .clipboard {\n    display: flex;\n    justify-content: space-between;\n    margin: 0;\n    margin-left: 5px !important;\n    margin-top: 10px !important; }\n  .date {\n    font-size: 12px; }\n  .name-order {\n    width: 100%;\n    display: flex;\n    justify-content: flex-end; }\n    .name-order a {\n      overflow: hidden;\n      text-overflow: ellipsis;\n      font-size: 12px; } }\n  .modal-show-pdf {\n  position: fixed;\n  background: #fff;\n  z-index: 10000;\n  top: -100%;\n  left: calc(50% - 400px);\n  width: 800px; }\n  .modal-show-pdf .modal-body {\n    padding-right: 0; }\n  .modal-show-pdf .modal-body .pdf-container {\n      max-height: 400px;\n      overflow-y: auto; }\n  .modal-show-pdf .modal-body .text-container {\n      max-height: 400px;\n      overflow-y: auto; }\n  .modal-show-pdf.active {\n    top: 200px;\n    left: calc(50% - 400px);\n    transition: top .5s; }\n  .modal-show-pdf .row {\n    width: 100%; }\n  .modal-show-pdf .row .mt-10 {\n      margin-top: 15px; }\n  .modal-show-pdf .button-wrapper {\n    display: flex;\n    justify-content: flex-end;\n    margin-top: 10px; }\n  .modal-show-pdf .button-wrapper .btn {\n      width: 100px;\n      margin: 0;\n      margin-left: 10px; }\n"

/***/ }),

/***/ "./src/app/projects-and-orders/project-and-orders/new-projects/new-projects/project-details/project-details.component.ts":
/*!*******************************************************************************************************************************!*\
  !*** ./src/app/projects-and-orders/project-and-orders/new-projects/new-projects/project-details/project-details.component.ts ***!
  \*******************************************************************************************************************************/
/*! exports provided: ProjectDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProjectDetailsComponent", function() { return ProjectDetailsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var file_saver__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! file-saver */ "./node_modules/file-saver/dist/FileSaver.min.js");
/* harmony import */ var file_saver__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(file_saver__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/cdk/drag-drop */ "./node_modules/@angular/cdk/esm5/drag-drop.es5.js");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ng2-file-upload */ "./node_modules/ng2-file-upload/index.js");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(ng2_file_upload__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var app_shared_constans_projects_and_orders_constans__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! app/shared/constans/projects-and-orders.constans */ "./src/app/shared/constans/projects-and-orders.constans.ts");
/* harmony import */ var app_shared_providers_services_project_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! app/shared/providers/services/project.service */ "./src/app/shared/providers/services/project.service.ts");
/* harmony import */ var app_shared_providers_services_customer_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! app/shared/providers/services/customer.service */ "./src/app/shared/providers/services/customer.service.ts");
/* harmony import */ var app_shared_providers_services_noitification_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! app/shared/providers/services/noitification.service */ "./src/app/shared/providers/services/noitification.service.ts");
/* harmony import */ var app_shared_providers_services_project_attachment_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! app/shared/providers/services/project-attachment.service */ "./src/app/shared/providers/services/project-attachment.service.ts");
/* harmony import */ var app_shared_providers_services_load_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! app/shared/providers/services/load.service */ "./src/app/shared/providers/services/load.service.ts");
/* harmony import */ var app_shared_providers_services_order_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! app/shared/providers/services/order.service */ "./src/app/shared/providers/services/order.service.ts");
/* harmony import */ var app_shared_models_users_get_all_users_view__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! app/shared/models/users/get-all-users.view */ "./src/app/shared/models/users/get-all-users.view.ts");
/* harmony import */ var app_shared_models_customer_get_all_customer_view__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! app/shared/models/customer/get-all-customer.view */ "./src/app/shared/models/customer/get-all-customer.view.ts");
/* harmony import */ var app_shared_models_project_update_project_view__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! app/shared/models/project/update-project.view */ "./src/app/shared/models/project/update-project.view.ts");
/* harmony import */ var app_shared_models_project_attachment_get_all_by_project_id_projects_attachments_view__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! app/shared/models/project-attachment/get-all-by-project-id-projects-attachments-view */ "./src/app/shared/models/project-attachment/get-all-by-project-id-projects-attachments-view.ts");
/* harmony import */ var app_shared_models_project_attachment_delete_project_attachment_view__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! app/shared/models/project-attachment/delete-project-attachment.view */ "./src/app/shared/models/project-attachment/delete-project-attachment.view.ts");
/* harmony import */ var app_shared_models_project_get_all_project_types_view__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! app/shared/models/project/get-all-project-types.view */ "./src/app/shared/models/project/get-all-project-types.view.ts");
/* harmony import */ var app_shared_models_order_get_all_order_by_projectId_view__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! app/shared/models/order/get-all-order-by-projectId.view */ "./src/app/shared/models/order/get-all-order-by-projectId.view.ts");
/* harmony import */ var app_shared_models_project_get_all_active_project_view__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! app/shared/models/project/get-all-active-project.view */ "./src/app/shared/models/project/get-all-active-project.view.ts");
/* harmony import */ var app_shared_models_project_attachment_get_project_attachment_view__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! app/shared/models/project-attachment/get-project-attachment.view */ "./src/app/shared/models/project-attachment/get-project-attachment.view.ts");
/* harmony import */ var app_shared_models_order_update_order_view__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! app/shared/models/order/update-order.view */ "./src/app/shared/models/order/update-order.view.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






















var ProjectDetailsComponent = /** @class */ (function () {
    function ProjectDetailsComponent(route, notificationService, loadService, customerService, projectAttachmentService, constans, projectService, ordersService) {
        this.route = route;
        this.notificationService = notificationService;
        this.loadService = loadService;
        this.customerService = customerService;
        this.projectAttachmentService = projectAttachmentService;
        this.constans = constans;
        this.projectService = projectService;
        this.ordersService = ordersService;
        this.closeDetailsComponent = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.updateProjectEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.updateProject = new app_shared_models_project_update_project_view__WEBPACK_IMPORTED_MODULE_14__["UpdateProjectView"]();
        this.projectManagers = new app_shared_models_users_get_all_users_view__WEBPACK_IMPORTED_MODULE_12__["GetAllUserView"]();
        this.currentProject = new app_shared_models_project_get_all_active_project_view__WEBPACK_IMPORTED_MODULE_19__["GetAllActiveProjectViewItem"]();
        this.projectTypesView = new app_shared_models_project_get_all_project_types_view__WEBPACK_IMPORTED_MODULE_17__["GetAllProjectTypesView"]();
        this.customers = new app_shared_models_customer_get_all_customer_view__WEBPACK_IMPORTED_MODULE_13__["GetAllCustomerView"]();
        this.projectAttachments = new app_shared_models_project_attachment_get_all_by_project_id_projects_attachments_view__WEBPACK_IMPORTED_MODULE_15__["GetAllByProjectIdProjectAttachmentsView"]();
        this.activeEditProject = false;
        this.showSpinner = false;
        //popups
        this.formValidation = false;
        this.openSaveAttachment = false;
        this.openDeleteAttachmentModal = false;
        this.droppedFiles = [];
        this.deletedAttachment = new app_shared_models_project_attachment_delete_project_attachment_view__WEBPACK_IMPORTED_MODULE_16__["DeleteProjectAttachmentView"]();
        this.hasBaseDropZoneOver = false;
        //view attachment
        this.uploader = new ng2_file_upload__WEBPACK_IMPORTED_MODULE_4__["FileUploader"]({ url: this.constans.URL });
        this.showAttachmentImg = false;
        this.showAttachedPdf = false;
        this.showAttachedTxt = false;
        this.showAttachment = false;
        this.currentProjectAttachment = new app_shared_models_project_attachment_get_project_attachment_view__WEBPACK_IMPORTED_MODULE_20__["GetProjectAttachmentView"]();
        //orders
        this.ordersByProjectId = new app_shared_models_order_get_all_order_by_projectId_view__WEBPACK_IMPORTED_MODULE_18__["GetAllOrdersByProjectIdView"]();
        this.newOrder = [];
        this.activeOrder = [];
        this.processingOrder = [];
        this.doneOrder = [];
        this.updateOrder = new app_shared_models_order_update_order_view__WEBPACK_IMPORTED_MODULE_21__["UpdateOrderView"]();
    }
    ProjectDetailsComponent.prototype.closeDetails = function () {
        this.closeDetailsComponent.emit(false);
    };
    ProjectDetailsComponent.prototype.ngOnChanges = function () {
        this.loadService.set(true);
        this.showProjectDetailsById(this.projectId);
        this.getProjectManager();
        this.getProjectTypes();
        this.getCustomers();
        this.getAllOrdersByProjectId(this.projectId);
    };
    ProjectDetailsComponent.prototype.getProjectManager = function () {
        var _this = this;
        this.projectService.getProjectManager().subscribe(function (response) {
            _this.projectManagers = response;
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    ProjectDetailsComponent.prototype.showOrdersDetails = function (id) {
        this.route.navigate(['projects-and-orders/orders/orderDetails', id]);
    };
    ProjectDetailsComponent.prototype.drop = function (event) {
        var _this = this;
        if (event.previousContainer === event.container) {
            Object(_angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_3__["moveItemInArray"])(event.container.data, event.previousIndex, event.currentIndex);
        }
        else {
            Object(_angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_3__["transferArrayItem"])(event.previousContainer.data, event.container.data, event.previousIndex, event.currentIndex);
            this.curentOrder = event.container.data[event.currentIndex];
            if (event.container.id == this.constans.kanbanBlockStatus.new) {
                this.curentOrder.status = this.constans.status.new;
            }
            if (event.container.id == this.constans.kanbanBlockStatus.active) {
                this.curentOrder.status = this.constans.status.active;
            }
            if (event.container.id == this.constans.kanbanBlockStatus.processing) {
                this.curentOrder.status = this.constans.status.processing;
            }
            if (event.container.id == this.constans.kanbanBlockStatus.done) {
                this.curentOrder.status = this.constans.status.done;
            }
            this.updateOrder.id = this.curentOrder.id;
            this.updateOrder.date = this.curentOrder.date;
            this.updateOrder.dueDate = this.curentOrder.dueDate;
            this.updateOrder.memo = this.curentOrder.memo;
            this.updateOrder.status = this.curentOrder.status;
            this.updateOrder.customerId = this.curentOrder.customer.id;
            this.updateOrder.projectId = this.curentOrder.project.id;
            this.updateOrder.itemId = this.curentOrder.item.id;
            this.ordersService.updateOrder(this.updateOrder).subscribe(function (response) {
                _this.notificationService.showSuccess(_this.constans.orderUpdateMessage);
            });
        }
    };
    ProjectDetailsComponent.prototype.getAllOrdersByProjectId = function (projectId) {
        var _this = this;
        this.ordersService.getAllOrdersByProjectId(projectId).subscribe(function (response) {
            _this.ordersByProjectId = response;
            console.log(_this.ordersByProjectId);
            _this.sortOrderByStatus(_this.ordersByProjectId.orders);
        }, function (error) {
            _this.errors = error;
        });
    };
    ProjectDetailsComponent.prototype.sortOrderByStatus = function (allOrders) {
        var _this = this;
        this.newOrder = allOrders.filter(function (x) { return x.status == _this.constans.status.new; });
        this.activeOrder = allOrders.filter(function (x) { return x.status == _this.constans.status.active; });
        this.processingOrder = allOrders.filter(function (x) { return x.status == _this.constans.status.processing; });
        this.doneOrder = allOrders.filter(function (x) { return x.status == _this.constans.status.done; });
    };
    ProjectDetailsComponent.prototype.getProjectTypes = function () {
        var _this = this;
        this.projectService.getAllProjectTypes().subscribe(function (response) {
            _this.projectTypesView = response;
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    ProjectDetailsComponent.prototype.displayFn = function (val) {
        return val ? val.name : val;
    };
    ProjectDetailsComponent.prototype.selectedProjectType = function (item) {
        this.currentProject.projectType.id = item.option.value.id;
        this.currentProject.projectType.name = item.option.value.name;
    };
    ProjectDetailsComponent.prototype.typedProjectType = function (event) {
        this.currentProject.projectType.name = '';
        this.currentProject.projectType.id = null;
        this.currentProject.projectType.name = event.target.value;
    };
    ProjectDetailsComponent.prototype.getCustomers = function () {
        var _this = this;
        this.customerService.getCustomers().subscribe(function (response) {
            _this.customers = response;
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    ProjectDetailsComponent.prototype.activateEditProject = function () {
        this.activeEditProject = true;
    };
    ProjectDetailsComponent.prototype.getProjectAttachments = function (id) {
        var _this = this;
        this.projectAttachmentService.getAllProjectsAttachmentsByProjectId(id).subscribe(function (response) {
            _this.projectAttachments = response;
            _this.showSpinner = false;
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    ProjectDetailsComponent.prototype.showProjectDetailsById = function (id) {
        var _this = this;
        this.projectService.getProjectById(id).subscribe(function (response) {
            _this.currentProject = response;
            if (_this.currentProject.name && _this.currentProject.name.length > 40) {
                _this.currentProject.name = _this.currentProject.name.substring(0, 40) + "...";
            }
            _this.activeEditProject = false;
            _this.loadService.set(false);
        });
        this.getProjectAttachments(id);
    };
    ProjectDetailsComponent.prototype.canceltProjectChange = function () {
        this.showProjectDetailsById(this.currentProject.id);
    };
    ProjectDetailsComponent.prototype.saveProjectChange = function () {
        var _this = this;
        if (!this.currentProject.name) {
            this.currentProject.name = "";
        }
        this.updateProject = this.currentProject;
        if (this.updateDate != null) {
            this.updateProject.date = this.updateDate;
        }
        if (this.updateDate == null) {
            this.updateProject.date = this.currentProject.date;
        }
        this.projectService.updateProject(this.updateProject).subscribe(function (response) {
            _this.showProjectDetailsById(_this.currentProject.id);
            _this.notificationService.showSuccess(_this.constans.projectUpdateMessage);
            _this.updateProjectEvent.emit(_this.currentProject.id);
        }),
            function (error) {
                _this.notificationService.showError(error.error);
            };
    };
    //attachments
    ProjectDetailsComponent.prototype.openShowAttachment = function (id) {
        var _this = this;
        this.projectAttachmentService.getProjectAttachment(id).subscribe(function (response) {
            _this.currentProjectAttachment = response;
            if (_this.currentProjectAttachment.fileName.length > 35) {
                _this.currentProjectAttachment.fileName = _this.currentProjectAttachment.fileName.substring(0, 35) + "...";
            }
            _this.constans.allowableImageExtensions.forEach(function (x) {
                if (x === _this.currentProjectAttachment.fileExtantion) {
                    _this.showAttachmentImg = true;
                }
            });
            if (_this.currentProjectAttachment.fileExtantion === _this.constans.attacmentsData.pdf.extension) {
                _this.projectAttachmentService.downloadAttachmentToArray(id).subscribe(function (response) {
                    _this.attachmentPdf = window.URL.createObjectURL(response);
                });
                _this.showAttachedPdf = true;
            }
            if (_this.currentProjectAttachment.fileExtantion === _this.constans.attacmentsData.text.extension) {
                _this.projectAttachmentService.downloadAttachment(id).subscribe(function (data) {
                    _this.attachmentTxtText = data;
                });
                _this.showAttachedTxt = true;
            }
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    ProjectDetailsComponent.prototype.closeShowAttachment = function () {
        this.showAttachedTxt = false;
        this.showAttachment = false;
        this.showAttachmentImg = false;
        this.showAttachedPdf = false;
        this.currentProjectAttachment = new app_shared_models_project_attachment_get_project_attachment_view__WEBPACK_IMPORTED_MODULE_20__["GetProjectAttachmentView"]();
        this.currentPdfLink = "";
        this.attachmentTxtText = "";
    };
    ProjectDetailsComponent.prototype.openDeleteAttachment = function (id) {
        this.deletedAttachmentId = id;
        this.openDeleteAttachmentModal = true;
    };
    ProjectDetailsComponent.prototype.cancelDeleteAttachment = function () {
        this.deletedAttachmentId = null;
        this.openDeleteAttachmentModal = false;
    };
    ProjectDetailsComponent.prototype.yesDeleteAttachment = function () {
        this.deleteAttachment();
        this.openDeleteAttachmentModal = false;
    };
    ProjectDetailsComponent.prototype.openSaveAttachmentModal = function () {
        this.openSaveAttachment = true;
        this.formValidation = false;
        this.droppedFiles = [];
    };
    ProjectDetailsComponent.prototype.closeSaveAttachmentModal = function () {
        this.openSaveAttachment = false;
        this.formValidation = false;
    };
    ProjectDetailsComponent.prototype.checkAttachmentExtensions = function (files) {
        var invalidFiles = [];
        for (var i = 0; i < files.length; i++) {
            this.showSpinner = true;
            var success = false;
            this.constans.allowableExtensions.forEach(function (ex) {
                var index = files[i].name.lastIndexOf(".");
                var extensions = files[i].name.substr(index + 1);
                if (ex == extensions) {
                    success = true;
                    return;
                }
            });
            if (!success) {
                invalidFiles.push(files[i].name);
                this.showSpinner = false;
            }
            if (success) {
                this.saveAttachments(files[i]);
            }
        }
        ;
        return invalidFiles;
    };
    ProjectDetailsComponent.prototype.dropped = function (fileList) {
        var invalidFiles = this.checkAttachmentExtensions(fileList);
        if (invalidFiles.length > 0) {
            this.notificationService.showWarning(this.constans.fileExtensionNotAllowedMessage);
        }
    };
    ProjectDetailsComponent.prototype.inputAdd = function (event) {
        var fileList = event.target.files;
        var invalidFiles = this.checkAttachmentExtensions(fileList);
        if (invalidFiles.length > 0) {
            this.notificationService.showWarning(this.constans.fileExtensionNotAllowedMessage);
        }
    };
    ProjectDetailsComponent.prototype.fileLeave = function (file) {
        var index = this.droppedFiles.indexOf(file);
        if (index > -1) {
            this.droppedFiles.splice(index, 1);
        }
    };
    ProjectDetailsComponent.prototype.fileOver = function (event) { };
    ProjectDetailsComponent.prototype.fileOverBase = function (event) {
        this.hasBaseDropZoneOver = event;
    };
    ProjectDetailsComponent.prototype.saveAttachments = function (file) {
        var _this = this;
        var userId = "9c4989af-de13-4852-9fa8-d5a40d851a75";
        var formData = new FormData();
        formData.append("file", file, file.name);
        formData.append("userId", userId);
        formData.append("projectId", this.currentProject.id);
        this.projectAttachmentService.createProjectAttachment(formData).subscribe(function (response) {
            _this.notificationService.showSuccess(_this.constans.fileUploadeMessage + " " + file.name);
            _this.inputFileElement.nativeElement.value = "";
            _this.getProjectAttachments(_this.currentProject.id);
        });
        this.openSaveAttachment = false;
        this.formValidation = true;
    };
    //download files
    ProjectDetailsComponent.prototype.downloadProjectAttachment = function (id) {
        var _this = this;
        this.projectAttachmentService.getProjectAttachment(id).subscribe(function (response) {
            _this.currentProjectAttachment = response;
            _this.projectAttachmentService.downloadAttachmentToBlob(id).subscribe(function (result) {
                var file = result;
                var filename = _this.currentProjectAttachment.fileName + _this.currentProjectAttachment.fileExtantion;
                if (_this.currentProjectAttachment.fileExtantion === _this.constans.attacmentsData.pdf.extension) {
                    var blob = new Blob([file], { type: _this.constans.attacmentsData.pdf.type });
                    file_saver__WEBPACK_IMPORTED_MODULE_1___default()(blob, filename);
                }
                if (_this.currentProjectAttachment.fileExtantion === _this.constans.attacmentsData.imageJpeg.extension) {
                    var blob = new Blob([file], { type: _this.constans.attacmentsData.imageJpeg.type });
                    file_saver__WEBPACK_IMPORTED_MODULE_1___default()(blob, filename);
                }
                if (_this.currentProjectAttachment.fileExtantion === _this.constans.attacmentsData.imagePng.extension) {
                    var blob = new Blob([file], { type: _this.constans.attacmentsData.imagePng.type });
                    file_saver__WEBPACK_IMPORTED_MODULE_1___default()(blob, filename);
                }
                if (_this.currentProjectAttachment.fileExtantion === _this.constans.attacmentsData.text.extension) {
                    var blob = new Blob([file], { type: _this.constans.attacmentsData.text.type });
                    file_saver__WEBPACK_IMPORTED_MODULE_1___default()(blob, filename);
                }
                if (_this.currentProjectAttachment.fileExtantion === _this.constans.attacmentsData.docx.extension) {
                    var blob = new Blob([file], { type: _this.constans.attacmentsData.docx.type });
                    file_saver__WEBPACK_IMPORTED_MODULE_1___default()(blob, filename);
                }
                if (_this.currentProjectAttachment.fileExtantion === _this.constans.attacmentsData.xlsx.extension) {
                    var blob = new Blob([file], { type: _this.constans.attacmentsData.xlsx.type });
                    file_saver__WEBPACK_IMPORTED_MODULE_1___default()(blob, filename);
                }
                if (_this.currentProjectAttachment.fileExtantion === _this.constans.attacmentsData.msg.extension) {
                    var blob = new Blob([file], { type: _this.constans.attacmentsData.msg.type });
                    file_saver__WEBPACK_IMPORTED_MODULE_1___default()(blob, filename);
                }
            });
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    ProjectDetailsComponent.prototype.downloadAllProjectAttachment = function (id) {
        var _this = this;
        this.projectAttachmentService.downloadAllProjectsAttachment(id).subscribe(function (response) {
            var zip = response;
            var filename = _this.constans.attacmentsData.zip.filename;
            var blob = new Blob([zip], { type: _this.constans.attacmentsData.zip.type });
            file_saver__WEBPACK_IMPORTED_MODULE_1___default()(blob, filename);
        });
    };
    ProjectDetailsComponent.prototype.downloadFromBlob = function (link) {
        window.open(link);
    };
    ProjectDetailsComponent.prototype.deleteAttachment = function () {
        var _this = this;
        this.deletedAttachment.id = this.deletedAttachmentId;
        this.projectAttachmentService.deleteAttachment(this.deletedAttachment).subscribe(function (response) {
            _this.notificationService.showSuccess(_this.constans.atachmentDeleteMessage);
            _this.getProjectAttachments(_this.projectAttachments.projectId);
        });
        this.deletedAttachmentId = null;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('projectId'),
        __metadata("design:type", String)
    ], ProjectDetailsComponent.prototype, "projectId", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"])
    ], ProjectDetailsComponent.prototype, "closeDetailsComponent", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"])
    ], ProjectDetailsComponent.prototype, "updateProjectEvent", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('fileUploader'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], ProjectDetailsComponent.prototype, "inputFileElement", void 0);
    ProjectDetailsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-project-details',
            template: __webpack_require__(/*! ./project-details.component.html */ "./src/app/projects-and-orders/project-and-orders/new-projects/new-projects/project-details/project-details.component.html"),
            styles: [__webpack_require__(/*! ./project-details.component.scss */ "./src/app/projects-and-orders/project-and-orders/new-projects/new-projects/project-details/project-details.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            app_shared_providers_services_noitification_service__WEBPACK_IMPORTED_MODULE_8__["NotificationService"],
            app_shared_providers_services_load_service__WEBPACK_IMPORTED_MODULE_10__["LoadService"],
            app_shared_providers_services_customer_service__WEBPACK_IMPORTED_MODULE_7__["CustomerService"],
            app_shared_providers_services_project_attachment_service__WEBPACK_IMPORTED_MODULE_9__["ProjectAttachmentservice"],
            app_shared_constans_projects_and_orders_constans__WEBPACK_IMPORTED_MODULE_5__["ProjectsAndOrdersConstans"],
            app_shared_providers_services_project_service__WEBPACK_IMPORTED_MODULE_6__["ProjectService"],
            app_shared_providers_services_order_service__WEBPACK_IMPORTED_MODULE_11__["OrderService"]])
    ], ProjectDetailsComponent);
    return ProjectDetailsComponent;
}());



/***/ }),

/***/ "./src/app/shared/models/project/get-with-opened-orders.view.ts":
/*!**********************************************************************!*\
  !*** ./src/app/shared/models/project/get-with-opened-orders.view.ts ***!
  \**********************************************************************/
/*! exports provided: GetWithOpenedOrdersProjectView, GetWithOpenedOrdersProjectViewItem, GetWithOpenedOrdersProjectProjectTypeViewItem, GetWithOpenedOrdersProjectUserViewItem, GetWithOpenedOrdersProjectCustomerViewItem */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetWithOpenedOrdersProjectView", function() { return GetWithOpenedOrdersProjectView; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetWithOpenedOrdersProjectViewItem", function() { return GetWithOpenedOrdersProjectViewItem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetWithOpenedOrdersProjectProjectTypeViewItem", function() { return GetWithOpenedOrdersProjectProjectTypeViewItem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetWithOpenedOrdersProjectUserViewItem", function() { return GetWithOpenedOrdersProjectUserViewItem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetWithOpenedOrdersProjectCustomerViewItem", function() { return GetWithOpenedOrdersProjectCustomerViewItem; });
var GetWithOpenedOrdersProjectView = /** @class */ (function () {
    function GetWithOpenedOrdersProjectView() {
        this.projects = [];
    }
    return GetWithOpenedOrdersProjectView;
}());

var GetWithOpenedOrdersProjectViewItem = /** @class */ (function () {
    function GetWithOpenedOrdersProjectViewItem() {
    }
    return GetWithOpenedOrdersProjectViewItem;
}());

var GetWithOpenedOrdersProjectProjectTypeViewItem = /** @class */ (function () {
    function GetWithOpenedOrdersProjectProjectTypeViewItem() {
    }
    return GetWithOpenedOrdersProjectProjectTypeViewItem;
}());

var GetWithOpenedOrdersProjectUserViewItem = /** @class */ (function () {
    function GetWithOpenedOrdersProjectUserViewItem() {
    }
    return GetWithOpenedOrdersProjectUserViewItem;
}());

var GetWithOpenedOrdersProjectCustomerViewItem = /** @class */ (function () {
    function GetWithOpenedOrdersProjectCustomerViewItem() {
    }
    return GetWithOpenedOrdersProjectCustomerViewItem;
}());



/***/ })

}]);
//# sourceMappingURL=project-and-orders-new-projects-new-projects-module.js.map