"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var projects_and_workflows_component_1 = require("./project-and-workflows/projects-and-workflows.component");
var projects_component_1 = require("./project-and-workflows/projects/projects.component");
var tasks_component_1 = require("./project-and-workflows/tasks/tasks.component");
var project_details_component_1 = require("./project-and-workflows/projects/project-details/project-details.component");
var projects_list_component_1 = require("./project-and-workflows/projects/projects-list/projects-list.component");
var create_project_component_1 = require("./project-and-workflows/projects/create-project/create-project.component");
var orders_component_1 = require("./project-and-workflows/orders/orders.component");
var order_list_component_1 = require("./project-and-workflows/orders/order-list/order-list.component");
var create_order_component_1 = require("./project-and-workflows/orders/create-order/create-order.component");
exports.routes = [
    {
        path: '',
        component: projects_and_workflows_component_1.ProjectsAndWorkflowsComponent,
        data: { title: 'Projects&Workflows' },
        children: [
            {
                path: '',
                redirectTo: 'projects',
                pathMatch: 'full'
            },
            {
                path: 'projects',
                component: projects_component_1.ProjectsComponent,
                children: [
                    {
                        path: '',
                        redirectTo: 'projectslist',
                        pathMatch: 'full',
                    },
                    {
                        path: 'projectslist',
                        component: projects_list_component_1.ProjectsListComponent
                    },
                    {
                        path: 'projectdetails/:id',
                        component: project_details_component_1.ProjectDetailsComponent
                    },
                    {
                        path: 'createproject',
                        component: create_project_component_1.CreateProjectComponent
                    }
                ]
            },
            {
                path: 'orders',
                component: orders_component_1.OrdersComponent,
                children: [
                    {
                        path: '',
                        redirectTo: 'ordersList',
                        pathMatch: 'full',
                    },
                    {
                        path: 'ordersList',
                        component: order_list_component_1.OrderListComponent
                    },
                    {
                        path: 'createOrder',
                        component: create_order_component_1.CreateOrderComponent
                    }
                ]
            },
            {
                path: 'tasks',
                component: tasks_component_1.TasksComponent
            }
        ]
    }
];
var ProjectsAndWorkflowsRoutingModule = /** @class */ (function () {
    function ProjectsAndWorkflowsRoutingModule() {
    }
    ProjectsAndWorkflowsRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forChild(exports.routes)],
            exports: [router_1.RouterModule]
        })
    ], ProjectsAndWorkflowsRoutingModule);
    return ProjectsAndWorkflowsRoutingModule;
}());
exports.ProjectsAndWorkflowsRoutingModule = ProjectsAndWorkflowsRoutingModule;
//# sourceMappingURL=projects-and-workflows-routing.module.js.map