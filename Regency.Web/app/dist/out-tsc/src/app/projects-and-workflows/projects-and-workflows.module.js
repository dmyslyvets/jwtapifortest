"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var project_data_filter_pipe_1 = require("app/shared/filters/project-data-filter.pipe");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var angular_6_datatable_1 = require("angular-6-datatable");
var datepicker_1 = require("ngx-bootstrap/datepicker");
var ng_busy_1 = require("ng-busy");
var ngx_uploader_1 = require("ngx-uploader");
var ng2_file_upload_1 = require("ng2-file-upload");
var ng_multiselect_dropdown_1 = require("ng-multiselect-dropdown");
var projects_and_workflows_routing_module_1 = require("./projects-and-workflows-routing.module");
var slide_toggle_1 = require("@angular/material/slide-toggle");
var ng2_pdf_viewer_1 = require("ng2-pdf-viewer");
var kendo_angular_grid_1 = require("@progress/kendo-angular-grid");
var material_module_1 = require("app/shared/modules/material.module");
var table_1 = require("@angular/material/table");
var material_1 = require("@angular/material");
var devextreme_angular_1 = require("devextreme-angular");
var drag_drop_1 = require("@angular/cdk/drag-drop");
var grid_list_1 = require("@angular/material/grid-list");
var projects_and_workflows_component_1 = require("./project-and-workflows/projects-and-workflows.component");
var projects_component_1 = require("./project-and-workflows/projects/projects.component");
var projects_list_component_1 = require("./project-and-workflows/projects/projects-list/projects-list.component");
var project_details_component_1 = require("./project-and-workflows/projects/project-details/project-details.component");
var create_project_component_1 = require("./project-and-workflows/projects/create-project/create-project.component");
var orders_component_1 = require("./project-and-workflows/orders/orders.component");
var order_list_component_1 = require("./project-and-workflows/orders/order-list/order-list.component");
var create_order_component_1 = require("./project-and-workflows/orders/create-order/create-order.component");
var tasks_component_1 = require("./project-and-workflows/tasks/tasks.component");
var ProjectsAndWorkflowsModule = /** @class */ (function () {
    function ProjectsAndWorkflowsModule() {
    }
    ProjectsAndWorkflowsModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                devextreme_angular_1.DxDataGridModule,
                devextreme_angular_1.DxSelectBoxModule,
                devextreme_angular_1.DxCheckBoxModule,
                material_module_1.MaterialModule,
                angular_6_datatable_1.DataTableModule,
                ng2_file_upload_1.FileUploadModule,
                forms_1.FormsModule,
                kendo_angular_grid_1.GridModule,
                table_1.MatTableModule,
                material_1.MatPaginatorModule,
                slide_toggle_1.MatSlideToggleModule,
                forms_1.ReactiveFormsModule,
                projects_and_workflows_routing_module_1.ProjectsAndWorkflowsRoutingModule,
                ng_multiselect_dropdown_1.NgMultiSelectDropDownModule.forRoot(),
                projects_and_workflows_routing_module_1.ProjectsAndWorkflowsRoutingModule,
                datepicker_1.BsDatepickerModule.forRoot(),
                ng_busy_1.NgBusyModule,
                ngx_uploader_1.NgxUploaderModule,
                ng2_pdf_viewer_1.PdfViewerModule,
                material_1.MatSortModule,
                drag_drop_1.DragDropModule,
                grid_list_1.MatGridListModule
            ],
            declarations: [
                project_data_filter_pipe_1.ProjectDataFilter,
                projects_and_workflows_component_1.ProjectsAndWorkflowsComponent,
                projects_component_1.ProjectsComponent,
                projects_list_component_1.ProjectsListComponent,
                project_details_component_1.ProjectDetailsComponent,
                create_project_component_1.CreateProjectComponent,
                orders_component_1.OrdersComponent,
                order_list_component_1.OrderListComponent,
                create_order_component_1.CreateOrderComponent,
                tasks_component_1.TasksComponent
            ]
        })
    ], ProjectsAndWorkflowsModule);
    return ProjectsAndWorkflowsModule;
}());
exports.ProjectsAndWorkflowsModule = ProjectsAndWorkflowsModule;
//# sourceMappingURL=projects-and-workflows.module.js.map