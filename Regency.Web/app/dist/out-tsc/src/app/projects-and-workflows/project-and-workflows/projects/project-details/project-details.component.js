"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var ng2_file_upload_1 = require("ng2-file-upload");
var file_saver_1 = require("file-saver");
var project_service_1 = require("app/shared/providers/services/project.service");
var customer_service_1 = require("app/shared/providers/services/customer.service");
var noitification_service_1 = require("app/shared/providers/services/noitification.service");
var project_attachment_service_1 = require("app/shared/providers/services/project-attachment.service");
var get_all_users_view_1 = require("app/shared/models/users/get-all-users.view");
var get_project_view_1 = require("app/shared/models/project/get-project.view");
var get_all_customer_view_1 = require("app/shared/models/customer/get-all-customer.view");
var update_project_view_1 = require("app/shared/models/project/update-project.view");
var get_all_by_project_id_projects_attachments_view_1 = require("app/shared/models/project-attachment/get-all-by-project-id-projects-attachments-view");
var get_project_attachment_view_1 = require("app/shared/models/project-attachment/get-project-attachment.view");
var delete_project_attachment_view_1 = require("app/shared/models/project-attachment/delete-project-attachment.view");
var projectUpdateMessage = "Project is update";
var fileUploadeMessage = "File is upload";
var allowableExtensions = [
    "pdf",
    "jpg",
    "png",
    "docs",
    "xlsx",
    "txt",
    "msg"
];
var allowableImageExtensions = [
    ".jpg",
    ".png"
];
var URL = " ";
var ProjectDetailsComponent = /** @class */ (function () {
    function ProjectDetailsComponent(notificationService, projectService, customerService, projectAttachmentService, activateRoute, route) {
        this.notificationService = notificationService;
        this.projectService = projectService;
        this.customerService = customerService;
        this.projectAttachmentService = projectAttachmentService;
        this.activateRoute = activateRoute;
        this.route = route;
        //post request
        this.updateProject = new update_project_view_1.UpdateProjectView();
        this.projectManagers = new get_all_users_view_1.GetAllUserView();
        this.currentProject = new get_project_view_1.GetProjectView();
        this.customers = new get_all_customer_view_1.GetAllCustomerView();
        this.projectAttachments = new get_all_by_project_id_projects_attachments_view_1.GetAllByProjectIdProjectAttachmentsView();
        //popups
        this.activeEditProject = false;
        this.formValidation = false;
        this.openSaveAttachment = false;
        this.openDeleteAttachmentModal = false;
        this.droppedFiles = [];
        this.deletedAttachment = new delete_project_attachment_view_1.DeleteProjectAttachmentView();
        this.hasBaseDropZoneOver = false;
        //view attachment
        this.uploader = new ng2_file_upload_1.FileUploader({ url: URL });
        this.showAttachmentImg = false;
        this.showAttachedPdf = false;
        this.showAttachedTxt = false;
        this.showAttachment = false;
        this.currentProjectAttachment = new get_project_attachment_view_1.GetProjectAttachmentView();
        this.projectId = this.activateRoute.snapshot.params['id'];
    }
    ProjectDetailsComponent.prototype.ngOnInit = function () {
        this.getProjectManager();
        this.getCustomers();
        this.showProjectDetailsById(this.projectId);
    };
    //gets
    ProjectDetailsComponent.prototype.getProjectAttachments = function (id) {
        var _this = this;
        this.projectAttachmentService
            .getAllProjectsAttachmentsByProjectId(id)
            .subscribe(function (response) {
            _this.projectAttachments = response;
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    ProjectDetailsComponent.prototype.getCustomers = function () {
        var _this = this;
        this.customerService.getCustomers().subscribe(function (response) {
            _this.customers = response;
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    ProjectDetailsComponent.prototype.getProjectManager = function () {
        var _this = this;
        this.projectService.getProjectManager().subscribe(function (response) {
            _this.projectManagers = response;
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    ProjectDetailsComponent.prototype.openDeleteAttachment = function (id) {
        this.deletedAttachmentId = id;
        this.openDeleteAttachmentModal = true;
    };
    ProjectDetailsComponent.prototype.cancelDeleteAttachment = function () {
        this.deletedAttachmentId = null;
        this.openDeleteAttachmentModal = false;
    };
    ProjectDetailsComponent.prototype.yesDeleteAttachment = function () {
        this.deleteAttachment();
        this.openDeleteAttachmentModal = false;
    };
    ProjectDetailsComponent.prototype.openSaveAttachmentModal = function () {
        this.openSaveAttachment = true;
        this.formValidation = false;
        this.droppedFiles = [];
    };
    ProjectDetailsComponent.prototype.closeSaveAttachmentModal = function () {
        this.openSaveAttachment = false;
        this.formValidation = false;
    };
    ProjectDetailsComponent.prototype.checkAttachmentExtensions = function (files) {
        var invalidFiles = [];
        for (var i = 0; i < files.length; i++) {
            var success = false;
            allowableExtensions.forEach(function (ex) {
                var index = files[i].name.lastIndexOf(".");
                var extensions = files[i].name.substr(index + 1);
                if (ex == extensions) {
                    success = true;
                    return;
                }
            });
            if (!success) {
                invalidFiles.push(files[i].name);
            }
            if (success) {
                this.saveAttachments(files[i]);
            }
        }
        ;
        return invalidFiles;
    };
    ProjectDetailsComponent.prototype.dropped = function (fileList) {
        var invalidFiles = this.checkAttachmentExtensions(fileList);
        if (invalidFiles.length > 0) {
            this.notificationService.showErrors(invalidFiles);
        }
    };
    ProjectDetailsComponent.prototype.inputAdd = function (event) {
        var fileList = event.target.files;
        var invalidFiles = this.checkAttachmentExtensions(fileList);
        if (invalidFiles.length > 0) {
            this.notificationService.showErrors(invalidFiles);
        }
    };
    ProjectDetailsComponent.prototype.fileLeave = function (file) {
        var index = this.droppedFiles.indexOf(file);
        if (index > -1) {
            this.droppedFiles.splice(index, 1);
        }
    };
    ProjectDetailsComponent.prototype.fileOver = function (event) { };
    ProjectDetailsComponent.prototype.fileOverBase = function (event) {
        this.hasBaseDropZoneOver = event;
    };
    ProjectDetailsComponent.prototype.saveAttachments = function (file) {
        var _this = this;
        var formData = new FormData();
        formData.append("file", file, file.name);
        var userId = "9c4989af-de13-4852-9fa8-d5a40d851a75";
        formData.append("userId", userId);
        formData.append("projectId", this.currentProject.id);
        this.projectAttachmentService.createProjectAttachment(formData).subscribe(function (response) {
            if (response.length > 0) {
                _this.notificationService.showErrors(response);
            }
            _this.notificationService.showSuccess(fileUploadeMessage + " " + file.name);
            _this.getProjectAttachments(_this.currentProject.id);
        });
        this.openSaveAttachment = false;
        this.formValidation = true;
    };
    //download files
    ProjectDetailsComponent.prototype.downloadProjectAttachment = function (id) {
        var _this = this;
        this.projectAttachmentService.getProjectAttachment(id).subscribe(function (response) {
            _this.currentProjectAttachment = response;
            _this.projectAttachmentService.downloadAttachmentToBlob(id).subscribe(function (result) {
                var file = result;
                var filename = _this.currentProjectAttachment.fileName + _this.currentProjectAttachment.fileExtantion;
                if (_this.currentProjectAttachment.fileExtantion === ".pdf") {
                    var blob = new Blob([file], { type: 'application/pdf' });
                    file_saver_1.default(blob, filename);
                }
                if (_this.currentProjectAttachment.fileExtantion === ".jpg") {
                    var blob = new Blob([file], { type: 'image/jpeg' });
                    file_saver_1.default(blob, filename);
                }
                if (_this.currentProjectAttachment.fileExtantion === ".png") {
                    var blob = new Blob([file], { type: 'image/png' });
                    file_saver_1.default(blob, filename);
                }
                if (_this.currentProjectAttachment.fileExtantion === ".txt") {
                    var blob = new Blob([file], { type: 'text/txt' });
                    file_saver_1.default(blob, filename);
                }
                if (_this.currentProjectAttachment.fileExtantion === ".docx") {
                    var blob = new Blob([file], { type: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' });
                    file_saver_1.default(blob, filename);
                }
                if (_this.currentProjectAttachment.fileExtantion === ".xlsx") {
                    var blob = new Blob([file], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
                    file_saver_1.default(blob, filename);
                }
                if (_this.currentProjectAttachment.fileExtantion === ".msg") {
                    var blob = new Blob([file], { type: 'application/octet-stream' });
                    file_saver_1.default(blob, filename);
                }
            });
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    ProjectDetailsComponent.prototype.downloadAllProjectAttachment = function (id) {
        this.projectAttachmentService.downloadAllProjectsAttachment(id).subscribe(function (response) {
            var zip = response;
            var filename = 'Attachments.zip';
            var blob = new Blob([zip], { type: 'application/zip' });
            file_saver_1.default(blob, filename);
        });
    };
    ProjectDetailsComponent.prototype.downloadFromBlob = function (link) {
        window.open(link);
    };
    ProjectDetailsComponent.prototype.deleteAttachment = function () {
        var _this = this;
        this.deletedAttachment.id = this.deletedAttachmentId;
        this.projectAttachmentService
            .deleteAttachment(this.deletedAttachment)
            .subscribe(function (response) {
            _this.notificationService.showSuccess("Atachment delete");
            _this.getProjectAttachments(_this.projectAttachments.projectId);
        });
        this.deletedAttachmentId = null;
    };
    //back on page
    ProjectDetailsComponent.prototype.showingProjectsList = function () {
        this.route.navigate(['projects-and-workflows/projects/projectslist']);
    };
    ProjectDetailsComponent.prototype.showProjectDetailsById = function (id) {
        var _this = this;
        this.busyGetProjectById = this.projectService
            .getProjectById(id)
            .subscribe(function (response) {
            _this.currentProject = response;
            if (_this.currentProject.name.length > 40) {
                _this.currentProject.name = _this.currentProject.name.substring(0, 40) + "...";
            }
            _this.activeEditProject = false;
        });
        this.getProjectAttachments(id);
    };
    //update project
    ProjectDetailsComponent.prototype.activateEditProject = function () {
        this.activeEditProject = true;
    };
    ProjectDetailsComponent.prototype.canceltProjectChange = function () {
        this.showProjectDetailsById(this.currentProject.id);
    };
    ProjectDetailsComponent.prototype.saveProjectChange = function () {
        var _this = this;
        if (!this.currentProject.name) {
            this.notificationService.showError("Can't save project without name");
        }
        if (this.currentProject.name) {
            this.updateProject = this.currentProject;
            if (this.updateDate != null) {
                this.updateProject.date = this.updateDate;
            }
            if (this.updateDate == null) {
                this.updateProject.date = this.currentProject.date;
            }
            this.projectService
                .updateProject(this.updateProject)
                .subscribe(function (response) {
                _this.showProjectDetailsById(_this.currentProject.id);
                _this.notificationService.showSuccess(projectUpdateMessage);
            }),
                function (error) {
                    _this.notificationService.showError(error.error);
                };
        }
    };
    ProjectDetailsComponent.prototype.openShowAttachment = function (id) {
        var _this = this;
        this.projectAttachmentService.getProjectAttachment(id).subscribe(function (response) {
            _this.currentProjectAttachment = response;
            if (_this.currentProjectAttachment.fileName.length > 35) {
                _this.currentProjectAttachment.fileName = _this.currentProjectAttachment.fileName.substring(0, 35) + "...";
            }
            allowableImageExtensions.forEach(function (x) {
                if (x === _this.currentProjectAttachment.fileExtantion) {
                    _this.showAttachmentImg = true;
                }
            });
            if (_this.currentProjectAttachment.fileExtantion === ".pdf") {
                //this.currentPdfLink = environment.apiUrl + "api/ProjectAttachment/DownloadAttachment?id=" + this.currentProjectAttachment.id;
                _this.projectAttachmentService.downloadAttachmentToArray(id).subscribe(function (response) {
                    _this.attachmentPdf = window.URL.createObjectURL(response);
                    // this.attachmentPdf = response;
                    // let blob = new Blob([pdf], { type: 'application/pdf' });
                });
                _this.showAttachedPdf = true;
            }
            if (_this.currentProjectAttachment.fileExtantion === ".txt") {
                _this.projectAttachmentService.downloadAttachment(id).subscribe(function (data) { _this.attachmentTxtText = data; });
                _this.showAttachedTxt = true;
            }
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    ProjectDetailsComponent.prototype.closeShowAttachment = function () {
        this.showAttachedTxt = false;
        this.showAttachment = false;
        this.showAttachmentImg = false;
        this.showAttachedPdf = false;
        this.currentProjectAttachment = new get_project_attachment_view_1.GetProjectAttachmentView();
        this.currentPdfLink = "";
        this.attachmentTxtText = "";
    };
    ProjectDetailsComponent = __decorate([
        core_1.Component({
            selector: 'app-project-details',
            templateUrl: './project-details.component.html',
            styleUrls: ['./project-details.component.scss']
        }),
        __metadata("design:paramtypes", [noitification_service_1.NotificationService,
            project_service_1.ProjectService,
            customer_service_1.CustomerService,
            project_attachment_service_1.ProjectAttachmentservice,
            router_1.ActivatedRoute,
            router_1.Router])
    ], ProjectDetailsComponent);
    return ProjectDetailsComponent;
}());
exports.ProjectDetailsComponent = ProjectDetailsComponent;
//# sourceMappingURL=project-details.component.js.map