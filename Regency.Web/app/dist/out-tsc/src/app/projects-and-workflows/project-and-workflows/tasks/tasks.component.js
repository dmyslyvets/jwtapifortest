"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var devextreme_angular_1 = require("devextreme-angular");
var project_service_1 = require("app/shared/providers/services/project.service");
var reassign_project_manager_view_1 = require("app/shared/models/project/reassign-project-manager.view");
var get_all_users_view_1 = require("app/shared/models/users/get-all-users.view");
var noitification_service_1 = require("app/shared/providers/services/noitification.service");
var TasksComponent = /** @class */ (function () {
    function TasksComponent(projectService, notificationService, route) {
        this.projectService = projectService;
        this.notificationService = notificationService;
        this.route = route;
        this.formValidation = false;
        this.openReassignPM = false;
        this.reassignProjectManager = new reassign_project_manager_view_1.ReassignProjectManagerView();
        this.projectManagers = new get_all_users_view_1.GetAllUserView();
        this.showFilterRow = true;
        this.showHeaderFilter = true;
        this.currentFilter = "auto";
    }
    TasksComponent.prototype.ngOnInit = function () {
        this.getAllActiveProjects();
        this.getProjectManager();
    };
    TasksComponent.prototype.getProjectManager = function () {
        var _this = this;
        this.projectService.getProjectManager().subscribe(function (response) {
            _this.projectManagers = response;
        }, function (error) {
            console.log(error.error);
        });
    };
    TasksComponent.prototype.getAllActiveProjects = function () {
        var _this = this;
        this.projectService.getAllActivePorjects().subscribe(function (response) {
            _this.projects = response.projects;
            _this.dataGrid.dataSource = _this.projects;
        });
    };
    TasksComponent.prototype.openReassignPMDialog = function (projectId) {
        this.formValidation = false;
        this.reassignProjectManager = new reassign_project_manager_view_1.ReassignProjectManagerView();
        this.reassignProjectManager.projectId = projectId;
        this.openReassignPM = true;
    };
    TasksComponent.prototype.closeReassignPMDialog = function () {
        this.reassignProjectManager = new reassign_project_manager_view_1.ReassignProjectManagerView();
        this.openReassignPM = false;
    };
    TasksComponent.prototype.saveReasignPM = function () {
        var _this = this;
        this.projectService.reassignProjectManeger(this.reassignProjectManager).subscribe(function (response) {
            _this.getAllActiveProjects();
            _this.notificationService.showSuccess("Pm has changed");
            _this.openReassignPM = false;
        });
    };
    TasksComponent.prototype.showProjectDetailsById = function (event) {
        if (this.openReassignPM != true)
            this.route.navigate(['projects-and-workflows/projects/projectdetails', event.data.id]);
    };
    TasksComponent.prototype.clearFilter = function () {
        this.dataGrid.instance.clearFilter();
    };
    __decorate([
        core_1.ViewChild(devextreme_angular_1.DxDataGridComponent),
        __metadata("design:type", devextreme_angular_1.DxDataGridComponent)
    ], TasksComponent.prototype, "dataGrid", void 0);
    TasksComponent = __decorate([
        core_1.Component({
            selector: 'tasks',
            templateUrl: './tasks.component.html',
            styleUrls: ['./tasks.component.scss']
        }),
        __metadata("design:paramtypes", [project_service_1.ProjectService, noitification_service_1.NotificationService, router_1.Router])
    ], TasksComponent);
    return TasksComponent;
}());
exports.TasksComponent = TasksComponent;
//# sourceMappingURL=tasks.component.js.map