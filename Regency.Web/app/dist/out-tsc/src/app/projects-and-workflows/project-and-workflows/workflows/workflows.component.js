"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var noitification_service_1 = require("app/shared/providers/services/noitification.service");
var project_service_1 = require("app/shared/providers/services/project.service");
var reassign_project_manager_view_1 = require("app/shared/models/project/reassign-project-manager.view");
var get_all_users_view_1 = require("app/shared/models/users/get-all-users.view");
var router_1 = require("@angular/router");
var material_1 = require("@angular/material");
var WorkflowsComponent = /** @class */ (function () {
    function WorkflowsComponent(notificationService, projectService, route) {
        this.notificationService = notificationService;
        this.projectService = projectService;
        this.route = route;
        this.displayedColumns = ['customer', 'name', 'description', 'date', 'manager', 'status'];
        this.formValidation = false;
        this.openReassignPM = false;
        this.reassignProjectManager = new reassign_project_manager_view_1.ReassignProjectManagerView();
        this.projectManagers = new get_all_users_view_1.GetAllUserView();
        this.state = {
            skip: 0,
            take: 10,
            // Initial filter descriptor
            filter: {
                logic: 'and',
                filters: []
            }
        };
        this.dataSource = new material_1.MatTableDataSource();
        this.getAllActiveProjects();
        this.getProjectManager();
    }
    WorkflowsComponent.prototype.ngOnInit = function () {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    };
    WorkflowsComponent.prototype.getAllActiveProjects = function () {
        var _this = this;
        this.busyGetAllProject = this.projectService
            .getAllActivePorjects()
            .subscribe(function (respone) {
            _this.activeProjects = respone.projects;
            _this.dataSource = new material_1.MatTableDataSource(_this.activeProjects);
        });
    };
    WorkflowsComponent.prototype.openReassignPMDialog = function (projectId) {
        this.formValidation = false;
        this.reassignProjectManager = new reassign_project_manager_view_1.ReassignProjectManagerView();
        this.reassignProjectManager.projectId = projectId;
        this.openReassignPM = true;
    };
    WorkflowsComponent.prototype.closeReassignPMDialog = function () {
        this.reassignProjectManager = new reassign_project_manager_view_1.ReassignProjectManagerView();
        this.openReassignPM = false;
    };
    WorkflowsComponent.prototype.saveReasignPM = function () {
        var _this = this;
        this.openReassignPM = false;
        this.projectService.reassignProjectManeger(this.reassignProjectManager).subscribe(function (response) {
            _this.getAllActiveProjects();
            _this.notificationService.showSuccess("Pm has changed");
        });
    };
    WorkflowsComponent.prototype.getProjectManager = function () {
        var _this = this;
        this.projectService.getProjectManager().subscribe(function (response) {
            _this.projectManagers = response;
        }, function (error) {
            console.log(error.error);
        });
    };
    WorkflowsComponent.prototype.showProjectDetailsById = function (element) {
        this.route.navigate(['projects-and-workflows/projects/projectdetails', element.id]);
    };
    __decorate([
        core_1.ViewChild(material_1.MatPaginator),
        __metadata("design:type", material_1.MatPaginator)
    ], WorkflowsComponent.prototype, "paginator", void 0);
    __decorate([
        core_1.ViewChild(material_1.MatSort),
        __metadata("design:type", material_1.MatSort)
    ], WorkflowsComponent.prototype, "sort", void 0);
    WorkflowsComponent = __decorate([
        core_1.Component({
            selector: 'workflows',
            templateUrl: './workflows.component.html',
            styleUrls: ['./workflows.component.scss']
        }),
        __metadata("design:paramtypes", [noitification_service_1.NotificationService,
            project_service_1.ProjectService,
            router_1.Router])
    ], WorkflowsComponent);
    return WorkflowsComponent;
}());
exports.WorkflowsComponent = WorkflowsComponent;
//# sourceMappingURL=workflows.component.js.map