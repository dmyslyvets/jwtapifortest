"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var projectLabel = "Projects";
var tasksLabel = "Tasks";
var ordersLabel = "Orders";
var ProjectsAndWorkflowsComponent = /** @class */ (function () {
    function ProjectsAndWorkflowsComponent(router) {
        this.router = router;
    }
    ProjectsAndWorkflowsComponent.prototype.navigate = function (event) {
        if (event.tab.textLabel == projectLabel) {
            this.router.navigate(['projects-and-workflows', 'projects']);
            return;
        }
        if (event.tab.textLabel == tasksLabel) {
            this.router.navigate(['projects-and-workflows', 'tasks']);
            return;
        }
        if (event.tab.textLabel == ordersLabel) {
            this.router.navigate(['projects-and-workflows', 'orders']);
            return;
        }
    };
    ProjectsAndWorkflowsComponent = __decorate([
        core_1.Component({
            selector: 'projects-and-workflows',
            templateUrl: './projects-and-workflows.component.html',
            styleUrls: ['./projects-and-workflows.component.scss']
        }),
        __metadata("design:paramtypes", [router_1.Router])
    ], ProjectsAndWorkflowsComponent);
    return ProjectsAndWorkflowsComponent;
}());
exports.ProjectsAndWorkflowsComponent = ProjectsAndWorkflowsComponent;
//# sourceMappingURL=projects-and-workflows.component.js.map