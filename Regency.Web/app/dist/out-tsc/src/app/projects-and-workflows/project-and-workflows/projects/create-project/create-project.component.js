"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var ng2_file_upload_1 = require("ng2-file-upload");
var project_service_1 = require("app/shared/providers/services/project.service");
var customer_service_1 = require("app/shared/providers/services/customer.service");
var noitification_service_1 = require("app/shared/providers/services/noitification.service");
var project_attachment_service_1 = require("app/shared/providers/services/project-attachment.service");
var get_all_users_view_1 = require("app/shared/models/users/get-all-users.view");
var get_all_customer_view_1 = require("app/shared/models/customer/get-all-customer.view");
var create_project_view_1 = require("app/shared/models/project/create-project.view");
var projectUpdateMessage = "Project is update";
var fileUploadeMessage = "File is upload";
var allowableExtensions = [
    "pdf",
    "jpg",
    "png",
    "docs",
    "xlsx",
    "txt",
    "msg"
];
var allowableImageExtensions = [
    ".jpg",
    ".png"
];
var URL = " ";
var CreateProjectComponent = /** @class */ (function () {
    function CreateProjectComponent(notificationService, projectService, customerService, projectAttachmentService, activateRoute, route) {
        this.notificationService = notificationService;
        this.projectService = projectService;
        this.customerService = customerService;
        this.projectAttachmentService = projectAttachmentService;
        this.activateRoute = activateRoute;
        this.route = route;
        //post request
        this.createProject = new create_project_view_1.CreateProjectView();
        this.projectManagers = new get_all_users_view_1.GetAllUserView();
        this.customers = new get_all_customer_view_1.GetAllCustomerView();
        //popups
        this.activeEditProject = false;
        this.formValidation = false;
        this.openSaveAttachment = false;
        this.openDeleteAttachmentModal = false;
        this.droppedFiles = [];
        this.hasBaseDropZoneOver = false;
        //view attachment
        this.uploader = new ng2_file_upload_1.FileUploader({ url: URL });
        this.showAttachmentImg = false;
        this.showAttachedPdf = false;
        this.showAttachedTxt = false;
        this.showAttachment = false;
        this.createProjectFileList = [];
    }
    CreateProjectComponent.prototype.ngOnInit = function () {
        this.getProjectManager();
        this.getCustomers();
    };
    //gets
    CreateProjectComponent.prototype.getCustomers = function () {
        var _this = this;
        this.customerService.getCustomers().subscribe(function (response) {
            _this.customers = response;
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    CreateProjectComponent.prototype.getProjectManager = function () {
        var _this = this;
        this.projectService.getProjectManager().subscribe(function (response) {
            _this.projectManagers = response;
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    //back on page
    CreateProjectComponent.prototype.showingProjectsList = function () {
        this.route.navigate(['projects-and-workflows/projects/projectslist']);
    };
    //update project
    CreateProjectComponent.prototype.activateEditProject = function () {
        this.activeEditProject = true;
    };
    CreateProjectComponent.prototype.saveProjectChange = function () {
        var _this = this;
        if (!this.createProject.name) {
            this.notificationService.showError("Can't create project without name");
        }
        if (!this.createProject.userId) {
            this.notificationService.showError("Can't create project without project manager");
        }
        if (!this.createProject.customerId) {
            this.notificationService.showError("Can't create project without customer");
        }
        if (this.createProject.name && this.createProject.customerId && this.createProject.userId) {
            if (this.updateDate != null) {
                this.createProject.date = this.updateDate;
            }
            if (this.updateDate == null) {
                this.createProject.date = this.createProject.date;
            }
            var invalidFiles = [];
            var formData = new FormData();
            for (var i = 0; i < this.createProjectFileList.length; i++) {
                var success = false;
                allowableExtensions.forEach(function (ex) {
                    var index = _this.createProjectFileList[i].name.lastIndexOf(".");
                    var extensions = _this.createProjectFileList[i].name.substr(index + 1);
                    if (ex == extensions) {
                        success = true;
                        return;
                    }
                });
                if (success) {
                    formData.append("file", this.createProjectFileList[i], this.createProjectFileList[i].name);
                }
            }
            ;
            console.log(this.createProject.memo);
            formData.append("name", this.createProject.name);
            formData.append("description", this.createProject.description);
            formData.append("memo", this.createProject.memo);
            formData.append("customerId", this.createProject.customerId);
            formData.append("userId", this.createProject.userId);
            formData.append("date", this.createProject.date.toUTCString());
            this.projectService
                .createProject(formData)
                .subscribe(function (response) {
                _this.notificationService.showSuccess("Project create success");
                _this.showingProjectsList();
            }),
                function (error) {
                    _this.notificationService.showError(error.error);
                };
        }
    };
    CreateProjectComponent.prototype.openDeleteAttachment = function (index) {
        this.deletedAttachmentId = index;
        this.openDeleteAttachmentModal = true;
    };
    CreateProjectComponent.prototype.cancelDeleteAttachment = function () {
        this.deletedAttachmentId = null;
        this.openDeleteAttachmentModal = false;
    };
    CreateProjectComponent.prototype.yesDeleteAttachment = function () {
        this.deleteAttachment();
        this.openDeleteAttachmentModal = false;
    };
    CreateProjectComponent.prototype.deleteAttachment = function () {
        this.createProjectFileList.splice(this.deletedAttachmentId, 1);
        this.deletedAttachmentId = null;
    };
    CreateProjectComponent.prototype.inputAdd = function (event) {
        var fileList = event.target.files;
        var invalidFiles = this.checkAttachmentExtensions(fileList);
        if (invalidFiles.length > 0) {
            this.notificationService.showErrors(invalidFiles);
        }
    };
    CreateProjectComponent.prototype.fileLeave = function (file) {
        var index = this.droppedFiles.indexOf(file);
        if (index > -1) {
            this.droppedFiles.splice(index, 1);
        }
    };
    CreateProjectComponent.prototype.fileOver = function (event) { };
    CreateProjectComponent.prototype.fileOverBase = function (event) {
        this.hasBaseDropZoneOver = event;
    };
    CreateProjectComponent.prototype.dropped = function (fileList) {
        var invalidFiles = this.checkAttachmentExtensions(fileList);
        if (invalidFiles.length > 0) {
            this.notificationService.showErrors(invalidFiles);
        }
    };
    CreateProjectComponent.prototype.checkAttachmentExtensions = function (files) {
        var invalidFiles = [];
        for (var i = 0; i < files.length; i++) {
            var success = false;
            allowableExtensions.forEach(function (ex) {
                var index = files[i].name.lastIndexOf(".");
                var extensions = files[i].name.substr(index + 1);
                if (ex == extensions) {
                    success = true;
                    return;
                }
            });
            if (!success) {
                invalidFiles.push(files[i].name);
            }
            if (success) {
                this.createProjectFileList.push(files[i]);
            }
        }
        ;
        return invalidFiles;
    };
    CreateProjectComponent.prototype.closeShowAttachment = function () {
        this.showAttachmentImg = false;
    };
    CreateProjectComponent = __decorate([
        core_1.Component({
            selector: 'app-create-project',
            templateUrl: './create-project.component.html',
            styleUrls: ['./create-project.component.scss']
        }),
        __metadata("design:paramtypes", [noitification_service_1.NotificationService,
            project_service_1.ProjectService,
            customer_service_1.CustomerService,
            project_attachment_service_1.ProjectAttachmentservice,
            router_1.ActivatedRoute,
            router_1.Router])
    ], CreateProjectComponent);
    return CreateProjectComponent;
}());
exports.CreateProjectComponent = CreateProjectComponent;
//# sourceMappingURL=create-project.component.js.map