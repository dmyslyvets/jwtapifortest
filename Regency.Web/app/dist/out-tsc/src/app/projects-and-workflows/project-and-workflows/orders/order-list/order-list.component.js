"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var drag_drop_1 = require("@angular/cdk/drag-drop");
var router_1 = require("@angular/router");
var order_service_1 = require("app/shared/providers/services/order.service");
var get_all_order_view_1 = require("app/shared/models/order/get-all-order.view");
var update_order_view_1 = require("app/shared/models/order/update-order.view");
var noitification_service_1 = require("app/shared/providers/services/noitification.service");
var OrderListComponent = /** @class */ (function () {
    function OrderListComponent(route, orderService, notificationService) {
        this.route = route;
        this.orderService = orderService;
        this.notificationService = notificationService;
        this.newOrder = [];
        this.activeOrder = [];
        this.processingOrder = [];
        this.doneOrder = [];
        this.allOrders = new get_all_order_view_1.GetAllOrderView();
        this.updateOrder = new update_order_view_1.UpdateOrderView();
        this.getAllOrders();
    }
    OrderListComponent.prototype.drop = function (event) {
        var _this = this;
        if (event.previousContainer === event.container) {
            drag_drop_1.moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
        }
        else {
            drag_drop_1.transferArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.currentIndex);
            this.curentOrder = event.container.data[event.currentIndex];
            console.log(event);
            if (event.container.id == "cdk-drop-list-0") {
                this.curentOrder.status = "New";
            }
            if (event.container.id == "cdk-drop-list-1") {
                this.curentOrder.status = "Active";
            }
            if (event.container.id == "cdk-drop-list-2") {
                this.curentOrder.status = "Processing";
            }
            if (event.container.id == "cdk-drop-list-3") {
                this.curentOrder.status = "Done";
            }
            this.updateOrder.id = this.curentOrder.id;
            this.updateOrder.date = this.curentOrder.date;
            this.updateOrder.dueDate = this.curentOrder.dueDate;
            this.updateOrder.memo = this.curentOrder.memo;
            this.updateOrder.status = this.curentOrder.status;
            this.updateOrder.customerId = this.curentOrder.customer.id;
            this.updateOrder.projectId = this.curentOrder.project.id;
            this.updateOrder.itemId = this.curentOrder.item.id;
            this.orderService.updateOrder(this.updateOrder).subscribe(function (response) {
                _this.notificationService.showSuccess("Order update success");
            });
        }
    };
    OrderListComponent.prototype.getAllOrders = function () {
        var _this = this;
        this.busyGetOrders = this.orderService.getAllOrders().subscribe(function (response) {
            _this.allOrders = response;
            _this.sortOrderByStatus(_this.allOrders.orders);
        });
    };
    OrderListComponent.prototype.sortOrderByStatus = function (allOrders) {
        console.log(this.allOrders.orders);
        this.newOrder = allOrders.filter(function (x) { return x.status == "New"; });
        this.activeOrder = allOrders.filter(function (x) { return x.status == "Active"; });
        this.processingOrder = allOrders.filter(function (x) { return x.status == "Processing"; });
        this.doneOrder = allOrders.filter(function (x) { return x.status == "Done"; });
    };
    OrderListComponent.prototype.navigateToCreateOrder = function () {
        this.route.navigate(['projects-and-workflows/orders/createOrder']);
    };
    OrderListComponent = __decorate([
        core_1.Component({
            selector: 'orderList',
            templateUrl: './order-list.component.html',
            styleUrls: ['./order-list.component.scss']
        }),
        __metadata("design:paramtypes", [router_1.Router, order_service_1.OrderService, noitification_service_1.NotificationService])
    ], OrderListComponent);
    return OrderListComponent;
}());
exports.OrderListComponent = OrderListComponent;
//# sourceMappingURL=order-list.component.js.map