"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
// import { Validators, NgForm, FormControl } from "@angular/forms";
var router_1 = require("@angular/router");
var project_service_1 = require("app/shared/providers/services/project.service");
var customer_service_1 = require("app/shared/providers/services/customer.service");
var noitification_service_1 = require("app/shared/providers/services/noitification.service");
var get_all_users_view_1 = require("app/shared/models/users/get-all-users.view");
var create_project_view_1 = require("app/shared/models/project/create-project.view");
var reassign_project_manager_view_1 = require("app/shared/models/project/reassign-project-manager.view");
var get_all_active_project_view_1 = require("app/shared/models/project/get-all-active-project.view");
var get_all_customer_view_1 = require("app/shared/models/customer/get-all-customer.view");
var devextreme_angular_1 = require("devextreme-angular");
var addProjectMessage = "Project is added";
var projectUpdateMessage = "Project is update";
var ProjectsListComponent = /** @class */ (function () {
    function ProjectsListComponent(notificationService, projectService, customerService, route) {
        this.notificationService = notificationService;
        this.projectService = projectService;
        this.customerService = customerService;
        this.route = route;
        //post request
        this.newProject = new create_project_view_1.CreateProjectView();
        this.reassignProjectManager = new reassign_project_manager_view_1.ReassignProjectManagerView();
        //get request
        this.projectManagers = new get_all_users_view_1.GetAllUserView();
        this.activeProjects = new get_all_active_project_view_1.GetAllActiveProjectView();
        this.customers = new get_all_customer_view_1.GetAllCustomerView();
        //popups
        this.openAddProject = false;
        this.formValidation = false;
        this.openReassignPM = false;
        //slider 
        this.isChecked = false;
        //#region hard code for table and filter
        this.projectsFilter = "";
        this.selectedFilters = [];
        this.dropdownSettings = {
            IdField: "id",
            textField: "name",
            allowSearchFilter: true
        };
        this.filters = [
            { name: "Active", id: 1, selected: false },
            { name: "Non active", id: 2, selected: false },
            { name: "Filter1", id: 3, selected: false },
            { name: "Filter2", id: 4, selected: false },
            { name: "Filter3", id: 5, selected: false },
            { name: "Monopoly", id: 6, selected: false }
        ];
        this.showFilterRow = true;
        this.showHeaderFilter = true;
        this.currentFilter = "auto";
    }
    ProjectsListComponent.prototype.ngOnInit = function () {
        this.getProjectManager();
        this.getCustomers();
        this.getActiveProject();
    };
    //gets
    ProjectsListComponent.prototype.getCustomers = function () {
        var _this = this;
        this.customerService.getCustomers().subscribe(function (response) {
            _this.customers = response;
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    ProjectsListComponent.prototype.getProjectManager = function () {
        var _this = this;
        this.projectService.getProjectManager().subscribe(function (response) {
            _this.projectManagers = response;
            _this.projectManagers.users.forEach(function (projectManager) {
                projectManager.fullName = projectManager.firstName + " " + projectManager.lastName;
            });
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    ProjectsListComponent.prototype.getActiveProject = function () {
        var _this = this;
        this.busyGetAllProject = this.projectService
            .getAllActivePorjects()
            .subscribe(function (respone) {
            _this.activeProjects = respone;
            _this.updateDate = null;
            _this.projectsCount = _this.activeProjects.projects.length;
            _this.dataGrid.dataSource = _this.activeProjects.projects;
        });
    };
    ProjectsListComponent.prototype.OnProjectSliderChange = function (event) {
        var _this = this;
        this.isChecked = event.checked;
        if (event.checked) {
            this.busyGetAllProject = this.projectService.getAllPorjects().subscribe(function (respone) {
                _this.activeProjects = respone;
                _this.updateDate = null;
                _this.projectsCount = _this.activeProjects.projects.length;
            });
        }
        if (!event.checked) {
            this.busyGetAllProject = this.projectService.getAllActivePorjects().subscribe(function (respone) {
                _this.activeProjects = respone;
                _this.updateDate = null;
                _this.projectsCount = _this.activeProjects.projects.length;
            });
        }
    };
    //popups
    ProjectsListComponent.prototype.openReassignPMDialog = function (projectId) {
        this.formValidation = false;
        this.reassignProjectManager = new reassign_project_manager_view_1.ReassignProjectManagerView();
        this.reassignProjectManager.projectId = projectId;
        this.openReassignPM = true;
    };
    ProjectsListComponent.prototype.closeReassignPMDialog = function () {
        this.reassignProjectManager = new reassign_project_manager_view_1.ReassignProjectManagerView();
        this.openReassignPM = false;
    };
    ProjectsListComponent.prototype.saveReasignPM = function (event) {
        var _this = this;
        this.reassignProjectManager = new reassign_project_manager_view_1.ReassignProjectManagerView();
        this.reassignProjectManager.projectId = event.key.id;
        this.reassignProjectManager.projectManagerId = event.newData.user.id;
        this.openReassignPM = false;
        this.projectService.reassignProjectManeger(this.reassignProjectManager).subscribe(function (response) {
            _this.getActiveProject();
            _this.notificationService.showSuccess(projectUpdateMessage);
        });
    };
    ProjectsListComponent.prototype.openAddProjectModal = function () {
        this.openAddProject = true;
        this.formValidation = false;
        this.newProject = new create_project_view_1.CreateProjectView();
    };
    ProjectsListComponent.prototype.closeAddProjectModal = function () {
        this.openAddProject = false;
        this.formValidation = false;
        this.newProject = new create_project_view_1.CreateProjectView();
    };
    ProjectsListComponent.prototype.saveNewProject = function () {
        this.route.navigate(['projects-and-workflows/projects/createproject']);
    };
    //back on page
    ProjectsListComponent.prototype.showingProjectsList = function () {
        this.getActiveProject();
    };
    ProjectsListComponent.prototype.showProjectDetailsById = function (event) {
        if (this.openReassignPM != true) {
            this.route.navigate(['projects-and-workflows/projects/projectdetails', event.data.id]);
        }
    };
    ProjectsListComponent.prototype.addFilter = function (item) {
        this.selectedFilters.push(item);
    };
    ProjectsListComponent.prototype.deleteFilter = function (item) {
        var gameIndex = this.filters.indexOf(item);
        var selectedIndex = this.selectedFilters.indexOf(item);
        this.filters[gameIndex].selected = false;
        this.selectedFilters.splice(selectedIndex, 1);
    };
    __decorate([
        core_1.ViewChild(devextreme_angular_1.DxDataGridComponent),
        __metadata("design:type", devextreme_angular_1.DxDataGridComponent)
    ], ProjectsListComponent.prototype, "dataGrid", void 0);
    ProjectsListComponent = __decorate([
        core_1.Component({
            selector: 'app-projects-list',
            templateUrl: './projects-list.component.html',
            styleUrls: ['./projects-list.component.scss']
        }),
        __metadata("design:paramtypes", [noitification_service_1.NotificationService,
            project_service_1.ProjectService,
            customer_service_1.CustomerService,
            router_1.Router])
    ], ProjectsListComponent);
    return ProjectsListComponent;
}());
exports.ProjectsListComponent = ProjectsListComponent;
//# sourceMappingURL=projects-list.component.js.map