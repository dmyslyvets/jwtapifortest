"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var environment_1 = require("environments/environment");
var UserService = /** @class */ (function () {
    function UserService(http) {
        this.http = http;
        this.apiControllerURL = environment_1.environment.apiUrl;
    }
    UserService.prototype.getTeamByUserId = function (id) {
        return this.http.get(this.apiControllerURL + ("api/Team/GetTeamByUserId/?id=" + id));
    };
    UserService.prototype.getUser = function (id) {
        return this.http.get(this.apiControllerURL + ("api/User/GetUser/?id=" + id));
    };
    UserService.prototype.updateUser = function (viewModel) {
        return this.http.post(this.apiControllerURL + "api/User/Update", viewModel);
    };
    UserService.prototype.updateUserStatus = function (viewModel) {
        return this.http.post(this.apiControllerURL + "api/User/UpdateStatus", viewModel);
    };
    UserService.prototype.createUser = function (model) {
        return this.http.post(this.apiControllerURL + 'api/User/Create', model);
    };
    UserService.prototype.getUserActivity = function (id) {
        return this.http.get(this.apiControllerURL + ("api/UserActivity/GetAllByUserId/?id=" + id));
    };
    UserService.prototype.setUserRole = function (setUserRoleModel) {
        return this.http.post(this.apiControllerURL + 'api/User/SetUserRole', setUserRoleModel);
    };
    UserService.prototype.resetPassword = function (model) {
        return this.http.post(this.apiControllerURL + 'api/User/ResetPassword', model);
    };
    UserService.prototype.addUserToTeamView = function (model) {
        return this.http.post(this.apiControllerURL + 'api/User/AddUserToTeam', model);
    };
    UserService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], UserService);
    return UserService;
}());
exports.UserService = UserService;
//# sourceMappingURL=users.service.js.map