"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var http_1 = require("@angular/common/http");
var core_1 = require("@angular/core");
var environment_1 = require("environments/environment");
/**
 * Api is a generic REST Api handler. Set your API url first.
 */
var ApiService = /** @class */ (function () {
    function ApiService(http) {
        this.http = http;
        this.url = environment_1.environment.apiUrl;
    }
    ApiService.prototype.get = function (endpoint, params, reqOpts) {
        if (!reqOpts) {
            reqOpts = {
                params: new http_1.HttpParams()
            };
        }
        // Support easy query params for GET requests
        if (params) {
            reqOpts.params = new http_1.HttpParams();
            for (var k in params) {
                reqOpts.params = reqOpts.params.set(k, params[k]);
            }
        }
        return this.http.get(this.url + "/" + endpoint, reqOpts);
    };
    ApiService.prototype.post = function (endpoint, body, reqOpts) {
        return this.http.post(this.url + "/" + endpoint, body, reqOpts);
    };
    ApiService.prototype.put = function (endpoint, body, reqOpts) {
        return this.http.put(this.url + "/" + endpoint, body, reqOpts);
    };
    ApiService.prototype.delete = function (endpoint, reqOpts) {
        return this.http.delete(this.url + "/" + endpoint, reqOpts);
    };
    ApiService.prototype.patch = function (endpoint, body, reqOpts) {
        return this.http.patch(this.url + "/" + endpoint, body, reqOpts);
    };
    ApiService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], ApiService);
    return ApiService;
}());
exports.ApiService = ApiService;
//# sourceMappingURL=api.service.js.map