"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var environment_1 = require("environments/environment");
var TeamMembersService = /** @class */ (function () {
    function TeamMembersService(http) {
        this.http = http;
        this.apiControllerURL = environment_1.environment.apiUrl;
    }
    TeamMembersService.prototype.getTeamMemresByTeamId = function (teamId) {
        return this.http.get(this.apiControllerURL + ("api/TeamMember/GetTeamMembersByTeamId/?id=" + teamId));
    };
    TeamMembersService.prototype.deleteTeamMembersById = function (deleteTeamMember) {
        return this.http.post(this.apiControllerURL + "api/TeamMember/Delete", deleteTeamMember);
    };
    TeamMembersService.prototype.getAllUsers = function () {
        return this.http.get(this.apiControllerURL + "api/User/GetAll");
    };
    TeamMembersService.prototype.createTeamMember = function (teamMemberView) {
        return this.http.post(this.apiControllerURL + "api/TeamMember/Create", teamMemberView);
    };
    TeamMembersService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], TeamMembersService);
    return TeamMembersService;
}());
exports.TeamMembersService = TeamMembersService;
//# sourceMappingURL=team-members.service.js.map