"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_1 = require("@angular/platform-browser");
var animations_1 = require("@angular/platform-browser/animations");
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var app_component_1 = require("./app.component");
var ng_busy_1 = require("ng-busy");
var material_1 = require("@angular/material");
// Import containers
var containers_1 = require("./containers");
var APP_CONTAINERS = [
    containers_1.FullLayoutComponent,
    containers_1.SimpleLayoutComponent
];
// Import components
var components_1 = require("app/shared/components");
var APP_COMPONENTS = [
    components_1.AppAsideComponent,
    components_1.AppBreadcrumbsComponent,
    components_1.AppFooterComponent,
    components_1.AppHeaderComponent,
    components_1.AppSidebarComponent,
    components_1.AppSidebarFooterComponent,
    components_1.AppSidebarFormComponent,
    components_1.AppSidebarHeaderComponent,
    components_1.AppSidebarMinimizerComponent,
    components_1.APP_SIDEBAR_NAV
];
// Import directives
var directives_1 = require("./directives");
var APP_DIRECTIVES = [
    directives_1.AsideToggleDirective,
    directives_1.NAV_DROPDOWN_DIRECTIVES,
    directives_1.ReplaceDirective,
    directives_1.SIDEBAR_TOGGLE_DIRECTIVES
];
// Import routing module
var app_routing_1 = require("./app.routing");
// Import 3rd party components
var ng2_charts_1 = require("ng2-charts/ng2-charts");
var dropdown_1 = require("ngx-bootstrap/dropdown");
var ngx_bootstrap_1 = require("ngx-bootstrap");
var tabs_1 = require("ngx-bootstrap/tabs");
var interceptors_1 = require("app/shared/providers/interceptors");
var http_1 = require("@angular/common/http");
var services_1 = require("app/shared/providers/services");
var providers_1 = require("app/shared/providers/providers");
var guards_1 = require("app/shared/providers/guards");
var ngx_toastr_1 = require("ngx-toastr");
var angular2_moment_1 = require("angular2-moment");
var kendo_angular_grid_1 = require("@progress/kendo-angular-grid");
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [
                platform_browser_1.BrowserModule,
                animations_1.BrowserAnimationsModule,
                material_1.MatProgressBarModule,
                app_routing_1.AppRoutingModule,
                ng2_charts_1.ChartsModule,
                ngx_toastr_1.ToastrModule.forRoot(),
                dropdown_1.BsDropdownModule.forRoot(),
                ngx_bootstrap_1.ModalModule.forRoot(),
                tabs_1.TabsModule.forRoot(),
                http_1.HttpClientModule,
                ng_busy_1.NgBusyModule,
                angular2_moment_1.MomentModule,
                kendo_angular_grid_1.GridModule
            ],
            declarations: [
                app_component_1.AppComponent
            ].concat(APP_CONTAINERS, APP_COMPONENTS, APP_DIRECTIVES),
            providers: [
                {
                    provide: common_1.LocationStrategy,
                    useClass: common_1.HashLocationStrategy
                },
                {
                    provide: http_1.HTTP_INTERCEPTORS,
                    useClass: interceptors_1.AuthInterceptor,
                    multi: true
                },
                guards_1.AuthGuard,
                services_1.ApiService,
                services_1.AccountService,
                providers_1.AccountProvider
            ],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map