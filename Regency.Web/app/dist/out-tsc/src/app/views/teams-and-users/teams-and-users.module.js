"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var tabs_1 = require("ngx-bootstrap/tabs");
var angular_6_datatable_1 = require("angular-6-datatable");
var http_1 = require("@angular/http");
var forms_1 = require("@angular/forms");
var ng_select_1 = require("@ng-select/ng-select");
var teams_and_users_routing_module_1 = require("./teams-and-users-routing.module");
var data_filter_pipe_1 = require("../../shared/filters/data-filter.pipe");
var users_filter_pipe_1 = require("../../shared/filters/users-filter.pipe");
var teams_and_users_component_1 = require("./teams-and-users/teams-and-users.component");
var TeamsAndUsersModule = /** @class */ (function () {
    function TeamsAndUsersModule() {
    }
    TeamsAndUsersModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                tabs_1.TabsModule,
                forms_1.FormsModule,
                angular_6_datatable_1.DataTableModule,
                http_1.HttpModule,
                ng_select_1.NgSelectModule,
                teams_and_users_routing_module_1.TeamsAndUsersRoutingModule
            ],
            declarations: [
                teams_and_users_component_1.TeamsAndUsersComponent,
                data_filter_pipe_1.DataFilterPipe,
                users_filter_pipe_1.UsersFilterPipe
            ]
        })
    ], TeamsAndUsersModule);
    return TeamsAndUsersModule;
}());
exports.TeamsAndUsersModule = TeamsAndUsersModule;
//# sourceMappingURL=teams-and-users.module.js.map