"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var uset_status_enum_1 = require("app/shared/models/enums/uset-status.enum");
var team_member_type_enum_1 = require("app/shared//models/enums/team-member-type.enum");
var noitification_service_1 = require("app/providers/services/noitification.service");
var team_members_type_service_1 = require("app/providers/services/team-members-type.service");
var users_service_1 = require("app/providers/services/users.service");
var teams_service_1 = require("app/providers/services/teams.service");
var team_members_service_1 = require("app/providers/services/team-members.service");
var get_team_members_by_teamId_model_1 = require("app/shared/models/team-member/get-team-members-by-teamId.model");
var get_team_model_1 = require("app/shared/models/teams/get-team.model");
var update_team_model_1 = require("app/shared/models/teams/update-team.model");
var create_team_model_1 = require("app/shared/models/teams/create-team.model");
var get_all_users_model_1 = require("app/shared/models/users/get-all-users.model");
var create_team_member_models_1 = require("app/shared/models/team-member/create-team-member.models");
var delete_team_member_models_1 = require("app/shared/models/team-member/delete-team-member.models");
var get_user_model_1 = require("app/shared/models/users/get-user.model");
var update_user_model_1 = require("app/shared/models/users/update-user.model");
var get_all_member_types_model_1 = require("app/shared/models/team-member-types/get-all-member-types.model");
var create_team_member_model_1 = require("app/shared/models/team-member-types/create-team-member.model");
var delete_team_member_model_1 = require("app/shared/models/team-member-types/delete-team-member.model");
var reset_password_user_view_1 = require("app/shared/models/users/reset-password-user.view");
var add_user_view_1 = require("app/shared/models/users/add-user.view");
var GetByUserIdTeamView_1 = require("app/shared/models/users/GetByUserIdTeamView");
var get_user_activity_model_1 = require("app/shared/models/users/get-user-activity.model");
var get_team_model_2 = require("app/shared/models/teams/get-team.model");
var set_user_role_model_1 = require("app/shared/models/users/set-user-role.model");
var add_user_to_team_view_1 = require("app/shared/models/users/add-user-to-team.view");
var showMoreMessage = "Show More";
var showLessMessage = "Show Less";
var passwordChangeMessage = "Password change";
//const badRequestMessage: string = "Bad request";
var userAddedMessage = "User added";
var TeamsAndUsersComponent = /** @class */ (function () {
    function TeamsAndUsersComponent(teamsService, teamMembersService, teamMemberTypesService, usersService, notificationService, route) {
        this.teamsService = teamsService;
        this.teamMembersService = teamMembersService;
        this.teamMemberTypesService = teamMemberTypesService;
        this.usersService = usersService;
        this.notificationService = notificationService;
        this.route = route;
        this.filterQuery = '';
        this.usersFilter = '';
        this.allteams = new get_team_model_2.GetAllTeamsView();
        this.currentTeam = new get_team_model_1.GetTeamView();
        this.currentTeamMebers = new get_team_members_by_teamId_model_1.GetTeamMembersByTeamId();
        this.creteNewTeam = new create_team_model_1.CreateTeamView();
        this.deleteTeamMember = new delete_team_member_models_1.DeleteTeamMemberView();
        this.updateCurrentTeam = new update_team_model_1.UpdateTeamView();
        this.teamsCurrentUser = new GetByUserIdTeamView_1.GetByUserIdTeamView();
        this.showEditableTeamButtons = false;
        this.allUsers = new get_all_users_model_1.GetAllUsersViewModel();
        this.currentUser = new get_user_model_1.GetUserViewModel();
        this.selectedUserForNewTypeMember = new get_all_users_model_1.GetAllUsersViewItem();
        this.setUserAdmin = new set_user_role_model_1.SetUserRoleView();
        this.updateCurrentUser = new update_user_model_1.UpdateUserViewModel();
        this.currentUserActivity = new get_user_activity_model_1.GetUserActivitiesView();
        this.showEditableUserButtons = false;
        this.selectedTeamForAddUser = new get_team_model_1.GetTeamView();
        this.selectedTeamMemberTypeForAddUser = new get_all_member_types_model_1.GetAllTeamMemberTypeItem();
        this.addUserToTeamModel = new add_user_to_team_view_1.AddUserToTeamView();
        this.createNewTeamMember = new create_team_member_models_1.CreateTeamMemberView();
        this.selectedMemberType = new get_all_member_types_model_1.GetAllTeamMemberTypeItem();
        this.resetPassword = new reset_password_user_view_1.ResetPasswordUserView();
        this.newUser = new add_user_view_1.AddUserView();
        this.allTeamMemberTypes = new get_all_member_types_model_1.GetAllTeamMemberTypesView();
        this.createTeamMemberTypes = new create_team_member_model_1.CreateTeamMemberTypeView();
        this.deletingTeamMemberType = new delete_team_member_model_1.DeleteTeamMemberTypeView();
        this.checkData = false;
        this.addTeamIsOpen = false;
        this.addTeamMemberIsOpen = false;
        this.addNewMemberTypeIsOpen = false;
        this.userActivityShowMore = false;
        this.buttonShowMoreText = showMoreMessage;
        this.addUserToTeamIsOpen = false;
        this.statuses = Object.keys(uset_status_enum_1.UserStatus).map(function (key) { return uset_status_enum_1.UserStatus[key]; }).filter(function (value) { return typeof value === 'string'; });
        this.openResetPassword = false;
        this.openAddUser = false;
        this.validateForm = false;
        this.showDefaultImage = true;
        this.testDisabled = false;
        this.sortByWordLength = function (a) {
            return a.name.length;
        };
        this.getAllTeams();
    }
    TeamsAndUsersComponent.prototype.ngOnInit = function () { };
    TeamsAndUsersComponent.prototype.openDialogAddNewUser = function () {
        this.openAddUser = true;
        this.validateForm = false;
        this.newUser = new add_user_view_1.AddUserView();
    };
    TeamsAndUsersComponent.prototype.closeDialogAddNewUser = function () {
        this.openAddUser = false;
        this.validateForm = false;
        this.newUser = new add_user_view_1.AddUserView();
    };
    TeamsAndUsersComponent.prototype.saveNewUser = function () {
        var _this = this;
        this.usersService.createUser(this.newUser).subscribe(function (response) {
            _this.notificationService.showSuccess(userAddedMessage);
            _this.getAllUsers();
            _this.newUser = new add_user_view_1.AddUserView();
        }, function (error) {
            _this.notificationService.showError(error.error);
        });
        this.validateForm = true;
        this.openAddUser = false;
    };
    TeamsAndUsersComponent.prototype.openDialogResetPassword = function () {
        this.openResetPassword = true;
        this.validateForm = false;
        this.resetPassword = new reset_password_user_view_1.ResetPasswordUserView();
    };
    TeamsAndUsersComponent.prototype.passwordCheck = function () {
        if (this.resetPassword.password == null || this.resetPassword.confirmPassword == null) {
            return true;
        }
        if (this.resetPassword.password != this.resetPassword.confirmPassword) {
            return false;
        }
        return true;
    };
    TeamsAndUsersComponent.prototype.closeDialogResetPassword = function () {
        this.openResetPassword = false;
        this.validateForm = false;
        this.resetPassword = new reset_password_user_view_1.ResetPasswordUserView();
    };
    TeamsAndUsersComponent.prototype.saveNewPasword = function () {
        var _this = this;
        if (!this.passwordCheck()) {
            return;
        }
        this.resetPassword.email = this.currentUser.email;
        this.usersService.resetPassword(this.resetPassword).subscribe(function (response) {
            _this.notificationService.showSuccess(passwordChangeMessage);
            _this.resetPassword = new reset_password_user_view_1.ResetPasswordUserView();
        }, function (error) {
            _this.notificationService.showError(error.error);
        });
        this.openResetPassword = false;
        this.validateForm = true;
    };
    TeamsAndUsersComponent.prototype.getAllTeams = function () {
        var _this = this;
        this.teamsService.getAllTeams()
            .subscribe(function (teams) {
            _this.allteams = teams;
            _this.checkData = true;
            _this.getTeam(teams.teams[0].id);
            _this.getAllUsers();
            _this.getAllTeamMembersType();
        });
    };
    TeamsAndUsersComponent.prototype.getTeam = function (id) {
        var _this = this;
        this.teamsService.getTeam(id).subscribe(function (team) {
            _this.showDefaultImage = true;
            _this.currentTeam = team;
            if (_this.currentTeam.linkToImage != null) {
                _this.imagePath = _this.currentTeam.linkToImage;
                _this.showDefaultImage = false;
            }
            _this.showEditableTeamButtons = false;
            _this.getTeamMembersByTeamId(_this.currentTeam.id);
        });
    };
    TeamsAndUsersComponent.prototype.getTeamMembersByTeamId = function (teamId) {
        var _this = this;
        this.teamMembersService.getTeamMemresByTeamId(teamId).subscribe(function (response) {
            _this.currentTeamMebers = response;
        });
    };
    TeamsAndUsersComponent.prototype.createNewTeam = function () {
        var _this = this;
        this.teamsService.createNewTeam(this.creteNewTeam).subscribe(function (response) {
            _this.getAllTeams();
            _this.addTeamIsOpen = false;
        });
    };
    TeamsAndUsersComponent.prototype.activetedTeamChanges = function () {
        this.showEditableTeamButtons = true;
    };
    TeamsAndUsersComponent.prototype.cancelChanges = function (id) {
        this.getTeam(id);
    };
    TeamsAndUsersComponent.prototype.saveChanges = function () {
        var _this = this;
        this.updateCurrentTeam.linkToImage = this.imagePath;
        this.updateCurrentTeam.id = this.currentTeam.id;
        this.updateCurrentTeam.teamName = this.currentTeam.teamName;
        this.updateCurrentTeam.teamDesc = this.currentTeam.teamDesc;
        this.updateCurrentTeam.active = this.currentTeam.active;
        this.updateCurrentTeam.dateCreated = this.currentTeam.dateCreated;
        this.teamsService.updateTeam(this.updateCurrentTeam).subscribe(function (upateTeamId) {
            _this.showEditableTeamButtons = false;
            _this.getAllTeams();
            _this.getTeam(upateTeamId);
        });
    };
    TeamsAndUsersComponent.prototype.deleteTeamMembers = function (id) {
        var _this = this;
        this.deleteTeamMember.id = id;
        this.teamMembersService.deleteTeamMembersById(this.deleteTeamMember).subscribe(function (response) {
            _this.getTeamMembersByTeamId(_this.currentTeam.id);
        });
    };
    TeamsAndUsersComponent.prototype.getAllUsers = function () {
        var _this = this;
        this.teamMembersService.getAllUsers().subscribe(function (users) {
            _this.allUsers = users;
            _this.getUser(users.users[0].id);
        });
    };
    TeamsAndUsersComponent.prototype.getUser = function (id) {
        var _this = this;
        this.usersService.getUser(id).subscribe(function (user) {
            _this.currentUser = user;
            _this.showEditableUserButtons = false;
            _this.setUserRole = _this.currentUser.isAdmin;
        });
        this.getCurrentUserActivity(id);
        this.showMoreRefresh();
    };
    TeamsAndUsersComponent.prototype.changeUserRole = function () {
        var _this = this;
        this.setUserAdmin.id = this.currentUser.id;
        this.setUserAdmin.isAdmin = this.setUserRole;
        this.usersService.setUserRole(this.setUserAdmin).subscribe(function (response) {
            _this.getUser(_this.currentUser.id);
        });
    };
    TeamsAndUsersComponent.prototype.getCurrentUserActivity = function (id) {
        var _this = this;
        this.usersService.getUserActivity(id).subscribe(function (response) {
            _this.currentUserActivity = response;
        });
        this.usersService.getTeamByUserId(id).subscribe(function (data) {
            _this.teamsCurrentUser = data;
        });
    };
    TeamsAndUsersComponent.prototype.activetedUserChanges = function () {
        this.showEditableUserButtons = true;
    };
    TeamsAndUsersComponent.prototype.cancelUserChanges = function (id) {
        this.getUser(id);
    };
    TeamsAndUsersComponent.prototype.saveUserChanges = function () {
        var _this = this;
        this.updateCurrentUser = this.currentUser;
        this.usersService.updateUser(this.updateCurrentUser).subscribe(function (response) {
            _this.showEditableUserButtons = false;
            _this.getAllUsers();
            _this.getUser(response);
        });
    };
    TeamsAndUsersComponent.prototype.addUserToTeam = function () {
        var _this = this;
        this.addUserToTeamModel.userId = this.currentUser.id;
        this.addUserToTeamModel.teamMemberTypeId = this.selectedTeamMemberTypeForAddUser.id;
        this.addUserToTeamModel.teamId = this.selectedTeamForAddUser.id;
        this.addUserToTeamModel.active = true;
        this.usersService.addUserToTeamView(this.addUserToTeamModel)
            .subscribe(function (response) {
            _this.usersService.getTeamByUserId(_this.currentUser.id).subscribe(function (data) {
                _this.teamsCurrentUser = data;
            });
            _this.addUserToTeamIsOpen = false;
        });
    };
    TeamsAndUsersComponent.prototype.retriveUserActivity = function (id) {
    };
    TeamsAndUsersComponent.prototype.addTeamMember = function () {
        var _this = this;
        this.createNewTeamMember.teamId = this.currentTeam.id;
        this.createNewTeamMember.userId = this.selectedUserForNewTypeMember.id;
        this.createNewTeamMember.teamMemberTypeId = this.selectedMemberType.id;
        this.createNewTeamMember.active = true;
        this.teamMembersService.createTeamMember(this.createNewTeamMember).subscribe(function (response) {
            _this.getTeamMembersByTeamId(_this.currentTeam.id);
            _this.addTeamMemberIsOpen = false;
        });
    };
    TeamsAndUsersComponent.prototype.uploadImage = function (event) {
        var _this = this;
        var formData = new FormData();
        this.image = event.target.files[0];
        formData.append('image', this.image, this.image.name);
        this.teamsService.uploadImage(formData).subscribe(function (data) {
            _this.imagePath = data;
            _this.saveChanges();
        });
    };
    TeamsAndUsersComponent.prototype.getAllTeamMembersType = function () {
        var _this = this;
        this.teamMemberTypesService.getAllTeamMembersType().subscribe(function (response) {
            _this.allTeamMemberTypes = response;
        });
    };
    TeamsAndUsersComponent.prototype.createTeamMemberType = function () {
        var _this = this;
        this.teamMemberTypesService.createTeamMembers(this.createTeamMemberTypes).subscribe(function (response) {
            _this.addNewMemberTypeIsOpen = false;
            _this.getAllTeamMembersType();
        });
    };
    TeamsAndUsersComponent.prototype.deleteTeamMemberType = function (id) {
        var _this = this;
        this.deletingTeamMemberType.id = id;
        this.teamMemberTypesService.deleteTeamMember(this.deletingTeamMemberType).subscribe(function (response) {
            _this.getAllTeamMembersType();
        });
    };
    TeamsAndUsersComponent.prototype.getMemberType = function (typeId) {
        return team_member_type_enum_1.TeamMemberType[typeId];
    };
    TeamsAndUsersComponent.prototype.toInt = function (num) {
        return +num;
    };
    TeamsAndUsersComponent.prototype.showMoreRefresh = function () {
        this.userActivityShowMore = false;
        this.buttonShowMoreText = showMoreMessage;
    };
    TeamsAndUsersComponent.prototype.showMoreClick = function () {
        this.userActivityShowMore = !this.userActivityShowMore;
        if (this.userActivityShowMore) {
            this.buttonShowMoreText = showLessMessage;
        }
        if (!this.userActivityShowMore) {
            this.buttonShowMoreText = showMoreMessage;
        }
    };
    TeamsAndUsersComponent = __decorate([
        core_1.Component({
            selector: 'teams-and-users',
            templateUrl: './teams-and-users.component.html',
            styleUrls: ['./teams-and-users.component.scss']
        }),
        core_1.Injectable(),
        __metadata("design:paramtypes", [teams_service_1.TeamsService,
            team_members_service_1.TeamMembersService,
            team_members_type_service_1.TeamMembersTypeService,
            users_service_1.UserService,
            noitification_service_1.NotificationService,
            router_1.Router])
    ], TeamsAndUsersComponent);
    return TeamsAndUsersComponent;
}());
exports.TeamsAndUsersComponent = TeamsAndUsersComponent;
//# sourceMappingURL=teams-and-users.component.js.map