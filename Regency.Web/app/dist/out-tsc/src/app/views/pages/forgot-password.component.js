"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var providers_1 = require("app/shared/providers/providers");
var router_1 = require("@angular/router");
var account_1 = require("../../shared/models/account");
var ForgotPasswordComponent = /** @class */ (function () {
    function ForgotPasswordComponent(accountProvider, router) {
        this.accountProvider = accountProvider;
        this.router = router;
        this.model = new account_1.ForgotPasswordModel();
    }
    ForgotPasswordComponent.prototype.forgotPassword = function () {
        var _this = this;
        this.isLoading = true;
        this.accountProvider.forgotPassword(this.model).subscribe(function () {
            _this.router.navigateByUrl('pages/login');
        }, function (err) {
            _this.isLoading = false;
        });
    };
    ForgotPasswordComponent = __decorate([
        core_1.Component({
            templateUrl: 'forgot-password.component.html'
        }),
        __metadata("design:paramtypes", [providers_1.AccountProvider,
            router_1.Router])
    ], ForgotPasswordComponent);
    return ForgotPasswordComponent;
}());
exports.ForgotPasswordComponent = ForgotPasswordComponent;
//# sourceMappingURL=forgot-password.component.js.map