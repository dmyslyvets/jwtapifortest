"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var providers_1 = require("app/shared/providers/providers");
var router_1 = require("@angular/router");
var services_1 = require("app/shared/providers/services");
var account_1 = require("app/shared/models/account");
var ResetPasswordComponent = /** @class */ (function () {
    function ResetPasswordComponent(accountProvider, route, router, accountService) {
        this.accountProvider = accountProvider;
        this.route = route;
        this.router = router;
        this.accountService = accountService;
        this.model = new account_1.ResetPasswordModel();
    }
    ResetPasswordComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            //this.model.token = params['token'];
            _this.model.token = _this.accountService.getToken();
        });
    };
    ResetPasswordComponent.prototype.resetPassword = function () {
        var _this = this;
        this.isLoading = true;
        this.accountProvider.resetPassword(this.model).subscribe(function () {
            _this.router.navigateByUrl('pages/login');
        }, function (err) {
            _this.isLoading = false;
        });
    };
    ResetPasswordComponent = __decorate([
        core_1.Component({
            templateUrl: 'reset-password.component.html'
        }),
        __metadata("design:paramtypes", [providers_1.AccountProvider,
            router_1.ActivatedRoute,
            router_1.Router,
            services_1.AccountService])
    ], ResetPasswordComponent);
    return ResetPasswordComponent;
}());
exports.ResetPasswordComponent = ResetPasswordComponent;
//# sourceMappingURL=reset-password.component.js.map