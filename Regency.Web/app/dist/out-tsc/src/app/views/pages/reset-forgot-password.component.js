"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var providers_1 = require("app/shared/providers/providers");
var router_1 = require("@angular/router");
var services_1 = require("app/shared/providers/services");
var account_1 = require("../../shared/models/account");
var noitification_service_1 = require("../../shared/providers/services/noitification.service");
var ResetForgotPasswordComponent = /** @class */ (function () {
    function ResetForgotPasswordComponent(accountProvider, route, router, accountService, notificationService) {
        this.accountProvider = accountProvider;
        this.route = route;
        this.router = router;
        this.accountService = accountService;
        this.notificationService = notificationService;
        this.model = new account_1.ResetPasswordModel();
        this.regexForUpperCase = /[A-Z]+/;
        this.regexForLowerCase = /[a-z]+/;
        this.regexForDigit = /[\d]+/;
        this.containsOneUpperCase = false;
        this.containsOneDigit = false;
        this.containsOneLowerCase = false;
        this.containsSixChars = false;
        this.matchConfirmPassword = false;
        this.validPassword = true;
    }
    ResetForgotPasswordComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.model.token = params['token'];
            _this.model.email = params['email'];
        });
    };
    ResetForgotPasswordComponent.prototype.validatePassword = function () {
        this.validateSixChars();
        this.validateForDigit();
        this.validateLowerCase();
        this.validateUpperCase();
    };
    ResetForgotPasswordComponent.prototype.validateConfirmPassword = function () {
        this.matchConfirmPassword = (this.model.confirmPassword === this.model.password);
        if (!this.matchConfirmPassword) {
            this.validPassword = false;
            this.notificationService.showError("Confirm password doesn't match!");
        }
    };
    ResetForgotPasswordComponent.prototype.validateForDigit = function () {
        this.containsOneDigit = this.regexForDigit.test(this.model.password);
        if (!this.containsOneDigit) {
            this.validPassword = false;
            this.notificationService.showError("Should contain at least 1 digit!");
        }
    };
    ResetForgotPasswordComponent.prototype.validateLowerCase = function () {
        if (!(this.model.password === undefined)) {
            this.containsOneLowerCase = this.regexForLowerCase.test(this.model.password);
        }
        if (this.model.password === undefined) {
            this.containsOneLowerCase = false;
        }
        if (!this.containsOneLowerCase) {
            this.validPassword = false;
            this.notificationService.showError("Should contain at least 1 latin lowercase letter!");
        }
    };
    ResetForgotPasswordComponent.prototype.validateUpperCase = function () {
        this.containsOneUpperCase = this.regexForUpperCase.test(this.model.password);
        if (!this.containsOneUpperCase) {
            this.validPassword = false;
            this.notificationService.showError("Should contain at least 1 latin uppercase letter!");
        }
    };
    ResetForgotPasswordComponent.prototype.validateSixChars = function () {
        if (!(this.model.password === undefined)) {
            this.containsSixChars = (this.model.password.length >= 6);
        }
        if (this.model.password === undefined) {
            this.containsSixChars = false;
        }
        if (!this.containsSixChars) {
            this.validPassword = false;
            this.notificationService.showError("Should contain at least 6 charters!");
        }
    };
    ResetForgotPasswordComponent.prototype.resetPassword = function () {
        var _this = this;
        this.validPassword = true;
        this.validatePassword();
        this.validateConfirmPassword();
        if (this.validPassword) {
            this.isLoading = true;
            this.accountProvider.resetPassword(this.model).subscribe(function () {
                _this.router.navigateByUrl('pages/login');
            }, function (err) {
                _this.notificationService.showError("Change password failed! Invalid token!");
                _this.isLoading = false;
            });
        }
    };
    ResetForgotPasswordComponent = __decorate([
        core_1.Component({
            templateUrl: 'reset-forgot-password.component.html'
        }),
        __metadata("design:paramtypes", [providers_1.AccountProvider,
            router_1.ActivatedRoute,
            router_1.Router,
            services_1.AccountService,
            noitification_service_1.NotificationService])
    ], ResetForgotPasswordComponent);
    return ResetForgotPasswordComponent;
}());
exports.ResetForgotPasswordComponent = ResetForgotPasswordComponent;
//# sourceMappingURL=reset-forgot-password.component.js.map