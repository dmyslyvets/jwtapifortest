"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var providers_1 = require("app/shared/providers/providers");
var services_1 = require("app/shared/providers/services");
var noitification_service_1 = require("app/shared/providers/services/noitification.service");
var account_1 = require("app/shared/models/account");
var LoginComponent = /** @class */ (function () {
    function LoginComponent(accountService, accountProvider, router, notificationService) {
        this.accountService = accountService;
        this.accountProvider = accountProvider;
        this.router = router;
        this.notificationService = notificationService;
        this.account = new account_1.LoginModel();
        this.isLoading = false;
    }
    LoginComponent.prototype.login = function () {
        var _this = this;
        this.isLoading = true;
        this.accountProvider.login(this.account).subscribe(function (res) {
            _this.accountService.onLogin(res);
            _this.router.navigate(['dashboard']);
        }, function (err) {
            _this.notificationService.showError("Incorrect login or password");
            _this.isLoading = false;
        });
    };
    LoginComponent.prototype.forgotPassword = function () {
        this.router.navigateByUrl('pages/forgot-password');
    };
    LoginComponent = __decorate([
        core_1.Component({
            templateUrl: 'login.component.html',
            styleUrls: ['login.component.scss']
        }),
        __metadata("design:paramtypes", [services_1.AccountService,
            providers_1.AccountProvider,
            router_1.Router,
            noitification_service_1.NotificationService])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=login.component.js.map