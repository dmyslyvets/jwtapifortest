"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
// Toastr
var angular2_toaster_1 = require("angular2-toaster/angular2-toaster");
var toastr_component_1 = require("./toastr.component");
//Routing
var toastr_routing_module_1 = require("./toastr-routing.module");
var ToastrModule = /** @class */ (function () {
    function ToastrModule() {
    }
    ToastrModule = __decorate([
        core_1.NgModule({
            imports: [
                toastr_routing_module_1.ToastrRoutingModule,
                common_1.CommonModule,
                angular2_toaster_1.ToasterModule,
            ],
            declarations: [
                toastr_component_1.ToastrComponent
            ]
        })
    ], ToastrModule);
    return ToastrModule;
}());
exports.ToastrModule = ToastrModule;
//# sourceMappingURL=toastr.module.js.map