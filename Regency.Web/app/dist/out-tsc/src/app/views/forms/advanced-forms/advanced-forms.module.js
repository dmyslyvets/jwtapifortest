"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
// Angular 2 Input Mask
var angular2_text_mask_1 = require("angular2-text-mask");
// Timepicker
var ngx_bootstrap_1 = require("ngx-bootstrap");
// Datepicker
var ngx_bootstrap_2 = require("ngx-bootstrap");
// Ng2-select
var ng_select_1 = require("ng-select");
//Routing
var advanced_forms_routing_module_1 = require("./advanced-forms-routing.module");
var advanced_forms_component_1 = require("./advanced-forms.component");
var AdvancedFormsModule = /** @class */ (function () {
    function AdvancedFormsModule() {
    }
    AdvancedFormsModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                forms_1.FormsModule,
                advanced_forms_routing_module_1.AdvancedFormsRoutingModule,
                angular2_text_mask_1.TextMaskModule,
                ngx_bootstrap_1.TimepickerModule.forRoot(),
                ngx_bootstrap_2.BsDatepickerModule.forRoot(),
                ng_select_1.SelectModule
            ],
            declarations: [
                advanced_forms_component_1.AdvancedFormsComponent
            ]
        })
    ], AdvancedFormsModule);
    return AdvancedFormsModule;
}());
exports.AdvancedFormsModule = AdvancedFormsModule;
//# sourceMappingURL=advanced-forms.module.js.map