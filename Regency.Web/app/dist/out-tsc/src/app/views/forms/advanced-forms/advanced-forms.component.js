"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var AdvancedFormsComponent = /** @class */ (function () {
    function AdvancedFormsComponent() {
        // Angular 2 Input Mask
        this.dateModel = '';
        this.dateMask = [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/];
        this.phoneModel = '';
        this.phoneMask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
        this.taxModel = '';
        this.taxMask = [/\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/];
        this.ssnModel = '';
        this.ssnMask = [/\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
        this.eyeScriptModel = '';
        this.eyeScriptMask = ['~', /\d/, '.', /\d/, /\d/, ' ', '~', /\d/, '.', /\d/, /\d/, ' ', /\d/, /\d/, /\d/];
        this.ccnModel = '';
        this.ccnMask = [/\d/, /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/];
        // Datepicker
        this.minDate = new Date(2017, 5, 10);
        this.maxDate = new Date(2018, 9, 15);
        this.bsValue = new Date();
        this.bsRangeValue = [new Date(2017, 7, 4), new Date(2017, 7, 20)];
        // Timepicker
        this.hstep = 1;
        this.mstep = 15;
        this.ismeridian = true;
        this.isEnabled = true;
        this.mytime = new Date();
        this.options = {
            hstep: [1, 2, 3],
            mstep: [1, 5, 10, 15, 25, 30]
        };
        // ng2-select
        this.countries = [
            { label: 'Belgium', value: 'BE' },
            { label: 'Luxembourg', value: 'LU' },
            { label: 'Netherlands', value: 'NL' }
        ];
        this.selectedCountries = ['BE', 'NL'];
    }
    AdvancedFormsComponent.prototype.toggleMode = function () {
        this.ismeridian = !this.ismeridian;
    };
    ;
    AdvancedFormsComponent.prototype.update = function () {
        var d = new Date();
        d.setHours(14);
        d.setMinutes(0);
        this.mytime = d;
    };
    ;
    AdvancedFormsComponent.prototype.changed = function () {
        console.log('Time changed to: ' + this.mytime);
    };
    ;
    AdvancedFormsComponent.prototype.clear = function () {
        this.mytime = void 0;
    };
    ;
    AdvancedFormsComponent = __decorate([
        core_1.Component({
            templateUrl: 'advanced-forms.component.html',
            styleUrls: [
                '../../../../scss/vendors/bs-datepicker/bs-datepicker.scss',
                '../../../../scss/vendors/ng-select/ng-select.scss'
            ],
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], AdvancedFormsComponent);
    return AdvancedFormsComponent;
}());
exports.AdvancedFormsComponent = AdvancedFormsComponent;
//# sourceMappingURL=advanced-forms.component.js.map