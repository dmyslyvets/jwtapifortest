"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
//Routing
var basic_forms_routing_module_1 = require("./basic-forms-routing.module");
var basic_forms_component_1 = require("./basic-forms.component");
var dropdown_1 = require("ngx-bootstrap/dropdown");
var BasicFormsModule = /** @class */ (function () {
    function BasicFormsModule() {
    }
    BasicFormsModule = __decorate([
        core_1.NgModule({
            imports: [
                basic_forms_routing_module_1.BasicFormsRoutingModule,
                dropdown_1.BsDropdownModule.forRoot(),
                forms_1.FormsModule
            ],
            declarations: [
                basic_forms_component_1.BasicFormsComponent
            ]
        })
    ], BasicFormsModule);
    return BasicFormsModule;
}());
exports.BasicFormsModule = BasicFormsModule;
//# sourceMappingURL=basic-forms.module.js.map