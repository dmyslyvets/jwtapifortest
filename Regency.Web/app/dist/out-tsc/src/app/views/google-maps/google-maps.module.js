"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var core_2 = require("@agm/core");
var google_maps_component_1 = require("./google-maps.component");
var google_maps_routing_module_1 = require("./google-maps-routing.module");
var GoogleMapsModule = /** @class */ (function () {
    function GoogleMapsModule() {
    }
    GoogleMapsModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                forms_1.FormsModule,
                google_maps_routing_module_1.GoogleMapsRoutingModule,
                core_2.AgmCoreModule.forRoot({
                    apiKey: 'AIzaSyASyYRBZmULmrmw_P9kgr7_266OhFNinPA'
                    // To use the Google Maps JavaScript API, you must register your app project on the Google API Console and get a Google API key which you can add to your app
                })
            ],
            providers: [],
            declarations: [google_maps_component_1.GoogleMapsComponent],
            bootstrap: [google_maps_component_1.GoogleMapsComponent]
        })
    ], GoogleMapsModule);
    return GoogleMapsModule;
}());
exports.GoogleMapsModule = GoogleMapsModule;
//# sourceMappingURL=google-maps.module.js.map