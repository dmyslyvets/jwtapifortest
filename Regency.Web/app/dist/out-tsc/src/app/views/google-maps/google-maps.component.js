"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var GoogleMapsComponent = /** @class */ (function () {
    function GoogleMapsComponent() {
        this.title = '';
        this.lat = 37.431489;
        this.lng = -122.163719;
        this.zoom = 11;
        this.markers = [
            {
                lat: 37.431489,
                lng: -122.163719,
                label: 'S',
                draggable: false,
                title: 'Stanford',
                www: 'https://www.stanford.edu/'
            },
            {
                lat: 37.394694,
                lng: -122.150333,
                label: 'T',
                draggable: false,
                title: 'Tesla',
                www: 'https://www.tesla.com/'
            },
            {
                lat: 37.331681,
                lng: -122.030100,
                label: 'A',
                draggable: false,
                title: 'Apple',
                www: 'https://www.apple.com/'
            },
            {
                lat: 37.484722,
                lng: -122.148333,
                label: 'F',
                draggable: false,
                title: 'Facebook',
                www: 'https://www.facebook.com/'
            }
        ];
    }
    GoogleMapsComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            templateUrl: 'google-maps.component.html',
            styleUrls: ['google-maps.component.css'],
        })
    ], GoogleMapsComponent);
    return GoogleMapsComponent;
}());
exports.GoogleMapsComponent = GoogleMapsComponent;
//# sourceMappingURL=google-maps.component.js.map