"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
// Ngx-Quill
var ngx_quill_1 = require("ngx-quill");
//Routing
var text_editors_routing_module_1 = require("./text-editors-routing.module");
var text_editors_component_1 = require("./text-editors.component");
var TextEditorsModule = /** @class */ (function () {
    function TextEditorsModule() {
    }
    TextEditorsModule = __decorate([
        core_1.NgModule({
            imports: [
                forms_1.FormsModule,
                text_editors_routing_module_1.TextEditorsRoutingModule,
                ngx_quill_1.QuillModule
            ],
            declarations: [
                text_editors_component_1.TextEditorsComponent
            ]
        })
    ], TextEditorsModule);
    return TextEditorsModule;
}());
exports.TextEditorsModule = TextEditorsModule;
//# sourceMappingURL=text-editors.module.js.map