"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var text_editors_component_1 = require("./text-editors.component");
var routes = [
    {
        path: '',
        component: text_editors_component_1.TextEditorsComponent,
        data: {
            title: 'Text Editors'
        }
    }
];
var TextEditorsRoutingModule = /** @class */ (function () {
    function TextEditorsRoutingModule() {
    }
    TextEditorsRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forChild(routes)],
            exports: [router_1.RouterModule]
        })
    ], TextEditorsRoutingModule);
    return TextEditorsRoutingModule;
}());
exports.TextEditorsRoutingModule = TextEditorsRoutingModule;
//# sourceMappingURL=text-editors-routing.module.js.map