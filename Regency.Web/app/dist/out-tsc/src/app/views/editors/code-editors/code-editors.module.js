"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
// ng2-ace-editor
var ng2_ace_editor_1 = require("ng2-ace-editor");
//Routing
var code_editors_routing_module_1 = require("./code-editors-routing.module");
var code_editors_component_1 = require("./code-editors.component");
var CodeEditorsModule = /** @class */ (function () {
    function CodeEditorsModule() {
    }
    CodeEditorsModule = __decorate([
        core_1.NgModule({
            imports: [
                forms_1.FormsModule,
                code_editors_routing_module_1.CodeEditorsRoutingModule,
                ng2_ace_editor_1.AceEditorModule
            ],
            declarations: [
                code_editors_component_1.CodeEditorsComponent
            ]
        })
    ], CodeEditorsModule);
    return CodeEditorsModule;
}());
exports.CodeEditorsModule = CodeEditorsModule;
//# sourceMappingURL=code-editors.module.js.map