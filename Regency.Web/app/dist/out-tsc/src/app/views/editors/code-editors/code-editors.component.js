"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
require("brace/index");
require("brace/theme/eclipse");
require("brace/theme/monokai");
require("brace/mode/markdown");
require("brace/mode/javascript");
require("brace/ext/language_tools.js");
// declare const ace: any;
var defaults = {
    markdown: '# Ace \n(Ajax.org Cloud9 Editor)  \n' +
        '============================  \n' +
        'Ace is a standalone `code editor` written in **JavaScript**. \n' +
        '- Our goal is to create a _browser based_ editor that matches and extends the features, usability and performance of existing native editors such as TextMate, Vim or Eclipse.  \n' +
        '- It can be easily embedded in any web page or JavaScript application.  \n' +
        '- Ace is developed as the primary editor for [Cloud9 IDE](http://www.cloud9ide.com/) and the successor of the Mozilla Skywriter (Bespin) Project.  \n',
    javascript: 'const component = {\n\tname: "ng2-ace-editor",\n\tauthor: "François-Xavier Montigny",\n\trepo: "https://github.com/fxmontigny/ng2-ace-editor"\n};',
    html: '<h1>I ♥ ng2-ace-editor</h1>'
};
var CodeEditorsComponent = /** @class */ (function () {
    function CodeEditorsComponent() {
        this.text = defaults.markdown;
        this.options = {
            maxLines: 1000,
            printMargin: false,
            wrap: true
        };
    }
    CodeEditorsComponent.prototype.ngAfterViewInit = function () {
        this.editor.setMode('markdown');
        this.editor.setTheme('monokai');
    };
    CodeEditorsComponent.prototype.onThemeChange = function (e) {
        var theme = e.target.value;
        this.editor.setTheme(theme);
    };
    CodeEditorsComponent.prototype.onModeChange = function (e) {
        var mode = e.target.value;
        this.text = defaults[mode];
        this.editor.setMode(mode);
    };
    __decorate([
        core_1.ViewChild('editor'),
        __metadata("design:type", Object)
    ], CodeEditorsComponent.prototype, "editor", void 0);
    CodeEditorsComponent = __decorate([
        core_1.Component({
            templateUrl: 'code-editors.component.html'
        })
    ], CodeEditorsComponent);
    return CodeEditorsComponent;
}());
exports.CodeEditorsComponent = CodeEditorsComponent;
//# sourceMappingURL=code-editors.component.js.map