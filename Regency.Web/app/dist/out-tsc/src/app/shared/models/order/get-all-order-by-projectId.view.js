"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GetAllOrdersByProjectIdView = /** @class */ (function () {
    function GetAllOrdersByProjectIdView() {
        this.orders = [];
    }
    return GetAllOrdersByProjectIdView;
}());
exports.GetAllOrdersByProjectIdView = GetAllOrdersByProjectIdView;
var GetAllOrdersByProjectIdViewItem = /** @class */ (function () {
    function GetAllOrdersByProjectIdViewItem() {
    }
    return GetAllOrdersByProjectIdViewItem;
}());
exports.GetAllOrdersByProjectIdViewItem = GetAllOrdersByProjectIdViewItem;
var GetAllOrdersByProjectIdProjectViewItem = /** @class */ (function () {
    function GetAllOrdersByProjectIdProjectViewItem() {
    }
    return GetAllOrdersByProjectIdProjectViewItem;
}());
exports.GetAllOrdersByProjectIdProjectViewItem = GetAllOrdersByProjectIdProjectViewItem;
var GetAllOrderByProjectIdUserViewItem = /** @class */ (function () {
    function GetAllOrderByProjectIdUserViewItem() {
    }
    return GetAllOrderByProjectIdUserViewItem;
}());
exports.GetAllOrderByProjectIdUserViewItem = GetAllOrderByProjectIdUserViewItem;
var GetAllOrdersByProjectIdCustomerViewItem = /** @class */ (function () {
    function GetAllOrdersByProjectIdCustomerViewItem() {
    }
    return GetAllOrdersByProjectIdCustomerViewItem;
}());
exports.GetAllOrdersByProjectIdCustomerViewItem = GetAllOrdersByProjectIdCustomerViewItem;
var GetAllOrdersByProjectIdItemViewItem = /** @class */ (function () {
    function GetAllOrdersByProjectIdItemViewItem() {
    }
    return GetAllOrdersByProjectIdItemViewItem;
}());
exports.GetAllOrdersByProjectIdItemViewItem = GetAllOrdersByProjectIdItemViewItem;
//# sourceMappingURL=get-all-order-by-projectId.view.js.map