"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GetAllTeamsView = /** @class */ (function () {
    function GetAllTeamsView() {
        this.teams = [];
    }
    return GetAllTeamsView;
}());
exports.GetAllTeamsView = GetAllTeamsView;
var GetAllTeamViewItem = /** @class */ (function () {
    function GetAllTeamViewItem() {
    }
    return GetAllTeamViewItem;
}());
exports.GetAllTeamViewItem = GetAllTeamViewItem;
//# sourceMappingURL=get-all-team.model.js.map