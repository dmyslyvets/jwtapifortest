"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GetAllProjectTypesView = /** @class */ (function () {
    function GetAllProjectTypesView() {
        this.projectTypes = [];
    }
    return GetAllProjectTypesView;
}());
exports.GetAllProjectTypesView = GetAllProjectTypesView;
var GetAllProjectTypesViewItem = /** @class */ (function () {
    function GetAllProjectTypesViewItem() {
    }
    return GetAllProjectTypesViewItem;
}());
exports.GetAllProjectTypesViewItem = GetAllProjectTypesViewItem;
//# sourceMappingURL=get-all-project-types.view.js.map