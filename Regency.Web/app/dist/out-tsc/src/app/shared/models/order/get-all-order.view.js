"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GetAllOrderView = /** @class */ (function () {
    function GetAllOrderView() {
        this.orders = [];
    }
    return GetAllOrderView;
}());
exports.GetAllOrderView = GetAllOrderView;
var GetAllOrderViewItem = /** @class */ (function () {
    function GetAllOrderViewItem() {
    }
    return GetAllOrderViewItem;
}());
exports.GetAllOrderViewItem = GetAllOrderViewItem;
var GetAllOrderProjectViewItem = /** @class */ (function () {
    function GetAllOrderProjectViewItem() {
    }
    return GetAllOrderProjectViewItem;
}());
exports.GetAllOrderProjectViewItem = GetAllOrderProjectViewItem;
var GetUserViewItem = /** @class */ (function () {
    function GetUserViewItem() {
    }
    return GetUserViewItem;
}());
exports.GetUserViewItem = GetUserViewItem;
var GetAllOrderCustomerViewItem = /** @class */ (function () {
    function GetAllOrderCustomerViewItem() {
    }
    return GetAllOrderCustomerViewItem;
}());
exports.GetAllOrderCustomerViewItem = GetAllOrderCustomerViewItem;
var GetAllOrderItemViewItem = /** @class */ (function () {
    function GetAllOrderItemViewItem() {
    }
    return GetAllOrderItemViewItem;
}());
exports.GetAllOrderItemViewItem = GetAllOrderItemViewItem;
//# sourceMappingURL=get-all-order.view.js.map