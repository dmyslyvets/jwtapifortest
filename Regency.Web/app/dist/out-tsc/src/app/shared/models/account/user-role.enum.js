"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var UserRole;
(function (UserRole) {
    UserRole[UserRole["None"] = 0] = "None";
    UserRole[UserRole["Admin"] = 1] = "Admin";
    UserRole[UserRole["User"] = 2] = "User";
})(UserRole = exports.UserRole || (exports.UserRole = {}));
//# sourceMappingURL=user-role.enum.js.map