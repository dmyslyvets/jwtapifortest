"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var OrderStatus;
(function (OrderStatus) {
    OrderStatus[OrderStatus["None"] = 0] = "None";
    OrderStatus[OrderStatus["New"] = 1] = "New";
    OrderStatus[OrderStatus["Active"] = 2] = "Active";
    OrderStatus[OrderStatus["Processing"] = 3] = "Processing";
    OrderStatus[OrderStatus["Done"] = 4] = "Done";
})(OrderStatus = exports.OrderStatus || (exports.OrderStatus = {}));
//# sourceMappingURL=order-status.enum.js.map