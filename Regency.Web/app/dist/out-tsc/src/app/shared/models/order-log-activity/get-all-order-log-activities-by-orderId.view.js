"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GetAllOrderLogActivitiesByOrderIdView = /** @class */ (function () {
    function GetAllOrderLogActivitiesByOrderIdView() {
        this.orderLogActivities = [];
    }
    return GetAllOrderLogActivitiesByOrderIdView;
}());
exports.GetAllOrderLogActivitiesByOrderIdView = GetAllOrderLogActivitiesByOrderIdView;
var GetAllOrderLogActivitiesByOrderIdViewItem = /** @class */ (function () {
    function GetAllOrderLogActivitiesByOrderIdViewItem() {
    }
    return GetAllOrderLogActivitiesByOrderIdViewItem;
}());
exports.GetAllOrderLogActivitiesByOrderIdViewItem = GetAllOrderLogActivitiesByOrderIdViewItem;
//# sourceMappingURL=get-all-order-log-activities-by-orderId.view.js.map