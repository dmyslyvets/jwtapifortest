"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GetAllActiveProjectView = /** @class */ (function () {
    function GetAllActiveProjectView() {
        this.projects = [];
    }
    return GetAllActiveProjectView;
}());
exports.GetAllActiveProjectView = GetAllActiveProjectView;
var GetAllActiveProjectViewItem = /** @class */ (function () {
    function GetAllActiveProjectViewItem() {
    }
    return GetAllActiveProjectViewItem;
}());
exports.GetAllActiveProjectViewItem = GetAllActiveProjectViewItem;
//# sourceMappingURL=get-all-active-project.view.js.map