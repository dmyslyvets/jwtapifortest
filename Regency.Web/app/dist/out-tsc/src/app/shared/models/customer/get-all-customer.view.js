"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GetAllCustomerView = /** @class */ (function () {
    function GetAllCustomerView() {
        this.customers = [];
    }
    return GetAllCustomerView;
}());
exports.GetAllCustomerView = GetAllCustomerView;
var GetAllCustomerViewItem = /** @class */ (function () {
    function GetAllCustomerViewItem() {
    }
    return GetAllCustomerViewItem;
}());
exports.GetAllCustomerViewItem = GetAllCustomerViewItem;
//# sourceMappingURL=get-all-customer.view.js.map