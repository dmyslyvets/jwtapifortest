"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GetOrderView = /** @class */ (function () {
    function GetOrderView() {
        this.confirmedCustomerQuote = new GetOrderOrderCustomerQuoteViewItem();
        this.confirmedVendorQuote = new GetOrderOrderVendorQuoteViewItem();
        this.item = new GetOrderItemViewItem();
        this.orderItemSpecs = [];
        this.orderCustomerQuotes = [];
        this.orderVendorQuotes = [];
    }
    return GetOrderView;
}());
exports.GetOrderView = GetOrderView;
var GetOrderOrderVendorViewItem = /** @class */ (function () {
    function GetOrderOrderVendorViewItem() {
    }
    return GetOrderOrderVendorViewItem;
}());
exports.GetOrderOrderVendorViewItem = GetOrderOrderVendorViewItem;
var GetOrderOrderVendorQuoteViewItem = /** @class */ (function () {
    function GetOrderOrderVendorQuoteViewItem() {
        this.orderCustomerQuoteItemSpecs = [];
        this.vendor = new GetOrderOrderVendorViewItem();
    }
    return GetOrderOrderVendorQuoteViewItem;
}());
exports.GetOrderOrderVendorQuoteViewItem = GetOrderOrderVendorQuoteViewItem;
var GetOrderOrderVendorQuoteItemSpecViewItem = /** @class */ (function () {
    function GetOrderOrderVendorQuoteItemSpecViewItem() {
    }
    return GetOrderOrderVendorQuoteItemSpecViewItem;
}());
exports.GetOrderOrderVendorQuoteItemSpecViewItem = GetOrderOrderVendorQuoteItemSpecViewItem;
var GetOrderOrderCustomerQuoteViewItem = /** @class */ (function () {
    function GetOrderOrderCustomerQuoteViewItem() {
        this.orderCustomerQuoteItemSpecs = [];
    }
    return GetOrderOrderCustomerQuoteViewItem;
}());
exports.GetOrderOrderCustomerQuoteViewItem = GetOrderOrderCustomerQuoteViewItem;
var GetOrderOrderCustomerQuoteItemSpecViewItem = /** @class */ (function () {
    function GetOrderOrderCustomerQuoteItemSpecViewItem() {
    }
    return GetOrderOrderCustomerQuoteItemSpecViewItem;
}());
exports.GetOrderOrderCustomerQuoteItemSpecViewItem = GetOrderOrderCustomerQuoteItemSpecViewItem;
var GetOrderProjectViewItem = /** @class */ (function () {
    function GetOrderProjectViewItem() {
    }
    return GetOrderProjectViewItem;
}());
exports.GetOrderProjectViewItem = GetOrderProjectViewItem;
var GetOrderUserViewItem = /** @class */ (function () {
    function GetOrderUserViewItem() {
    }
    return GetOrderUserViewItem;
}());
exports.GetOrderUserViewItem = GetOrderUserViewItem;
var GetOrderCustomerViewItem = /** @class */ (function () {
    function GetOrderCustomerViewItem() {
    }
    return GetOrderCustomerViewItem;
}());
exports.GetOrderCustomerViewItem = GetOrderCustomerViewItem;
var GetOrderItemViewItem = /** @class */ (function () {
    function GetOrderItemViewItem() {
    }
    return GetOrderItemViewItem;
}());
exports.GetOrderItemViewItem = GetOrderItemViewItem;
var GetOrderOrderItemSpecViewItem = /** @class */ (function () {
    function GetOrderOrderItemSpecViewItem() {
    }
    return GetOrderOrderItemSpecViewItem;
}());
exports.GetOrderOrderItemSpecViewItem = GetOrderOrderItemSpecViewItem;
//# sourceMappingURL=get-order-view.js.map