"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CustomerStatus;
(function (CustomerStatus) {
    CustomerStatus[CustomerStatus["None"] = 0] = "None";
    CustomerStatus[CustomerStatus["Active"] = 1] = "Active";
    CustomerStatus[CustomerStatus["Inactive"] = 2] = "Inactive";
})(CustomerStatus = exports.CustomerStatus || (exports.CustomerStatus = {}));
//# sourceMappingURL=customer-status.enum.js.map