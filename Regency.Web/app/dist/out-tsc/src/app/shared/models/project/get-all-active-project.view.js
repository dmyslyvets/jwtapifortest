"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GetAllActiveProjectView = /** @class */ (function () {
    function GetAllActiveProjectView() {
        this.projects = [];
    }
    return GetAllActiveProjectView;
}());
exports.GetAllActiveProjectView = GetAllActiveProjectView;
var GetAllActiveProjectViewItem = /** @class */ (function () {
    function GetAllActiveProjectViewItem() {
    }
    return GetAllActiveProjectViewItem;
}());
exports.GetAllActiveProjectViewItem = GetAllActiveProjectViewItem;
var GetAllActiveProjectProjectTypeViewItem = /** @class */ (function () {
    function GetAllActiveProjectProjectTypeViewItem() {
    }
    return GetAllActiveProjectProjectTypeViewItem;
}());
exports.GetAllActiveProjectProjectTypeViewItem = GetAllActiveProjectProjectTypeViewItem;
var GetAllActiveProjectUserViewItem = /** @class */ (function () {
    function GetAllActiveProjectUserViewItem() {
    }
    return GetAllActiveProjectUserViewItem;
}());
exports.GetAllActiveProjectUserViewItem = GetAllActiveProjectUserViewItem;
var GetAllActiveProjectCustomerViewItem = /** @class */ (function () {
    function GetAllActiveProjectCustomerViewItem() {
    }
    return GetAllActiveProjectCustomerViewItem;
}());
exports.GetAllActiveProjectCustomerViewItem = GetAllActiveProjectCustomerViewItem;
//# sourceMappingURL=get-all-active-project.view.js.map