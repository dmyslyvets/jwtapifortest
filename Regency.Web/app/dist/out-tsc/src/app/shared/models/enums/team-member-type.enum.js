"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TeamMemberType;
(function (TeamMemberType) {
    TeamMemberType[TeamMemberType["Manager"] = 0] = "Manager";
    TeamMemberType[TeamMemberType["Member"] = 1] = "Member";
})(TeamMemberType = exports.TeamMemberType || (exports.TeamMemberType = {}));
//# sourceMappingURL=team-member-type.enum.js.map