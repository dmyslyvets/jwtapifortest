"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ProjectStatus;
(function (ProjectStatus) {
    ProjectStatus[ProjectStatus["None"] = 0] = "None";
    ProjectStatus[ProjectStatus["Active"] = 1] = "Active";
    ProjectStatus[ProjectStatus["Inactive"] = 2] = "Inactive";
})(ProjectStatus = exports.ProjectStatus || (exports.ProjectStatus = {}));
//# sourceMappingURL=project-status.enum.js.map