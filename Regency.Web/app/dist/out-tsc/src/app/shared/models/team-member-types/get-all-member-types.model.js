"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GetAllTeamMemberTypesView = /** @class */ (function () {
    function GetAllTeamMemberTypesView() {
        this.teamMemberTypes = [];
    }
    return GetAllTeamMemberTypesView;
}());
exports.GetAllTeamMemberTypesView = GetAllTeamMemberTypesView;
var GetAllTeamMemberTypeItem = /** @class */ (function () {
    function GetAllTeamMemberTypeItem() {
    }
    return GetAllTeamMemberTypeItem;
}());
exports.GetAllTeamMemberTypeItem = GetAllTeamMemberTypeItem;
//# sourceMappingURL=get-all-member-types.model.js.map