"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GetAllUserViewModel = /** @class */ (function () {
    function GetAllUserViewModel() {
        this.users = [];
    }
    return GetAllUserViewModel;
}());
exports.GetAllUserViewModel = GetAllUserViewModel;
var GetAllUserViewItem = /** @class */ (function () {
    function GetAllUserViewItem() {
    }
    return GetAllUserViewItem;
}());
exports.GetAllUserViewItem = GetAllUserViewItem;
//# sourceMappingURL=get-all-users.model.js.map