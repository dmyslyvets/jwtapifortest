"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var UserStatus;
(function (UserStatus) {
    UserStatus[UserStatus["None"] = 0] = "None";
    UserStatus[UserStatus["Active"] = 1] = "Active";
    UserStatus[UserStatus["Inactive"] = 2] = "Inactive";
})(UserStatus = exports.UserStatus || (exports.UserStatus = {}));
//# sourceMappingURL=uset-status.enum.js.map