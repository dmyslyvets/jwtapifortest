"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GetActiveTeamsView = /** @class */ (function () {
    function GetActiveTeamsView() {
        this.teams = [];
    }
    return GetActiveTeamsView;
}());
exports.GetActiveTeamsView = GetActiveTeamsView;
var GetActiveTeamViewItem = /** @class */ (function () {
    function GetActiveTeamViewItem() {
    }
    return GetActiveTeamViewItem;
}());
exports.GetActiveTeamViewItem = GetActiveTeamViewItem;
//# sourceMappingURL=get-active-team.view.js.map