"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GetTeamMembersByTeamIdView = /** @class */ (function () {
    function GetTeamMembersByTeamIdView() {
        this.teamMembers = [];
    }
    return GetTeamMembersByTeamIdView;
}());
exports.GetTeamMembersByTeamIdView = GetTeamMembersByTeamIdView;
var GetTeamMemberByTeamIDViewItem = /** @class */ (function () {
    function GetTeamMemberByTeamIDViewItem() {
    }
    return GetTeamMemberByTeamIDViewItem;
}());
exports.GetTeamMemberByTeamIDViewItem = GetTeamMemberByTeamIDViewItem;
var GetTeamMemberTypeByTeamIdViewItem = /** @class */ (function () {
    function GetTeamMemberTypeByTeamIdViewItem() {
    }
    return GetTeamMemberTypeByTeamIdViewItem;
}());
exports.GetTeamMemberTypeByTeamIdViewItem = GetTeamMemberTypeByTeamIdViewItem;
//# sourceMappingURL=get-team-members-by-teamId.view.js.map