"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GetAllOrderTasksByOrderIdView = /** @class */ (function () {
    function GetAllOrderTasksByOrderIdView() {
        this.orderTasks = [];
    }
    return GetAllOrderTasksByOrderIdView;
}());
exports.GetAllOrderTasksByOrderIdView = GetAllOrderTasksByOrderIdView;
var GetAllOrderTasksByOrderIdGroupByTeamNameView = /** @class */ (function () {
    function GetAllOrderTasksByOrderIdGroupByTeamNameView() {
        this.orderTasksGroupByName = [];
    }
    return GetAllOrderTasksByOrderIdGroupByTeamNameView;
}());
exports.GetAllOrderTasksByOrderIdGroupByTeamNameView = GetAllOrderTasksByOrderIdGroupByTeamNameView;
var GetAllOrderTasksByOrderIdGroupByDueTimeView = /** @class */ (function () {
    function GetAllOrderTasksByOrderIdGroupByDueTimeView() {
        this.orderTasksGroupByDueTime = [];
    }
    return GetAllOrderTasksByOrderIdGroupByDueTimeView;
}());
exports.GetAllOrderTasksByOrderIdGroupByDueTimeView = GetAllOrderTasksByOrderIdGroupByDueTimeView;
var OrderTaskGetAllOrderTasksByOrderIdViewItem = /** @class */ (function () {
    function OrderTaskGetAllOrderTasksByOrderIdViewItem() {
    }
    return OrderTaskGetAllOrderTasksByOrderIdViewItem;
}());
exports.OrderTaskGetAllOrderTasksByOrderIdViewItem = OrderTaskGetAllOrderTasksByOrderIdViewItem;
var OrderGetAllOrderTasksByOrderIdViewItem = /** @class */ (function () {
    function OrderGetAllOrderTasksByOrderIdViewItem() {
    }
    return OrderGetAllOrderTasksByOrderIdViewItem;
}());
exports.OrderGetAllOrderTasksByOrderIdViewItem = OrderGetAllOrderTasksByOrderIdViewItem;
var TeamGetAllOrderTasksByOrderIdViewItem = /** @class */ (function () {
    function TeamGetAllOrderTasksByOrderIdViewItem() {
    }
    return TeamGetAllOrderTasksByOrderIdViewItem;
}());
exports.TeamGetAllOrderTasksByOrderIdViewItem = TeamGetAllOrderTasksByOrderIdViewItem;
var UserGetAllOrderTasksByOrderIdViewItem = /** @class */ (function () {
    function UserGetAllOrderTasksByOrderIdViewItem() {
    }
    return UserGetAllOrderTasksByOrderIdViewItem;
}());
exports.UserGetAllOrderTasksByOrderIdViewItem = UserGetAllOrderTasksByOrderIdViewItem;
var MyTasksGetAllOrderTasksByOrderIdViewItem = /** @class */ (function () {
    function MyTasksGetAllOrderTasksByOrderIdViewItem() {
    }
    return MyTasksGetAllOrderTasksByOrderIdViewItem;
}());
exports.MyTasksGetAllOrderTasksByOrderIdViewItem = MyTasksGetAllOrderTasksByOrderIdViewItem;
var TaskStatusGetAllOrderTasksByOrderIdViewItem = /** @class */ (function () {
    function TaskStatusGetAllOrderTasksByOrderIdViewItem() {
    }
    return TaskStatusGetAllOrderTasksByOrderIdViewItem;
}());
exports.TaskStatusGetAllOrderTasksByOrderIdViewItem = TaskStatusGetAllOrderTasksByOrderIdViewItem;
var ProjectOrderGetAllOrderTasksByOrderIdViewItem = /** @class */ (function () {
    function ProjectOrderGetAllOrderTasksByOrderIdViewItem() {
    }
    return ProjectOrderGetAllOrderTasksByOrderIdViewItem;
}());
exports.ProjectOrderGetAllOrderTasksByOrderIdViewItem = ProjectOrderGetAllOrderTasksByOrderIdViewItem;
//# sourceMappingURL=get-all-order-tasks-by-order-id.view.js.map