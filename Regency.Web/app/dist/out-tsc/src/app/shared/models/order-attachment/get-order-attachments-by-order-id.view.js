"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GetOrderAttachmentsByOrderIdView = /** @class */ (function () {
    function GetOrderAttachmentsByOrderIdView() {
        this.orderAttachments = [];
    }
    return GetOrderAttachmentsByOrderIdView;
}());
exports.GetOrderAttachmentsByOrderIdView = GetOrderAttachmentsByOrderIdView;
var GetOrderAttachmentsByOrderIdViewItem = /** @class */ (function () {
    function GetOrderAttachmentsByOrderIdViewItem() {
    }
    return GetOrderAttachmentsByOrderIdViewItem;
}());
exports.GetOrderAttachmentsByOrderIdViewItem = GetOrderAttachmentsByOrderIdViewItem;
//# sourceMappingURL=get-order-attachments-by-order-id.view.js.map