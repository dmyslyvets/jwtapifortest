"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GetTeamsView = /** @class */ (function () {
    function GetTeamsView() {
        this.teams = [];
    }
    return GetTeamsView;
}());
exports.GetTeamsView = GetTeamsView;
var GetTeamViewItem = /** @class */ (function () {
    function GetTeamViewItem() {
    }
    return GetTeamViewItem;
}());
exports.GetTeamViewItem = GetTeamViewItem;
//# sourceMappingURL=get-teams.model.js.map