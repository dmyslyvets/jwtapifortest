"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GetAllProjectView = /** @class */ (function () {
    function GetAllProjectView() {
        this.projects = [];
    }
    return GetAllProjectView;
}());
exports.GetAllProjectView = GetAllProjectView;
var GetAllProjectViewItem = /** @class */ (function () {
    function GetAllProjectViewItem() {
    }
    return GetAllProjectViewItem;
}());
exports.GetAllProjectViewItem = GetAllProjectViewItem;
var GetAllProjectUserViewItem = /** @class */ (function () {
    function GetAllProjectUserViewItem() {
    }
    return GetAllProjectUserViewItem;
}());
exports.GetAllProjectUserViewItem = GetAllProjectUserViewItem;
var GetAllProjectCustomerViewItem = /** @class */ (function () {
    function GetAllProjectCustomerViewItem() {
    }
    return GetAllProjectCustomerViewItem;
}());
exports.GetAllProjectCustomerViewItem = GetAllProjectCustomerViewItem;
//# sourceMappingURL=get-all-project.view.js.map