"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GetAllUserView = /** @class */ (function () {
    function GetAllUserView() {
        this.users = [];
    }
    return GetAllUserView;
}());
exports.GetAllUserView = GetAllUserView;
var GetAllUserViewItem = /** @class */ (function () {
    function GetAllUserViewItem() {
    }
    return GetAllUserViewItem;
}());
exports.GetAllUserViewItem = GetAllUserViewItem;
//# sourceMappingURL=get-all-users.view.js.map