"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GetWithOpenedOrdersProjectView = /** @class */ (function () {
    function GetWithOpenedOrdersProjectView() {
        this.projects = [];
    }
    return GetWithOpenedOrdersProjectView;
}());
exports.GetWithOpenedOrdersProjectView = GetWithOpenedOrdersProjectView;
var GetWithOpenedOrdersProjectViewItem = /** @class */ (function () {
    function GetWithOpenedOrdersProjectViewItem() {
    }
    return GetWithOpenedOrdersProjectViewItem;
}());
exports.GetWithOpenedOrdersProjectViewItem = GetWithOpenedOrdersProjectViewItem;
var GetWithOpenedOrdersProjectProjectTypeViewItem = /** @class */ (function () {
    function GetWithOpenedOrdersProjectProjectTypeViewItem() {
    }
    return GetWithOpenedOrdersProjectProjectTypeViewItem;
}());
exports.GetWithOpenedOrdersProjectProjectTypeViewItem = GetWithOpenedOrdersProjectProjectTypeViewItem;
var GetWithOpenedOrdersProjectUserViewItem = /** @class */ (function () {
    function GetWithOpenedOrdersProjectUserViewItem() {
    }
    return GetWithOpenedOrdersProjectUserViewItem;
}());
exports.GetWithOpenedOrdersProjectUserViewItem = GetWithOpenedOrdersProjectUserViewItem;
var GetWithOpenedOrdersProjectCustomerViewItem = /** @class */ (function () {
    function GetWithOpenedOrdersProjectCustomerViewItem() {
    }
    return GetWithOpenedOrdersProjectCustomerViewItem;
}());
exports.GetWithOpenedOrdersProjectCustomerViewItem = GetWithOpenedOrdersProjectCustomerViewItem;
//# sourceMappingURL=get-with-opened-orders.view.js.map