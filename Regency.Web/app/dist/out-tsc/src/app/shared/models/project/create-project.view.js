"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CreateProjectView = /** @class */ (function () {
    function CreateProjectView() {
        this.projectType = new CreateProjectProjectTypeViewItem();
    }
    return CreateProjectView;
}());
exports.CreateProjectView = CreateProjectView;
var CreateProjectProjectTypeViewItem = /** @class */ (function () {
    function CreateProjectProjectTypeViewItem() {
    }
    return CreateProjectProjectTypeViewItem;
}());
exports.CreateProjectProjectTypeViewItem = CreateProjectProjectTypeViewItem;
//# sourceMappingURL=create-project.view.js.map