"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GetProjectView = /** @class */ (function () {
    function GetProjectView() {
    }
    return GetProjectView;
}());
exports.GetProjectView = GetProjectView;
var GetProjectProjectTypeViewItem = /** @class */ (function () {
    function GetProjectProjectTypeViewItem() {
    }
    return GetProjectProjectTypeViewItem;
}());
exports.GetProjectProjectTypeViewItem = GetProjectProjectTypeViewItem;
var GetProjectUserViewItem = /** @class */ (function () {
    function GetProjectUserViewItem() {
    }
    return GetProjectUserViewItem;
}());
exports.GetProjectUserViewItem = GetProjectUserViewItem;
var GetProjectCustomerViewItem = /** @class */ (function () {
    function GetProjectCustomerViewItem() {
    }
    return GetProjectCustomerViewItem;
}());
exports.GetProjectCustomerViewItem = GetProjectCustomerViewItem;
//# sourceMappingURL=get-project.view.js.map