"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ManagementConstans = /** @class */ (function () {
    function ManagementConstans() {
        this.showMoreMessage = "Show More";
        this.showLessMessage = "Show Less";
        this.passwordChangeMessage = "Password change";
        this.teamMemberTypeDeletedMessage = "Member type was deleted";
        this.userAddedMessage = "User added";
        this.teamAddedMessage = "Team added";
        this.teamMemberAddedMessage = "Team member added";
        this.userToTeamAddedMessage = "User to team added";
        this.teamMemberTypeAddedMessage = "Team member type added";
        this.teamDeleteMessage = "Team deleted";
        this.imageDeleteMessage = "Image deleted";
        this.memberTypeDeleteMessage = "Team member type delete";
        this.wrongNumberFormatMessage = "Wrong number format";
        this.teamMemberUpdateMessage = "Team member update";
        this.invalidEmailMessage = "Invalid Email";
        this.invalidFirstNameMessage = "Invalid First Name";
        this.invalidLastNameMessage = "Invalid Last Name";
        this.teamsLabel = "Teams";
        this.usersLabel = "Users";
        this.emailRegex = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
        this.phoneRegex = new RegExp(/^([+]39)?((3[\d]{2})([ ,\-,\/]){0,1}([\d, ]{6,9}))|(((0[\d]{1,4}))([ ,\-,\/]){0,1}([\d, ]{5,10}))$/);
    }
    ManagementConstans = __decorate([
        core_1.Injectable()
    ], ManagementConstans);
    return ManagementConstans;
}());
exports.ManagementConstans = ManagementConstans;
//# sourceMappingURL=management.constans.js.map