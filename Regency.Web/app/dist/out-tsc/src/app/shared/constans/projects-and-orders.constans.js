"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ProjectsAndOrdersConstans = /** @class */ (function () {
    function ProjectsAndOrdersConstans() {
        this.cantCreateProjectWitoutDate = "Can't create project without date";
        this.addProjectMessage = "Project is added";
        this.projectUpdateMessage = "Project is update";
        this.projectManagerUpdateMessage = "Project Manager is updated";
        this.projectCreateMessage = "Project create success";
        this.saveProjectErrorMessage = "Can't save project without name";
        this.cantCreateProjectWithoutCustomerMessage = "Can't create project without customer";
        this.cantCreateProjectWithoutNameMessage = "Can't create project without name";
        this.cantCreateProjectWithoutPMMessage = "Can't create project without project manager";
        this.updateConfirmedOrderVenorQuoteMessage = "Confirmed order vendor quote was successfully updated";
        this.orderUploadMessage = "Order update success";
        this.orderUpdateMessage = "Order update success";
        this.fileUploadeMessage = "File is upload";
        this.fileExtensionNotAllowedMessage = "File with this extension not allowed";
        this.atachmentDeleteMessage = "Atachment delete";
        this.URL = " ";
        this.allowableExtensions = ["pdf", "jpg", "png", "docs", "xlsx", "txt", "msg"];
        this.allowableImageExtensions = [".jpg", ".png"];
        this.status = {
            new: "New",
            active: "Active",
            processing: "Processing",
            done: "Done"
        };
        this.kanbanBlockStatus = {
            new: "cdk-drop-list-0",
            active: "cdk-drop-list-1",
            processing: "cdk-drop-list-2",
            done: "cdk-drop-list-3"
        };
        this.tabsData = {
            projects: {
                index: 0,
                label: "Projects",
                routeLabel: "/newProjects"
            },
            ordersKanban: {
                index: 1,
                label: "Orders",
                routeLabel: "/ordersKanban"
            },
            ordersList: {
                index: 1,
                label: "Orders",
                routeLabel: "/ordersList"
            },
            tasksKanban: {
                index: 2,
                label: "Tasks",
                routeLabel: "/tasksKanban"
            },
            tasksList: {
                index: 2,
                label: "Tasks",
                routeLabel: "/tasksList"
            },
            newProjects: {
                index: 3,
                label: "New Projects",
                routeLabel: "/newProjects"
            }
        };
        this.attacmentsData = {
            pdf: {
                type: 'application/pdf',
                extension: ".pdf"
            },
            imageJpeg: {
                type: 'image/jpeg',
                extension: ".jpg"
            },
            imagePng: {
                type: 'image/png',
                extension: ".png"
            },
            text: {
                type: 'text/txt',
                extension: ".txt"
            },
            docx: {
                type: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                extension: ".docx"
            },
            xlsx: {
                type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                extension: ".xlsx"
            },
            msg: {
                type: 'application/octet-stream',
                extension: ".msg"
            },
            zip: {
                type: 'application/zip',
                filename: 'Attachments.zip'
            }
        };
    }
    ProjectsAndOrdersConstans = __decorate([
        core_1.Injectable()
    ], ProjectsAndOrdersConstans);
    return ProjectsAndOrdersConstans;
}());
exports.ProjectsAndOrdersConstans = ProjectsAndOrdersConstans;
//# sourceMappingURL=projects-and-orders.constans.js.map