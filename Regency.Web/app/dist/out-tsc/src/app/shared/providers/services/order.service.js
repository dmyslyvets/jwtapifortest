"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var environment_1 = require("environments/environment");
var http_1 = require("@angular/common/http");
var OrderService = /** @class */ (function () {
    function OrderService(http) {
        this.http = http;
        this.apiControllerURL = environment_1.environment.apiUrl;
    }
    OrderService.prototype.getAllOrders = function () {
        return this.http.get(this.apiControllerURL + "api/Order/GetAll");
    };
    OrderService.prototype.getAllOrdersByProjectId = function (projectId) {
        return this.http.get(this.apiControllerURL + ("api/Order/GetAllOrdersByProjectId/?id=" + projectId));
    };
    OrderService.prototype.getOrderDetails = function (orderId) {
        return this.http.get(this.apiControllerURL + ("api/Order/Get/?id=" + orderId));
    };
    OrderService.prototype.updateOrder = function (updateOrder) {
        return this.http.post(this.apiControllerURL + "api/Order/Update", updateOrder);
    };
    OrderService.prototype.updateConfirmedOrderVendoQuote = function (view) {
        return this.http.post(this.apiControllerURL + "api/Order/UpdateConfirmedOrderVendorQuote", view);
    };
    OrderService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], OrderService);
    return OrderService;
}());
exports.OrderService = OrderService;
//# sourceMappingURL=order.service.js.map