"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
require("rxjs/add/operator/do");
require("rxjs/add/observable/throw");
var services_1 = require("../services");
var AuthInterceptor = /** @class */ (function () {
    function AuthInterceptor(accountService) {
        this.accountService = accountService;
    }
    AuthInterceptor.prototype.intercept = function (request, next) {
        var _this = this;
        if (!request.url.includes('api/account/')) {
            var token = this.accountService.getToken();
            request = request.clone({
                setHeaders: {
                    Authorization: "Bearer " + token
                }
            });
        }
        return next.handle(request).do(function (event) { }, function (err) {
            console.log("From interseptor");
            console.log(err);
            if (err instanceof http_1.HttpErrorResponse) {
                // do error handling here
                if (err.status === 401) {
                    _this.accountService.logout();
                    return;
                }
            }
            return err;
        });
    };
    AuthInterceptor = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [services_1.AccountService])
    ], AuthInterceptor);
    return AuthInterceptor;
}());
exports.AuthInterceptor = AuthInterceptor;
//# sourceMappingURL=auth.interceptor.js.map