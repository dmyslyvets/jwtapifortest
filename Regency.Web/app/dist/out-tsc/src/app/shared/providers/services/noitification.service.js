"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ngx_toastr_1 = require("ngx-toastr");
var NotificationService = /** @class */ (function () {
    function NotificationService(toastr) {
        this.toastr = toastr;
    }
    NotificationService.prototype.showSuccess = function (message) {
        this.toastr.success(message, 'Success!');
    };
    NotificationService.prototype.showError = function (message) {
        this.toastr.error(message, 'Error!');
    };
    NotificationService.prototype.showErrors = function (data) {
        if (data.length == 0 || data == null) {
            return;
        }
        var errorMessage = "";
        data.forEach(function (file) {
            errorMessage = errorMessage.concat(file.concat("\n"));
        });
        this.toastr.error(errorMessage, 'Error!');
    };
    NotificationService.prototype.showWarning = function (message) {
        this.toastr.warning(message);
    };
    NotificationService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [ngx_toastr_1.ToastrService])
    ], NotificationService);
    return NotificationService;
}());
exports.NotificationService = NotificationService;
//# sourceMappingURL=noitification.service.js.map