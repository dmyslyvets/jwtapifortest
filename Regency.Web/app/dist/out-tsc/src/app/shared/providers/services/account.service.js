"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var jwt_decode = require("jwt-decode");
var local_storage_1 = require("@ngx-pwa/local-storage");
var router_1 = require("@angular/router");
var AccountService = /** @class */ (function () {
    function AccountService(storage, router) {
        var _this = this;
        this.storage = storage;
        this.router = router;
        this.authDataKey = "RegencyAuthData";
        this.storage.getItem(this.authDataKey).subscribe(function (data) {
            _this.user = data;
        });
    }
    AccountService.prototype.onLogin = function (data) {
        this.user = data;
        this.storage.setItem(this.authDataKey, data).subscribe(function () { });
    };
    AccountService.prototype.logout = function () {
        this.user = null;
        this.storage.removeItem(this.authDataKey).subscribe(function () { });
        this.router.navigateByUrl('pages/login');
    };
    AccountService.prototype.getToken = function () {
        if (this.user) {
            return this.user.auth_token;
        }
        return undefined;
    };
    AccountService.prototype.getUserIdFromToken = function () {
        var token = this.getToken();
        if (token) {
            return this.getDecodedAccessToken(token).id;
        }
        return null;
    };
    AccountService.prototype.getDecodedAccessToken = function (token) {
        try {
            return jwt_decode(token);
        }
        catch (error) {
            return null;
        }
    };
    AccountService.prototype.isAuthenticated = function () {
        var _this = this;
        return new Promise(function (resolve) {
            if (!_this.user) {
                _this.storage.getItem(_this.authDataKey).subscribe(function (data) {
                    resolve(!!data);
                });
            }
            else {
                resolve(!!_this.user);
            }
        });
    };
    AccountService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [local_storage_1.LocalStorage,
            router_1.Router])
    ], AccountService);
    return AccountService;
}());
exports.AccountService = AccountService;
//# sourceMappingURL=account.service.js.map