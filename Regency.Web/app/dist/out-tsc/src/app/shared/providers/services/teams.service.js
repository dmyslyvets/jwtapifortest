"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var get_all_team_view_1 = require("app/shared/models/teams/get-all-team.view");
var environment_1 = require("environments/environment");
var TeamsService = /** @class */ (function () {
    function TeamsService(http) {
        this.http = http;
        this.apiControllerURL = environment_1.environment.apiUrl;
        this.getAllTeamsView = new get_all_team_view_1.GetAllTeamsView();
    }
    TeamsService.prototype.getAllTeams = function () {
        return this.http.get(this.apiControllerURL + "api/Team/GetAllTeams");
    };
    TeamsService.prototype.getActiveTeams = function () {
        return this.http.get(this.apiControllerURL + "api/Team/GetActiveTeams");
    };
    TeamsService.prototype.getTeam = function (id) {
        return this.http.get(this.apiControllerURL + ("api/Team/GetTeam/?id=" + id));
    };
    TeamsService.prototype.getTeamsNotExistInUsers = function (id) {
        return this.http.get(this.apiControllerURL + ("api/User/GetNotExistInTeamUser/?id=" + id));
    };
    TeamsService.prototype.createNewTeam = function (createTeamView) {
        return this.http.post(this.apiControllerURL + "api/Team/CreateTeam", createTeamView);
    };
    TeamsService.prototype.updateTeam = function (updateTeamView) {
        return this.http.post(this.apiControllerURL + "api/Team/UpdateTeam", updateTeamView);
    };
    TeamsService.prototype.uploadImage = function (file) {
        return this.http.post(this.apiControllerURL + "api/Team/UploadImage", file);
    };
    TeamsService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], TeamsService);
    return TeamsService;
}());
exports.TeamsService = TeamsService;
//# sourceMappingURL=teams.service.js.map