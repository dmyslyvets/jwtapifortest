"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var LoadService = /** @class */ (function () {
    function LoadService() {
        var _this = this;
        this.set = function (flag) {
            _this.appComponent.show = flag;
        };
        this.passlinkObject = function (obj) {
            _this.appComponent = obj;
        };
    }
    LoadService.prototype.clear = function () {
        localStorage.clear();
    };
    LoadService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        })
    ], LoadService);
    return LoadService;
}());
exports.LoadService = LoadService;
//# sourceMappingURL=load.service.js.map