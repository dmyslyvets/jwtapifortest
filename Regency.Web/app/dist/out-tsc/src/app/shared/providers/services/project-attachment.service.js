"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var environment_1 = require("environments/environment");
var ProjectAttachmentservice = /** @class */ (function () {
    function ProjectAttachmentservice(http) {
        this.http = http;
        this.apiUrl = environment_1.environment.apiUrl;
    }
    ProjectAttachmentservice.prototype.createProjectAttachment = function (attachment) {
        return this.http.post(this.apiUrl + "api/ProjectAttachment/Create", attachment);
    };
    ProjectAttachmentservice.prototype.getAllProjectsAttachmentsByProjectId = function (id) {
        return this.http.get(this.apiUrl + "api/ProjectAttachment/GetAllByProjectId?id=" + id);
    };
    ProjectAttachmentservice.prototype.deleteAttachment = function (attachmentView) {
        return this.http.post(this.apiUrl + "api/ProjectAttachment/Delete", attachmentView);
    };
    ProjectAttachmentservice.prototype.getProjectAttachment = function (id) {
        return this.http.get(this.apiUrl + "api/ProjectAttachment/Get?id=" + id);
    };
    ProjectAttachmentservice.prototype.downloadAttachmentToBlob = function (id) {
        return this.http.get(this.apiUrl + "api/ProjectAttachment/DownloadAttachment?id=" + id, { responseType: 'blob' });
    };
    ProjectAttachmentservice.prototype.downloadAttachmentToArray = function (id) {
        return this.http.get(this.apiUrl + "api/ProjectAttachment/DownloadAttachment?id=" + id, { responseType: 'blob' });
    };
    ProjectAttachmentservice.prototype.downloadAttachment = function (id) {
        return this.http.get(this.apiUrl + "api/ProjectAttachment/DownloadAttachment?id=" + id, { responseType: 'text' });
    };
    ProjectAttachmentservice.prototype.downloadAllProjectsAttachment = function (id) {
        return this.http.get(this.apiUrl + "api/ProjectAttachment/DownloadAllForProject?id=" + id, { responseType: 'blob' });
    };
    ProjectAttachmentservice = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], ProjectAttachmentservice);
    return ProjectAttachmentservice;
}());
exports.ProjectAttachmentservice = ProjectAttachmentservice;
//# sourceMappingURL=project-attachment.service.js.map