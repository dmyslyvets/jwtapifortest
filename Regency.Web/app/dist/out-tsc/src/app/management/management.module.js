"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var angular_6_datatable_1 = require("angular-6-datatable");
var http_1 = require("@angular/http");
var forms_1 = require("@angular/forms");
var ng_select_1 = require("@ng-select/ng-select");
var ng_busy_1 = require("ng-busy");
var management_routing_module_1 = require("./management-routing.module");
var data_filter_pipe_1 = require("app/shared/filters/data-filter.pipe");
var users_filter_pipe_1 = require("app/shared/filters/users-filter.pipe");
var management_component_1 = require("./management/management.component");
var material_module_1 = require("app/shared/modules/material.module");
var management_constans_1 = require("../shared/constans/management.constans");
var ManagementModule = /** @class */ (function () {
    function ManagementModule() {
    }
    ManagementModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                material_module_1.MaterialModule,
                forms_1.FormsModule,
                angular_6_datatable_1.DataTableModule,
                http_1.HttpModule,
                ng_select_1.NgSelectModule,
                ng_busy_1.NgBusyModule,
                management_routing_module_1.ManagementRoutingModule
            ],
            declarations: [
                management_component_1.ManagementComponent,
                data_filter_pipe_1.DataFilterPipe,
                users_filter_pipe_1.UsersFilterPipe
            ],
            providers: [management_constans_1.ManagementConstans]
        })
    ], ManagementModule);
    return ManagementModule;
}());
exports.ManagementModule = ManagementModule;
//# sourceMappingURL=management.module.js.map