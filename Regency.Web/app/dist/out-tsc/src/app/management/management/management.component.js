"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var core_2 = require("@angular/core");
var router_1 = require("@angular/router");
var noitification_service_1 = require("app/shared/providers/services/noitification.service");
var team_members_type_service_1 = require("app/shared/providers/services/team-members-type.service");
var users_service_1 = require("app/shared/providers/services/users.service");
var teams_service_1 = require("app/shared/providers/services/teams.service");
var team_members_service_1 = require("app/shared/providers/services/team-members.service");
var load_service_1 = require("app/shared/providers/services/load.service");
var get_team_members_by_teamId_view_1 = require("app/shared/models/team-member/get-team-members-by-teamId.view");
var update_team_view_1 = require("app/shared/models/teams/update-team.view");
var create_team_view_1 = require("app/shared/models/teams/create-team.view");
var get_all_users_view_1 = require("app/shared/models/users/get-all-users.view");
var create_team_member_view_1 = require("app/shared/models/team-member/create-team-member.view");
var delete_team_member_view_1 = require("app/shared/models/team-member/delete-team-member.view");
var get_user_view_1 = require("app/shared/models/users/get-user.view");
var update_user_view_1 = require("app/shared/models/users/update-user.view");
var get_all_member_types_view_1 = require("app/shared/models/team-member-types/get-all-member-types.view");
var create_team_member_view_2 = require("app/shared/models/team-member-types/create-team-member.view");
var delete_team_member_view_2 = require("app/shared/models/team-member-types/delete-team-member.view");
var reset_password_user_view_1 = require("app/shared/models/users/reset-password-user.view");
var add_user_view_1 = require("app/shared/models/users/add-user.view");
var ge_by_userI_team_view_1 = require("app/shared/models/users/ge-by-userI-team.view");
var get_user_activity_view_1 = require("app/shared/models/users/get-user-activity.view");
var get_all_team_view_1 = require("app/shared/models/teams/get-all-team.view");
var set_user_role_view_1 = require("app/shared/models/users/set-user-role.view");
var add_user_to_team_view_1 = require("app/shared/models/users/add-user-to-team.view");
var update_team_member_view_1 = require("app/shared/models/team-member-types/update-team-member.view");
var get_teams_view_1 = require("app/shared/models/teams/get-teams.view");
var management_constans_1 = require("app/shared/constans/management.constans");
var ManagementComponent = /** @class */ (function () {
    function ManagementComponent(teamsService, teamMembersService, teamMemberTypesService, usersService, notificationService, route, loadService, constans) {
        this.teamsService = teamsService;
        this.teamMembersService = teamMembersService;
        this.teamMemberTypesService = teamMemberTypesService;
        this.usersService = usersService;
        this.notificationService = notificationService;
        this.route = route;
        this.loadService = loadService;
        this.constans = constans;
        this.notAddedTeams = new get_all_team_view_1.GetAllTeamsView();
        this.notAddedUsers = new get_all_users_view_1.GetAllUserView();
        this.allteams = new get_teams_view_1.GetTeamsView();
        this.currentTeam = new get_teams_view_1.GetTeamViewItem();
        this.currentTeamMebers = new get_team_members_by_teamId_view_1.GetTeamMembersByTeamIdView();
        this.teamsCurrentUser = new ge_by_userI_team_view_1.GetByUserIdTeamView();
        this.updateTeamMember = new update_team_member_view_1.UpdateTeamMemberView();
        this.selectedTeamMemberTypeForUpdate = new get_all_member_types_view_1.GetAllTeamMemberTypeItem();
        this.allTeamMemberTypes = new get_all_member_types_view_1.GetAllTeamMemberTypesView();
        this.allUsers = new get_all_users_view_1.GetAllUserView();
        this.currentUser = new get_user_view_1.GetUserView();
        this.currentUserActivity = new get_user_activity_view_1.GetUserActivitiesView();
        this.setUserAdmin = new set_user_role_view_1.SetUserRoleView();
        this.updateCurrentUser = new update_user_view_1.UpdateUserView();
        //for pop up
        this.newUser = new add_user_view_1.AddUserView();
        this.newUserToTeam = new add_user_to_team_view_1.AddUserToTeamView();
        this.newTeam = new create_team_view_1.CreateTeamView();
        this.newTeamMember = new create_team_member_view_1.CreateTeamMemberView();
        this.newTeamMemberType = new create_team_member_view_2.CreateTeamMemberTypeView();
        this.deleteTeamMember = new delete_team_member_view_1.DeleteTeamMemberView();
        this.deleteMemberType = new delete_team_member_view_2.DeleteTeamMemberTypeView();
        this.deleteTeam = new delete_team_member_view_1.DeleteTeamMemberView();
        this.updateCurrentTeam = new update_team_view_1.UpdateTeamView();
        this.resetPassword = new reset_password_user_view_1.ResetPasswordUserView();
        this.openResetPassword = false;
        this.openAddUser = false;
        this.openAddUserToTeam = false;
        this.openNewTeam = false;
        this.openNewTeamMember = false;
        this.openNewTeamMemberType = false;
        this.openDeleteTeamMember = false;
        this.openDeleteImage = false;
        this.openDeleteTeam = false;
        this.openDeleteMemberType = false;
        this.showEditableTeamButtons = false;
        this.validateForm = false;
        this.updateTeamMemberIsOpen = false;
        this.showEditableUserButtons = false;
        this.showEditableUserTeamButtons = false;
        this.showEditableUserPermissionButtons = false;
        this.checkData = false;
        this.userActivityShowMore = false;
        this.buttonShowMoreText = this.constans.showMoreMessage;
        this.selectedTeamIndex = 0;
        this.filterQuery = '';
        this.usersFilter = '';
        //for image
        this.showDefaultImage = true;
        //tabs
        this.usersTabIsSelect = false;
        this.teamsTabIsSelect = false;
    }
    ManagementComponent.prototype.ngOnInit = function () {
        this.loadService.set(true);
        this.getActiveTeams();
    };
    //----Users----
    ManagementComponent.prototype.clickOnUsersTab = function () {
        this.usersTabIsSelect = true;
        if (this.teamsTabIsSelect) {
            this.showEditableUserButtons = false;
        }
        this.teamsTabIsSelect = false;
    };
    ManagementComponent.prototype.openDialogAddNewUser = function () {
        this.openAddUser = true;
        this.validateForm = false;
        this.newUser = new add_user_view_1.AddUserView();
    };
    ManagementComponent.prototype.closeDialogAddNewUser = function () {
        this.openAddUser = false;
        this.validateForm = false;
        this.newUser = new add_user_view_1.AddUserView();
    };
    ManagementComponent.prototype.saveNewUser = function () {
        var _this = this;
        this.usersService.createUser(this.newUser).subscribe(function (response) {
            _this.notificationService.showSuccess(_this.constans.userAddedMessage);
            _this.getAllUsers();
            _this.newUser = new add_user_view_1.AddUserView();
            _this.validateForm = true;
            _this.openAddUser = false;
        }, function (error) {
            _this.notificationService.showError(error.error);
        });
    };
    ManagementComponent.prototype.openDialogResetPassword = function () {
        this.openResetPassword = true;
        this.validateForm = false;
        this.resetPassword = new reset_password_user_view_1.ResetPasswordUserView();
    };
    ManagementComponent.prototype.passwordCheck = function () {
        if (this.resetPassword.password == null || this.resetPassword.confirmPassword == null) {
            return true;
        }
        if (this.resetPassword.password != this.resetPassword.confirmPassword) {
            return false;
        }
        return true;
    };
    ManagementComponent.prototype.closeDialogResetPassword = function () {
        this.openResetPassword = false;
        this.validateForm = false;
        this.resetPassword = new reset_password_user_view_1.ResetPasswordUserView();
    };
    ManagementComponent.prototype.saveNewPasword = function () {
        var _this = this;
        if (!this.passwordCheck()) {
            return;
        }
        this.resetPassword.email = this.currentUser.email;
        this.usersService.resetPassword(this.resetPassword).subscribe(function (response) {
            _this.notificationService.showSuccess(_this.constans.passwordChangeMessage);
            _this.resetPassword = new reset_password_user_view_1.ResetPasswordUserView();
            _this.openResetPassword = false;
            _this.validateForm = true;
        }, function (error) {
            _this.notificationService.showError(error.error);
        });
    };
    ManagementComponent.prototype.openDialogAddUserToTeam = function () {
        this.openAddUserToTeam = true;
        this.validateForm = false;
        this.newUserToTeam = new add_user_to_team_view_1.AddUserToTeamView();
    };
    ManagementComponent.prototype.closeDialogAddUserToTeam = function () {
        this.openAddUserToTeam = false;
        this.validateForm = false;
        this.newUserToTeam = new add_user_to_team_view_1.AddUserToTeamView();
    };
    ManagementComponent.prototype.saveUserToTeam = function () {
        var _this = this;
        this.newUserToTeam.userId = this.currentUser.id;
        this.newUserToTeam.active = true;
        this.usersService.addUserToTeamView(this.newUserToTeam).subscribe(function (response) {
            _this.usersService.getTeamByUserId(_this.currentUser.id).subscribe(function (data) {
                _this.teamsCurrentUser = data;
                _this.notificationService.showSuccess(_this.constans.userToTeamAddedMessage);
                _this.newUserToTeam = new add_user_to_team_view_1.AddUserToTeamView();
                _this.openAddUserToTeam = false;
                _this.validateForm = true;
                _this.getNotAddedTeams(_this.currentUser.id);
            }, function (error) {
                _this.notificationService.showError(error.error);
            });
        }, function (error) {
            _this.notificationService.showError(error.error);
        });
    };
    ManagementComponent.prototype.openDialogDeleteTeam = function (id) {
        this.openDeleteTeam = true;
        this.deleteTeam.id = id;
    };
    ManagementComponent.prototype.closeDialogDeleteTeam = function () {
        this.openDeleteTeam = false;
    };
    ManagementComponent.prototype.deleteTeams = function () {
        var _this = this;
        this.teamMembersService.deleteTeamMembersById(this.deleteTeam).subscribe(function (response) {
            _this.usersService.getTeamByUserId(_this.currentUser.id).subscribe(function (data) {
                _this.teamsCurrentUser = data;
                _this.getNotAddedTeams(_this.currentUser.id);
            });
            _this.notificationService.showSuccess(_this.constans.teamDeleteMessage);
            _this.openDeleteTeam = false;
        }, function (error) {
            _this.notificationService.showError(error.error);
            _this.openDeleteTeam = false;
        });
    };
    ManagementComponent.prototype.getNotAddedTeams = function (id) {
        var _this = this;
        this.usersService.getNotAddedTeams(id).subscribe(function (response) {
            _this.notAddedTeams = response;
        }, function (error) {
            console.log(error.error);
        });
    };
    ManagementComponent.prototype.setSelectedTeamIndex = function (i) {
        this.selectedTeamIndex = i;
    };
    ManagementComponent.prototype.changeUserRole = function () {
        var _this = this;
        this.setUserAdmin.id = this.currentUser.id;
        this.setUserAdmin.isAdmin = this.setUserRole;
        this.usersService.setUserRole(this.setUserAdmin).subscribe(function (response) {
            _this.getUser(_this.currentUser.id);
        });
    };
    ManagementComponent.prototype.getCurrentUserActivity = function (id) {
        var _this = this;
        this.usersService.getUserActivity(id).subscribe(function (response) {
            _this.currentUserActivity = response;
        });
        this.usersService.getTeamByUserId(id).subscribe(function (data) {
            _this.teamsCurrentUser = data;
        });
    };
    ManagementComponent.prototype.activetedUserChanges = function () {
        this.showEditableUserButtons = true;
    };
    ManagementComponent.prototype.cancelUserChanges = function (id) {
        this.getUser(id);
    };
    ManagementComponent.prototype.saveUserChanges = function () {
        var _this = this;
        var validate = true;
        this.updateCurrentUser = this.currentUser;
        var emailValidator = this.constans.emailRegex;
        if (!emailValidator.test(this.currentUser.email)) {
            validate = false;
            this.notificationService.showError(this.constans.invalidEmailMessage);
        }
        if (!this.currentUser.firstName) {
            validate = false;
            this.notificationService.showError(this.constans.invalidFirstNameMessage);
        }
        if (!this.currentUser.lastName) {
            validate = false;
            this.notificationService.showError(this.constans.invalidLastNameMessage);
        }
        var phoneValidator = this.constans.phoneRegex;
        if (!phoneValidator.test(this.currentUser.phoneNumber)) {
            this.notificationService.showError(this.constans.wrongNumberFormatMessage);
            validate = false;
        }
        if (validate) {
            this.usersService.updateUser(this.updateCurrentUser).subscribe(function (response) {
                _this.showEditableUserButtons = false;
                _this.getAllUsers();
                _this.getUser(response);
            });
        }
    };
    ManagementComponent.prototype.formatTeamNameOnUsersTabs = function (teamName) {
        if (teamName.length > 45) {
            teamName = teamName.substring(0, 45) + "...";
        }
        return teamName;
    };
    ManagementComponent.prototype.getUser = function (id) {
        var _this = this;
        this.usersService.getUser(id).subscribe(function (user) {
            _this.currentUser = user;
            _this.showEditableUserButtons = false;
            _this.showEditableUserPermissionButtons = false;
            _this.showEditableUserTeamButtons = false;
            _this.setUserRole = _this.currentUser.isAdmin;
        });
        this.getNotAddedTeams(id);
        this.getCurrentUserActivity(id);
        this.showMoreRefresh();
    };
    ManagementComponent.prototype.showMoreRefresh = function () {
        this.userActivityShowMore = false;
        this.buttonShowMoreText = this.constans.showMoreMessage;
    };
    ManagementComponent.prototype.showMoreClick = function () {
        this.userActivityShowMore = !this.userActivityShowMore;
        if (this.userActivityShowMore) {
            this.buttonShowMoreText = this.constans.showLessMessage;
        }
        if (!this.userActivityShowMore) {
            this.buttonShowMoreText = this.constans.showMoreMessage;
        }
    };
    ManagementComponent.prototype.getAllUsers = function () {
        var _this = this;
        this.teamMembersService.getAllUsers().subscribe(function (users) {
            _this.allUsers = users;
            if (Object.keys(_this.currentUser).length) {
                _this.getUser(_this.currentUser.id);
            }
            if (!Object.keys(_this.currentUser).length) {
                _this.getUser(users.users[0].id);
            }
        });
    };
    //----Teams---- 
    ManagementComponent.prototype.clickOnTeamsTab = function () {
        this.teamsTabIsSelect = true;
        if (this.usersTabIsSelect) {
            this.showEditableTeamButtons = false;
        }
        this.usersTabIsSelect = false;
    };
    ManagementComponent.prototype.openDialogAddNewTeam = function () {
        this.openNewTeam = true;
        this.validateForm = false;
        this.newTeam = new create_team_view_1.CreateTeamView();
    };
    ManagementComponent.prototype.closeDialogAddNewTeam = function () {
        this.openNewTeam = false;
        this.validateForm = false;
        this.newTeam = new create_team_view_1.CreateTeamView();
    };
    ManagementComponent.prototype.saveNewTeam = function () {
        var _this = this;
        this.teamsService.createNewTeam(this.newTeam).subscribe(function (response) {
            _this.notificationService.showSuccess(_this.constans.teamAddedMessage);
            _this.getActiveTeams();
            _this.newTeam = new create_team_view_1.CreateTeamView();
            _this.validateForm = true;
            _this.openNewTeam = false;
        }, function (error) {
            _this.notificationService.showError(error.error);
        });
    };
    ManagementComponent.prototype.openDialogAddNewTeamMember = function () {
        this.validateForm = false;
        this.notAddedUsers = new get_all_users_view_1.GetAllUserView();
        this.newTeamMember = new create_team_member_view_1.CreateTeamMemberView();
        this.getUsersNotExistInCurrentTeam(this.currentTeam.id);
        this.openNewTeamMember = true;
    };
    ManagementComponent.prototype.closeDialogAddNewTeamMember = function () {
        this.openNewTeamMember = false;
        this.validateForm = false;
        this.newTeamMember = new create_team_member_view_1.CreateTeamMemberView();
    };
    ManagementComponent.prototype.saveNewTeamMember = function () {
        var _this = this;
        this.newTeamMember.teamId = this.currentTeam.id;
        this.newTeamMember.active = true;
        this.teamMembersService.createTeamMember(this.newTeamMember).subscribe(function (response) {
            _this.notificationService.showSuccess(_this.constans.teamMemberAddedMessage);
            _this.getTeamMembersByTeamId(_this.currentTeam.id);
            _this.validateForm = true;
            _this.openNewTeamMember = false;
            _this.getUsersNotExistInCurrentTeam(_this.currentTeam.id);
            _this.newTeamMember = new create_team_member_view_1.CreateTeamMemberView();
        }, function (error) {
            _this.notificationService.showError(error.error);
        });
    };
    ManagementComponent.prototype.openDialogDeleteTeamMembers = function (id) {
        this.openDeleteTeamMember = true;
        this.deleteTeamMember.id = id;
    };
    ManagementComponent.prototype.closeDialogDeleteTeamMember = function () {
        this.openDeleteTeamMember = false;
    };
    ManagementComponent.prototype.deleteTeamMembers = function () {
        var _this = this;
        this.teamMembersService.deleteTeamMembersById(this.deleteTeamMember).subscribe(function (response) {
            _this.getTeamMembersByTeamId(_this.currentTeam.id);
            _this.notificationService.showSuccess(_this.constans.teamMemberTypeDeletedMessage);
            _this.openDeleteTeamMember = false;
            _this.getUsersNotExistInCurrentTeam(_this.currentTeam.id);
        }, function (error) {
            _this.notificationService.showError(error.error);
        });
    };
    ManagementComponent.prototype.openDialogDeleteImage = function () {
        this.openDeleteImage = true;
    };
    ManagementComponent.prototype.closeDialogDeleteImage = function () {
        this.openDeleteImage = false;
    };
    ManagementComponent.prototype.deleteImage = function () {
        var _this = this;
        this.updateCurrentTeam.linkToImage = null;
        this.updateCurrentTeam.id = this.currentTeam.id;
        this.updateCurrentTeam.teamName = this.currentTeam.teamName;
        this.updateCurrentTeam.teamDesc = this.currentTeam.teamDesc;
        this.updateCurrentTeam.active = this.currentTeam.active;
        this.updateCurrentTeam.dateCreated = this.currentTeam.dateCreated;
        this.updateCurrentTeam.pm = this.currentTeam.pm;
        this.teamsService.updateTeam(this.updateCurrentTeam).subscribe(function (upateTeamId) {
            _this.showEditableTeamButtons = false;
            _this.getActiveTeams();
            _this.getTeam(upateTeamId);
            _this.notificationService.showSuccess(_this.constans.imageDeleteMessage);
            _this.openDeleteImage = false;
            _this.teamImageInputVariable.nativeElement.value = "";
        }, function (error) {
            _this.notificationService.showError(error.error);
        });
    };
    ManagementComponent.prototype.uploadImage = function (event) {
        var _this = this;
        var formData = new FormData();
        this.image = event.target.files[0];
        formData.append('image', this.image, this.image.name);
        this.teamsService.uploadImage(formData).subscribe(function (data) {
            _this.currentTeam.linkToImage = data;
            _this.teamImageInputVariable.nativeElement.value = "";
            _this.saveChanges();
        });
    };
    ManagementComponent.prototype.getAllTeams = function () {
        var _this = this;
        this.teamsService.getAllTeams().subscribe(function (teams) {
            _this.allteams = teams;
            _this.checkData = true;
            if (Object.keys(_this.currentTeam).length) {
                _this.getTeam(_this.currentTeam.id);
            }
            if (!Object.keys(_this.currentTeam).length) {
                _this.getTeam(teams.teams[0].id);
            }
            _this.getAllUsers();
            _this.getAllTeamMembersType();
        });
    };
    ManagementComponent.prototype.getActiveTeams = function () {
        var _this = this;
        this.teamsService.getActiveTeams().subscribe(function (teams) {
            _this.allteams = teams;
            _this.checkData = true;
            if (Object.keys(_this.currentTeam).length) {
                _this.getTeam(_this.currentTeam.id);
            }
            if (!Object.keys(_this.currentTeam).length) {
                _this.getTeam(teams.teams[0].id);
            }
            _this.getAllUsers();
            _this.getAllTeamMembersType();
        });
    };
    ManagementComponent.prototype.getTeam = function (id) {
        var _this = this;
        this.teamsService.getTeam(id).subscribe(function (team) {
            _this.showDefaultImage = true;
            _this.currentTeam = team;
            if (_this.currentTeam.linkToImage != null) {
                _this.showDefaultImage = false;
            }
            _this.showEditableTeamButtons = false;
            _this.getTeamMembersByTeamId(_this.currentTeam.id);
            _this.loadService.set(false);
        });
    };
    ManagementComponent.prototype.getUsersNotExistInCurrentTeam = function (id) {
        var _this = this;
        this.teamsService.getTeamsNotExistInUsers(id).subscribe(function (users) {
            _this.notAddedUsers.users = users.users;
        });
    };
    ManagementComponent.prototype.getTeamMembersByTeamId = function (teamId) {
        var _this = this;
        this.teamMembersService.getTeamMemresByTeamId(teamId).subscribe(function (response) {
            _this.currentTeamMebers = response;
        });
    };
    ManagementComponent.prototype.activetedTeamChanges = function () {
        this.showEditableTeamButtons = true;
    };
    ManagementComponent.prototype.cancelChanges = function (id) {
        this.getTeam(id);
    };
    ManagementComponent.prototype.saveChanges = function () {
        var _this = this;
        this.updateCurrentTeam.linkToImage = this.currentTeam.linkToImage;
        this.updateCurrentTeam.id = this.currentTeam.id;
        this.updateCurrentTeam.teamName = this.currentTeam.teamName;
        this.updateCurrentTeam.teamDesc = this.currentTeam.teamDesc;
        this.updateCurrentTeam.active = this.currentTeam.active;
        this.updateCurrentTeam.dateCreated = this.currentTeam.dateCreated;
        this.updateCurrentTeam.pm = this.currentTeam.pm;
        this.teamsService.updateTeam(this.updateCurrentTeam).subscribe(function (upateTeamId) {
            _this.showEditableTeamButtons = false;
            _this.getActiveTeams();
            _this.getTeam(upateTeamId);
        });
    };
    ManagementComponent.prototype.openUpdateTeamMember = function (id) {
        this.selecterTeamMemberIdForUpdate = id;
        this.selectedTeamMemberTypeForUpdate = null;
        this.updateTeamMemberIsOpen = true;
    };
    ManagementComponent.prototype.cancelTeamMembers = function () {
        this.updateTeamMemberIsOpen = false;
        this.selectedTeamMemberTypeForUpdate = null;
    };
    ManagementComponent.prototype.updateTeamMembers = function () {
        var _this = this;
        this.updateTeamMember.id = this.selecterTeamMemberIdForUpdate;
        this.updateTeamMember.teamMemberTypeId = this.selectedTeamMemberTypeForUpdate.id;
        this.teamMembersService.updateTeamMembers(this.updateTeamMember).subscribe(function (response) {
            _this.getTeamMembersByTeamId(_this.currentTeam.id);
            _this.updateTeamMemberIsOpen = false;
            _this.notificationService.showSuccess(_this.constans.teamMemberUpdateMessage);
            _this.selectedTeamMemberTypeForUpdate = null;
        });
    };
    //----Team member type----
    ManagementComponent.prototype.openDialogAddNewTeamMemberType = function () {
        this.openNewTeamMemberType = true;
        this.validateForm = false;
        this.newTeamMemberType = new create_team_member_view_2.CreateTeamMemberTypeView();
    };
    ManagementComponent.prototype.closeDialogAddNewTeamMemberType = function () {
        this.openNewTeamMemberType = false;
        this.validateForm = false;
        this.newTeamMemberType = new create_team_member_view_2.CreateTeamMemberTypeView();
    };
    ManagementComponent.prototype.saveNewTeamMemberType = function () {
        var _this = this;
        this.teamMemberTypesService.createTeamMembers(this.newTeamMemberType).subscribe(function (response) {
            _this.newTeamMemberType = new create_team_member_view_2.CreateTeamMemberTypeView();
            _this.notificationService.showSuccess(_this.constans.teamMemberTypeAddedMessage);
            _this.getAllTeamMembersType();
            _this.openNewTeamMemberType = false;
            _this.validateForm = true;
        }, function (error) {
            _this.notificationService.showError(error.error);
        });
    };
    ManagementComponent.prototype.openDialogDeleteMemberType = function (id) {
        this.openDeleteMemberType = true;
        this.deleteMemberType.id = id;
    };
    ManagementComponent.prototype.closeDialogDeleteMemberType = function () {
        this.openDeleteMemberType = false;
    };
    ManagementComponent.prototype.deleteMemberTypes = function () {
        var _this = this;
        this.teamMemberTypesService.deleteTeamMember(this.deleteMemberType).subscribe(function (response) {
            _this.notificationService.showSuccess(_this.constans.memberTypeDeleteMessage);
            _this.getAllTeamMembersType();
            _this.openDeleteMemberType = false;
        }, function (error) {
            _this.notificationService.showError(error.error);
        });
    };
    ManagementComponent.prototype.getAllTeamMembersType = function () {
        var _this = this;
        this.teamMemberTypesService.getAllTeamMembersType().subscribe(function (response) {
            _this.allTeamMemberTypes = response;
        });
    };
    //shared
    ManagementComponent.prototype.tabChange = function (event) {
        if (event.tab.textLabel == this.constans.teamsLabel) {
            this.clickOnTeamsTab();
        }
        if (event.tab.textLabel == this.constans.usersLabel) {
            this.clickOnUsersTab();
        }
    };
    __decorate([
        core_2.ViewChild('uploader'),
        __metadata("design:type", core_1.ElementRef)
    ], ManagementComponent.prototype, "teamImageInputVariable", void 0);
    ManagementComponent = __decorate([
        core_1.Component({
            selector: 'management',
            templateUrl: './management.component.html',
            styleUrls: ['./management.component.scss']
        }),
        core_1.Injectable(),
        __metadata("design:paramtypes", [teams_service_1.TeamsService,
            team_members_service_1.TeamMembersService,
            team_members_type_service_1.TeamMembersTypeService,
            users_service_1.UserService,
            noitification_service_1.NotificationService,
            router_1.Router,
            load_service_1.LoadService,
            management_constans_1.ManagementConstans])
    ], ManagementComponent);
    return ManagementComponent;
}());
exports.ManagementComponent = ManagementComponent;
//# sourceMappingURL=management.component.js.map