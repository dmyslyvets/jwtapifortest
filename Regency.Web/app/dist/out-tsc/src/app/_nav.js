"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.navigation = [
    {
        title: true,
        name: 'Dashboard',
        wrapper: {
            element: 'span',
            attributes: {}
        },
        class: 'text-center'
    },
    {
        name: 'Dashboard',
        url: '/dashboard',
    },
    {
        name: 'Projects & Orders',
        url: '/projects-and-orders',
    },
    {
        name: 'Customers',
        url: '/customers',
    },
    {
        name: 'Vendors',
        url: '/vendors',
    },
    {
        name: 'Items',
        url: '/items',
    },
    {
        name: 'Reporting',
        url: '/reporting',
    },
    {
        name: 'Management',
        url: '/management',
    },
    {
        name: 'Settings',
        url: '/settings',
    }
];
//# sourceMappingURL=_nav.js.map