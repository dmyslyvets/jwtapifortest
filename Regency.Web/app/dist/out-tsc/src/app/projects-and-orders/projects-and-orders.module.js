"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var material_module_1 = require("app/shared/modules/material.module");
var projects_and_orders_routing_module_1 = require("./projects-and-orders-routing.module");
var projects_and_orders_component_1 = require("./project-and-orders/projects-and-orders.component");
var projects_and_orders_constans_1 = require("../shared/constans/projects-and-orders.constans");
var ProjectsAndOrdersModule = /** @class */ (function () {
    function ProjectsAndOrdersModule() {
    }
    ProjectsAndOrdersModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                material_module_1.MaterialModule,
                projects_and_orders_routing_module_1.ProjectsAndOrdersRoutingModule,
            ],
            declarations: [projects_and_orders_component_1.ProjectsAndOrdersComponent],
            providers: [projects_and_orders_constans_1.ProjectsAndOrdersConstans]
        })
    ], ProjectsAndOrdersModule);
    return ProjectsAndOrdersModule;
}());
exports.ProjectsAndOrdersModule = ProjectsAndOrdersModule;
//# sourceMappingURL=projects-and-orders.module.js.map