"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var projects_and_orders_component_1 = require("./project-and-orders/projects-and-orders.component");
exports.routes = [
    {
        path: '',
        component: projects_and_orders_component_1.ProjectsAndOrdersComponent,
        children: [
            {
                path: '',
                redirectTo: 'newProjects'
            },
            {
                path: 'projects',
                data: { title: 'Projects', hidden: true },
                loadChildren: './project-and-orders/projects/projects-module.module#ProjectsModuleModule'
            },
            {
                path: 'orders',
                data: { title: 'Orders', hidden: true },
                loadChildren: './project-and-orders/orders/orders-module.module#OrdersModuleModule'
            },
            {
                path: 'tasks',
                data: { title: 'Tasks', hidden: true },
                loadChildren: './project-and-orders/tasks/tasks-module.module#TasksModuleModule'
            },
            {
                path: 'newProjects',
                data: { title: 'Projects', hidden: true },
                loadChildren: './project-and-orders/new-projects/new-projects.module#NewProjectsModule'
            }
        ]
    }
];
var ProjectsAndOrdersRoutingModule = /** @class */ (function () {
    function ProjectsAndOrdersRoutingModule() {
    }
    ProjectsAndOrdersRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forChild(exports.routes)],
            exports: [router_1.RouterModule]
        })
    ], ProjectsAndOrdersRoutingModule);
    return ProjectsAndOrdersRoutingModule;
}());
exports.ProjectsAndOrdersRoutingModule = ProjectsAndOrdersRoutingModule;
//# sourceMappingURL=projects-and-orders-routing.module.js.map