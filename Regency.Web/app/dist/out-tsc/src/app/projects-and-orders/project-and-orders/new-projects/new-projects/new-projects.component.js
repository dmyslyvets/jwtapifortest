"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var _ = require("lodash");
var router_1 = require("@angular/router");
var project_service_1 = require("app/shared/providers/services/project.service");
var noitification_service_1 = require("app/shared/providers/services/noitification.service");
var load_service_1 = require("app/shared/providers/services/load.service");
var get_all_users_view_1 = require("app/shared/models/users/get-all-users.view");
var get_all_active_project_view_1 = require("app/shared/models/project/get-all-active-project.view");
var reassign_project_manager_view_1 = require("app/shared/models/project/reassign-project-manager.view");
var projects_and_orders_constans_1 = require("app/shared/constans/projects-and-orders.constans");
var get_with_opened_orders_view_1 = require("../../../../shared/models/project/get-with-opened-orders.view");
var NewProjectsComponent = /** @class */ (function () {
    function NewProjectsComponent(route, notificationService, loadService, projectService, constans) {
        this.route = route;
        this.notificationService = notificationService;
        this.loadService = loadService;
        this.projectService = projectService;
        this.constans = constans;
        // public myControl = new FormControl();
        this.projectManagers = new get_all_users_view_1.GetAllUserView();
        this.activeProjects = new get_all_active_project_view_1.GetAllActiveProjectView();
        this.activeProjectsWithOpenedOrders = new get_with_opened_orders_view_1.GetWithOpenedOrdersProjectView();
        this.activeProjectsForSearch = new get_all_active_project_view_1.GetAllActiveProjectView();
        this.activeProjectsWithOpenedOrdersForSearch = new get_with_opened_orders_view_1.GetWithOpenedOrdersProjectView();
        this.reassignProjectManager = new reassign_project_manager_view_1.ReassignProjectManagerView();
        this.projectDetailsIsShow = false;
        this.createProjectIsShow = false;
        this.closeProjectShow = false;
    }
    NewProjectsComponent.prototype.ngOnInit = function () {
        this.loadService.set(true);
        //this.getActiveProject();
        this.getActiveProjectsWithOpenedOrders();
        this.getProjectManager();
    };
    NewProjectsComponent.prototype.getActiveProjectsWithOpenedOrders = function () {
        var _this = this;
        this.projectService.getAllActivePorjectsWithOpenedOrders().subscribe(function (response) {
            _this.activeProjectsWithOpenedOrders = response;
            _this.activeProjectsWithOpenedOrdersForSearch.projects = response.projects.slice(0);
            for (var i = 0; i < _this.activeProjectsWithOpenedOrdersForSearch.projects.length; i++) {
                if (!_this.activeProjectsWithOpenedOrdersForSearch.projects[i].name) {
                    _this.activeProjectsWithOpenedOrders.projects[i].name = _this.activeProjectsWithOpenedOrdersForSearch.projects[i].numericId.toString();
                    _this.activeProjectsWithOpenedOrdersForSearch.projects[i].name = _this.activeProjectsWithOpenedOrdersForSearch.projects[i].numericId.toString();
                }
            }
            _this.updateDate = null;
            _this.loadService.set(false);
            _this.projectsCount = _this.activeProjectsWithOpenedOrders.projects.length;
        }, function (error) {
            _this.loadService.set(false);
        });
    };
    NewProjectsComponent.prototype.getActiveProject = function () {
        var _this = this;
        this.projectService.getAllActivePorjects().subscribe(function (response) {
            _this.activeProjects = response;
            _this.activeProjectsForSearch.projects = response.projects.slice(0);
            for (var i = 0; i < _this.activeProjectsForSearch.projects.length; i++) {
                if (!_this.activeProjectsForSearch.projects[i].name) {
                    _this.activeProjects.projects[i].name = _this.activeProjectsForSearch.projects[i].numericId.toString();
                    _this.activeProjectsForSearch.projects[i].name = _this.activeProjectsForSearch.projects[i].numericId.toString();
                }
            }
            _this.updateDate = null;
            _this.loadService.set(false);
            _this.projectsCount = _this.activeProjects.projects.length;
        }, function (error) {
            _this.loadService.set(false);
        });
    };
    NewProjectsComponent.prototype.showClosedProjects = function () {
        if (this.closeProjectShow) {
            this.getActiveProject();
        }
        if (!this.closeProjectShow) {
            this.getActiveProjectsWithOpenedOrders();
        }
    };
    NewProjectsComponent.prototype.projectSearch = function () {
        var _this = this;
        if (this.showClosedProjects) {
            this.activeProjects.projects = this.activeProjectsForSearch.projects;
            this.activeProjects.projects = _.filter(this.activeProjects.projects, function (row) { return ((row.name).toLowerCase()).indexOf(_this.searcText.toLowerCase()) > -1; });
            this.projectsCount = this.activeProjects.projects.length;
        }
        if (this.showClosedProjects) {
            this.activeProjectsWithOpenedOrders.projects = this.activeProjectsWithOpenedOrdersForSearch.projects;
            this.activeProjectsWithOpenedOrders.projects = _.filter(this.activeProjectsWithOpenedOrders.projects, function (row) { return ((row.name).toLowerCase()).indexOf(_this.searcText.toLowerCase()) > -1; });
            this.projectsCount = this.activeProjectsWithOpenedOrders.projects.length;
        }
    };
    NewProjectsComponent.prototype.stopPropagation = function (event) {
        event.stopPropagation();
    };
    NewProjectsComponent.prototype.getProjectManager = function () {
        var _this = this;
        this.projectService.getProjectManager().subscribe(function (response) {
            _this.projectManagers = response;
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    NewProjectsComponent.prototype.saveNewProject = function () {
        this.createProjectIsShow = true;
        this.projectDetailsIsShow = false;
    };
    NewProjectsComponent.prototype.closeDetails = function () {
        this.projectDetailsIsShow = false;
    };
    NewProjectsComponent.prototype.closeCreatePorject = function () {
        this.createProjectIsShow = false;
        this.getActiveProject();
    };
    NewProjectsComponent.prototype.showProjectDetailsById = function (id) {
        this.createProjectIsShow = false;
        this.projectDetailsIsShow = true;
        this.currentProjectId = id;
    };
    NewProjectsComponent.prototype.reasignProjectManager = function (projectId, userId) {
        var _this = this;
        this.reassignProjectManager = new reassign_project_manager_view_1.ReassignProjectManagerView();
        this.reassignProjectManager.projectId = projectId;
        this.reassignProjectManager.projectManagerId = userId;
        this.projectService.reassignProjectManeger(this.reassignProjectManager).subscribe(function (response) {
            _this.getActiveProject();
            _this.notificationService.showSuccess(_this.constans.projectManagerUpdateMessage);
        });
    };
    NewProjectsComponent = __decorate([
        core_1.Component({
            selector: 'app-new-projects',
            templateUrl: './new-projects.component.html',
            styleUrls: ['./new-projects.component.scss']
        }),
        __metadata("design:paramtypes", [router_1.Router,
            noitification_service_1.NotificationService,
            load_service_1.LoadService,
            project_service_1.ProjectService,
            projects_and_orders_constans_1.ProjectsAndOrdersConstans])
    ], NewProjectsComponent);
    return NewProjectsComponent;
}());
exports.NewProjectsComponent = NewProjectsComponent;
//# sourceMappingURL=new-projects.component.js.map