"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var tasks_kanban_component_1 = require("./tasks-kanban/tasks-kanban.component");
var tasks_list_component_1 = require("./tasks-list/tasks-list.component");
var routes = [
    {
        path: '',
        redirectTo: 'tasksKanban',
        pathMatch: 'full'
    },
    {
        path: 'tasksKanban',
        component: tasks_kanban_component_1.TasksKanbanComponent
    },
    {
        path: 'tasksList',
        component: tasks_list_component_1.TasksListComponent
    }
];
var TasksModuleRoutingModule = /** @class */ (function () {
    function TasksModuleRoutingModule() {
    }
    TasksModuleRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forChild(routes)],
            exports: [router_1.RouterModule]
        })
    ], TasksModuleRoutingModule);
    return TasksModuleRoutingModule;
}());
exports.TasksModuleRoutingModule = TasksModuleRoutingModule;
//# sourceMappingURL=tasks-module-routing.module.js.map