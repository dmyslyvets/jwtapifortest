"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var projects_and_orders_constans_1 = require("app/shared/constans/projects-and-orders.constans");
var file_saver_1 = require("file-saver");
var order_service_1 = require("app/shared/providers/services/order.service");
var customer_service_1 = require("app/shared/providers/services/customer.service");
var noitification_service_1 = require("app/shared/providers/services/noitification.service");
var project_service_1 = require("app/shared/providers/services/project.service");
var get_order_view_1 = require("app/shared/models/order/get-order-view");
var get_all_customer_view_1 = require("app/shared/models/customer/get-all-customer.view");
var update_order_view_1 = require("app/shared/models/order/update-order.view");
var get_all_users_view_1 = require("app/shared/models/users/get-all-users.view");
var reassign_project_manager_view_1 = require("app/shared/models/project/reassign-project-manager.view");
var ng2_file_upload_1 = require("ng2-file-upload");
var order_attachment_service_1 = require("app/shared/providers/services/order-attachment.service");
var get_order_attachments_by_order_id_view_1 = require("app/shared/models/order-attachment/get-order-attachments-by-order-id.view");
var delete_order_attachment_view_1 = require("app/shared/models/order-attachment/delete-order-attachment.view");
var get_order_attachment_by_orderId_view_1 = require("app/shared/models/order-attachment/get-order-attachment-by-orderId.view");
var get_all_order_log_activities_by_orderId_view_1 = require("app/shared/models/order-log-activity/get-all-order-log-activities-by-orderId.view");
var order_log_activity_service_1 = require("app/shared/providers/services/order-log-activity.service");
var update_order_confirmed_order_vendor_quote_view_1 = require("../../../../shared/models/order/update-order-confirmed-order-vendor-quote.view");
var OrderDetailsComponent = /** @class */ (function () {
    function OrderDetailsComponent(activateRoute, orderService, route, customerService, notificationService, constans, projectService, orderAttachmentService, orderLogActivitiesService) {
        this.activateRoute = activateRoute;
        this.orderService = orderService;
        this.route = route;
        this.customerService = customerService;
        this.notificationService = notificationService;
        this.constans = constans;
        this.projectService = projectService;
        this.orderAttachmentService = orderAttachmentService;
        this.orderLogActivitiesService = orderLogActivitiesService;
        //drag and drop variables
        this.hasBaseDropZoneOver = false;
        this.uploader = new ng2_file_upload_1.FileUploader({ url: this.constans.URL });
        this.showAttachmentImg = false;
        this.showAttachedPdf = false;
        this.showAttachedTxt = false;
        this.showAttachment = false;
        //spinner
        this.showSpinner = false;
        this.formValidation = false;
        //popup
        this.openSaveAttachment = false;
        this.openDeleteAttachmentModal = false;
        this.openOrderVendorQuoteModal = false;
        this.alreadyGetOrderDetails = false;
        this.activeEditOrder = false;
        this.logs = new get_all_order_log_activities_by_orderId_view_1.GetAllOrderLogActivitiesByOrderIdView();
        this.currentOrder = new get_order_view_1.GetOrderView();
        this.currentOrderAttachment = new get_order_attachment_by_orderId_view_1.GetOrderAttachmentByOrderIdView();
        this.orderAttachments = new get_order_attachments_by_order_id_view_1.GetOrderAttachmentsByOrderIdView();
        this.deletedAttachment = new delete_order_attachment_view_1.DeleteOrderAttachmentView();
        this.customers = new get_all_customer_view_1.GetAllCustomerView();
        this.projectManagers = new get_all_users_view_1.GetAllUserView();
        this.tempOrderQuote = new get_order_view_1.GetOrderOrderVendorQuoteViewItem();
        this.updateOrder = new update_order_view_1.UpdateOrderView();
        this.updateProjectManager = new reassign_project_manager_view_1.ReassignProjectManagerView();
        this.orderId = this.activateRoute.snapshot.params['id'];
        this.getOrderDetails(this.orderId);
        this.getCustomers();
        this.getProjectManager();
        this.getAllOrderLogActivities();
    }
    OrderDetailsComponent.prototype.ngOnInit = function () {
    };
    OrderDetailsComponent.prototype.cancelOrderVendorQuoteAttachment = function () {
        this.openOrderVendorQuoteModal = false;
    };
    OrderDetailsComponent.prototype.openOrderVendorQuoteAttachmentModal = function (orderQuote) {
        this.tempOrderQuote = orderQuote;
        this.openOrderVendorQuoteModal = true;
    };
    OrderDetailsComponent.prototype.confirmOrderVendorQuoteAttachment = function () {
        this.updateConfirmedOrderVendorQuote(this.tempOrderQuote);
        this.openOrderVendorQuoteModal = false;
    };
    //Update confirmed vendor quote
    OrderDetailsComponent.prototype.updateConfirmedOrderVendorQuote = function (orderQuote) {
        var _this = this;
        var updateOrderconfirmedVendorQuote = new update_order_confirmed_order_vendor_quote_view_1.UpdateOrderConfirmedOrderVendorQuoteView();
        updateOrderconfirmedVendorQuote.id = this.currentOrder.id;
        updateOrderconfirmedVendorQuote.confirmedOrderVendorQuoteId = orderQuote.id;
        orderQuote.isConfirmed = true;
        this.currentOrder.orderVendorQuotes.find(function (x) { return x.id == _this.currentOrder.confirmedVendorQuote.id; }).isConfirmed = false;
        this.orderService.updateConfirmedOrderVendoQuote(updateOrderconfirmedVendorQuote)
            .subscribe(function (response) {
            _this.notificationService.showSuccess(_this.constans.updateConfirmedOrderVenorQuoteMessage);
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
        this.currentOrder.confirmedVendorQuote = this.currentOrder.orderVendorQuotes.find(function (x) { return x.id == orderQuote.id; });
    };
    OrderDetailsComponent.prototype.getAllOrderLogActivities = function () {
        var _this = this;
        this.orderLogActivitiesService.getAllOrderLogActivities(this.orderId).subscribe(function (response) {
            _this.logs = response;
        });
    };
    OrderDetailsComponent.prototype.fileOverBase = function (event) {
        this.hasBaseDropZoneOver = event;
        console.log(event);
    };
    OrderDetailsComponent.prototype.dropped = function (fileList) {
        var invalidFiles = this.checkAttachmentExtensions(fileList);
        if (invalidFiles.length > 0) {
            this.notificationService.showWarning(this.constans.fileExtensionNotAllowedMessage);
        }
        this.showSpinner = false;
    };
    OrderDetailsComponent.prototype.openDeleteAttachment = function (id) {
        this.deletedAttachmentId = id;
        this.openDeleteAttachmentModal = true;
    };
    OrderDetailsComponent.prototype.cancelDeleteAttachment = function () {
        this.deletedAttachmentId = null;
        this.openDeleteAttachmentModal = false;
    };
    OrderDetailsComponent.prototype.yesDeleteAttachment = function () {
        this.deleteAttachment();
        this.openDeleteAttachmentModal = false;
    };
    OrderDetailsComponent.prototype.deleteAttachment = function () {
        var _this = this;
        this.deletedAttachment.id = this.deletedAttachmentId;
        this.orderAttachmentService.deleteAttachment(this.deletedAttachment).subscribe(function (response) {
            _this.notificationService.showSuccess(_this.constans.atachmentDeleteMessage);
            _this.getOrderAttachments(_this.currentOrder.id);
        });
        this.deletedAttachmentId = null;
    };
    OrderDetailsComponent.prototype.closeShowAttachment = function () {
        this.showAttachedTxt = false;
        this.showAttachment = false;
        this.showAttachmentImg = false;
        this.showAttachedPdf = false;
        this.currentOrderAttachment = new get_order_attachment_by_orderId_view_1.GetOrderAttachmentByOrderIdView();
        this.currentPdfLink = "";
        this.attachmentTxtText = "";
    };
    OrderDetailsComponent.prototype.checkAttachmentExtensions = function (files) {
        var invalidFiles = [];
        for (var i = 0; i < files.length; i++) {
            this.showSpinner = true;
            var success = false;
            this.constans.allowableExtensions.forEach(function (ex) {
                var index = files[i].name.lastIndexOf(".");
                var extensions = files[i].name.substr(index + 1);
                if (ex == extensions) {
                    success = true;
                    return;
                }
            });
            if (!success) {
                invalidFiles.push(files[i].name);
                this.showSpinner = false;
            }
            if (success) {
                this.saveAttachments(files[i]);
            }
        }
        ;
        return invalidFiles;
    };
    //preview attachments
    OrderDetailsComponent.prototype.openShowAttachment = function (id) {
        var _this = this;
        this.orderAttachmentService.getOrderAttachment(id).subscribe(function (response) {
            _this.currentOrderAttachment = response;
            console.log(_this.currentOrderAttachment);
            if (_this.currentOrderAttachment.fileName.length > 35) {
                _this.currentOrderAttachment.fileName = _this.currentOrderAttachment.fileName.substring(0, 35) + "...";
            }
            _this.constans.allowableImageExtensions.forEach(function (x) {
                if (x === _this.currentOrderAttachment.fileExtantion) {
                    _this.showAttachmentImg = true;
                }
            });
            if (_this.currentOrderAttachment.fileExtantion === _this.constans.attacmentsData.pdf.extension) {
                _this.orderAttachmentService.downloadAttachmentToArray(id).subscribe(function (response) {
                    _this.attachmentPdf = window.URL.createObjectURL(response);
                });
                _this.showAttachedPdf = true;
            }
            if (_this.currentOrderAttachment.fileExtantion === _this.constans.attacmentsData.text.extension) {
                _this.orderAttachmentService.downloadAttachment(id).subscribe(function (data) {
                    _this.attachmentTxtText = data;
                });
                _this.showAttachedTxt = true;
            }
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    //download all attachments
    OrderDetailsComponent.prototype.downloadAllOrderAttachments = function (id) {
        var _this = this;
        this.orderAttachmentService.downloadAllOrderAttachment(id).subscribe(function (response) {
            var zip = response;
            var filename = _this.constans.attacmentsData.zip.filename;
            var blob = new Blob([zip], { type: _this.constans.attacmentsData.zip.type });
            file_saver_1.default(blob, filename);
        });
    };
    OrderDetailsComponent.prototype.inputAdd = function (event) {
        var fileList = event.target.files;
        var invalidFiles = this.checkAttachmentExtensions(fileList);
        if (invalidFiles.length > 0) {
            this.notificationService.showWarning(this.constans.fileExtensionNotAllowedMessage);
        }
    };
    //download files
    OrderDetailsComponent.prototype.downloadOrderAttachment = function (id) {
        var _this = this;
        this.orderAttachmentService.getOrderAttachment(id).subscribe(function (response) {
            _this.currentOrderAttachment = response;
            _this.orderAttachmentService.downloadAttachmentToBlob(id).subscribe(function (result) {
                var file = result;
                var filename = _this.currentOrderAttachment.fileName + _this.currentOrderAttachment.fileExtantion;
                if (_this.currentOrderAttachment.fileExtantion === _this.constans.attacmentsData.pdf.extension) {
                    var blob = new Blob([file], { type: _this.constans.attacmentsData.pdf.type });
                    file_saver_1.default(blob, filename);
                }
                if (_this.currentOrderAttachment.fileExtantion === _this.constans.attacmentsData.imageJpeg.extension) {
                    var blob = new Blob([file], { type: _this.constans.attacmentsData.imageJpeg.type });
                    file_saver_1.default(blob, filename);
                }
                if (_this.currentOrderAttachment.fileExtantion === _this.constans.attacmentsData.imagePng.extension) {
                    var blob = new Blob([file], { type: _this.constans.attacmentsData.imagePng.type });
                    file_saver_1.default(blob, filename);
                }
                if (_this.currentOrderAttachment.fileExtantion === _this.constans.attacmentsData.text.extension) {
                    var blob = new Blob([file], { type: _this.constans.attacmentsData.text.type });
                    file_saver_1.default(blob, filename);
                }
                if (_this.currentOrderAttachment.fileExtantion === _this.constans.attacmentsData.docx.extension) {
                    var blob = new Blob([file], { type: _this.constans.attacmentsData.docx.type });
                    file_saver_1.default(blob, filename);
                }
                if (_this.currentOrderAttachment.fileExtantion === _this.constans.attacmentsData.xlsx.extension) {
                    var blob = new Blob([file], { type: _this.constans.attacmentsData.xlsx.type });
                    file_saver_1.default(blob, filename);
                }
                if (_this.currentOrderAttachment.fileExtantion === _this.constans.attacmentsData.msg.extension) {
                    var blob = new Blob([file], { type: _this.constans.attacmentsData.msg.type });
                    file_saver_1.default(blob, filename);
                }
            });
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    OrderDetailsComponent.prototype.saveAttachments = function (file) {
        var _this = this;
        var formData = new FormData();
        formData.append("file", file, file.name);
        formData.append("orderId", this.currentOrder.id);
        this.orderAttachmentService.createOrderAttachment(formData).subscribe(function (response) {
            _this.notificationService.showSuccess(_this.constans.fileUploadeMessage + " " + file.name);
            _this.getOrderAttachments(_this.currentOrder.id);
        });
        this.openSaveAttachment = false;
        this.formValidation = true;
    };
    OrderDetailsComponent.prototype.getOrderAttachments = function (id) {
        var _this = this;
        console.log(id);
        this.orderAttachmentService.getAllOrderAttachmentsByOrderId(id).subscribe(function (response) {
            _this.orderAttachments = response;
            console.log(_this.orderAttachments);
            _this.showSpinner = false;
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    OrderDetailsComponent.prototype.getOrderDetails = function (orderId) {
        var _this = this;
        this.orderService.getOrderDetails(orderId).subscribe(function (response) {
            _this.currentOrder = response;
            console.log(_this.currentOrder);
            _this.activeEditOrder = false;
            _this.alreadyGetOrderDetails = true;
            console.log(_this.currentOrder);
            _this.getOrderAttachments(_this.currentOrder.id);
        });
    };
    OrderDetailsComponent.prototype.navigateToListOrders = function () {
        this.route.navigate(['projects-and-orders/orders/ordersKanban']);
    };
    OrderDetailsComponent.prototype.showProjectDetailsById = function (id) {
        this.route.navigate(['projects-and-orders/projects/projectdetails', id]);
    };
    OrderDetailsComponent.prototype.activeEditableOrder = function () {
        this.activeEditOrder = true;
    };
    OrderDetailsComponent.prototype.cancelUpdateOrder = function () {
        this.getOrderDetails(this.currentOrder.id);
    };
    OrderDetailsComponent.prototype.getCustomers = function () {
        var _this = this;
        this.customerService.getCustomers().subscribe(function (response) {
            _this.customers = response;
        });
    };
    OrderDetailsComponent.prototype.getProjectManager = function () {
        var _this = this;
        this.projectService.getProjectManager().subscribe(function (response) {
            _this.projectManagers = response;
            console.log(_this.projectManagers);
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    OrderDetailsComponent.prototype.saveChanges = function () {
        var _this = this;
        this.updateOrder.id = this.currentOrder.id;
        this.updateOrder.itemId = this.currentOrder.item.id;
        this.updateOrder.customerId = this.currentOrder.customer.id;
        this.updateOrder.projectId = this.currentOrder.project.id;
        this.updateOrder.date = this.currentOrder.date;
        this.updateOrder.dueDate = this.currentOrder.dueDate;
        this.updateOrder.memo = this.currentOrder.memo;
        this.updateOrder.status = this.currentOrder.status;
        this.updateProjectManager.projectId = this.currentOrder.project.id;
        //this.updateProjectManager.projectManagerId = this.currentOrder.project.user.id;
        this.projectService.reassignProjectManeger(this.updateProjectManager).subscribe(function (response) {
            _this.orderService.updateOrder(_this.updateOrder).subscribe(function (response) {
                _this.getOrderDetails(_this.currentOrder.id);
                _this.notificationService.showSuccess(_this.constans.orderUpdateMessage);
            });
        });
    };
    OrderDetailsComponent = __decorate([
        core_1.Component({
            selector: 'app-order-details',
            templateUrl: './order-details.component.html',
            styleUrls: ['./order-details.component.scss']
        }),
        __metadata("design:paramtypes", [router_1.ActivatedRoute,
            order_service_1.OrderService,
            router_1.Router,
            customer_service_1.CustomerService,
            noitification_service_1.NotificationService,
            projects_and_orders_constans_1.ProjectsAndOrdersConstans,
            project_service_1.ProjectService,
            order_attachment_service_1.OrderAttachmentService,
            order_log_activity_service_1.OrderLogActivityService])
    ], OrderDetailsComponent);
    return OrderDetailsComponent;
}());
exports.OrderDetailsComponent = OrderDetailsComponent;
//# sourceMappingURL=order-details.component.js.map