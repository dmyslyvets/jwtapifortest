"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var ngx_bootstrap_1 = require("ngx-bootstrap");
var tasks_module_routing_module_1 = require("./tasks-module-routing.module");
var tasks_kanban_component_1 = require("./tasks-kanban/tasks-kanban.component");
var tasks_list_component_1 = require("./tasks-list/tasks-list.component");
var task_details_component_1 = require("./task-details/task-details.component");
var create_task_component_1 = require("./create-task/create-task.component");
var material_module_1 = require("app/shared/modules/material.module");
var TasksModuleModule = /** @class */ (function () {
    function TasksModuleModule() {
    }
    TasksModuleModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                tasks_module_routing_module_1.TasksModuleRoutingModule,
                ngx_bootstrap_1.ModalModule.forRoot(),
                material_module_1.MaterialModule
            ],
            declarations: [
                tasks_kanban_component_1.TasksKanbanComponent,
                tasks_list_component_1.TasksListComponent,
                task_details_component_1.TaskDetailsComponent,
                create_task_component_1.CreateTaskComponent
            ],
            exports: [
                task_details_component_1.TaskDetailsComponent
            ],
            entryComponents: [
                task_details_component_1.TaskDetailsComponent,
                create_task_component_1.CreateTaskComponent
            ],
        })
    ], TasksModuleModule);
    return TasksModuleModule;
}());
exports.TasksModuleModule = TasksModuleModule;
//# sourceMappingURL=tasks-module.module.js.map