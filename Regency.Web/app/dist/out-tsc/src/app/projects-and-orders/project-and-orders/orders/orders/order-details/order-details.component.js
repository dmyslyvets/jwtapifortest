"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var projects_and_orders_constans_1 = require("app/shared/constans/projects-and-orders.constans");
var order_service_1 = require("app/shared/providers/services/order.service");
var customer_service_1 = require("app/shared/providers/services/customer.service");
var noitification_service_1 = require("app/shared/providers/services/noitification.service");
var project_service_1 = require("app/shared/providers/services/project.service");
var get_order_view_1 = require("app/shared/models/order/get-order-view");
var get_all_customer_view_1 = require("app/shared/models/customer/get-all-customer.view");
var update_order_view_1 = require("app/shared/models/order/update-order.view");
var get_all_users_view_1 = require("app/shared/models/users/get-all-users.view");
var reassign_project_manager_view_1 = require("app/shared/models/project/reassign-project-manager.view");
var OrderDetailsComponent = /** @class */ (function () {
    function OrderDetailsComponent(activateRoute, orderService, route, customerService, notificationService, constans, projectService) {
        this.activateRoute = activateRoute;
        this.orderService = orderService;
        this.route = route;
        this.customerService = customerService;
        this.notificationService = notificationService;
        this.constans = constans;
        this.projectService = projectService;
        this.alreadyGetOrderDetails = false;
        this.activeEditOrder = false;
        this.currentOrder = new get_order_view_1.GetOrderView();
        this.customers = new get_all_customer_view_1.GetAllCustomerView();
        this.projectManagers = new get_all_users_view_1.GetAllUserView();
        this.updateOrder = new update_order_view_1.UpdateOrderView();
        this.updateProjectManager = new reassign_project_manager_view_1.ReassignProjectManagerView();
        this.orderId = this.activateRoute.snapshot.params['id'];
        this.getOrderDetails(this.orderId);
        this.getCustomers();
        this.getProjectManager();
    }
    OrderDetailsComponent.prototype.ngOnInit = function () {
    };
    OrderDetailsComponent.prototype.getOrderDetails = function (orderId) {
        var _this = this;
        this.orderService.getOrderDetails(orderId).subscribe(function (response) {
            _this.currentOrder = response;
            _this.activeEditOrder = false;
            _this.alreadyGetOrderDetails = true;
            console.log(_this.currentOrder);
        });
    };
    OrderDetailsComponent.prototype.navigateToListOrders = function () {
        this.route.navigate(['projects-and-orders/orders/ordersList']);
    };
    OrderDetailsComponent.prototype.showProjectDetailsById = function (id) {
        this.route.navigate(['projects-and-orders/projects/projectdetails', id]);
    };
    OrderDetailsComponent.prototype.activeEditableOrder = function () {
        this.activeEditOrder = true;
    };
    OrderDetailsComponent.prototype.cancelUpdateOrder = function () {
        this.getOrderDetails(this.currentOrder.id);
    };
    OrderDetailsComponent.prototype.getCustomers = function () {
        var _this = this;
        this.customerService.getCustomers().subscribe(function (response) {
            _this.customers = response;
        });
    };
    OrderDetailsComponent.prototype.getProjectManager = function () {
        var _this = this;
        this.projectService.getProjectManager().subscribe(function (response) {
            _this.projectManagers = response;
            console.log(_this.projectManagers);
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    OrderDetailsComponent.prototype.saveChanges = function () {
        var _this = this;
        this.updateOrder.id = this.currentOrder.id;
        this.updateOrder.itemId = this.currentOrder.item.id;
        this.updateOrder.customerId = this.currentOrder.customer.id;
        this.updateOrder.projectId = this.currentOrder.project.id;
        this.updateOrder.date = this.currentOrder.date;
        this.updateOrder.dueDate = this.currentOrder.dueDate;
        this.updateOrder.memo = this.currentOrder.memo;
        this.updateOrder.status = this.currentOrder.status;
        this.updateProjectManager.projectId = this.currentOrder.project.id;
        this.updateProjectManager.projectManagerId = this.currentOrder.project.user.id;
        this.projectService.reassignProjectManeger(this.updateProjectManager).subscribe(function (response) {
            _this.orderService.updateOrder(_this.updateOrder).subscribe(function (response) {
                _this.getOrderDetails(_this.currentOrder.id);
                _this.notificationService.showSuccess(_this.constans.orderUpdateMessage);
            });
        });
    };
    OrderDetailsComponent = __decorate([
        core_1.Component({
            selector: 'app-order-details',
            templateUrl: './order-details.component.html',
            styleUrls: ['./order-details.component.scss']
        }),
        __metadata("design:paramtypes", [router_1.ActivatedRoute,
            order_service_1.OrderService,
            router_1.Router,
            customer_service_1.CustomerService,
            noitification_service_1.NotificationService,
            projects_and_orders_constans_1.ProjectsAndOrdersConstans,
            project_service_1.ProjectService])
    ], OrderDetailsComponent);
    return OrderDetailsComponent;
}());
exports.OrderDetailsComponent = OrderDetailsComponent;
//# sourceMappingURL=order-details.component.js.map