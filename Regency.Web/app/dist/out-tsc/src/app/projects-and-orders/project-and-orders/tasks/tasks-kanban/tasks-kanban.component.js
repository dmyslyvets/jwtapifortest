"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ngx_bootstrap_1 = require("ngx-bootstrap");
var task_details_component_1 = require("../task-details/task-details.component");
var create_task_component_1 = require("../create-task/create-task.component");
var router_1 = require("@angular/router");
var TasksKanbanComponent = /** @class */ (function () {
    function TasksKanbanComponent(modalService, route, activateRoute) {
        this.modalService = modalService;
        this.route = route;
        this.activateRoute = activateRoute;
        this.selected = 'option1';
    }
    TasksKanbanComponent.prototype.ngOnInit = function () {
    };
    TasksKanbanComponent.prototype.openTaskDetails = function (id, event) {
        event.preventDefault();
        var initialState = {
            taskDetails: "task details" //task details view
        };
        this.modalRef = this.modalService.show(task_details_component_1.TaskDetailsComponent, Object.assign({}, { class: 'gray modal-lg', initialState: initialState }));
        this.modalRef.content.closeBtnName = 'Close';
    };
    TasksKanbanComponent.prototype.openCreateTask = function (event) {
        event.preventDefault();
        this.modalRef = this.modalService.show(create_task_component_1.CreateTaskComponent, Object.assign({}, { class: 'gray modal-lg' }));
        this.modalRef.content.closeBtnName = 'Close';
    };
    TasksKanbanComponent.prototype.showListView = function () {
        this.route.navigate(['../tasksList'], { relativeTo: this.activateRoute });
    };
    TasksKanbanComponent = __decorate([
        core_1.Component({
            selector: 'app-tasks-kanban',
            templateUrl: './tasks-kanban.component.html',
            styleUrls: ['./tasks-kanban.component.scss']
        }),
        __metadata("design:paramtypes", [ngx_bootstrap_1.BsModalService,
            router_1.Router,
            router_1.ActivatedRoute])
    ], TasksKanbanComponent);
    return TasksKanbanComponent;
}());
exports.TasksKanbanComponent = TasksKanbanComponent;
//# sourceMappingURL=tasks-kanban.component.js.map