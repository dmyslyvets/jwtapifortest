"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var projects_and_orders_constans_1 = require("app/shared/constans/projects-and-orders.constans");
var order_service_1 = require("app/shared/providers/services/order.service");
var load_service_1 = require("app/shared/providers/services/load.service");
var noitification_service_1 = require("app/shared/providers/services/noitification.service");
var get_all_order_view_1 = require("app/shared/models/order/get-all-order.view");
var update_order_view_1 = require("app/shared/models/order/update-order.view");
var OrderListComponent = /** @class */ (function () {
    function OrderListComponent(route, orderService, notificationService, loadService, constans) {
        this.route = route;
        this.orderService = orderService;
        this.notificationService = notificationService;
        this.loadService = loadService;
        this.constans = constans;
        this.newOrder = [];
        this.activeOrder = [];
        this.processingOrder = [];
        this.doneOrder = [];
        this.allOrders = new get_all_order_view_1.GetAllOrderView();
        this.updateOrder = new update_order_view_1.UpdateOrderView();
        this.orderTaskKanbanIsShow = false;
        this.orderTaskListIsShow = false;
    }
    OrderListComponent.prototype.ngOnInit = function () {
        this.loadService.set(true);
        this.getAllOrders();
    };
    OrderListComponent.prototype.getAllOrders = function () {
        var _this = this;
        this.orderService.getAllOrders().subscribe(function (response) {
            _this.allOrders = response;
            _this.ordersCount = response.orders.length;
            console.log(_this.allOrders);
            _this.sortOrderByStatus(_this.allOrders.orders);
            _this.loadService.set(false);
        }, function (error) {
            _this.errors = error;
            _this.loadService.set(false);
        });
    };
    OrderListComponent.prototype.sortOrderByStatus = function (allOrders) {
        var _this = this;
        this.newOrder = allOrders.filter(function (x) { return x.status == _this.constans.status.new; });
        this.activeOrder = allOrders.filter(function (x) { return x.status == _this.constans.status.active; });
        this.processingOrder = allOrders.filter(function (x) { return x.status == _this.constans.status.processing; });
        this.doneOrder = allOrders.filter(function (x) { return x.status == _this.constans.status.done; });
    };
    OrderListComponent.prototype.navigateToCreateOrder = function () {
        this.route.navigate(['projects-and-orders/orders/createOrder']);
    };
    OrderListComponent.prototype.showOrdersDetails = function (id) {
        this.route.navigate(['projects-and-orders/orders/orderDetails', id]);
    };
    OrderListComponent.prototype.showKanbanView = function () {
        this.route.navigate(["projects-and-orders/orders/ordersKanban"]);
    };
    OrderListComponent.prototype.showOrderTask = function (orderId) {
        this.currentOrderId = orderId;
        console.log("item id");
        console.log(orderId);
        this.orderTaskKanbanIsShow = true;
    };
    OrderListComponent.prototype.hideOrderTasks = function () {
        this.currentOrderId = null;
        this.orderTaskKanbanIsShow = false;
    };
    OrderListComponent.prototype.callBack = function (event) {
        event.stopPropagation();
        alert('hello');
    };
    OrderListComponent = __decorate([
        core_1.Component({
            selector: 'orderList',
            templateUrl: './order-list.component.html',
            styleUrls: ['./order-list.component.scss']
        }),
        __metadata("design:paramtypes", [router_1.Router,
            order_service_1.OrderService,
            noitification_service_1.NotificationService,
            load_service_1.LoadService,
            projects_and_orders_constans_1.ProjectsAndOrdersConstans])
    ], OrderListComponent);
    return OrderListComponent;
}());
exports.OrderListComponent = OrderListComponent;
//# sourceMappingURL=order-list.component.js.map