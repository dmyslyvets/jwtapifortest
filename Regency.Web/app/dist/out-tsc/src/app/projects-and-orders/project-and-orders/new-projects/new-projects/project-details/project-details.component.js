"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var file_saver_1 = require("file-saver");
var router_1 = require("@angular/router");
var drag_drop_1 = require("@angular/cdk/drag-drop");
var ng2_file_upload_1 = require("ng2-file-upload");
var projects_and_orders_constans_1 = require("app/shared/constans/projects-and-orders.constans");
var project_service_1 = require("app/shared/providers/services/project.service");
var customer_service_1 = require("app/shared/providers/services/customer.service");
var noitification_service_1 = require("app/shared/providers/services/noitification.service");
var project_attachment_service_1 = require("app/shared/providers/services/project-attachment.service");
var load_service_1 = require("app/shared/providers/services/load.service");
var order_service_1 = require("app/shared/providers/services/order.service");
var get_all_users_view_1 = require("app/shared/models/users/get-all-users.view");
var get_all_customer_view_1 = require("app/shared/models/customer/get-all-customer.view");
var update_project_view_1 = require("app/shared/models/project/update-project.view");
var get_all_by_project_id_projects_attachments_view_1 = require("app/shared/models/project-attachment/get-all-by-project-id-projects-attachments-view");
var delete_project_attachment_view_1 = require("app/shared/models/project-attachment/delete-project-attachment.view");
var get_all_project_types_view_1 = require("app/shared/models/project/get-all-project-types.view");
var get_all_order_by_projectId_view_1 = require("app/shared/models/order/get-all-order-by-projectId.view");
var get_all_active_project_view_1 = require("app/shared/models/project/get-all-active-project.view");
var get_project_attachment_view_1 = require("app/shared/models/project-attachment/get-project-attachment.view");
var update_order_view_1 = require("app/shared/models/order/update-order.view");
var ProjectDetailsComponent = /** @class */ (function () {
    function ProjectDetailsComponent(route, notificationService, loadService, customerService, projectAttachmentService, constans, projectService, ordersService) {
        this.route = route;
        this.notificationService = notificationService;
        this.loadService = loadService;
        this.customerService = customerService;
        this.projectAttachmentService = projectAttachmentService;
        this.constans = constans;
        this.projectService = projectService;
        this.ordersService = ordersService;
        this.closeDetailsComponent = new core_1.EventEmitter();
        this.updateProjectEvent = new core_1.EventEmitter();
        this.updateProject = new update_project_view_1.UpdateProjectView();
        this.projectManagers = new get_all_users_view_1.GetAllUserView();
        this.currentProject = new get_all_active_project_view_1.GetAllActiveProjectViewItem();
        this.projectTypesView = new get_all_project_types_view_1.GetAllProjectTypesView();
        this.customers = new get_all_customer_view_1.GetAllCustomerView();
        this.projectAttachments = new get_all_by_project_id_projects_attachments_view_1.GetAllByProjectIdProjectAttachmentsView();
        this.activeEditProject = false;
        this.showSpinner = false;
        //popups
        this.formValidation = false;
        this.openSaveAttachment = false;
        this.openDeleteAttachmentModal = false;
        this.droppedFiles = [];
        this.deletedAttachment = new delete_project_attachment_view_1.DeleteProjectAttachmentView();
        this.hasBaseDropZoneOver = false;
        //view attachment
        this.uploader = new ng2_file_upload_1.FileUploader({ url: this.constans.URL });
        this.showAttachmentImg = false;
        this.showAttachedPdf = false;
        this.showAttachedTxt = false;
        this.showAttachment = false;
        this.currentProjectAttachment = new get_project_attachment_view_1.GetProjectAttachmentView();
        //orders
        this.ordersByProjectId = new get_all_order_by_projectId_view_1.GetAllOrdersByProjectIdView();
        this.newOrder = [];
        this.activeOrder = [];
        this.processingOrder = [];
        this.doneOrder = [];
        this.updateOrder = new update_order_view_1.UpdateOrderView();
    }
    ProjectDetailsComponent.prototype.closeDetails = function () {
        this.closeDetailsComponent.emit(false);
    };
    ProjectDetailsComponent.prototype.ngOnChanges = function () {
        this.loadService.set(true);
        this.showProjectDetailsById(this.projectId);
        this.getProjectManager();
        this.getProjectTypes();
        this.getCustomers();
        this.getAllOrdersByProjectId(this.projectId);
    };
    ProjectDetailsComponent.prototype.getProjectManager = function () {
        var _this = this;
        this.projectService.getProjectManager().subscribe(function (response) {
            _this.projectManagers = response;
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    ProjectDetailsComponent.prototype.showOrdersDetails = function (id) {
        this.route.navigate(['projects-and-orders/orders/orderDetails', id]);
    };
    ProjectDetailsComponent.prototype.drop = function (event) {
        var _this = this;
        if (event.previousContainer === event.container) {
            drag_drop_1.moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
        }
        else {
            drag_drop_1.transferArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.currentIndex);
            this.curentOrder = event.container.data[event.currentIndex];
            if (event.container.id == this.constans.kanbanBlockStatus.new) {
                this.curentOrder.status = this.constans.status.new;
            }
            if (event.container.id == this.constans.kanbanBlockStatus.active) {
                this.curentOrder.status = this.constans.status.active;
            }
            if (event.container.id == this.constans.kanbanBlockStatus.processing) {
                this.curentOrder.status = this.constans.status.processing;
            }
            if (event.container.id == this.constans.kanbanBlockStatus.done) {
                this.curentOrder.status = this.constans.status.done;
            }
            this.updateOrder.id = this.curentOrder.id;
            this.updateOrder.date = this.curentOrder.date;
            this.updateOrder.dueDate = this.curentOrder.dueDate;
            this.updateOrder.memo = this.curentOrder.memo;
            this.updateOrder.status = this.curentOrder.status;
            this.updateOrder.customerId = this.curentOrder.customer.id;
            this.updateOrder.projectId = this.curentOrder.project.id;
            this.updateOrder.itemId = this.curentOrder.item.id;
            this.ordersService.updateOrder(this.updateOrder).subscribe(function (response) {
                _this.notificationService.showSuccess(_this.constans.orderUpdateMessage);
            });
        }
    };
    ProjectDetailsComponent.prototype.getAllOrdersByProjectId = function (projectId) {
        var _this = this;
        this.ordersService.getAllOrdersByProjectId(projectId).subscribe(function (response) {
            _this.ordersByProjectId = response;
            console.log(_this.ordersByProjectId);
            _this.sortOrderByStatus(_this.ordersByProjectId.orders);
        }, function (error) {
            _this.errors = error;
        });
    };
    ProjectDetailsComponent.prototype.sortOrderByStatus = function (allOrders) {
        var _this = this;
        this.newOrder = allOrders.filter(function (x) { return x.status == _this.constans.status.new; });
        this.activeOrder = allOrders.filter(function (x) { return x.status == _this.constans.status.active; });
        this.processingOrder = allOrders.filter(function (x) { return x.status == _this.constans.status.processing; });
        this.doneOrder = allOrders.filter(function (x) { return x.status == _this.constans.status.done; });
    };
    ProjectDetailsComponent.prototype.getProjectTypes = function () {
        var _this = this;
        this.projectService.getAllProjectTypes().subscribe(function (response) {
            _this.projectTypesView = response;
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    ProjectDetailsComponent.prototype.displayFn = function (val) {
        return val ? val.name : val;
    };
    ProjectDetailsComponent.prototype.selectedProjectType = function (item) {
        this.currentProject.projectType.id = item.option.value.id;
        this.currentProject.projectType.name = item.option.value.name;
    };
    ProjectDetailsComponent.prototype.typedProjectType = function (event) {
        this.currentProject.projectType.name = '';
        this.currentProject.projectType.id = null;
        this.currentProject.projectType.name = event.target.value;
    };
    ProjectDetailsComponent.prototype.getCustomers = function () {
        var _this = this;
        this.customerService.getCustomers().subscribe(function (response) {
            _this.customers = response;
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    ProjectDetailsComponent.prototype.activateEditProject = function () {
        this.activeEditProject = true;
    };
    ProjectDetailsComponent.prototype.getProjectAttachments = function (id) {
        var _this = this;
        this.projectAttachmentService.getAllProjectsAttachmentsByProjectId(id).subscribe(function (response) {
            _this.projectAttachments = response;
            _this.showSpinner = false;
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    ProjectDetailsComponent.prototype.showProjectDetailsById = function (id) {
        var _this = this;
        this.projectService.getProjectById(id).subscribe(function (response) {
            _this.currentProject = response;
            if (_this.currentProject.name && _this.currentProject.name.length > 40) {
                _this.currentProject.name = _this.currentProject.name.substring(0, 40) + "...";
            }
            _this.activeEditProject = false;
            _this.loadService.set(false);
        });
        this.getProjectAttachments(id);
    };
    ProjectDetailsComponent.prototype.canceltProjectChange = function () {
        this.showProjectDetailsById(this.currentProject.id);
    };
    ProjectDetailsComponent.prototype.saveProjectChange = function () {
        var _this = this;
        if (!this.currentProject.name) {
            this.currentProject.name = "";
        }
        this.updateProject = this.currentProject;
        if (this.updateDate != null) {
            this.updateProject.date = this.updateDate;
        }
        if (this.updateDate == null) {
            this.updateProject.date = this.currentProject.date;
        }
        this.projectService.updateProject(this.updateProject).subscribe(function (response) {
            _this.showProjectDetailsById(_this.currentProject.id);
            _this.notificationService.showSuccess(_this.constans.projectUpdateMessage);
            _this.updateProjectEvent.emit(_this.currentProject.id);
        }),
            function (error) {
                _this.notificationService.showError(error.error);
            };
    };
    //attachments
    ProjectDetailsComponent.prototype.openShowAttachment = function (id) {
        var _this = this;
        this.projectAttachmentService.getProjectAttachment(id).subscribe(function (response) {
            _this.currentProjectAttachment = response;
            if (_this.currentProjectAttachment.fileName.length > 35) {
                _this.currentProjectAttachment.fileName = _this.currentProjectAttachment.fileName.substring(0, 35) + "...";
            }
            _this.constans.allowableImageExtensions.forEach(function (x) {
                if (x === _this.currentProjectAttachment.fileExtantion) {
                    _this.showAttachmentImg = true;
                }
            });
            if (_this.currentProjectAttachment.fileExtantion === _this.constans.attacmentsData.pdf.extension) {
                _this.projectAttachmentService.downloadAttachmentToArray(id).subscribe(function (response) {
                    _this.attachmentPdf = window.URL.createObjectURL(response);
                });
                _this.showAttachedPdf = true;
            }
            if (_this.currentProjectAttachment.fileExtantion === _this.constans.attacmentsData.text.extension) {
                _this.projectAttachmentService.downloadAttachment(id).subscribe(function (data) {
                    _this.attachmentTxtText = data;
                });
                _this.showAttachedTxt = true;
            }
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    ProjectDetailsComponent.prototype.closeShowAttachment = function () {
        this.showAttachedTxt = false;
        this.showAttachment = false;
        this.showAttachmentImg = false;
        this.showAttachedPdf = false;
        this.currentProjectAttachment = new get_project_attachment_view_1.GetProjectAttachmentView();
        this.currentPdfLink = "";
        this.attachmentTxtText = "";
    };
    ProjectDetailsComponent.prototype.openDeleteAttachment = function (id) {
        this.deletedAttachmentId = id;
        this.openDeleteAttachmentModal = true;
    };
    ProjectDetailsComponent.prototype.cancelDeleteAttachment = function () {
        this.deletedAttachmentId = null;
        this.openDeleteAttachmentModal = false;
    };
    ProjectDetailsComponent.prototype.yesDeleteAttachment = function () {
        this.deleteAttachment();
        this.openDeleteAttachmentModal = false;
    };
    ProjectDetailsComponent.prototype.openSaveAttachmentModal = function () {
        this.openSaveAttachment = true;
        this.formValidation = false;
        this.droppedFiles = [];
    };
    ProjectDetailsComponent.prototype.closeSaveAttachmentModal = function () {
        this.openSaveAttachment = false;
        this.formValidation = false;
    };
    ProjectDetailsComponent.prototype.checkAttachmentExtensions = function (files) {
        var invalidFiles = [];
        for (var i = 0; i < files.length; i++) {
            this.showSpinner = true;
            var success = false;
            this.constans.allowableExtensions.forEach(function (ex) {
                var index = files[i].name.lastIndexOf(".");
                var extensions = files[i].name.substr(index + 1);
                if (ex == extensions) {
                    success = true;
                    return;
                }
            });
            if (!success) {
                invalidFiles.push(files[i].name);
                this.showSpinner = false;
            }
            if (success) {
                this.saveAttachments(files[i]);
            }
        }
        ;
        return invalidFiles;
    };
    ProjectDetailsComponent.prototype.dropped = function (fileList) {
        var invalidFiles = this.checkAttachmentExtensions(fileList);
        if (invalidFiles.length > 0) {
            this.notificationService.showWarning(this.constans.fileExtensionNotAllowedMessage);
        }
    };
    ProjectDetailsComponent.prototype.inputAdd = function (event) {
        var fileList = event.target.files;
        var invalidFiles = this.checkAttachmentExtensions(fileList);
        if (invalidFiles.length > 0) {
            this.notificationService.showWarning(this.constans.fileExtensionNotAllowedMessage);
        }
    };
    ProjectDetailsComponent.prototype.fileLeave = function (file) {
        var index = this.droppedFiles.indexOf(file);
        if (index > -1) {
            this.droppedFiles.splice(index, 1);
        }
    };
    ProjectDetailsComponent.prototype.fileOver = function (event) { };
    ProjectDetailsComponent.prototype.fileOverBase = function (event) {
        this.hasBaseDropZoneOver = event;
    };
    ProjectDetailsComponent.prototype.saveAttachments = function (file) {
        var _this = this;
        var userId = "9c4989af-de13-4852-9fa8-d5a40d851a75";
        var formData = new FormData();
        formData.append("file", file, file.name);
        formData.append("userId", userId);
        formData.append("projectId", this.currentProject.id);
        this.projectAttachmentService.createProjectAttachment(formData).subscribe(function (response) {
            _this.notificationService.showSuccess(_this.constans.fileUploadeMessage + " " + file.name);
            _this.inputFileElement.nativeElement.value = "";
            _this.getProjectAttachments(_this.currentProject.id);
        });
        this.openSaveAttachment = false;
        this.formValidation = true;
    };
    //download files
    ProjectDetailsComponent.prototype.downloadProjectAttachment = function (id) {
        var _this = this;
        this.projectAttachmentService.getProjectAttachment(id).subscribe(function (response) {
            _this.currentProjectAttachment = response;
            _this.projectAttachmentService.downloadAttachmentToBlob(id).subscribe(function (result) {
                var file = result;
                var filename = _this.currentProjectAttachment.fileName + _this.currentProjectAttachment.fileExtantion;
                if (_this.currentProjectAttachment.fileExtantion === _this.constans.attacmentsData.pdf.extension) {
                    var blob = new Blob([file], { type: _this.constans.attacmentsData.pdf.type });
                    file_saver_1.default(blob, filename);
                }
                if (_this.currentProjectAttachment.fileExtantion === _this.constans.attacmentsData.imageJpeg.extension) {
                    var blob = new Blob([file], { type: _this.constans.attacmentsData.imageJpeg.type });
                    file_saver_1.default(blob, filename);
                }
                if (_this.currentProjectAttachment.fileExtantion === _this.constans.attacmentsData.imagePng.extension) {
                    var blob = new Blob([file], { type: _this.constans.attacmentsData.imagePng.type });
                    file_saver_1.default(blob, filename);
                }
                if (_this.currentProjectAttachment.fileExtantion === _this.constans.attacmentsData.text.extension) {
                    var blob = new Blob([file], { type: _this.constans.attacmentsData.text.type });
                    file_saver_1.default(blob, filename);
                }
                if (_this.currentProjectAttachment.fileExtantion === _this.constans.attacmentsData.docx.extension) {
                    var blob = new Blob([file], { type: _this.constans.attacmentsData.docx.type });
                    file_saver_1.default(blob, filename);
                }
                if (_this.currentProjectAttachment.fileExtantion === _this.constans.attacmentsData.xlsx.extension) {
                    var blob = new Blob([file], { type: _this.constans.attacmentsData.xlsx.type });
                    file_saver_1.default(blob, filename);
                }
                if (_this.currentProjectAttachment.fileExtantion === _this.constans.attacmentsData.msg.extension) {
                    var blob = new Blob([file], { type: _this.constans.attacmentsData.msg.type });
                    file_saver_1.default(blob, filename);
                }
            });
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    ProjectDetailsComponent.prototype.downloadAllProjectAttachment = function (id) {
        var _this = this;
        this.projectAttachmentService.downloadAllProjectsAttachment(id).subscribe(function (response) {
            var zip = response;
            var filename = _this.constans.attacmentsData.zip.filename;
            var blob = new Blob([zip], { type: _this.constans.attacmentsData.zip.type });
            file_saver_1.default(blob, filename);
        });
    };
    ProjectDetailsComponent.prototype.downloadFromBlob = function (link) {
        window.open(link);
    };
    ProjectDetailsComponent.prototype.deleteAttachment = function () {
        var _this = this;
        this.deletedAttachment.id = this.deletedAttachmentId;
        this.projectAttachmentService.deleteAttachment(this.deletedAttachment).subscribe(function (response) {
            _this.notificationService.showSuccess(_this.constans.atachmentDeleteMessage);
            _this.getProjectAttachments(_this.projectAttachments.projectId);
        });
        this.deletedAttachmentId = null;
    };
    __decorate([
        core_1.Input('projectId'),
        __metadata("design:type", String)
    ], ProjectDetailsComponent.prototype, "projectId", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], ProjectDetailsComponent.prototype, "closeDetailsComponent", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], ProjectDetailsComponent.prototype, "updateProjectEvent", void 0);
    __decorate([
        core_1.ViewChild('fileUploader'),
        __metadata("design:type", core_1.ElementRef)
    ], ProjectDetailsComponent.prototype, "inputFileElement", void 0);
    ProjectDetailsComponent = __decorate([
        core_1.Component({
            selector: 'app-project-details',
            templateUrl: './project-details.component.html',
            styleUrls: ['./project-details.component.scss']
        }),
        __metadata("design:paramtypes", [router_1.Router,
            noitification_service_1.NotificationService,
            load_service_1.LoadService,
            customer_service_1.CustomerService,
            project_attachment_service_1.ProjectAttachmentservice,
            projects_and_orders_constans_1.ProjectsAndOrdersConstans,
            project_service_1.ProjectService,
            order_service_1.OrderService])
    ], ProjectDetailsComponent);
    return ProjectDetailsComponent;
}());
exports.ProjectDetailsComponent = ProjectDetailsComponent;
//# sourceMappingURL=project-details.component.js.map