"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var get_all_order_tasks_by_order_id_view_1 = require("app/shared/models/order-tasks/get-all-order-tasks-by-order-id.view");
var order_tasks_service_1 = require("app/shared/providers/services/order-tasks.service");
var load_service_1 = require("app/shared/providers/services/load.service");
var OrderTasksKanbanComponent = /** @class */ (function () {
    function OrderTasksKanbanComponent(orderTastService, loaderService) {
        this.orderTastService = orderTastService;
        this.loaderService = loaderService;
        this.allOrderTasksByOrderId = new get_all_order_tasks_by_order_id_view_1.GetAllOrderTasksByOrderIdView();
    }
    OrderTasksKanbanComponent.prototype.ngOnChanges = function () {
        this.loaderService.set(true);
        this.getAllOrderTaskByOrderId(this.orderId);
    };
    OrderTasksKanbanComponent.prototype.getAllOrderTaskByOrderId = function (orderId) {
        var _this = this;
        this.orderTastService.getAllOrderTasksByOrderId(orderId).subscribe(function (response) {
            _this.allOrderTasksByOrderId = response;
            _this.loaderService.set(false);
            console.log(_this.allOrderTasksByOrderId);
        });
    };
    __decorate([
        core_1.Input('orderId'),
        __metadata("design:type", String)
    ], OrderTasksKanbanComponent.prototype, "orderId", void 0);
    OrderTasksKanbanComponent = __decorate([
        core_1.Component({
            selector: 'app-order-tasks-kanban',
            templateUrl: './order-tasks-kanban.component.html',
            styleUrls: ['./order-tasks-kanban.component.scss']
        }),
        __metadata("design:paramtypes", [order_tasks_service_1.OrderTasksService, load_service_1.LoadService])
    ], OrderTasksKanbanComponent);
    return OrderTasksKanbanComponent;
}());
exports.OrderTasksKanbanComponent = OrderTasksKanbanComponent;
//# sourceMappingURL=order-tasks-kanban.component.js.map