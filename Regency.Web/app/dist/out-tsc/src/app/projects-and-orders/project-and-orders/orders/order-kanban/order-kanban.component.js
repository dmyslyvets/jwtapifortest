"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var drag_drop_1 = require("@angular/cdk/drag-drop");
var router_1 = require("@angular/router");
var projects_and_orders_constans_1 = require("app/shared/constans/projects-and-orders.constans");
var order_service_1 = require("app/shared/providers/services/order.service");
var load_service_1 = require("app/shared/providers/services/load.service");
var noitification_service_1 = require("app/shared/providers/services/noitification.service");
var get_all_order_view_1 = require("app/shared/models/order/get-all-order.view");
var update_order_view_1 = require("app/shared/models/order/update-order.view");
var OrderKanbanComponent = /** @class */ (function () {
    function OrderKanbanComponent(route, orderService, notificationService, loadService, constans) {
        this.route = route;
        this.orderService = orderService;
        this.notificationService = notificationService;
        this.loadService = loadService;
        this.constans = constans;
        this.newOrder = [];
        this.activeOrder = [];
        this.processingOrder = [];
        this.doneOrder = [];
        this.allOrders = new get_all_order_view_1.GetAllOrderView();
        this.updateOrder = new update_order_view_1.UpdateOrderView();
    }
    OrderKanbanComponent.prototype.ngOnInit = function () {
        this.loadService.set(true);
        this.getAllOrders();
    };
    OrderKanbanComponent.prototype.showOrdersDetails = function (id) {
        this.route.navigate(['projects-and-orders/orders/orderDetails', id]);
    };
    OrderKanbanComponent.prototype.drop = function (event) {
        var _this = this;
        if (event.previousContainer === event.container) {
            drag_drop_1.moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
        }
        else {
            drag_drop_1.transferArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.currentIndex);
            this.curentOrder = event.container.data[event.currentIndex];
            if (event.container.id == this.constans.kanbanBlockStatus.new) {
                this.curentOrder.status = this.constans.status.new;
            }
            if (event.container.id == this.constans.kanbanBlockStatus.active) {
                this.curentOrder.status = this.constans.status.active;
            }
            if (event.container.id == this.constans.kanbanBlockStatus.processing) {
                this.curentOrder.status = this.constans.status.processing;
            }
            if (event.container.id == this.constans.kanbanBlockStatus.done) {
                this.curentOrder.status = this.constans.status.done;
            }
            this.updateOrder.id = this.curentOrder.id;
            this.updateOrder.date = this.curentOrder.date;
            this.updateOrder.dueDate = this.curentOrder.dueDate;
            this.updateOrder.memo = this.curentOrder.memo;
            this.updateOrder.status = this.curentOrder.status;
            this.updateOrder.customerId = this.curentOrder.customer.id;
            this.updateOrder.projectId = this.curentOrder.project.id;
            this.updateOrder.itemId = this.curentOrder.item.id;
            this.updateOrder.confirmedCustomerQuoteId = this.curentOrder.confirmedCustomerQuoteId;
            this.updateOrder.confirmedVendorQuoteId = this.curentOrder.confirmedVendorQuoteId;
            console.log(this.updateOrder);
            this.orderService.updateOrder(this.updateOrder).subscribe(function (response) {
                _this.notificationService.showSuccess(_this.constans.orderUpdateMessage);
            });
        }
    };
    OrderKanbanComponent.prototype.getAllOrders = function () {
        var _this = this;
        this.orderService.getAllOrders().subscribe(function (response) {
            _this.allOrders = response;
            _this.ordersCount = response.orders.length;
            console.log(_this.allOrders);
            _this.sortOrderByStatus(_this.allOrders.orders);
            _this.loadService.set(false);
        }, function (error) {
            _this.errors = error;
            _this.loadService.set(false);
        });
    };
    OrderKanbanComponent.prototype.sortOrderByStatus = function (allOrders) {
        var _this = this;
        this.newOrder = allOrders.filter(function (x) { return x.status == _this.constans.status.new; });
        this.activeOrder = allOrders.filter(function (x) { return x.status == _this.constans.status.active; });
        this.processingOrder = allOrders.filter(function (x) { return x.status == _this.constans.status.processing; });
        this.doneOrder = allOrders.filter(function (x) { return x.status == _this.constans.status.done; });
    };
    OrderKanbanComponent.prototype.navigateToCreateOrder = function () {
        this.route.navigate(['projects-and-orders/orders/createOrder']);
    };
    OrderKanbanComponent.prototype.showListView = function () {
        this.route.navigate(["projects-and-orders/orders/ordersList"]);
    };
    OrderKanbanComponent.prototype.callBack = function (event) {
        event.stopPropagation();
        alert('hello');
    };
    OrderKanbanComponent = __decorate([
        core_1.Component({
            selector: 'app-order-kanban',
            templateUrl: './order-kanban.component.html',
            styleUrls: ['./order-kanban.component.scss']
        }),
        __metadata("design:paramtypes", [router_1.Router,
            order_service_1.OrderService,
            noitification_service_1.NotificationService,
            load_service_1.LoadService,
            projects_and_orders_constans_1.ProjectsAndOrdersConstans])
    ], OrderKanbanComponent);
    return OrderKanbanComponent;
}());
exports.OrderKanbanComponent = OrderKanbanComponent;
//# sourceMappingURL=order-kanban.component.js.map