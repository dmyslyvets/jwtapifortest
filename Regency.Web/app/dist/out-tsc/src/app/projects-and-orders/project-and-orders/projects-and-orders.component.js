"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var projects_and_orders_constans_1 = require("app/shared/constans/projects-and-orders.constans");
var ProjectsAndOrdersComponent = /** @class */ (function () {
    function ProjectsAndOrdersComponent(router, constans) {
        this.router = router;
        this.constans = constans;
    }
    ProjectsAndOrdersComponent.prototype.ngOnInit = function () {
        this.selectedTab();
    };
    ProjectsAndOrdersComponent.prototype.selectedTab = function () {
        var currentRouteLabel = this.router.url.substring(this.router.url.lastIndexOf('/'));
        if (currentRouteLabel == this.constans.tabsData.projects.routeLabel) {
            this.selectedIndex = this.constans.tabsData.projects.index;
        }
        if (currentRouteLabel == this.constans.tabsData.ordersKanban.routeLabel) {
            this.selectedIndex = this.constans.tabsData.ordersKanban.index;
        }
        if (currentRouteLabel == this.constans.tabsData.ordersList.routeLabel) {
            this.selectedIndex = this.constans.tabsData.ordersList.index;
        }
        if (currentRouteLabel == this.constans.tabsData.tasksKanban.routeLabel) {
            this.selectedIndex = this.constans.tabsData.tasksKanban.index;
        }
        if (currentRouteLabel == this.constans.tabsData.tasksList.routeLabel) {
            this.selectedIndex = this.constans.tabsData.tasksList.index;
        }
        // if (currentRouteLabel == this.constans.tabsData.newProjects.routeLabel) {
        //   this.selectedIndex = this.constans.tabsData.newProjects.index;
        // }
    };
    ProjectsAndOrdersComponent.prototype.navigate = function (event) {
        if (event.tab.textLabel == this.constans.tabsData.projects.label) {
            this.selectedIndex = this.constans.tabsData.projects.index;
            this.router.navigate(['projects-and-orders', 'newProjects']);
        }
        if (event.tab.textLabel == this.constans.tabsData.ordersKanban.label) {
            this.selectedIndex = this.constans.tabsData.ordersKanban.index;
            this.router.navigate(['projects-and-orders', 'orders']);
        }
        if (event.tab.textLabel == this.constans.tabsData.ordersList.label) {
            this.selectedIndex = this.constans.tabsData.ordersList.index;
            this.router.navigate(['projects-and-orders', 'orders']);
        }
        if (event.tab.textLabel == this.constans.tabsData.tasksKanban.label) {
            this.selectedIndex = this.constans.tabsData.tasksKanban.index;
            this.router.navigate(['projects-and-orders', 'tasks']);
        }
        if (event.tab.textLabel == this.constans.tabsData.tasksList.label) {
            this.selectedIndex = this.constans.tabsData.tasksList.index;
            this.router.navigate(['projects-and-orders', 'tasks']);
        }
        // if (event.tab.textLabel == this.constans.tabsData.newProjects.label) {
        //   this.selectedIndex = this.constans.tabsData.newProjects.index;
        //   this.router.navigate(['projects-and-orders', 'newProjects']);
        // }
    };
    ProjectsAndOrdersComponent = __decorate([
        core_1.Component({
            selector: 'projects-and-orders',
            templateUrl: './projects-and-orders.component.html',
        }),
        __metadata("design:paramtypes", [router_1.Router, projects_and_orders_constans_1.ProjectsAndOrdersConstans])
    ], ProjectsAndOrdersComponent);
    return ProjectsAndOrdersComponent;
}());
exports.ProjectsAndOrdersComponent = ProjectsAndOrdersComponent;
//# sourceMappingURL=projects-and-orders.component.js.map