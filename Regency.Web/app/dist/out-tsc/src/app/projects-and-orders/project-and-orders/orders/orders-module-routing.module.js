"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var order_details_component_1 = require("./order-details/order-details.component");
var create_order_component_1 = require("./create-order/create-order.component");
var order_list_component_1 = require("./order-list/order-list.component");
var order_kanban_component_1 = require("./order-kanban/order-kanban.component");
var routes = [
    {
        path: '',
        redirectTo: 'ordersKanban',
    },
    {
        path: 'ordersKanban',
        component: order_kanban_component_1.OrderKanbanComponent
    },
    {
        path: 'ordersList',
        component: order_list_component_1.OrderListComponent
    },
    {
        path: 'orderDetails/:id',
        component: order_details_component_1.OrderDetailsComponent
    },
    {
        path: 'createOrder',
        component: create_order_component_1.CreateOrderComponent
    }
];
var OrdersModuleRoutingModule = /** @class */ (function () {
    function OrdersModuleRoutingModule() {
    }
    OrdersModuleRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forChild(routes)],
            exports: [router_1.RouterModule]
        })
    ], OrdersModuleRoutingModule);
    return OrdersModuleRoutingModule;
}());
exports.OrdersModuleRoutingModule = OrdersModuleRoutingModule;
//# sourceMappingURL=orders-module-routing.module.js.map