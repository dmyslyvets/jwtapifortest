"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var projects_and_orders_constans_1 = require("app/shared/constans/projects-and-orders.constans");
var devextreme_angular_1 = require("devextreme-angular");
var project_service_1 = require("app/shared/providers/services/project.service");
var customer_service_1 = require("app/shared/providers/services/customer.service");
var noitification_service_1 = require("app/shared/providers/services/noitification.service");
var load_service_1 = require("app/shared/providers/services/load.service");
var get_all_users_view_1 = require("app/shared/models/users/get-all-users.view");
var create_project_view_1 = require("app/shared/models/project/create-project.view");
var reassign_project_manager_view_1 = require("app/shared/models/project/reassign-project-manager.view");
var get_all_active_project_view_1 = require("app/shared/models/project/get-all-active-project.view");
var get_all_customer_view_1 = require("app/shared/models/customer/get-all-customer.view");
var ProjectsListComponent = /** @class */ (function () {
    function ProjectsListComponent(notificationService, projectService, customerService, route, loadService, constans) {
        this.notificationService = notificationService;
        this.projectService = projectService;
        this.customerService = customerService;
        this.route = route;
        this.loadService = loadService;
        this.constans = constans;
        //post request
        this.newProject = new create_project_view_1.CreateProjectView();
        this.reassignProjectManager = new reassign_project_manager_view_1.ReassignProjectManagerView();
        //get request
        this.projectManagers = new get_all_users_view_1.GetAllUserView();
        this.activeProjects = new get_all_active_project_view_1.GetAllActiveProjectView();
        this.customers = new get_all_customer_view_1.GetAllCustomerView();
        //popups
        this.openAddProject = false;
        this.formValidation = false;
        this.openReassignPM = false;
        //loader
        this.showProgresBar = true;
        //slider 
        this.isChecked = false;
        this.showFilterRow = true;
        this.showHeaderFilter = true;
        this.currentFilter = "auto";
        this.popupPosition = { of: window, at: "top", my: "top", offset: { y: 10 } };
    }
    ProjectsListComponent.prototype.ngOnInit = function () {
        this.loadService.set(true);
        this.getProjectManager();
        this.getCustomers();
        this.getActiveProject();
    };
    //get with Api
    ProjectsListComponent.prototype.getCustomers = function () {
        var _this = this;
        this.customerService.getCustomers().subscribe(function (response) {
            _this.customers = response;
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    ProjectsListComponent.prototype.getProjectManager = function () {
        var _this = this;
        this.projectService.getProjectManager().subscribe(function (response) {
            _this.projectManagers = response;
            _this.projectManagers.users.forEach(function (projectManager) {
                projectManager.fullName = projectManager.firstName + " " + projectManager.lastName;
            });
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    ProjectsListComponent.prototype.getActiveProject = function () {
        var _this = this;
        this.projectService.getAllActivePorjects().subscribe(function (respone) {
            _this.activeProjects = respone;
            _this.updateDate = null;
            _this.projectsCount = _this.activeProjects.projects.length;
            _this.dataGrid.dataSource = _this.activeProjects.projects;
            _this.loadService.set(false);
        }, function (error) {
            _this.errors = error;
            _this.loadService.set(false);
        });
    };
    //popups
    ProjectsListComponent.prototype.openReassignPMDialog = function (projectId) {
        this.formValidation = false;
        this.reassignProjectManager = new reassign_project_manager_view_1.ReassignProjectManagerView();
        this.reassignProjectManager.projectId = projectId;
        this.openReassignPM = true;
    };
    ProjectsListComponent.prototype.closeReassignPMDialog = function () {
        this.reassignProjectManager = new reassign_project_manager_view_1.ReassignProjectManagerView();
        this.openReassignPM = false;
    };
    ProjectsListComponent.prototype.saveReasignPM = function (event) {
        var _this = this;
        this.reassignProjectManager = new reassign_project_manager_view_1.ReassignProjectManagerView();
        this.reassignProjectManager.projectId = event.key.id;
        this.reassignProjectManager.projectManagerId = event.newData.user.id;
        this.openReassignPM = false;
        this.projectService.reassignProjectManeger(this.reassignProjectManager).subscribe(function (response) {
            _this.getActiveProject();
            _this.notificationService.showSuccess(_this.constans.projectManagerUpdateMessage);
        });
    };
    ProjectsListComponent.prototype.openAddProjectModal = function () {
        this.openAddProject = true;
        this.formValidation = false;
        this.newProject = new create_project_view_1.CreateProjectView();
    };
    ProjectsListComponent.prototype.closeAddProjectModal = function () {
        this.openAddProject = false;
        this.formValidation = false;
        this.newProject = new create_project_view_1.CreateProjectView();
    };
    ProjectsListComponent.prototype.saveNewProject = function () {
        this.route.navigate(['projects-and-orders/projects/createproject']);
    };
    //back on page
    ProjectsListComponent.prototype.showingProjectsList = function () {
        this.getActiveProject();
    };
    ProjectsListComponent.prototype.showProjectDetailsById = function (event) {
        if (this.openReassignPM != true) {
            this.route.navigate(['projects-and-orders/projects/projectdetails', event.data.id]);
        }
    };
    __decorate([
        core_1.ViewChild(devextreme_angular_1.DxDataGridComponent),
        __metadata("design:type", devextreme_angular_1.DxDataGridComponent)
    ], ProjectsListComponent.prototype, "dataGrid", void 0);
    ProjectsListComponent = __decorate([
        core_1.Component({
            selector: 'app-projects-list',
            templateUrl: './projects-list.component.html',
            styleUrls: ['./projects-list.component.scss']
        }),
        __metadata("design:paramtypes", [noitification_service_1.NotificationService,
            project_service_1.ProjectService,
            customer_service_1.CustomerService,
            router_1.Router,
            load_service_1.LoadService,
            projects_and_orders_constans_1.ProjectsAndOrdersConstans])
    ], ProjectsListComponent);
    return ProjectsListComponent;
}());
exports.ProjectsListComponent = ProjectsListComponent;
//# sourceMappingURL=projects-list.component.js.map