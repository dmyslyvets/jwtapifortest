"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var new_projects_routing_module_1 = require("./new-projects-routing.module");
var new_projects_component_1 = require("./new-projects/new-projects.component");
var forms_1 = require("@angular/forms");
var angular_6_datatable_1 = require("angular-6-datatable");
var datepicker_1 = require("ngx-bootstrap/datepicker");
var ng_busy_1 = require("ng-busy");
var ngx_uploader_1 = require("ngx-uploader");
var ng2_file_upload_1 = require("ng2-file-upload");
var ng_multiselect_dropdown_1 = require("ng-multiselect-dropdown");
var slide_toggle_1 = require("@angular/material/slide-toggle");
var ng2_pdf_viewer_1 = require("ng2-pdf-viewer");
var kendo_angular_grid_1 = require("@progress/kendo-angular-grid");
var material_module_1 = require("app/shared/modules/material.module");
var table_1 = require("@angular/material/table");
var material_1 = require("@angular/material");
var devextreme_angular_1 = require("devextreme-angular");
var drag_drop_1 = require("@angular/cdk/drag-drop");
var grid_list_1 = require("@angular/material/grid-list");
var project_details_component_1 = require("./new-projects/project-details/project-details.component");
var create_project_component_1 = require("./new-projects/create-project/create-project.component");
var NewProjectsModule = /** @class */ (function () {
    function NewProjectsModule() {
    }
    NewProjectsModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                new_projects_routing_module_1.NewProjectsRoutingModule,
                devextreme_angular_1.DxDataGridModule,
                devextreme_angular_1.DxSelectBoxModule,
                devextreme_angular_1.DxCheckBoxModule,
                material_module_1.MaterialModule,
                angular_6_datatable_1.DataTableModule,
                ng2_file_upload_1.FileUploadModule,
                forms_1.FormsModule,
                kendo_angular_grid_1.GridModule,
                table_1.MatTableModule,
                material_1.MatPaginatorModule,
                slide_toggle_1.MatSlideToggleModule,
                forms_1.ReactiveFormsModule,
                ng_multiselect_dropdown_1.NgMultiSelectDropDownModule.forRoot(),
                datepicker_1.BsDatepickerModule.forRoot(),
                ng_busy_1.NgBusyModule,
                ngx_uploader_1.NgxUploaderModule,
                ng2_pdf_viewer_1.PdfViewerModule,
                material_1.MatSortModule,
                drag_drop_1.DragDropModule,
                grid_list_1.MatGridListModule
            ],
            declarations: [new_projects_component_1.NewProjectsComponent, project_details_component_1.ProjectDetailsComponent, create_project_component_1.CreateProjectComponent]
        })
    ], NewProjectsModule);
    return NewProjectsModule;
}());
exports.NewProjectsModule = NewProjectsModule;
//# sourceMappingURL=new-projects.module.js.map