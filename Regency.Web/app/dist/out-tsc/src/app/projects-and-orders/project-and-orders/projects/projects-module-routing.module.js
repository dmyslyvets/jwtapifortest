"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var projects_list_component_1 = require("./projects-list/projects-list.component");
var project_details_component_1 = require("./project-details/project-details.component");
var create_project_component_1 = require("./create-project/create-project.component");
var routes = [
    {
        path: '',
        redirectTo: 'projectslist',
    },
    {
        path: 'projectslist',
        component: projects_list_component_1.ProjectsListComponent
    },
    {
        path: 'projectdetails/:id',
        component: project_details_component_1.ProjectDetailsComponent
    },
    {
        path: 'createproject',
        component: create_project_component_1.CreateProjectComponent
    }
];
var ProjectsModuleRoutingModule = /** @class */ (function () {
    function ProjectsModuleRoutingModule() {
    }
    ProjectsModuleRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forChild(routes)],
            exports: [router_1.RouterModule]
        })
    ], ProjectsModuleRoutingModule);
    return ProjectsModuleRoutingModule;
}());
exports.ProjectsModuleRoutingModule = ProjectsModuleRoutingModule;
//# sourceMappingURL=projects-module-routing.module.js.map