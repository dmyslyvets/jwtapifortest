"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var ng2_file_upload_1 = require("ng2-file-upload");
var forms_1 = require("@angular/forms");
var projects_and_orders_constans_1 = require("app/shared/constans/projects-and-orders.constans");
var project_service_1 = require("app/shared/providers/services/project.service");
var customer_service_1 = require("app/shared/providers/services/customer.service");
var noitification_service_1 = require("app/shared/providers/services/noitification.service");
var project_attachment_service_1 = require("app/shared/providers/services/project-attachment.service");
var load_service_1 = require("app/shared/providers/services/load.service");
var get_all_users_view_1 = require("app/shared/models/users/get-all-users.view");
var get_all_customer_view_1 = require("app/shared/models/customer/get-all-customer.view");
var create_project_view_1 = require("app/shared/models/project/create-project.view");
var get_all_project_types_view_1 = require("app/shared/models/project/get-all-project-types.view");
var CreateProjectComponent = /** @class */ (function () {
    function CreateProjectComponent(notificationService, projectService, customerService, projectAttachmentService, activateRoute, route, loadService, constans) {
        this.notificationService = notificationService;
        this.projectService = projectService;
        this.customerService = customerService;
        this.projectAttachmentService = projectAttachmentService;
        this.activateRoute = activateRoute;
        this.route = route;
        this.loadService = loadService;
        this.constans = constans;
        //post request
        this.createProject = new create_project_view_1.CreateProjectView();
        this.currentPdfLink = " ";
        this.projectManagers = new get_all_users_view_1.GetAllUserView();
        this.customers = new get_all_customer_view_1.GetAllCustomerView();
        this.projectTypesView = new get_all_project_types_view_1.GetAllProjectTypesView();
        //form 
        this.myControl = new forms_1.FormControl();
        //spinner
        this.showSpinner = false;
        //popups
        this.activeEditProject = false;
        this.formValidation = false;
        this.openSaveAttachment = false;
        this.openDeleteAttachmentModal = false;
        this.droppedFiles = [];
        this.hasBaseDropZoneOver = false;
        //attachment
        this.uploader = new ng2_file_upload_1.FileUploader({ url: this.constans.URL });
        this.showAttachmentImg = false;
        this.showAttachedPdf = false;
        this.showAttachedTxt = false;
        this.showAttachment = false;
        this.createProjectFileList = [];
    }
    CreateProjectComponent.prototype.ngOnInit = function () {
        this.loadService.set(true);
        this.getProjectManager();
        this.getCustomers();
        this.getProjectTypes();
    };
    //temp
    CreateProjectComponent.prototype.change = function () {
        console.log(this.createProject.projectType);
    };
    //get
    CreateProjectComponent.prototype.getCustomers = function () {
        var _this = this;
        this.customerService.getCustomers().subscribe(function (response) {
            _this.customers = response;
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    CreateProjectComponent.prototype.getProjectTypes = function () {
        var _this = this;
        this.projectService.getAllProjectTypes().subscribe(function (response) {
            _this.projectTypesView = response;
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    CreateProjectComponent.prototype.getProjectManager = function () {
        var _this = this;
        this.projectService.getProjectManager().subscribe(function (response) {
            _this.projectManagers = response;
            _this.loadService.set(false);
        }, function (error) {
            _this.notificationService.showErrors(error.error);
        });
    };
    //functions for project types
    CreateProjectComponent.prototype.displayFn = function (val) {
        return val ? val.name : val;
    };
    CreateProjectComponent.prototype.selectedProjectType = function (item) {
        this.createProject.projectType.id = item.option.value.id;
        this.createProject.projectType.name = item.option.value.name;
    };
    CreateProjectComponent.prototype.typedProjectType = function (event) {
        this.createProject.projectType.name = '';
        this.createProject.projectType.id = null;
        this.createProject.projectType.name = event.target.value;
    };
    //back on page
    CreateProjectComponent.prototype.showingProjectsList = function () {
        this.route.navigate(['projects-and-orders/projects/projectslist']);
    };
    //update project
    CreateProjectComponent.prototype.activateEditProject = function () {
        this.activeEditProject = true;
    };
    CreateProjectComponent.prototype.saveProjectChange = function () {
        var _this = this;
        if (!this.createProject.name) {
            this.notificationService.showError(this.constans.cantCreateProjectWithoutNameMessage);
        }
        if (!this.createProject.userId) {
            this.notificationService.showError(this.constans.cantCreateProjectWithoutPMMessage);
        }
        if (!this.createProject.customerId) {
            this.notificationService.showError(this.constans.cantCreateProjectWithoutCustomerMessage);
        }
        if (!this.createProject.date) {
            this.notificationService.showError(this.constans.cantCreateProjectWitoutDate);
        }
        if (this.createProject.name && this.createProject.customerId && this.createProject.userId && this.createProject.date) {
            var invalidFiles = [];
            var formData = new FormData();
            for (var i = 0; i < this.createProjectFileList.length; i++) {
                var success = false;
                this.constans.allowableExtensions.forEach(function (ex) {
                    var index = _this.createProjectFileList[i].name.lastIndexOf(".");
                    var extensions = _this.createProjectFileList[i].name.substr(index + 1);
                    if (ex == extensions) {
                        success = true;
                        return;
                    }
                });
                if (success) {
                    formData.append("file", this.createProjectFileList[i], this.createProjectFileList[i].name);
                }
            }
            ;
            formData.append("name", this.createProject.name);
            formData.append("description", this.createProject.description);
            formData.append("memo", this.createProject.memo);
            formData.append("projectType[id]", this.createProject.projectType.id);
            formData.append("projectType[name]", this.createProject.projectType.name);
            formData.append("customerId", this.createProject.customerId);
            formData.append("userId", this.createProject.userId);
            formData.append("date", this.createProject.date.toUTCString());
            this.projectService.createProject(formData).subscribe(function (response) {
                _this.notificationService.showSuccess(_this.constans.projectCreateMessage);
                _this.showingProjectsList();
            }),
                function (error) {
                    _this.notificationService.showError(error.error);
                };
        }
    };
    //attacments
    CreateProjectComponent.prototype.openDeleteAttachment = function (index) {
        this.deletedAttachmentId = index;
        this.openDeleteAttachmentModal = true;
    };
    CreateProjectComponent.prototype.cancelDeleteAttachment = function () {
        this.deletedAttachmentId = null;
        this.openDeleteAttachmentModal = false;
    };
    CreateProjectComponent.prototype.yesDeleteAttachment = function () {
        this.deleteAttachment();
        this.openDeleteAttachmentModal = false;
    };
    CreateProjectComponent.prototype.deleteAttachment = function () {
        this.createProjectFileList.splice(this.deletedAttachmentId, 1);
        this.deletedAttachmentId = null;
    };
    CreateProjectComponent.prototype.inputAdd = function (event) {
        var fileList = event.target.files;
        var invalidFiles = this.checkAttachmentExtensions(fileList);
        if (invalidFiles.length > 0) {
            this.notificationService.showWarning(this.constans.fileExtensionNotAllowedMessage);
        }
    };
    CreateProjectComponent.prototype.fileLeave = function (file) {
        var index = this.droppedFiles.indexOf(file);
        if (index > -1) {
            this.droppedFiles.splice(index, 1);
        }
    };
    CreateProjectComponent.prototype.fileOver = function (event) { };
    CreateProjectComponent.prototype.fileOverBase = function (event) {
        this.hasBaseDropZoneOver = event;
    };
    CreateProjectComponent.prototype.dropped = function (fileList) {
        var invalidFiles = this.checkAttachmentExtensions(fileList);
        if (invalidFiles.length > 0) {
            this.notificationService.showWarning(this.constans.fileExtensionNotAllowedMessage);
        }
    };
    CreateProjectComponent.prototype.checkAttachmentExtensions = function (files) {
        var invalidFiles = [];
        for (var i = 0; i < files.length; i++) {
            this.showSpinner = true;
            var success = false;
            this.constans.allowableExtensions.forEach(function (ex) {
                var index = files[i].name.lastIndexOf(".");
                var extensions = files[i].name.substr(index + 1);
                if (ex == extensions) {
                    success = true;
                    return;
                }
            });
            if (!success) {
                invalidFiles.push(files[i].name);
            }
            if (success) {
                this.createProjectFileList.push(files[i]);
            }
        }
        ;
        this.showSpinner = false;
        return invalidFiles;
    };
    CreateProjectComponent.prototype.closeShowAttachment = function () {
        this.showAttachmentImg = false;
    };
    CreateProjectComponent = __decorate([
        core_1.Component({
            selector: 'app-create-project',
            templateUrl: './create-project.component.html',
            styleUrls: ['./create-project.component.scss']
        }),
        __metadata("design:paramtypes", [noitification_service_1.NotificationService,
            project_service_1.ProjectService,
            customer_service_1.CustomerService,
            project_attachment_service_1.ProjectAttachmentservice,
            router_1.ActivatedRoute,
            router_1.Router,
            load_service_1.LoadService,
            projects_and_orders_constans_1.ProjectsAndOrdersConstans])
    ], CreateProjectComponent);
    return CreateProjectComponent;
}());
exports.CreateProjectComponent = CreateProjectComponent;
//# sourceMappingURL=create-project.component.js.map