import { Component } from '@angular/core';
import { LoginModel } from '../../models/account';
import { Router } from '@angular/router';
import { AccountProvider } from '../../providers/providers';

@Component({
  templateUrl: 'login.component.html'
})
export class LoginComponent {
  public account: LoginModel = new LoginModel();
  public isLoading: boolean = false;

  constructor(
    public accountProvider: AccountProvider,
    public router: Router
  ) {
  }

  login() {
    this.isLoading = true;
    this.accountProvider.login(this.account).subscribe(() => {
      this.router.navigate(['dashboard']);
    }, (err) => {
      this.isLoading = false;
    });
  }
}
