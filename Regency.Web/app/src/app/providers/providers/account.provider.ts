import { Injectable } from '@angular/core';
import { LoginModel } from '../../models/account';
import { AccountService, ApiService } from '../services';

@Injectable()
export class AccountProvider {
  private readonly controller: string = 'account';
  constructor(public api: ApiService,
    private accountService: AccountService) {
  }

  login(account: LoginModel) {
    const endpotin = 'account/login';
    return this.api.post(endpotin, account).map((res: any) => {
      this.accountService._loggedIn(res);
      return res;
    });
  }
}
