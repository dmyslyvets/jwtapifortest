import { Injectable } from '@angular/core';
import * as jwt_decode from "jwt-decode";
import { LocalStorage } from '@ngx-pwa/local-storage';
import { Router } from '@angular/router';
import { AuthData } from '../../models/account';

@Injectable()
export class AccountService {
  private user: AuthData;
  private readonly authDataKey: string = "RegencyAuthData";
  constructor(private storage: LocalStorage,
    private router: Router) {
    this.storage.getItem<AuthData>(this.authDataKey).subscribe((data) => {
      this.user = data;
    });
  }

  _loggedIn(data: AuthData): void {
    this.user = data;
    this.storage.setItem(this.authDataKey, data).subscribe(() => { });
  }

  logout(): void {
    this.user = null;
    this.storage.removeItem(this.authDataKey).subscribe(() => { });
    this.router.navigateByUrl('pages/login');
  }

  getToken(): string {
    if (this.user) {
      return this.user.auth_token
    }
    return undefined;
  }

  getUserIdFromToken() {
    const token = this.getToken();
    if (token) {
      return this.getDecodedAccessToken(token).id;
    }
    return null;
  }

  getDecodedAccessToken(token: string): any {
    try {
      return jwt_decode(token);
    }
    catch (error) {
      return null;
    }
  }

  isAuthenticated(): Promise<boolean> {
    return new Promise((resolve) => {
      if (!this.user) {
        this.storage.getItem<AuthData>(this.authDataKey).subscribe((data) => {
          resolve(!!data);
        });
      }
      else {
        resolve(!!this.user);
      }
    });
  }
}
