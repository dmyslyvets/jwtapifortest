import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AccountService } from '../services';


@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private accountService: AccountService) { }

  async canActivate(): Promise<boolean> {
    const isAuthenticated = await this.accountService.isAuthenticated();
    if (isAuthenticated) {
      return true;
    }
    this.router.navigate(['pages/login'], { replaceUrl: true });
    return false;
  }
}

