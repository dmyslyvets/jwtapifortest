import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpErrorResponse,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do'
import 'rxjs/add/observable/throw';
import { AccountService } from '../services';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(public accountService: AccountService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!request.url.includes('api/account/')) {
      const token = this.accountService.getToken();
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${token}`
        }
      });
    }
    return next.handle(request).do((event: HttpEvent<any>) => { }, (err: any) => {
      if (err instanceof HttpErrorResponse) {
        // do error handling here
        if (err.status === 401) {
          this.accountService.logout();
          return;
        }
      }
      return Observable.throw(err);
    });
  }
}
