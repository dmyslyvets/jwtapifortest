export enum UserRole {
  None = 0,
  Admin = 1,
  Manager = 2,
  Member = 3,
}
