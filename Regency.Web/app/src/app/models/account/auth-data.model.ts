export class AuthData {
  public expires_in: number;
  public auth_token: string;
}
