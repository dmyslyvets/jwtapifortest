﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Regency.Entities.Entities;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Regency.DataAccess
{
    public static class DbInitializer
    {
        public static async Task Initialize(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {

            string[] roles = new string[] { "Admin", "Manager", "Member"};

            foreach (string role in roles)
            {
                var roleStore = new RoleStore<IdentityRole>(context);

                if (!context.Roles.Any(r => r.Name == role))
                {
                    var nr = new IdentityRole(role)
                    {
                        NormalizedName = role.ToUpper()
                    };
                    await roleStore.CreateAsync(nr);
                }
            }

            var user = new ApplicationUser
            {
                FirstName = "Admin",
                LastName = "Admin",
                Email = "Admin@Admin.com",
                NormalizedEmail = "ADMIN@ADMIN.COM",
                UserName = "Admin@Admin.com",
                NormalizedUserName = "ADMIN@ADMIN.COM",
                PhoneNumber = "+111111111111",
                EmailConfirmed = true,
                PhoneNumberConfirmed = true,
                SecurityStamp = Guid.NewGuid().ToString("D")
            };

            if (!context.Users.Any(u => u.UserName == user.UserName))
            {
                var password = new PasswordHasher<ApplicationUser>();
                var hashed = password.HashPassword(user, "Admin123");
                user.PasswordHash = hashed;

                var userStore = new UserStore<ApplicationUser>(context);
                var result = await userStore.CreateAsync(user);
            }

            var amdinRole = roles.First();
            await AssignRoles(userManager, user.Email, amdinRole);

         
            await context.SaveChangesAsync();
        }

        public static async Task<IdentityResult> AssignRoles(UserManager<ApplicationUser> userManager, string email, string role)
        {
            ApplicationUser user = await userManager.FindByEmailAsync(email);
            var result = await userManager.AddToRoleAsync(user, role);
            return result;
        }
        }
    }

